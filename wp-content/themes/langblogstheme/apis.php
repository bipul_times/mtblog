<?php 

/**
*
* The ajax json API
*
**/
add_action('wp_ajax_nopriv_getArticle', 'getArticle');
add_action('wp_ajax_getArticle', 'getArticle');

function getArticle() {
    global $wpdb;
    $id = $_REQUEST['id'];
    $result = $wpdb->get_row("SELECT post_title as title,post_date as date_published, post_content as content,post_name as url, (SELECT display_name from wp_users as u where u.ID = p.post_author) as author FROM  wp_posts as p where p.ID=".$id.";");
    $tags = array();
    $posttags = get_the_tags($id);
    if ($posttags) {
        foreach($posttags as $tag) {
            array_push ($tags, $tag->name);
        }
    }
    $tagsString = implode(",", $tags);
    $result->tags = $tagsString; 
    header('Content-type: application/json');
    echo json_encode($result);
    die();
}

/**
*
* The ajax json API.
*
**/
add_action('wp_ajax_nopriv_getRecentArticles', 'getRecentArticles');
add_action('wp_ajax_getRecentArticles', 'getRecentArticles');

function getRecentArticles() {
    global $wpdb;
    $query = '';
    $query = "SELECT p.ID, post_title AS title, post_date AS pubTime, post_author AS aid,u.ID AS aid,u.display_name AS author,u.user_login AS username FROM  wp_posts AS p,(SELECT ID,display_name,user_login FROM wp_users) AS u WHERE p.post_status='publish' AND u.ID = p.post_author ORDER BY p.post_date DESC ";
    if(!empty($_GET['limit'])){
        $query .= 'LIMIT 0,'.$_GET['limit'].';';
    }else {
        $query .= 'LIMIT 0,10;';
    }
    //echo $query;
    $results = $wpdb->get_results($query);
    $html = '<?xml version="1.0" encoding="UTF-8"?><channel>';
    foreach($results as $result) : 
        $blog = wp_get_object_terms( $result->ID , 'blog', array('fields' => 'all'));
        $html .= '<item>';
        $html .= '<blogname>'.htmlspecialchars ($blog[0]->name).'</blogname>';
        $html .= '<blogUrl>'.htmlspecialchars (site_url().'/'.$blog[0]->slug).'</blogUrl>';
        $html .= '<author>'.htmlspecialchars ($result->author).'</author>';
        $post = get_post($result->ID);
        setup_postdata($post); 
        $html .= '<authorUrl>'.get_author_posts_url($result->aid).'</authorUrl>';
        if ( has_post_thumbnail($result->ID)) { 
            $html .= '<photo>'.wp_get_attachment_image_src( get_post_thumbnail_id( $result->ID ), 'single-post-thumbnail' )[0].'</photo>';
            //the_post_thumbnail('thumbnail');
        }else{
            $html .= '<photo>'.get_user_avatar($result->aid).'</photo>';
        }
        $html .= '<weburl>'.get_permalink($result->ID).'</weburl>';
        $html .= '<excerpt>'.htmlspecialchars(get_the_excerpt()).'</excerpt>';
        $html .= '<headline>'.htmlspecialchars ($result->title).'</headline>';
        $html .= '<dateline>'.$result->pubTime.'</dateline>';
        $html .= '</item>';
    endforeach;
    wp_reset_postdata();
    header('Content-type:text/xml');
    echo $html.'</channel>';
    die();
}


/**
*
* The ajax json API.
*
**/
add_action('wp_ajax_nopriv_getRecentArticlesJSON', 'getRecentArticlesJSON');
add_action('wp_ajax_getRecentArticlesJSON', 'getRecentArticlesJSON');

function getRecentArticlesJSON() {
    global $wpdb;
    $query = '';
    $query = "SELECT p.ID, post_title AS title, post_date AS pubTime, post_author AS aid,u.ID AS aid,u.display_name AS author,u.user_login AS username FROM  wp_posts AS p,(SELECT ID,display_name,user_login FROM wp_users) AS u WHERE p.post_status='publish' AND u.ID = p.post_author ORDER BY p.post_date DESC ";
    if(!empty($_GET['limit'])){
        $query .= 'LIMIT 0,'.$_GET['limit'].';';
    }else {
        $query .= 'LIMIT 0,10;';
    }
    //echo $query;
    $results = $wpdb->get_results($query);
    $res = new stdClass();
    $res->NewsItem = array() ;
    foreach($results as $result) : 
        $blog = wp_get_object_terms( $result->ID , 'blog', array('fields' => 'all'));
        $post = get_post($result->ID);
        setup_postdata($post);

        $ob = new stdClass();
        $ob->HeadLine = htmlspecialchars ($result->title);
        $ob->ByLine = htmlspecialchars ($result->author);
        $ob->DateLine = $result->pubTime;
        $ob->Agency = "TNN";
        $ob->NewsItemId = $result->ID;
        $ob->WebURL = get_permalink($result->ID);
        $ob->Image = new stdClass();
        $ob->Image->Photo = get_user_avatar($result->aid);
        $ob->Image->Thumb = get_user_avatar($result->aid);
        $ob->Video = new stdClass();
        $ob->Video->Thumb = "";
        $ob->Video->VideoUrl = "";
        $ob->Slideshow = new stdClass();
        $ob->Slideshow->Headline = "";
        $ob->Slideshow->Thumb = ""; 
        $ob->Slideshow->SlideshowUrl = ""; 
        $ob->Caption = htmlspecialchars(get_the_excerpt());
        $ob->MetaDataURL = '';
        $ob->storyview = "news";
        $ob->Story = strip_tags($result->content,'<br>');
        array_push ( $res->NewsItem, $ob );
    endforeach;
    wp_reset_postdata();
    header('Content-type: application/json');
    echo json_encode($res);
    die();
}

/**
*
* The ajax json API.
*
**/
add_action('wp_ajax_nopriv_getArticlesByBlog', 'getArticlesByBlog');
add_action('wp_ajax_getArticlesByBlog', 'getArticlesByBlog');

function getArticlesByBlog() {
    global $wpdb;
    $limit = 10;
    if(!empty($_GET['limit'])){
        $limit = $_GET['limit'];
    }
    if(!empty($_GET['blog'])){
        $args = array(
        'posts_per_page' => $limit,
        'tax_query' => array(
            array(
                'taxonomy' => 'blog',
                'field'    => 'slug',
                'terms'    => $_GET['blog'],
            ),
            ),
        );
    }else{
        echo 'no results';
        die();
    }
    $query = new WP_Query( $args );
    $html = '<?xml version="1.0" encoding="UTF-8"?><channel>';
        while ( $query->have_posts() ) {
                    $query->the_post();
        $html .= '<item>';
        $html .= '<author>'.htmlspecialchars (get_the_author_meta('display_name')).'</author>';
        $html .= '<authorUrl>'.get_author_posts_url(get_the_author_meta( 'ID' )).'</authorUrl>';
        $id = get_the_ID();
        if ( has_post_thumbnail($id)) { 
            $html .= '<photo>'.wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' )[0].'</photo>';
            //the_post_thumbnail('thumbnail');
        }else{
            $html .= '<photo>'.get_user_avatar(get_the_author_meta( 'ID' )).'</photo>';
        }
        //$html .= '<photo>'.get_user_avatar(get_the_author_meta( 'ID' )).'</photo>';
        $html .= '<weburl>'.get_permalink().'</weburl>';
        $html .= '<excerpt>'.get_the_excerpt().'</excerpt>';
        $title = get_the_title();
		$subtitle = get_post_meta( get_the_ID(), 'wps_subtitle', true );
        $headline = $subtitle ? $subtitle : $title;
        $html .= '<headline>'.$headline.'</headline>';
        $html .= '<dateline>'.get_the_date().'</dateline>';
        $html .= '</item>';          
    }
    wp_reset_postdata();
    header('Content-type:text/xml');
    echo $html.'</channel>';
    die();
}

/**
*
* The ajax json API
*
**/
add_action('wp_ajax_nopriv_getJSSFeed', 'getJSSFeed');
add_action('wp_ajax_getJSSFeed', 'getJSSFeed');

function getJSSFeed() {
    global $wpdb;
    $results = $wpdb->get_results("SELECT p.ID, post_content AS content, post_title AS title, post_date AS pubTime, post_author AS aid,u.ID AS aid,u.display_name AS author,u.user_login AS username FROM  wp_posts AS p,(SELECT ID,display_name,user_login FROM wp_users) AS u WHERE p.post_status='publish' AND u.ID = p.post_author AND u.user_login IN ('vssunder','vrajabalasv','vinitadawranangia','vinitanangia','vinaykamat','vikramsinha','vikassingh','VijuB','vijaysingh','venkatkrishnann','vnarayanswamy','vanand','tkarun','toinewsowl','photoblog','tarunvijay','tanmoymookherjee','swatyprakash','swatisucharita','swatimathur','swatideshpande','swapandasgupta','swagatoganguly','susmitamukherjee','surendrasingh','sunilwarrier','sumitmukherjee','sukhbirsiwach','sujitjohn','suchetadasgupta','subodhvarma','SubhayuMazumder','subhabrataguha','srijanamitradas','soumittrabose','soumittrabose','siddharthsaxena','shubhammukherjee','shrutisetiachhabra','shreyabhandary','shrabontibagchi','shounakghosal','shobhansaxena','shobhaade','shobhajohn','sharvanipanditsingh','sharmisthagooptu','shankarraghuraman','sdpradhan','sayalibhagat','saurabhsharma','saumyapant','santoshdesai','santanapathak','samidhasharma','sameerarshad','sairakurup','saibalbose','swaminathansanklesariaaiyar','sradhika','rupamsinghgupta','rupasengupta','rudroneelghosh','ruchachitrodia','ronojaysen','rolisrivastava','robinroy','robindavid','reshmi','remanagarajan','rebeccasamervel','reaganrasquinha','ravirao','rashmiudaysingh','ranjanroy','rameshkhazanchi','ramaninderkaurbhatia','rajivshah','rajibborah','rajeshkalra','rajeevdeshpande','rajatpandit','rajabose','rajkumar','edwinsudhir','pyaralalraghavan','purbakalita','pronotidatta','priyayadav','priyamenon','pritishnandy','prithvijitmitra','prashanthgn','prasenjitmund','prajwalhegde','pradipkbagchi','pradeepvijaykar123','pooja.bedi','parthabhaduri','parakramrautela','prameshkumar','ninamartyris','nauzerkbharucha','narayaniganesh','narayanankrishnaswami','nanditasengupta','naheedataulla','mythlibhusnurmath','monobinagupta','meghasandhu','mayurshetty','marcusm','manvirsaini','mansichoksi','manojramachandran','manojmitta','manjuv','manimugdhasharma','manaspratimgohain','manasgupta','kunal.kapoor','kulraj.randhawa','krishnakumarmangalam','krsreenivas','kingshuknag','kingshukmukherji','kimarora','KannanSomasundaram','kshriniwasrao','jugsuraiya','johncheeran','jitendraverma','jayanthkodkani','javedanwer','jaspreetnijher','ipsitabhattacharya','indranibagchi','indrajeetrai','ipsingh','hsbalram','harshadpandharipande','haritmehta','gurcharandas','gautamsiddharth','gautamadhikari','errol','edisonthomas','ejayashreekurup','doelsengupta','dileeppadgaonkar','dhananjaymahapatra','deepanjoshi','chidanandrajghatta','chetanbhagat','chandrimapal','chandanroysanyal','chandannandy','bobillivijaykumar','biswajyotibrahma','biswadeepghosh','bharatdesai','bachikarkaria','babitabasu','avijitghosh','atulsethi','ashokraaj','ashishvashi','ashishtripathi','asharai','arunram','archanakhareghose','aratijerath','anshulchaturvedi','anjalithomas','anindodey','aninditadattachoudhury','anilnair','anandsoondas','anandbodh','anahitamukherji','amitkarmarkar','amitbhattacharya','amitarora','aloktiwari','aloksinha','allwynfernandes','alisaschubertyuasa','akshayamukul','akhileshkumarsingh','ajaysura','aartitikoosingh') ORDER BY p.post_date DESC LIMIT 0,20;");
    $html = "var related_post_by_date ={ weblogPost: [ ";
    foreach($results as $result) : 
        $blog = wp_get_object_terms( $result->ID , 'blog', array('fields' => 'all'));
        $html .= "{";
        $html .= "'image':'".get_user_avatar($result->aid)."',";
        $html .= "'authorName':'".htmlspecialchars ($result->author)."',";
        $html .= "'postUrl':'".get_permalink($result->ID)."',";
        $html .= "'title':'".htmlspecialchars ($result->title)."',";
        $html .= "'description':'".htmlspecialchars ($result->content)."'";
        $html .= '}';
    endforeach;
    $html .= "]}";
    header('Content-type:text/html');
    echo $html;
    die();
}

add_action('wp_ajax_nopriv_performFlush', 'performFlush');
add_action('wp_ajax_performFlush', 'performFlush');
function performFlush(){
    no_category_base_refresh_rules();
    echo json_encode('flush done');
    die();
}

add_action('wp_ajax_nopriv_clearCache', 'clearCache');
add_action('wp_ajax_clearCache', 'clearCache');
function clearCache(){
    /*if($_SERVER['HTTP_HOST']!='localhost'){
        echo json_encode('Go Away');
        die();
    }*/
    if(function_exists('w3tc_pgcache_flush')){
        w3tc_pgcache_flush();
        echo json_encode('cleared cache');
    }else{
        echo json_encode('error');
    }
    die();
}

/**
*
* The ajax json API.
*
**/
add_action('wp_ajax_nopriv_getArticlesByIDs', 'getArticlesByIDs');
add_action('wp_ajax_getArticlesByIDs', 'getArticlesByIDs');

function getArticlesByIDs() {
    global $wpdb;
    $query = 'SELECT p.ID, post_title AS title, post_date AS pubTime, post_author AS aid,u.ID AS aid,u.display_name AS author,u.user_login AS username FROM  wp_posts AS p,(SELECT ID,display_name,user_login FROM wp_users) AS u WHERE p.post_status="publish" AND u.ID = p.post_author AND p.ID IN ('.$_GET['ids'].')  LIMIT 0,10';
    $results = $wpdb->get_results($query);
    $html = '<?xml version="1.0" encoding="UTF-8"?><channel>';
    foreach($results as $result) : 
        $blog = wp_get_object_terms( $result->ID , 'blog', array('fields' => 'all'));
        $html .= '<item>';
        $html .= '<blogname>'.htmlspecialchars ($blog[0]->name).'</blogname>';
        $html .= '<blogUrl>'.site_url().'/'.$blog[0]->slug.'</blogUrl>';
        $html .= '<author>'.htmlspecialchars ($result->author).'</author>';
        $html .= '<authorUrl>'.site_url().'/author/'.$result->username.'</authorUrl>';
        $html .= '<photo>'.get_user_avatar($result->aid).'</photo>';
        $html .= '<weburl>'.get_permalink($result->ID).'</weburl>';
        $html .= '<headline>'.htmlspecialchars ($result->title).'</headline>';
        $html .= '<dateline>'.$result->pubTime.'</dateline>';
        $html .= '</item>';
    endforeach;
    header('Content-type:text/xml');
    echo $html.'</channel>';
    die();
}


/**
*
* The ajax json API
*
**/
add_action('wp_ajax_nopriv_getRecoForArticles', 'getRecoForArticles');
add_action('wp_ajax_getRecoForArticles', 'getRecoForArticles');

function getRecoForArticles() {
    $msid = $_GET['msid'];
    $url = 'http://reco.indiatimes.com/Reco/GetRecommendedIdsJson?days=90&records=5&hostid=1b&&contentType=Articles&msid='.$msid;
    //echo $url;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,10); # timeout after 10 seconds, you can increase it
    curl_setopt($ch, CURLOPT_URL, $url ); #set the url and get string together    
    $res = json_decode (curl_exec($ch));
    $html = '';
    $i = 1;
    $arr = array();
    //print_r($res);
    foreach($res as $result) : 
      if(!empty($result->recommendedid)){
        array_push($arr, $result->recommendedid);                                                                    
      }
    endforeach;
    //echo $arr;
    global $wpdb;
    $query = 'SELECT p.ID, post_title AS title, post_date AS pubTime, post_author AS aid,u.ID AS aid,u.display_name AS author,u.user_login AS username FROM  wp_posts AS p,(SELECT ID,display_name,user_login FROM wp_users) AS u WHERE p.post_status="publish" AND u.ID = p.post_author AND p.ID IN ('.implode(',',$arr).')  LIMIT 0,10';
    //echo $query;
    $postResults = $wpdb->get_results($query);
    //print_r($postResults);
    $html = '<?xml version="1.0" encoding="UTF-8"?><channel>';
    foreach($postResults as $result) : 
        $blog = wp_get_object_terms( $result->ID , 'blog', array('fields' => 'all'));
        $html .= '<item>';
        $html .= '<blogname>'.htmlspecialchars ($blog[0]->name).'</blogname>';
        $html .= '<blogUrl>'.site_url().'/'.$blog[0]->slug.'</blogUrl>';
        $html .= '<author>'.htmlspecialchars ($result->author).'</author>';
        $html .= '<authorUrl>'.site_url().'/author/'.$result->username.'</authorUrl>';
        $html .= '<photo>'.get_user_avatar($result->aid).'</photo>';
        $html .= '<weburl>'.get_permalink($result->ID).'</weburl>';
        $html .= '<headline>'.htmlspecialchars ($result->title).'</headline>';
        $html .= '<dateline>'.$result->pubTime.'</dateline>';
        $html .= '</item>';
    endforeach;
    header('Content-type:text/xml');
    echo $html.'</channel>';
    die();
}

/**
*
* The ajax jsonp API.
*
**/
add_action('wp_ajax_nopriv_getRecentArticlesJSONP', 'getRecentArticlesJSONP');
add_action('wp_ajax_getRecentArticlesJSONP', 'getRecentArticlesJSONP');

function getRecentArticlesJSONP() {
    global $my_settings;
    if(isset($_GET['n']) && !empty($_GET['n'])){
        $posts_per_page = $_GET['n'];
    }else {
        $posts_per_page = 10;
    }
    query_posts( 'post_status=publish&posts_per_page=' . $posts_per_page );
    if(isset($_GET['authorid']) && !empty($_GET['authorid'])){
		query_posts( 'author=' . $_GET['authorid'] . 'post_status=publish&posts_per_page=' . $posts_per_page );
	}
    $results = array();
    $r = 0;
    while ( have_posts() ) : the_post();
		$results[$r]['authorName'] = get_the_author_meta( 'display_name' );
		$title = get_the_title();
		$subtitle = get_post_meta( get_the_ID(), 'wps_subtitle', true );
        $results[$r]['anchor'] = get_post_field( 'post_name', get_the_ID() );
        $results[$r]['title'] = $subtitle ? $subtitle : $title;
        $results[$r]['title'] = html_entity_decode($results[$r]['title']);
        $results[$r]['id'] = get_the_ID();
        $blog = array_values(get_the_terms(get_the_ID(), 'blog' ))[0];
        $category = array_values(get_the_terms(get_the_ID(), 'category' ))[0];
        $results[$r]['handle'] = html_entity_decode (getTaxonomyLocalTitle($blog->taxonomy, $blog->term_id, $blog->name));
        $results[$r]['category'] = html_entity_decode (getTaxonomyLocalTitle($category->taxonomy, $category->term_id, $category->name));
        $results[$r]['authorBlog'] = get_term_link($blog->term_id, $blog->taxonomy);
        $results[$r]['blogurl'] = get_bloginfo( 'url' );
        /*if ( has_post_thumbnail(get_the_ID())) { 
            $results[$r]['imageUrl'] = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' )[0];
        }else{
            $results[$r]['imageUrl'] = get_user_avatar(get_the_author_meta('ID'));
        }*/
        $results[$r]['imageUrl'] = get_user_avatar(get_the_author_meta('ID'));
        if(isset($_GET['desc']) && $_GET['desc'] == 'y'){
			$content = apply_filters('the_content', get_the_content());
			$content = str_replace(trim($my_settings['blogauthor_link'], '/'), WP_HOME, $content);
            $content = str_replace(trim($my_settings['old_blogauthor_link'], '/'), WP_HOME, $content);
			$results[$r]['description'] = html_entity_decode($content);
			$results[$r]['displaytruncatedContent'] = html_entity_decode(get_the_excerpt());
			$results[$r]['metadescription'] = html_entity_decode(get_the_excerpt());
		}
        $results[$r]['commentCount'] = get_comments_number();
        $results[$r]['postUrl'] = get_permalink();
        $results[$r]['pubTime'] = esc_attr( get_the_date('M d, Y')) . ', ' .strtoupper(esc_attr( get_the_time('h:i a') )).' IST';
		$r++;
	endwhile;
    wp_reset_query();
    $data = json_encode($results, JSON_UNESCAPED_UNICODE);
	if(array_key_exists('callback', $_GET)){
		header('Content-Type: text/javascript; charset=utf8');
		$callback = $_GET['callback'];
		echo $callback.'('.$data.');';
	}else{
		header('Content-Type: application/json; charset=utf8');
		echo $data;
	}
    die();
}

function getTaxonomyLocalTitle($taxonomy, $t_id, $taxonomy_name){
	$term_meta =  get_option( "$taxonomy_$t_id" );
	if(isset($term_meta['wps_subtitle']) && !empty($term_meta['wps_subtitle'])){
		$taxonomy_name = $term_meta['wps_subtitle'];
	}
	return $taxonomy_name;
}

/**
*
* The ajax xml API.
*
**/
add_action('wp_ajax_nopriv_recentEntriesFeed', 'recentEntriesFeed');
add_action('wp_ajax_recentEntriesFeed', 'recentEntriesFeed');

function recentEntriesFeed() {
    global $my_settings;
    if(isset($_GET['n']) && !empty($_GET['n'])){
        $posts_per_page = $_GET['n'];
    }else {
        $posts_per_page = 20;
    }
    if(isset($_GET['entryid']) && !empty($_GET['entryid'])){
        query_posts( array('post__in' => array($_GET['entryid'])) );
        //query_posts( array('post__in' => array($_GET['entryid']), 'post_status' => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash')) );
    }else {
        query_posts( 'post_status=publish&posts_per_page=' . $posts_per_page );
    }
    $results = array();
    $r = 0;
    $html = '<?xml version="1.0" encoding="UTF-16"?>'.PHP_EOL.'<urlset>'.PHP_EOL;
    while ( have_posts() ) : the_post();
		$html = $html . '<url>'.PHP_EOL;
		$results[$r]['authorName'] = get_the_author_meta( 'display_name' );
		$title = get_the_title();
		$subtitle = get_post_meta( get_the_ID(), 'wps_subtitle', true );
        $results[$r]['anchor'] = get_post_field( 'post_name', get_the_ID() );
        $results[$r]['title'] = $subtitle ? $subtitle : $title;
        $results[$r]['title'] = html_entity_decode($results[$r]['title']);
        $results[$r]['id'] = get_the_ID();
        $blog = array_values(get_the_terms(get_the_ID(), 'blog' ))[0];
        $category = array_values(get_the_terms(get_the_ID(), 'category' ))[0];
        $results[$r]['handle'] = html_entity_decode (getTaxonomyLocalTitle($blog->taxonomy, $blog->term_id, $blog->name));
        $results[$r]['category'] = html_entity_decode (getTaxonomyLocalTitle($category->taxonomy, $category->term_id, $category->name));
        $results[$r]['authorBlog'] = get_term_link($blog->term_id, $blog->taxonomy);
        $results[$r]['blogurl'] = get_bloginfo( 'url' );
        $results[$r]['imageUrl'] = get_user_avatar(get_the_author_meta('ID'));
        $results[$r]['commentCount'] = get_comments_number();
        $results[$r]['postUrl'] = get_permalink();
        $results[$r]['pubTime'] = esc_attr( get_the_date('M d, Y')) . ', ' .strtoupper(esc_attr( get_the_time('h:i a') )).' IST';
        $content = apply_filters('the_content', get_the_content());
        $content = str_replace(trim($my_settings['blogauthor_link'], '/'), WP_HOME, $content);
        $content = str_replace(trim($my_settings['old_blogauthor_link'], '/'), WP_HOME, $content);
        $result[$r]['desc'] = preg_replace_callback("/(<img[^>]*src *= *[\"']?)([^\"']*)/i", "addQuesMarkToImgSrc", $content);
        $html = $html . '<entryid>'.$results[$r]['id'].'</entryid>'.PHP_EOL;	
		$html = $html . '<authorBlog>'.$results[$r]['authorBlog'].'</authorBlog>'.PHP_EOL;
		$html = $html . '<blogName>'.$results[$r]['handle'].'</blogName>'.PHP_EOL;
		$html = $html . '<imageUrl>'.$results[$r]['imageUrl'].'</imageUrl>'.PHP_EOL;
		$html = $html . '<authorName>'.$results[$r]['authorName'].'</authorName>'.PHP_EOL;
		$html = $html . '<postUrl>'.$results[$r]['postUrl'].'</postUrl>'.PHP_EOL;
		$html = $html . '<title><![CDATA['.html_entity_decode($results[$r]['title']).']]></title>'.PHP_EOL;
		$html = $html . '<headline><![CDATA['.html_entity_decode($results[$r]['title']).']]></headline>'.PHP_EOL;
		$html = $html . '<description><![CDATA['.html_entity_decode($result[$r]['desc']).']]></description>'.PHP_EOL;
		$html = $html . '<displayContent><![CDATA['.html_entity_decode(get_the_excerpt()).']]></displayContent>'.PHP_EOL;
		$html = $html . '<pubTime>'.$results[$r]['pubTime'].'</pubTime>'.PHP_EOL;
		$html = $html . '<section>$getSection</section>'.PHP_EOL;
		$html = $html . '<commentCount>'.$results[$r]['commentCount'].'</commentCount>'.PHP_EOL;
        $html = $html . '</url>'.PHP_EOL;
		$r++;
	endwhile;
	$html = $html . '</urlset>';
    wp_reset_query();
    //header('Content-Type: text/xml');
	echo $html;
    die();
}

function addQuesMarkToImgSrc($matches) {
	return $matches[1] . $matches['2'] . '?';
}

function filter_authors($groupby) {
	global $wpdb;
	$groupby = " {$wpdb->posts}.post_author";
	return $groupby;
}

function max_fields($fields){
	global $wpdb;
	$fields = str_replace("{$wpdb->posts}.ID", "Max({$wpdb->posts}.ID)", $fields);
	return $fields;
}

add_action('wp_ajax_nopriv_getAuthorsRecentArticle', 'getAuthorsRecentArticle');
add_action('wp_ajax_getAuthorsRecentArticle', 'getAuthorsRecentArticle');

function getAuthorsRecentArticle() {
    global $my_settings;
    query_posts( 'post_status=publish&posts_per_page=10' );
    if(isset($_GET['authorid']) && !empty($_GET['authorid'])){
		add_filter('posts_groupby','filter_authors', 11);
		add_filter('posts_fields', 'max_fields');
		query_posts( 'fields=ids&author=' . $_GET['authorid'] . '&post_status=publish&posts_per_page=' . $posts_per_page );
		remove_filter('posts_groupby','filter_authors', 11);
		remove_filter('posts_fields', 'max_fields');
	}
    $results = array();
    $r = 0;
    $html = '<?xml version="1.0" encoding="UTF-16"?>'.PHP_EOL.'<urlset>'.PHP_EOL;
    while ( have_posts() ) : the_post();
		$html = $html . '<url>'.PHP_EOL;
		$post = get_post(get_the_ID());
        setup_postdata($post);
		$results[$r]['authorName'] = get_the_author_meta( 'display_name' );
		$title = get_the_title();
		$subtitle = get_post_meta( get_the_ID(), 'wps_subtitle', true );
        $results[$r]['anchor'] = get_post_field( 'post_name', get_the_ID() );
        $results[$r]['title'] = $subtitle ? $subtitle : $title;
        $results[$r]['title'] = html_entity_decode($results[$r]['title']);
        $results[$r]['id'] = get_the_ID();
        $blog = array_values(get_the_terms(get_the_ID(), 'blog' ))[0];
        $category = array_values(get_the_terms(get_the_ID(), 'category' ))[0];
        $results[$r]['handle'] = html_entity_decode (getTaxonomyLocalTitle($blog->taxonomy, $blog->term_id, $blog->name));
        $results[$r]['category'] = html_entity_decode (getTaxonomyLocalTitle($category->taxonomy, $category->term_id, $category->name));
        $results[$r]['authorBlog'] = get_term_link($blog->term_id, $blog->taxonomy);
        $results[$r]['blogurl'] = get_bloginfo( 'url' );
        /*if ( has_post_thumbnail(get_the_ID())) { 
            $results[$r]['imageUrl'] = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' )[0];
        }else{
            $results[$r]['imageUrl'] = get_user_avatar(get_the_author_meta('ID'));
        }*/
        $results[$r]['imageUrl'] = get_user_avatar(get_the_author_meta('ID'));
        if(isset($_GET['desc']) && $_GET['desc'] == 'y'){
			$content = apply_filters('the_content', get_the_content());
			$content = str_replace(trim($my_settings['blogauthor_link'], '/'), WP_HOME, $content);
            $content = str_replace(trim($my_settings['old_blogauthor_link'], '/'), WP_HOME, $content);
			$results[$r]['description'] = html_entity_decode($content);
			$results[$r]['displaytruncatedContent'] = html_entity_decode(get_the_excerpt());
			$results[$r]['metadescription'] = html_entity_decode(get_the_excerpt());
		}
        $results[$r]['commentCount'] = get_comments_number();
        $results[$r]['postUrl'] = get_permalink();
        $results[$r]['pubTime'] = esc_attr( get_the_date('M d, Y')) . ', ' .strtoupper(esc_attr( get_the_time('h:i a') )).' IST';
        $content = apply_filters('the_content', get_the_content());
        $content = str_replace(trim($my_settings['blogauthor_link'], '/'), WP_HOME, $content);
        $content = str_replace(trim($my_settings['old_blogauthor_link'], '/'), WP_HOME, $content);
        $result[$r]['desc'] = preg_replace_callback("/(<img[^>]*src *= *[\"']?)([^\"']*)/i", "addQuesMarkToImgSrc", $content);
		$html = $html . '<entryid>'.$results[$r]['id'].'</entryid>'.PHP_EOL;	
		$html = $html . '<authorBlog>'.$results[$r]['authorBlog'].'</authorBlog>'.PHP_EOL;
		$html = $html . '<blogName>'.$results[$r]['handle'].'</blogName>'.PHP_EOL;
		$html = $html . '<imageUrl>'.$results[$r]['imageUrl'].'</imageUrl>'.PHP_EOL;
		$html = $html . '<authorName>'.$results[$r]['authorName'].'</authorName>'.PHP_EOL;
		$html = $html . '<postUrl>'.$results[$r]['postUrl'].'</postUrl>'.PHP_EOL;
		$html = $html . '<title><![CDATA['.html_entity_decode($results[$r]['title']).']]></title>'.PHP_EOL;
		$html = $html . '<headline><![CDATA['.html_entity_decode($results[$r]['title']).']]></headline>'.PHP_EOL;
		//$html = $html . '<description><![CDATA['.html_entity_decode($result[$r]['desc']).']]></description>'.PHP_EOL;
		$html = $html . '<displayContent><![CDATA['.html_entity_decode(get_the_excerpt()).']]></displayContent>'.PHP_EOL;
		$html = $html . '<pubTime>'.$results[$r]['pubTime'].'</pubTime>'.PHP_EOL;
		$html = $html . '<section>$getSection</section>'.PHP_EOL;
		$html = $html . '<commentCount>'.$results[$r]['commentCount'].'</commentCount>'.PHP_EOL;
        $html = $html . '</url>'.PHP_EOL;
        $r++;
	endwhile;
	$html = $html . '</urlset>';
    wp_reset_query();
    if($_GET['feedtype'] == 'xml'){
		echo $html;
	} else {
		$data = json_encode($results, JSON_UNESCAPED_UNICODE);
		if(array_key_exists('callback', $_GET)){
			header('Content-Type: text/javascript; charset=utf8');
			$callback = $_GET['callback'];
			echo $callback.'('.$data.');';
		}else{
			header('Content-Type: application/json; charset=utf8');
			echo $data;
		}
	}
    die();
}

function getUcBrowserHomeArticle(){
	
	date_default_timezone_set('Asia/Calcutta'); 
	global $my_settings;
	
	$feed = '<?xml version="1.0" encoding="UTF-8"?><rss xmlns:xslthelper="com.times.utils.xslthelper.XMLHelper" xmlns:media="http://search.yahoo.com/mrss/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" version="2.0">';
		$feed .= '<channel>';
		
			$feed .= '<title>'.get_bloginfo('name').'</title>';
			$feed .= '<link>'.get_bloginfo('url').'</link>';
			$feed .= '<description>'.get_bloginfo('description').'</description>';
			$feed .= '<language>'.$my_settings['site_lang'].'</language>';
			//$feed .= '<lastBuildDate timestamp="'.current_time( 'timestamp', true ).'">'.date('M d, Y, H:i').'</lastBuildDate>';
			
			$paged = 1;
			$postsPerPage = 50;
			if(isset($_GET['pageNumber']) && $_GET['pageNumber'] != '' && is_numeric($_GET['pageNumber'])){
				$paged = $_GET['pageNumber'];
			}
			if(isset($_GET['pageSize']) && $_GET['pageSize'] != '' && is_numeric($_GET['pageSize'])){
				$postsPerPage = $_GET['pageSize'];
			}
			
			$query 	= new WP_Query( array( 'posts_per_page' => $postsPerPage, 'paged'=>$paged, 'orderby'=> 'DESC', 'post_status' => 'publish' ));
			
			$i = 0;
			while ( $query->have_posts() ) {
                    $query->the_post();	
                    
                    $postID = get_the_ID();	
					$img = get_the_post_thumbnail_url( $postID, 'post-thumbnail');
					if(!$img){
						$img = get_user_avatar(get_the_author_meta('ID'));
					}					
					$title = get_the_title($postID);
					$subtitle = get_post_meta($postID, 'wps_subtitle', true );
					$category = array_values(get_the_terms($postID, 'category' ))[0];
					$tags = wp_get_post_tags($postID,array( 'fields' => 'names' ));
					$tagNames = '';
					if(count($tags) > 0){
						$tagNames = implode(',',$tags);
					}
					
					if($i == 0){
						$feed .= '<lastBuildDate timestamp="'.number_format((strtotime(get_the_date('M d, Y, H:i',$postID))*1000), 0, '.', '').'">'.get_the_date('M d, Y, H:i',$postID).'</lastBuildDate>';
						$i++;
					}
					
					$feed .= '<item>';
						$feed .= '<title>'.($subtitle ? $subtitle : $title).'</title>';			
						//$feed .= '<link>'.get_the_permalink($singlePost->ID).'uc_browser</link>';			
						$feed .= '<link><![CDATA['.admin_url( 'admin-ajax.php' ).'?action=getUcBrowserArticleDetail&postID='.$postID.']]></link>';			
						$feed .= '<pubDate timestamp="'.number_format((strtotime(get_the_date('M d, Y, H:i',$postID))*1000), 0, '.', '').'">'.get_the_date('M d, Y, H:i',$postID).'</pubDate>';	
						$feed .= '<author>'.get_the_author_meta('display_name').'</author>';		
						$feed .= '<category>'.html_entity_decode (getTaxonomyLocalTitle($category->taxonomy, $category->term_id, $category->name)).'</category>';	
						$feed .= '<guid>'.$postID.'</guid>';	
						$feed .= '<content:encoded>';
							$feed .= '<![CDATA[';
								if($img){
									$feed .= '<img src="'.$img.'"/>';
								}
								$feed .= html_entity_decode(get_the_excerpt());
							$feed .= ']]>';
						$feed .= '</content:encoded>';
						$feed .= '<dc:language>hindi</dc:language>';
						$feed .= '<keywords>'.$tagNames.'</keywords>';
						$feed .= '<coverImages>';
							$feed .= '<image>'.$img.'</image>';
						$feed .= '</coverImages>';
						
					$feed .= '</item>';	
					
			}		
			
		$feed .= '</channel>';
	$feed .= '</rss>';
	
	header('Content-Type: text/xml');
	echo $feed;exit;
	
}

add_action('wp_ajax_nopriv_getUcBrowserHomeArticle', 'getUcBrowserHomeArticle');
add_action('wp_ajax_getUcBrowserHomeArticle', 'getUcBrowserHomeArticle');

function getUcBrowserArticleDetail(){
	if(isset($_GET['postID']) && $_GET['postID'] != ''){
		$postID = $_GET['postID'];
		$post = get_post($postID);
		setup_postdata( $post );
		include_once(get_template_directory().'/ucbrowser-template.php');
		//print_r($post);
	}
	exit;
}

add_action('wp_ajax_nopriv_getUcBrowserArticleDetail', 'getUcBrowserArticleDetail');
add_action('wp_ajax_getUcBrowserArticleDetail', 'getUcBrowserArticleDetail');


add_action('wp_ajax_nopriv_blogsCommentsFeed', 'blogsCommentsFeed');
add_action('wp_ajax_blogsCommentsFeed', 'blogsCommentsFeed');

function blogsCommentsFeed() {

    if(isset($_GET['perpage']) && !empty($_GET['perpage'])){
        $posts_per_page = $_GET['perpage'];
    }else {
        $posts_per_page = 20;
    }
    if(isset($_GET['curpage']) && !empty($_GET['curpage'])){
        $curpg = $_GET['curpage'];
    }else {
        $curpg = 1;
    }
    
    $postId = $_GET['msid'];
    $appKeyForMyTimes = getAppKeyForMyTimes();
   // $url =  COMMENTS_URL.'showcomments.cms?extid='.$postId.'&perpage=15&feedtype=json&curpg='.$curpg;
    $ordertype = 'desc';
    if(isset($_GET['ordertype']) && !empty($_GET['ordertype'])){
        $ordertype=$_GET['ordertype'];
    }

    $commenttype  = 'CreationDate';
    if(isset($_GET['commenttype']) && !empty($_GET['commenttype'])){
        $commenttype =$_GET['commenttype'];
    }
    

    switch($commenttype){
        
        case "disagree":
        $commenttype= 'disagree';
        $sortcriteria= 'DisagreeCount';
        $order = 'desc';
        break;

        case 'discussed':
        $commenttype = 'mostdiscussed'; 
        $sortcriteria = 'discussed';
        $order = 'desc';
        break;

        case 'agree':
        $commenttype='agree';
        $sortcriteria='AgreeCount';
        $order='desc';
        break;

        default:
        $sortcriteria = 'CreationDate';
        $order       = ($ordertype=="asc")?"desc":"asc";
        break;
    }

    $queryStringData = 'msid='.$postId.'&curpg='.$curpg.'&appkey='.$appKeyForMyTimes.'&commenttype='.$commenttype.'&sortcriteria='.$sortcriteria.'&order='.$order.'&size='.$posts_per_page.'&pagenum='.$curpg.'&withReward=true';
    //MYTIMES_URL/mytimes/getFeed/Activity?msid=".$postId."&curpg=1&appkey=FEMINA&sortcriteria=CreationDate&order=asc&size=10&pagenum=1&withReward=true
    $url = MYTIMES_URL.'mytimes/getFeed/Activity';
    $curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Accept-Encoding: gzip; deflate', 'Accept-Language: en-US');
    
    $url  = $url."/?".$queryStringData;
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url); 
    //curl_setopt($ch, CURLOPT_POST, 1);
    //curl_setopt($ch, CURLOPT_POSTFIELDS,array());
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
    $ch_result = curl_exec($ch);
    curl_close($ch);
    $rawComments = json_decode($ch_result);
    //echo "<pre>";
    //var_dump($rawComments);
    $comments = new stdClass();
    $comments->totalcount = $rawComments[0]->totalcount;
    $comments->ActionParams = array();
    $comments->rothrd= new stdClass();
    $comments->rothrd->op = array();
    $comments->rothrd->potime = "96";
    $comments->rothrd->opctr = $rawComments[0]->totalcount;
    $comments->rothrd->opctrtopcnt = $rawComments[0]->cmt_c;
    $comments->rothrd->recommendcount = $rawComments[0]->cmt_c;
    $comments->rothrd->agreecount = $rawComments[0]->cmt_c;
    $comments->rothrd->disagreecount = $rawComments[0]->cmt_c;
    $comments->rothrd->loggedopctr = "0";
    unset($rawComments[0]);
    foreach ($rawComments as $key => $value) 
    {
        
        $indi = extraDataFromAPI($value);
        array_push($comments->rothrd->op,$indi);

        $childIndi = array();
        formatChildForReplyShow($childIndi,$value);
        

        foreach ($childIndi as $key => $childValue) 
        {
            array_push($comments->rothrd->op,$childValue);
        }
    }

    $commentList = array();
    if(isset($comments->rothrd) && isset($comments->rothrd->op)){
        if(is_array($comments->rothrd->op)){
            $commentList = $comments->rothrd->op;
        } else {
            $commentList[0] = $comments->rothrd->op;
        }
        $total_comments = $comments->rothrd->opctr;
    } else {
        $total_comments = 0;
    }

    //echo "<pre>";
    if(isset($_GET['tag']) && $_GET['tag'] == 'cc'){
        $results = array('id' => $_GET['msid'], 'cc' => $total_comments);
    } else {
        $total_records = $total_comments;
        $total_pages = ceil($total_records / $posts_per_page);
        $pg_array = array('tr' => $total_records, 'pp' => $posts_per_page, 'tp' => $total_pages, 'cp' => $curpg);
        $items_array = array();
        $r = 0;
        foreach($commentList as $comm){
            $items_array[$r]['frm'] = $comm->roaltdetails['fromname'];
            $items_array[$r]['loc'] = $comm->roaltdetails['location'];
            $items_array[$r]['dl'] = strtotime(str_replace(',','', preg_replace('/(\d)(A|P)M/', "$1 $2M", $comm->rodate)));
            $items_array[$r]['ct'] = html_entity_decode($comm->optext);
            //$items_array[$r]['msgid'] = $comm->messageid;
            $items_array[$r]['iu'] = $comm->roaltdetails['imageurl'];
            $items_array[$r]['parentid'] = $comm->parentid;
            $items_array[$r]['rootid'] = $comm->rootid;
            $r++;
        }
        $results = array('pg' => $pg_array, 'it' => $items_array);
    }
    
    $results = (object) $results;
    $data = json_encode($results, JSON_UNESCAPED_UNICODE);
    header('Content-Type: application/json; charset=utf8');
    echo $data;
    die();
}

add_action('wp_ajax_nopriv_blogsPostComment', 'blogsPostComment');
add_action('wp_ajax_blogsPostComment', 'blogsPostComment');

function blogsPostComment(){
	$errorCount = 0;
	$errorMsg = array();
	if(isset($_POST['fromname']) && $_POST['fromname'] == ''){
		$errorMsg[] = 'fromname is empty';
		$errorCount++;
	}
	if(isset($_POST['fromaddress']) && $_POST['fromaddress'] == ''){
		$errorMsg[] = 'fromaddress is empty';
		$errorCount++;
	}
	if(isset($_POST['userid']) && $_POST['userid'] == ''){
		$errorMsg[] = 'userid is empty';
		$errorCount++;
	}
	if(isset($_POST['imageurl']) && $_POST['imageurl'] == ''){
		$errorMsg[] = 'imageurl is empty';
		$errorCount++;
	}
	if(isset($_POST['loggedstatus']) && $_POST['loggedstatus'] == ''){
		$errorMsg[] = 'loggedstatus is empty';
		$errorCount++;
	}
	if(isset($_POST['message']) && $_POST['message'] == ''){
		$errorMsg[] = 'message is empty';
		$errorCount++;
	}
	if(isset($_POST['roaltdetails']) && $_POST['roaltdetails'] == ''){
		$errorMsg[] = 'roaltdetails is empty';
		$errorCount++;
	}
	if(isset($_POST['ArticleID']) && $_POST['ArticleID'] == ''){
		$errorMsg[] = 'ArticleID is empty';
		$errorCount++;
	}
	if(isset($_POST['msid']) && $_POST['msid'] == ''){
		$errorMsg[] = 'msid is empty';
		$errorCount++;
	}
	if(isset($_POST['parentid']) && $_POST['parentid'] == ''){
		$errorMsg[] = 'parentid is empty';
		$errorCount++;
	}
	if(isset($_POST['rootid']) && $_POST['rootid'] == ''){
		$errorMsg[] = 'rootid is empty';
		$errorCount++;
	}
	if(isset($_POST['url']) && $_POST['url'] == ''){
		$errorMsg[] = 'url is empty';
		$errorCount++;
	}
	if($errorCount > 0){
		echo json_encode($errorMsg); exit;
	}
	validatecomment();
}
