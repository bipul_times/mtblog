<?php
/**
 * The blog template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage BlogsTheme
 * @since Blogs Theme 1.0
 */

get_header(); 
     $blog = get_term_by('slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
     $blogAuthorIds = get_option( "blogAuthors_".$blog->term_id);
?>
<script type='text/javascript'>
curPage = 'Blog';
pgname = "<?php echo $blog->slug; ?>";
</script>
<div class="container">
  <div class="row">
    <div class="col-md-8">
        <?php printf( __( '<h1 class="blog-heading"> %s </h1>', 'twentyfourteen' ), $blog->name ); 
    if ( !empty($blog->description) ) {
      echo '<p class="description">'.$blog->description.'</p>';     
    } 


        ?>
              
     <!--<div class="thumbnail">
      <img src="<?php echo z_taxonomy_image_url();?>" class="attachment-post-thumbnail wp-post-image" style="min-height: 80px;" alt="<?php echo single_cat_title( '', false ) ?>"/>
      <?php printf( __( '<h1 class="overlay"> %s <a href="%s" target="_blank" style="outline:none;"><img src="%s" style="width:20px;margin: 0px;padding: 0px;display: inline;margin-left: 10px;" alt="RSS" title="Subscribe to RSS"></a></h1>', 'twentyfourteen' ), $blog->name ,$_SERVER["REQUEST_URI"]."feed/rss2",get_template_directory_uri().'/images/rss.png'); ?>
    </div>
    <?php
    ?>
    -->
    <div class="feeds">
    <?php
    if ( have_posts() ) :
				// Start the Loop.
      while ( have_posts() ) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
            get_template_part('content', get_post_format());
					endwhile;
				// Previous/next post navigation.
					twentyfourteen_paging_nav();

					else :
				// If no content, include the "No posts found" template.
						get_template_part( 'content', 'none' );

					endif;
					?>
        </div>
        </div>
        <div class="col-md-4 sidebar">
          <div class="panel authors">
            <div class="panel-heading">
              <h3 class="panel-title"><?php echo $my_settings['author_txt']; ?></h3>
            </div>
            <div class="panel-body">
              <?php 
              foreach ($blogAuthorIds as $id) {
                $author = get_userdata($id);
                ?> 
                  <a class="pull-left" href="<?php echo get_author_posts_url( $id) ?>" title="<?php echo $my_settings['go_to_the_profile_of_txt']; ?> <?php echo $author->display_name; ?>">
                    <img class="media-object" src="<?php echo get_user_avatar($id); ?>" alt="<?php echo $author->display_name;?>">
                  </a>
                  <a class="media-heading" href="<?php echo get_author_posts_url( $id) ?>" rel="bookmark" title="<?php echo $my_settings['go_to_the_profile_of_txt']; ?> <?php echo $author->display_name; ?>"><b><?php echo $author->display_name; ?></b></a>
                  <?php 
                if(count($blogAuthorIds) == 1){
                $desc = get_the_author_meta( 'description', $id );
                if(!empty($desc)){
                ?>
              <div class="bio">
                <div id="showmore" style="display:none">
                  <?php echo $desc.'<br><br>';?>            
                </div>  
                <div id="showless">
                  <?php echo substr($desc, 0, strpos($desc, ' ', 156));
                  if(strlen ($desc) > 156){
                    echo '. . .';
                  }
                  echo '<br><br>';
                  ?>            
                </div>        
                <?php if(strlen($desc) > 161){ ?>
                <a href="javascript:void(0)" pg="Blog_Article_MoreButton" id="more-button"><?php echo $my_settings['more_txt']; ?></a>
                <a href="javascript:void(0)" pg="Blog_Article_LessButton" id="less-button" style="display:none"><?php echo $my_settings['less_txt']; ?></a>
                <script>
                var a1 = document.getElementById("more-button");
                a1.onclick = function() {
                  $("#showmore").show();
                  $("#showless").hide();  
                  $("#less-button").show();
                  $("#more-button").hide(); 
                  return false;
                }
                var a2 = document.getElementById("less-button");
                a2.onclick = function() {
                  $("#showmore").hide();
                  $("#showless").show();
                  $("#less-button").hide();
                  $("#more-button").show(); 
                  return false;
                }
                </script>
                <?php } ?>
              </div>
              <hr style="margin:0px" />
              <?php 
            }
            }
              echo'                 <div class="clear"></div>';
              }
              ?>
              </div>
            </div>
          <div class="panel">
              <div class="addwrapper">
                <?php echo $my_settings['google_ad_right_1']; ?>
              </div>
          </div>
          <?php include 'sidebar.php' ?>
      </div>
    </div>
  </div><!-- .container -->
  <?php
  get_footer();
