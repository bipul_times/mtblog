<?php 

define('Z_IMAGE_PLACEHOLDER', get_template_directory_uri()."/images/placeholder.png");
define('FB_PLACEHOLDER', get_template_directory_uri()."/images/blog-mt-facebook.png");
define('Z_AUTHOR_PLACEHOLDER', get_template_directory_uri()."/images/50.jpg");
define('RECO_URL','http://reco.indiatimes.com/Reco/RecoIndexer?eventType=update&hostid=1b&contentType=blog&msid=');
define('COMMENTS_URL','http://www.maharashtratimesblogcmtapi.indiatimes.com/');
define('RCHID', '2147478035');
//define('MYTIMES_URL','http://myt.indiatimes.com/');
define('MYTIMES_URL','http://mytpvt.indiatimes.com/');
define('MYT_COMMENTS_API_KEY', 'MTB');

global $my_settings;
$my_settings['site_lang'] = 'mr';
$my_settings['og_locale'] = 'mr_IN';
$my_settings['google_site_verification'] = 'ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw';
$my_settings['fblikeurl'] = 'https://www.facebook.com/maharashtratimesonline';
$my_settings['favicon'] = 'https://maharashtratimes.indiatimes.com/icons/mtfavicon.ico';
$my_settings['ga'] = "<script>
TimesGDPR.common.consentModule.gdprCallback(function(data) {
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-429254-4', 'auto', {'allowLinker': true});
  var userCookie = document.cookie.match(/(?:\s)?ssoid=(\w+);?/);
  if(!!(userCookie)) {
	ga('set', 'userId', userCookie[1]);
  }
  if( TimesGDPR.common.consentModule.isEUuser() ){
  ga('set', 'anonymizeIp', true); 
  }
  ga('require', 'displayfeatures'); 
  ga('require', 'linker');
  ga('linker:autoLink', ['superpacer.in', 'mtmobile.in', 'timesofindia.com', 'mtganeshotsav.com'] );
  ga('send', 'pageview');
  });
  </script>";
$my_settings['fbappid'] = '117787264903013';
$my_settings['channel_url_part'] = 'channel=mt';
$my_settings['main_site_url'] = 'https://maharashtratimes.indiatimes.com';
$my_settings['main_site_txt'] = 'Maharashtra Times Blog';
$my_settings['fb_url'] = 'https://www.facebook.com/maharashtratimesonline';
$my_settings['twitter_url'] = 'https://twitter.com/mataonline';
$my_settings['twitter_handle'] = 'mataonline';
$my_settings['google_url'] = 'https://plus.google.com/+maharashtratimes/';
$my_settings['rss_url'] = 'https://maharashtratimes.indiatimes.com/rss.cms';
$my_settings['logo_title'] = 'MT Blogs';
$my_settings['logo_url'] = 'https://maharashtratimes.indiatimes.com/photo/47630859.cms';
$my_settings['footer_logo_txt'] = 'महाराष्ट्र टाइम्स';
$my_settings['comscore_tag'] = '<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  var objComScore = { c1: "2", c2: "6036484" };
   TimesGDPR.common.consentModule.gdprCallback(function(data) {
    if( TimesGDPR.common.consentModule.isEUuser() ){
      objComScore["cs_ucfr"] = 0;
    }
   _comscore.push(objComScore); 
  (function() {
	var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
	s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
	el.parentNode.insertBefore(s, el);
  })();
  });
</script>
<noscript><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6036484&amp;cv=2.0&amp;cj=1"></noscript>
<!-- End comScore Tag -->';
$my_settings['ibeat_channel'] = 'MtBlog';
$my_settings['ibeat_host'] = 'blogs.maharashtratimes.indiatimes.com';
$my_settings['ibeat_key'] = '6eca6530ab3181aedfef17a11c9bbc5';
$my_settings['ibeat_domain'] = 'maharashtratimes.indiatimes.com';
//$my_settings['footer_dmp_tag'] = 'https://static.clmbtech.com/ase/7268/15/aa.js';
if(isMobile()){
    $my_settings['footer_dmp_tag'] = 'https://static.clmbtech.com/ase/7268/46/aa.js';
}else{
    $my_settings['footer_dmp_tag'] = 'https://static.clmbtech.com/ase/7268/15/aa.js';
}
$my_settings['footer_google_tag'] = "<div id='div-gpt-ad-1340608162797-0-oop1'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1340608162797-0-oop1'); });
</script>
</div>
<div id='div-gpt-ad-1340608162797-0-oop3'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1340608162797-0-oop3'); });
</script>
</div>
<div id='div-gpt-ad-1340608162797-0-oop4'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1340608162797-0-oop4'); });
</script>
</div>";

global $wp;
$my_settings['google_tag_js_head'] = "
<script type='text/javascript' src='https://ade.clmbtech.com/cde/aef/var=colaud?cid=7268:46&fpc=".$_COOKIE['_col_uuid']."&optout=1&dsmi=1&_u=".home_url( $wp->request )."'></script>
<script type='text/javascript'>
	var googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];
	(function() {
	var gads = document.createElement('script');
	gads.async = true;
	gads.type = 'text/javascript';
	var useSSL = 'https:' == document.location.protocol;
	gads.src = (useSSL ? 'https:' : 'http:') + 
	'//www.googletagservices.com/tag/js/gpt.js';
	var node = document.getElementsByTagName('script')[0];
	node.parentNode.insertBefore(gads, node);
	})();

	function setCookie(name,value,days) {
		var expires = '';
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days*24*60*60*1000));
			expires = '; expires=' + date.toUTCString();
		}
		document.cookie = name + '=' + (value || '')  + expires + '; path=/';
	}
	function getCookie(name) {
		var nameEQ = name + '=';
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}
	</script>
	
	<script type='text/javascript'>
	googletag.cmd.push(function() {
	
	<!-- Audience Segment Targeting -->
	var _auds = '';
	if(typeof(colaud)!='undefined') {
		_auds = colaud.aud;
		setCookie('colaud', _auds, 365)		
	}else{
		if(getCookie('colaud')!='undefined'){
			_auds = getCookie('colaud');
		}
	}
	<!-- End Audience Segment Targeting -->
	
	<!-- Contextual Targeting -->
	var _HDL = '';
	var _ARC1 = '';
	var _Hyp1 = '';
	var _article = '';
	var _tval = function(v) {
	if(typeof(v)=='undefined') return '';
	if(v.length>100) return v.substr(0,100);
	return v;
	}
	<!-- End Contextual Targeting -->
	
googletag.defineSlot('/7176/Maharashtratimes/MT_Home/MT_Home_Blog_AL/MT_ROS_ATF_HOM_BLG_AL_300', [[300, 250], [300, 1050]], 'div-gpt-ad-1340608162797-0').setTargeting('L1', '').setTargeting('L2', '').setTargeting('L3', '').setTargeting('H1', '').setTargeting('ARC1', '').setTargeting('Hyp1', '').addService(googletag.pubads());
googletag.defineSlot('/7176/Maharashtratimes/MT_Home/MT_Home_Blog_AL/MT_ROS_ATF_HOM_BLG_AL_728', [[728, 90], [1003, 90]], 'div-gpt-ad-1340608162797-1').setTargeting('L1', '').setTargeting('L2', '').setTargeting('L3', '').setTargeting('H1', '').setTargeting('ARC1', '').setTargeting('Hyp1', '').addService(googletag.pubads());
googletag.defineSlot('/7176/Maharashtratimes/MT_Home/MT_Home_Blog_AL/MT_ROS_BTF_HOM_BLG_AL_300', [300, 250], 'div-gpt-ad-1340608162797-2').setTargeting('L1', '').setTargeting('L2', '').setTargeting('L3', '').setTargeting('H1', '').setTargeting('ARC1', '').setTargeting('Hyp1', '').addService(googletag.pubads());
googletag.defineOutOfPageSlot('/7176/Maharashtratimes/MT_Home/MT_Home_Blog_AL/MT_ROS_OP_HOM_BLG_AL_Innov1','div-gpt-ad-1340608162797-0-oop1').setTargeting('L1', '').setTargeting('L2', '').setTargeting('L3', '').setTargeting('H1', '').setTargeting('ARC1', '').setTargeting('Hyp1', '').addService(googletag.pubads());
googletag.defineOutOfPageSlot('/7176/Maharashtratimes/MT_Home/MT_Home_Blog_AL/MT_ROS_OP_HOM_BLG_AL_Inter','div-gpt-ad-1340608162797-0-oop2').setTargeting('L1', '').setTargeting('L2', '').setTargeting('L3', '').setTargeting('H1', '').setTargeting('ARC1', '').setTargeting('Hyp1', '').addService(googletag.pubads());
googletag.defineOutOfPageSlot('/7176/Maharashtratimes/MT_Home/MT_Home_Blog_AL/MT_ROS_OP_HOM_BLG_AL_POP','div-gpt-ad-1340608162797-0-oop3').setTargeting('L1', '').setTargeting('L2', '').setTargeting('L3', '').setTargeting('H1', '').setTargeting('ARC1', '').setTargeting('Hyp1', '').addService(googletag.pubads());
googletag.defineOutOfPageSlot('/7176/Maharashtratimes/MT_Home/MT_Home_Blog_AL/MT_ROS_OP_HOM_BLG_AL_Shosh','div-gpt-ad-1340608162797-0-oop4').setTargeting('L1', '').setTargeting('L2', '').setTargeting('L3', '').setTargeting('H1', '').setTargeting('ARC1', '').setTargeting('Hyp1', '').addService(googletag.pubads());
googletag.pubads().setTargeting('sg', _auds).setTargeting('HDL', _tval(_HDL)).setTargeting('ARC1', _tval(_ARC1)).setTargeting('Hyp1', _tval(_Hyp1)).setTargeting('article', _tval(_article));

googletag.pubads().enableSingleRequest();
 
googletag.pubads().collapseEmptyDivs();
TimesGDPR.common.consentModule.gdprCallback(function(data) {
	if( TimesGDPR.common.consentModule.isEUuser() ) {
		googletag.pubads().setRequestNonPersonalizedAds(1);
	}
	googletag.enableServices();
} );
});
</script>";
$my_settings['google_ad_top'] = "<!-- /7176/Maharashtratimes/MT_Home/MT_Home_Blogs_AL/MT_ROS_ATF_HOM_BLG_AL_728 -->
<div id='div-gpt-ad-1340608162797-1'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1340608162797-1'); });
</script>
</div>";
$my_settings['google_ad_right_1'] = "<!-- /7176/Maharashtratimes/MT_Home/MT_Home_Blogs_AL/MT_ROS_ATF_HOM_BLG_AL_300 -->
<div id='div-gpt-ad-1340608162797-0'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1340608162797-0'); });
</script>
</div>";
$my_settings['google_ad_right_2'] = "<!-- /7176/Maharashtratimes/MT_Home/MT_Home_Blogs_AL/MT_ROS_BTF_HOM_BLG_AL_300 -->
<div id='div-gpt-ad-1340608162797-2'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1340608162797-2'); });
</script>
</div>";
$my_settings['visual_revenue_reader_response_tracking_script'] = "";
$my_settings['visual_revenue_reader_response_tracking_script_for_not_singular'] = "";
$my_settings['not_found_heading'] = 'सापडत नाही.';
$my_settings['not_found_txt'] = 'या सेक्शनमध्ये काहीही सापडत नाही. शोध घ्या.';
$my_settings['search_placeholder_txt'] = 'तीन अक्षरं टाइप करा';
$my_settings['go_to_the_profile_of_txt'] = '';
$my_settings['go_to_txt'] = '';
$my_settings['share_link_fb_txt'] = 'फेसबुकवर लिंक शेअर करा';
$my_settings['share_link_twitter_txt'] = 'ट्विटवर लिंक शेअर करा';
$my_settings['share_link_linkedin_txt'] = 'लिंक्डइनवर लिंक शेअर करा';
$my_settings['mail_link_txt'] = 'लिंक मेल करा';
$my_settings['read_complete_article_here_txt'] = 'इथे वाचा संपूर्ण लेख';
$my_settings['featured_txt'] = 'वैशिष्ट्यपूर्ण';
$my_settings['disclaimer_txt'] = 'डिस्क्लेमर';
$my_settings['disclaimer_on_txt'] = " हा लेख 'टाइम्स ऑफ इंडिया'च्या छापील आवृत्तीत संपादकीय लेख म्हणून प्रकाशित झाला.";
$my_settings['disclaimer_off_txt'] = 'उपरोक्त लेखातील मते लेखकाची व्यक्तिगत मते आहेत.';
$my_settings['most discussed_txt'] = 'सर्वाधिक चर्चित';
$my_settings['more_txt'] = 'आणखी';
$my_settings['less_txt'] = 'याहून कमी';
//$my_settings['login_txt'] = '';
//$my_settings['logout_txt'] = '';
$my_settings['login_txt'] = 'Log In';
$my_settings['logout_txt'] = 'Log Out';
$my_settings['view_all_posts_in_txt'] = '';
$my_settings['home_txt'] = 'होम';
$my_settings['blogs_txt'] = 'ब्लॉग्स';
$my_settings['search_results_for_txt'] = '…च्यासाठी सर्च करा.';
$my_settings['most_read_txt'] = 'सर्वाधिक वाचलेले';
$my_settings['popular_tags_txt'] = 'प्रसिद्ध टॅग्स';
$my_settings['recently_joined_authors_txt'] = 'नवे लेखक';
$my_settings['like_us_txt'] = 'लाइक करा';
$my_settings['author_txt'] = ' लेखक';
$my_settings['popular_from_author_txt'] = 'लेखकाचे प्रसिद्ध लेख';
$my_settings['search_authors_by_name_txt'] = 'नाव टाइप करून लेखक शोधा';
$my_settings['search_txt'] = 'शोधा';
$my_settings['back_to_authors_page_txt'] = 'लेखकाविषयी पानावर जा';
$my_settings['no_authors_found_txt'] = 'हे लेखक यादीत नाहीत.';
$my_settings['further_commenting_is_disabled_txt'] = 'यापेक्षा अधिक प्रतिक्रिया स्वीकारली जाणार नाही';
$my_settings['comments_on_this_post_are_closed_now_txt'] = 'या पोस्टवर प्रतिक्रिया बंद करण्यात आल्या आहेत.';
$my_settings['add_your_comment_here_txt'] = 'आपल्या प्रतिक्रिया इथे कळवा';
$my_settings['characters_remaining_txt'] = 'अक्षरे आणखी बसतील';
$my_settings['share_on_fb_txt'] = 'फेसबुकवर शेअर करा';
$my_settings['share_on_twitter_txt'] = 'ट्विटवर शेअर करा';
$my_settings['sort_by_txt'] = 'निवडा';
$my_settings['newest_txt'] = 'अगदी ताजे';
$my_settings['oldest_txt'] = 'सर्वात जुने';
$my_settings['discussed_txt'] = 'चर्चित';
$my_settings['up_voted_txt'] = 'अप मते';
$my_settings['down_voted_txt'] = 'डाउन मते';
$my_settings['be_the_first_one_to_review_txt'] = 'समीक्षक होण्याचा पहिला मान मिळवा.';
$my_settings['more_points_needed_to_reach_next_level_txt'] = 'पुढील टप्प्यावर जाण्यासाठी आणखी पॉइंट हवेत. ';
$my_settings['know_more_about_times_points_txt'] = 'टाइम्स पॉइंटविषयी अधिक माहिती घ्या.';
$my_settings['know_more_about_times_points_link'] = 'http://www.timespoints.com/about/7341332417312980992';
$my_settings['badges_earned_txt'] = 'पदके मिळाली.';
$my_settings['just_now_txt'] = 'नुकतेच';
$my_settings['follow_txt'] = 'फॉलो करा';
$my_settings['reply_txt'] = 'उत्तर द्या';
$my_settings['flag_txt'] = 'फ्लॅग';
$my_settings['up_vote_txt'] = 'अप मत';
$my_settings['down_vote_txt'] = 'डाउन मत';
$my_settings['mark_as_offensive_txt'] = 'आक्षेपार्ह म्हणून नोंद करा';
$my_settings['find_this_comment_offensive_txt'] = 'ही प्रतिक्रिया आक्षेपार्ह वाटते का?';
$my_settings['reason_submitted_to_admin_txt'] = 'तुमचं म्हणणं अॅडमिनपर्यंत पोहोचवण्यात आलं आहे.';
$my_settings['choose_reason_txt'] = 'खालीलपैकी एक कारण निवडा आणि सबमिट बटण दाबा. त्यामुळं आमच्या नियंत्रकांना सूचना मिळेल व कारवाई करता येईल.';
$my_settings['reason_for_reporting_txt'] = 'तक्रारीचं कारण';
$my_settings['foul_language_txt'] = 'गलिच्छ भाषा';
$my_settings['defamatory_txt'] = 'बदनामीकारक';
$my_settings['inciting_hatred_against_certain_community_txt'] = 'विशिष्ट समाजाविरोधात द्वेष पसरवणारी भाषा';
$my_settings['out_of_context_spam_txt'] = 'असंबंद्ध/अनाठायी/अनाकलनीय';
$my_settings['others_txt'] = 'इतर';
$my_settings['report_this_txt'] = 'रिपोर्ट करा!';
$my_settings['close_txt'] = 'बंद करा';
$my_settings['already_marked_as_offensive'] = 'आधीच आक्षेपार्ह म्हणून नोंद केलेली आहे';
$my_settings['flagged_txt'] = 'फ्लॅग';
$my_settings['blogauthor_link'] = 'http://author-blogs-maharashtratimes.indiatimes.com/';
$my_settings['old_blogauthor_link'] = 'http://author.blogs.maharashtratimes.indiatimes.com/';
$my_settings['blog_link'] = 'https://blogs.maharashtratimes.indiatimes.com/';
$my_settings['common_cookie_domain'] = 'indiatimes.com';
$my_settings['quill_lang'] = 'marathi';
$my_settings['quill_link_1'] = 'मराठीत लिहा (इनस्क्रिप्ट)';
$my_settings['quill_link_2'] = 'मराठीत लिहा (इंग्रजी अक्षरांमध्ये)';
$my_settings['quill_link_3'] = 'Write in English';
$my_settings['quill_link_4'] = 'व्हर्च्युअल की-बोर्ड';
$my_settings['footer'] = '<div class="container footercontainer">

  <div class="insideLinks">

    <h2>एक नजर बातम्यांवर</h2>

    <ul>

      <li><a href="http://maharashtratimes.indiatimes.com/maharashtra/articlelistmt/2429066.cms">महाराष्ट्र</a></li>

      <li class="current"><a href="http://maharashtratimes.indiatimes.com/nation/articlelist/2429064.cms">देश</a></li>

      <li><a href="http://maharashtratimes.indiatimes.com/international/articlelist/2499171.cms">विदेश</a></li>

      <li><a href="http://maharashtratimes.indiatimes.com/global-maharashtra/articlelist/2429053.cms">ग्लोबल महाराष्ट्र</a></li>

      <li><a href="http://maharashtratimes.indiatimes.com/business/articlelist/2429061.cms">अर्थ</a></li>

      <li><a href="http://maharashtratimes.indiatimes.com/sports/articlelist/2429056.cms">क्रीडा</a></li>

      <li><a href="http://maharashtratimes.indiatimes.com/edit/editorial/articlelistmt/2429054.cms">संपादकीय</a></li>

      <li><a href="http://maharashtratimes.indiatimes.com/cinemagic/movies/19359255.cms">सिनेमॅजिक</a></li>

      <li><a href="http://maharashtratimes.indiatimes.com/career/articlelist/2499216.cms">प्रगती फास्ट</a></li>

      <li><a href="http://maharashtratimes.indiatimes.com/lifestyle/articlelistmt/2429025.cms">लाइफस्टाइल</a></li>

      <li><a href="http://maharashtratimes.indiatimes.com/infotech/articlelistmt/2499221.cms">इन्फोटेक</a></li>

      <li><a href="http://maharashtratimes.indiatimes.com/bhavishya/todays-horoscope/articlelist/2429326.cms">भविष्य</a></li>

      <li><a href="http://maharashtratimes.indiatimes.com/jokes/articlelist/2428942.cms">हसा लेको</a></li>

      <li><a href="http://photogallery.maharashtratimes.indiatimes.com">फोटोगॅलरी</a></li>

      <li><a href="http://maharashtratimes.indiatimes.com/video/videolist/5314584.cms">व्हिडिओ</a></li>

    </ul>

  </div>

 <div class="newsletter">

    <div class="fottershareLinks">

      <h4>नियमित कनेक्टेड राहा<br>

        <strong>महाराष्ट्र टाइम्स </strong>अॅपसोबत</h4>

      <div class="fotterApplinks"><a class="ios" target="_blank" href="https://itunes.apple.com/us/app/maharashtra-times/id674365887?ls=1&amp;mt=8"></a><a class="andriod" target="_blank" href="https://play.google.com/store/apps/details?id=com.mt.reader&amp;hl=en"></a></div>

    </div>

  </div>

  <div class="timesotherLinks">

    <h2>आमच्या इतर साइट्स</h2>

    <a pg="fottgwslnk1" target="_blank" href="http://timesofindia.indiatimes.com/">Times of India</a>| <a pg="fottgwslnk2" href="http://economictimes.indiatimes.com/" target="_blank">Economic Times</a> | <a pg="fottngslnk4" target="_blank" href="http://navbharattimes.indiatimes.com/">Hindi News</a> | <a pg="fottgwslnk6" target="_blank" href="http://eisamay.indiatimes.com/">Bangla News</a> | <a pg="fottgwslnk7" target="_blank" href="http://vijaykarnataka.indiatimes.com/">Kannada News</a>| <a pg="fottgwslnk8" target="_blank" href="http://navgujaratsamay.indiatimes.com/">Gujarati News</a> | <a pg="fottgwslnk9" target="_blank" href="http://tamil.samayam.com/">Tamil News</a> | <a pg="fottgwslnk10" target="_blank" href="http://telugu.samayam.com/">Telugu News</a> | <a pg="fottgwslnk11" target="_blank" href="http://malayalam.samayam.com/">Malayalam News</a> | <a pg="fottgwslnk12" target="_blank" href="http://www.businessinsider.in/">Business Insider</a>| <a pg="fottgwslnk13" target="_blank" href="http://zoomtv.indiatimes.com/">ZoomTv</a> | <a pg="fottgwslnk16" target="_blank" href="http://boxtv.com/">BoxTV</a>| <a pg="fottgwslnk17" target="_blank" href="http://www.gaana.com/">Gaana</a> | <a pg="fottgwslnk18" target="_blank" href="http://shopping.indiatimes.com/">Shopping</a> | <a pg="fottgwslnk19" target="_blank" href="http://www.idiva.com/">IDiva</a> | <a pg="fottgwslnk21" href="http://www.simplymarry.com/" target="_blank">Matrimonial</a><span class="footfbLike">महाराष्ट्र टाइम्स ऑन फेसबुक</span>

    <div data-share="false" data-show-faces="false" data-action="like" data-layout="button_count" data-href="https://www.facebook.com/maharashtratimesonline" style="width:310px;height:20px;overflow:hidden;margin-bottom: 10px;" class="fb-like"></div>

  </div>

  <div class="seoLinks">

    <ul>

      <li><a href="http://maharashtratimes.indiatimes.com/maharashtra/pune/articlelist/2429654.cms" target="_blank" style="border-left:none !important"> Pune News </a></li>

      <li><a href=" http://maharashtratimes.indiatimes.com/maharashtra/nagpur-vidarbha/articlelist/2429651.cms " target="_blank"> Nagpur News </a></li>

      <li><a href="http://maharashtratimes.indiatimes.com/maharashtra/nashik/articlelist/2429652.cms" target="_blank">Nashik News</a></li>

      <li><a href="http://maharashtratimes.indiatimes.com/maharashtra/kolhapur/articlelist/16090634.cms" target="_blank">Kolhapur News</a></li>

      <li><a href="http://maharashtratimes.indiatimes.com/ " target="_blank">Marathi News</a> </li>

      <li> <a href="https://play.google.com/store/apps/details?id=com.mt.reader" target="_blank">Marathi News Android APP </a> </li>

      <li> <a href="http://maharashtratimes.indiatimes.com/jokes/articlelist/2428942.cms" target="_blank">Marathi Jokes</a> </li>

      <li> <a href="http://maharashtratimes.indiatimes.com/bhavishya/todays-horoscope/articlelist/2429326.cms" target="_blank">Horoscope in Marathi</a> </li>

      <li> <a style="border-right:none !important" href="http://maharashtratimes.indiatimes.com/cinemagic/movies/19359255.cms" target="_blank">Marathi movies</a> </li>

    </ul>

  </div>

  <div class="timesLinks"><a pg="fotlnk1" href="http://www.timesinternet.in/" target="_blank">About Us</a>&nbsp;|&nbsp; <a pg="fotlnk2" href="http://advertise.indiatimes.com/" target="_blank">Advertise with Us</a>&nbsp;|&nbsp; <a pg="fotlnk3" href="http://maharashtratimes.indiatimes.com/termsandcondition.cms" target="_blank">Terms of Use and Grievance Redressal Policy</a>&nbsp;|&nbsp; <a pg="fotlnk4" href="http://maharashtratimes.indiatimes.com/privacypolicy.cms" target="_blank">Privacy Policy</a>&nbsp;|&nbsp; <span class="feedbackeu"><a pg="fotlnk5" href="mailto:grievance.mt@timesinternet.in" target="_blank">Feedback</a>&nbsp;|&nbsp;</span> <a pg="fotlnk6" href="http://maharashtratimes.indiatimes.com/sitemap.cms" target="_blank">Sitemap</a></div><div class="copyText">Copyright &copy;&nbsp; '.date('Y').' &nbsp;Bennett Coleman &amp; Co. Ltd. All rights reserved. For reprint rights: <a pg="fotlnk7" href="http://timescontent.com/" target="_blank">Times Syndication Service</a> </div></div>';
$my_settings['footer_css'] = '<style type="text/css">
.footercontainer{background:#333333; clear:both; overflow:hidden; padding:10px 0;}
.footercontainer a{color:#FDFDFD;}
.footercontainer h2{ color:#fff; font-size:20px; line-height:30px; padding:0 0 10px 2px;font-family: Arial, Helvetica, sans-serif;}

.insideLinks{ width:36%; padding-left:2%; float:left;}
.insideLinks ul{ list-style:none; display: inline-block; padding: 0; margin: 0}
.insideLinks ul li{ float:left; margin:0 10px 0 0; width:90px; }
.insideLinks ul li a{ font-family:Arial, Helvetica, sans-serif;color:#B5B3B4;font-size:14px;font-weight:bold;line-height:25px;}

.newsletter{width:32%; padding:0 2%; float:left;}
.newsletterpost {background:#CCCCCC; padding:14px; margin:10px 0 10px 0;float:left; width:300px;}

.newsletterpost input[type="text"]{background:#fff; border:1px #B7B7B7 solid; color:#333;height:21px; width:205px; font-size:14px;font-weight:bold;color:#999;float:left; padding:2px 10px;}
.newsletterpost input[type="submit"]{ background:#f5cc10; color:#000; padding:0 5px; *padding:0 4px; height:26px; cursor:pointer; border:none;font-weight:bold;float:left;}
.fottershareLinks span{ font-size:14px; color:#fff; width:100%; float:left; margin-bottom:10px;}
.fottershareLinks img{ vertical-align:middle;margin-top:5px;}
.fottershareLinks a{margin: 0 2px;}

.fotterApplinks{padding: 0 0 15px 0;}
.fotterApplinks a{padding: 0 10px 0 0; background-image:url(https://navbharattimes.indiatimes.com/photo/42711833.cms); float:left; width:47px; height:65px;background-repeat: no-repeat;}
.fotterApplinks a.andriod{background-position:0 4px}
.fotterApplinks a.andriod:hover{background-position:0 -68px}

.fotterApplinks a.ios{background-position: 0 -144px;}
.fotterApplinks a.ios:hover{background-position:0 -219px}

.fotterApplinks a.java{background-position:0 -296px;}
.fotterApplinks a.java:hover{background-position:0 -378px;}

.fotterApplinks a.win{background-position:0 -447px}
.fotterApplinks a.win:hover{background-position:0 -526px}

.fottershareLinks{clear: both;}
.fottershareLinks h4{color:#fff; font-size:16px;line-height: 23px;}
.fottershareLinks strong{color:#53a3e4; font-weight:normal}

.footfbLike{display: block;margin: 25px 0 12px 0;font-size: 14px;color: #fff;}

.timesotherLinks{width:32%; float:left; padding-right:2%; color:#CCCCCC;font-size:12px;}
.timesotherLinks a{ font-size:11px; padding:0 1px; color:#CCCCCC; line-height:18px;}

.timesLinks{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:10px 0;}
.timesLinks a {color:#FDFDFD; font-family:Arial;}

.copyText{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:0 35px;}
.copyText a {color:#FDFDFD; font-family:Arial;}

.seoLinks{clear:both; margin:10px 0; background:#585757; overflow:hidden;padding-left: 11px;}
.seoLinks ul{list-style:none; padding:0;}
.seoLinks ul li{float:left;}
.seoLinks ul li a{font-family:arial;padding:0 7px;color:#fff;font-size:12px; line-height:32px; display:block; float:left; border-left:#4a4a4a 1px solid;  border-right:#696969 1px solid;}
@media (max-width: 767px){.insideLinks,.newsletter,.timesotherLinks,.seoLinks,.timesLinks{display: none;}}
</style>';
$my_settings['ctn_homepage'] = '<!-- MT_Blog_HP_CTN_NAT,position=1--><div id="div-clmb-ctn-208857-1" data-slot="208857" data-position="1" data-section="0"  class="colombia media "></div>';
$my_settings['ctn_homepage_rhs'] = '<!-- MT_Blog_HP_RHS_RCMW_CTN_NAT,position=1--><div id="div-clmb-ctn-208856-1" data-slot="208856" data-position="1" data-section="0"  class="colombia panel " data-paidad-head="FROM WEB"></div>';
$my_settings['ctn_article_list'] = '<!-- MT_Blog_ROS_AL_CTN_NAT,position=1--><div id="div-clmb-ctn-208854-1" data-slot="208854" data-position="1" data-section="0"  class="colombia media article "></div>';
$my_settings['ctn_article_list_rhs'] = '<!-- MT_Blog_ROS_AL_RHS_RCMW_CTN_NAT,position=1--><div id="div-clmb-ctn-208855-1" data-slot="208855" data-position="1" data-section="0"  class="colombia panel " data-paidad-head="FROM WEB"></div>';
$my_settings['ctn_article_show_rhs'] = '<!-- MT_Blog_ROS_AS_RHS_RCMW_CTN_NAT,position=1--><div id="div-clmb-ctn-208851-1" data-slot="208851" data-position="1" data-section="0"  class="colombia panel " data-paidad-head="FROM WEB"></div>';
$my_settings['ctn_article_show_end_article_1'] = '<!-- MT_Blog_ROS_AS_EOA_RCMW_CTN_NAT,position=1--><div id="div-clmb-ctn-208852-1" data-slot="208852" data-position="1" data-section="0"  class="colombia " data-paidad-head="FROM WEB" data-organicad-head="FROM MAHARASHTRA TIMES" data-clear-float="1"></div>';
$my_settings['ctn_article_show_end_article_2'] = '<!-- MT_Blog_ROS_AS_EOA1_RCMW_CTN_NAT,position=1--><div id="div-clmb-ctn-208850-1" data-slot="208850" data-position="1" data-section="0"   class="colombia " data-paidad-head="FROM WEB" data-organicad-head="FROM MAHARASHTRA TIMES" data-clear-float="1" data-separate-container="1"></div>';

// esi ads setting

$my_settings['esi_status'] = 'NONE';// TEST, LIVE, NONE

if(isMobile()){
	                             
	$esiHeaderStr = '<!--esi 
           <esi:eval src="/fetch_native_content/?fpc=$url_encode($(HTTP_COOKIE{\'_col_uuid\'}))&ab=$(HTTP_COOKIE{\'mtm_ce\'})&id=$url_encode(\'##ESIIDS##\')&_t=4&_u=$url_encode($(HTTP_HOST)+$(REQUEST_PATH))&ua=$url_encode($(HTTP_USER_AGENT))&ip=$(REMOTE_ADDR)&_v=0&dpv=1&r=$rand()" dca="esi"/> 
           -->';

        $esiHeaderStr .= <<<'ESISTR'
<!--esi           
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_uuid'}))">
                		        $add_header('Set-Cookie', $(native_content{'_col_uuid'}))
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_script'}))">
                		        $(native_content{'_col_script'})
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_ab_call'}))">
                		        $(native_content{'_col_ab_call'})
                            </esi:when>
                        </esi:choose>
                    -->
ESISTR;
$my_settings['esi_header'] = $esiHeaderStr;

	$my_settings['esi_homepage_ids'] = '310779~1~0';
	
	/*HP_CTN_NAT*/
	$my_settings['esi_homepage'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'310779~1~0\'}))">
								$(native_content{\'310779~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	/*HP_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_homepage_rhs'] = "";
	
	$my_settings['esi_article_list_ids'] = '310780~1~0';
	
	/*ROS_AL_CTN_NAT*/
	$my_settings['esi_article_list'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'310780~1~0\'}))">
								$(native_content{\'310780~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*ROS_AL_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_list_rhs'] = "";
	
	$my_settings['esi_article_show_ids'] = '310781~1~0';
	
	/*ROS_AS_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_rhs'] = "";
	
	/*ROS_AS_EOA_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_1'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'310781~1~0\'}))">
								$(native_content{\'310781~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*ROS_AS_EOA1_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_2'] = ""; 
	
	/*Mweb_AS_BLY_VDO_CTN_NAT*/                               
	$my_settings['ctn_article_show_mid_article_video'] = '';
	
}else{

	$esiHeaderStr = '<!--esi 
           <esi:eval src="/fetch_native_content/?fpc=$url_encode($(HTTP_COOKIE{\'_col_uuid\'}))&ab=$(HTTP_COOKIE{\'mt_ce\'})&id=$url_encode(\'##ESIIDS##\')&_t=4&_u=$url_encode($(HTTP_HOST)+$(REQUEST_PATH))&ua=$url_encode($(HTTP_USER_AGENT))&ip=$(REMOTE_ADDR)&_v=0&dpv=1&r=$rand()" dca="esi"/> 
           -->';

        $esiHeaderStr .= <<<'ESISTR'
<!--esi           
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_uuid'}))">
                		        $add_header('Set-Cookie', $(native_content{'_col_uuid'}))
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_script'}))">
                		        $(native_content{'_col_script'})
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_ab_call'}))">
                		        $(native_content{'_col_ab_call'})
                            </esi:when>
                        </esi:choose>
                    -->
ESISTR;
$my_settings['esi_header'] = $esiHeaderStr;


	$my_settings['esi_homepage_ids'] = '208857~1~0,208856~1~0';
	
	/*HP_CTN_NAT*/
	$my_settings['esi_homepage'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208857~1~0\'}))">
								$(native_content{\'208857~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*HP_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_homepage_rhs'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208856~1~0\'}))">
								$(native_content{\'208856~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	$my_settings['esi_article_list_ids'] = '208854~1~0,208855~1~0';
	
	/*ROS_AL_CTN_NAT*/
	$my_settings['esi_article_list'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208854~1~0\'}))">
								$(native_content{\'208854~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*ROS_AL_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_list_rhs'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208855~1~0\'}))">
								$(native_content{\'208855~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	$my_settings['esi_article_show_ids'] = '208851~1~0,208852~1~0';
	
	/*ROS_AS_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_rhs'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208851~1~0\'}))">
								$(native_content{\'208851~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	/*ROS_AS_EOA_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_1'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208852~1~0\'}))">
								$(native_content{\'208852~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	/*ROS_AS_EOA1_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_2'] = '';          
	
	/*AS_BLY_VDO_CTN_NAT*/  
	$my_settings['ctn_article_show_mid_article_video'] = '';                                                
}
