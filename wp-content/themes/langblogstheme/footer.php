<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<?php global $my_settings; ?>
<?php if(isset($_GET['frmapp']) && $_GET['frmapp'] == 'yes'){?>
<?php }else{ ?>
<?php echo $my_settings['footer_css'];?>
<?php echo $my_settings['footer'];?>
<?php } ?>
<script type="text/javascript">

    TimesGDPR.common.consentModule.gdprCallback(function(){
			if(window._euuser){
			//alert('EU');		
			enablegdpr();
			if(TimesGDPR.common.consentModule.isConsentGiven()){
				//_setCookie();
			}
			//$('.cmtbtn-wrapper').hide();
			}
	});
</script>
<?php wp_footer(); ?>
<?php if(!isInitAd()) { ?>
<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<script src="//platform.linkedin.com/in.js" type="text/javascript">
 lang: en_US
</script>
<?php } ?>
<?php echo $my_settings['comscore_tag'];?>
<?php 
if(is_singular()){
?>
<script type="text/javascript">
date = date.replace(':','.');
//date = date.replace(' AM','AM');
//date = date.replace(' PM','PM');

var _page_config, _ibeat_config ={};
        
function setPageConfig(config){
	_page_config = {
	  host: '<?php echo $my_settings['ibeat_host'];?>', 
	  key: '<?php echo $my_settings['ibeat_key'];?>',
	  domain: '<?php echo $my_settings['ibeat_domain'];?>',
	  channel: config._iBeat_channel,
	  action : config._iBeat_action,
	  articleId: config._iBeat_articleid,
	  contentType: 21,
	  location : 1,
	  url : config._iBeat_url,
	  cat : config._iBeat_cat,
	  subcat: config._iBeat_subcat,
	  contenttag: config._iBeat_tag,
	  catIds   : config._iBeat_catIds,
	  articledt: config._iBeat_articledt,
	  author: config._iBeat_author,
	  position: config._iBeat_position
	};
}
function trim(str){return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');}


 _ibeat_articleid = a;
 _ibeat_config[_ibeat_articleid] = {};
 _ibeat_config[_ibeat_articleid]['_iBeat_articleid']=_ibeat_articleid;
 _ibeat_config[_ibeat_articleid]['_iBeat_tag']=tags;
 _ibeat_config[_ibeat_articleid]['_iBeat_catIds']=catIds;
 _ibeat_config[_ibeat_articleid]['_iBeat_url'] = window.location.href;
 _ibeat_config[_ibeat_articleid]['_iBeat_type']=1;
 _ibeat_config[_ibeat_articleid]['_iBeat_action']=1;
 _ibeat_config[_ibeat_articleid]['_iBeat_cat']= trim(cat);
 _ibeat_config[_ibeat_articleid]['_iBeat_subcat']='';
 _ibeat_config[_ibeat_articleid]['_iBeat_articledt']=date;
 _ibeat_config[_ibeat_articleid]['_iBeat_author']=n;
 _ibeat_config[_ibeat_articleid]['_iBeat_channel'] ="<?php echo $my_settings['ibeat_channel'];?>";
 _ibeat_config[_ibeat_articleid]['_iBeat_position'] = " ";


if(location.hostname == '<?php echo $my_settings['ibeat_host'];?>' && !TimesGDPR.common.consentModule.isEUuser()){
	setPageConfig(_ibeat_config[_ibeat_articleid]);
	_page_config['position'] = Get_Ckie('PlRef') ? Get_Ckie('PlRef') : '';
	var iBeatTimer = setTimeout(function(){loadIbeatJS();},20000);
	$(window).load(function(){loadIbeatJS();clearTimeout(iBeatTimer);});
}

function loadIbeatJS(){
	try{
		if(typeof iBeatPgTrend == 'undefined'){
			var ele = document.createElement('script');
			ele.setAttribute('language', 'javascript');
			ele.setAttribute('type', 'text/javascript');
			ele.setAttribute('src',"https://ibeat.indiatimes.com/js/pgtrackingV9.js");
			document.body.appendChild(ele);
		}
	}catch(e){}
}
function Get_Ckie(name){
	var value = "; " + document.cookie;
	var parts = value.split("; " + name + "=");
	if (parts.length == 2) return parts.pop().split(";").shift();
}

jQuery(document).ready(function(){
	//var i = 1;
	/*jQuery('.chooseLang a').on('click',function(event){  
			jQuery('.QuillAbstable').remove();
	});
	jQuery('body').on('click',function(){
		jQuery('.QuillAbstable').remove();
	});*/
	
	/*jQuery('body').on('mousemove',function(){
		if(jQuery('[data-action="comment-reply"]').length > 0 && jQuery('.QuillAbstable').length > 0 ){
			if(jQuery('.QuillKeyboardContainer').length >= i){
				i++;
				jQuery('.chooseLang a').on('click',function(event){  
					jQuery('.QuillAbstable').remove();
				});
			}
		}
	});*/
});
</script>

<?php
}
?>
<script>
if(!TimesGDPR.common.consentModule.isEUuser()){	
	var tilDmpWebSrc = '<?php echo $my_settings['footer_dmp_tag'];?>';
	if(typeof( tilDmpWebSrc ) != 'undefined' ){
            var tilDmpSrc  = tilDmpWebSrc;
            var tilDmpScriptTag = document.createElement('script');
            tilDmpScriptTag.setAttribute('src', tilDmpSrc);
            document.body.appendChild(tilDmpScriptTag);
        }


}
</script>
<?php 

if(!isInitAd()) { echo $my_settings['footer_google_tag']; } 

?>
</body>
</html>
