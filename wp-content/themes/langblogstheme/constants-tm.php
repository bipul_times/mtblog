<?php 

define('Z_IMAGE_PLACEHOLDER', get_template_directory_uri()."/images/placeholder.png");
define('FB_PLACEHOLDER', get_template_directory_uri()."/images/blog-tm-facebook.png");
define('Z_AUTHOR_PLACEHOLDER', get_template_directory_uri()."/images/50.jpg");
define('RECO_URL','http://reco.indiatimes.com/Reco/RecoIndexer?eventType=update&hostid=1b&contentType=blog&msid=');
define('COMMENTS_URL','http://www.tamilblogcmtapi.indiatimes.com/');
define('RCHID', '2147478028');
define('MYT_COMMENTS_API_KEY', 'TAMB');
define('MYTIMES_URL','http://myt.indiatimes.com/');

global $my_settings;
$my_settings['site_lang'] = 'tm';
$my_settings['og_locale'] = 'ta_IN';
$my_settings['google_site_verification'] = 'zU5qA2HPdN2PFPyZd70q25_5NHXI8AE8AbqWNwXsoFA';
$my_settings['fblikeurl'] = 'https://www.facebook.com/TamilSamayam';
$my_settings['favicon'] = 'https://tamil.samayam.com/icons/tamil.ico';
$my_settings['ga'] = "<script>

TimesGDPR.common.consentModule.gdprCallback(function(data) {
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-29031733-11', 'auto');
  var userCookie = document.cookie.match(/(?:\s)?ssoid=(\w+);?/);
  if(!!(userCookie)) {
	ga('set', 'userId', userCookie[1]);
  }
  if( TimesGDPR.common.consentModule.isEUuser() ){
  ga('set', 'anonymizeIp', true); 
  }
  ga('require', 'displayfeatures');
  ga('send', 'pageview');
  });
  </script>";
$my_settings['fbappid'] = '980029552049573';
$my_settings['channel_url_part'] = 'channel=tm';
$my_settings['main_site_url'] = 'https://tamil.samayam.com';
$my_settings['main_site_txt'] = 'Tamil Samayam Blog';
$my_settings['fb_url'] = 'https://www.facebook.com/TamilSamayam';
$my_settings['twitter_url'] = 'https://twitter.com/TamilSamayam';
$my_settings['twitter_handle'] = 'TamilSamayam';
$my_settings['google_url'] = 'https://plus.google.com/b/116030764148407709961/116030764148407709961/about?hl=en&service=PLUS';
$my_settings['rss_url'] = 'https://tamil.samayam.com/rssfeedsdefault.cms';
$my_settings['logo_title'] = 'Tamil Samayam';
$my_settings['logo_url'] = 'https://tamil.samayam.com/photo/48055720.cms';
$my_settings['footer_logo_txt'] = 'சமயம்';
$my_settings['comscore_tag'] = '<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  var objComScore = { c1: "2", c2: "6036484" };
  TimesGDPR.common.consentModule.gdprCallback(function(data) {
    if( TimesGDPR.common.consentModule.isEUuser() ){
      objComScore["cs_ucfr"] = 0;
    }
     _comscore.push(objComScore); 
  (function() {
	var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
	s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
	el.parentNode.insertBefore(s, el);
  })();
  });
</script>
<noscript><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6036484&amp;cv=2.0&amp;cj=1"></noscript>
<!-- End comScore Tag -->';
$my_settings['ibeat_channel'] = 'TamilBlog';
$my_settings['ibeat_host'] = 'blogs.tamil.samayam.com';
$my_settings['ibeat_key'] = '2c1a39ecd8c9bc3cd6a575c75901b20';
$my_settings['ibeat_domain'] = 'tamil.samayam.com';
$my_settings['footer_google_tag'] = "<!-- Tamil/TML_Home/TML_Home_Home/TML_HP_OP_Innov1 -->
<div id='div-gpt-ad-1436513313820-4' style='width:1px; height:1px;'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1436513313820-4'); });
</script>
</div>

<!-- Tamil/TML_Home/TML_Home_Home/TML_HP_OP_Inter -->
<div id='div-gpt-ad-1436513313820-5' style='width:1px; height:1px;'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1436513313820-5'); });
</script>
</div>

<!-- Tamil/TML_Home/TML_Home_Home/TML_HP_OP_Pop -->
<div id='div-gpt-ad-1436513313820-6' style='width:1px; height:1px;'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1436513313820-6'); });
</script>
</div>

<!-- Tamil/TML_Home/TML_Home_Home/TML_HP_OP_Shosh -->
<div id='div-gpt-ad-1436513313820-7' style='width:1px; height:1px;'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1436513313820-7'); });
</script>
</div>";
$my_settings['google_tag_js_head'] = "<script type='text/javascript'>
	var googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];
	(function() {
	var gads = document.createElement('script');
	gads.async = true;
	gads.type = 'text/javascript';
	var useSSL = 'https:' == document.location.protocol;
	gads.src = (useSSL ? 'https:' : 'http:') + 
	'//www.googletagservices.com/tag/js/gpt.js';
	var node = document.getElementsByTagName('script')[0];
	node.parentNode.insertBefore(gads, node);
	})();
	</script>
	
	<script type='text/javascript'>
	googletag.cmd.push(function() {

	<!-- Audience Segment Targeting -->
	var _auds = new Array();
	if(typeof(_ccaud)!='undefined') {
	for(var i=0;i<_ccaud.Profile.Audiences.Audience.length;i++)
	if(i<200)
	_auds.push(_ccaud.Profile.Audiences.Audience[i].abbr);
	}
	<!-- End Audience Segment Targeting -->
	
	<!-- Contextual Targeting -->
	var _HDL = '';
	var _ARC1 = '';
	var _Hyp1 = '';
	var _article = '';
	var _tval = function(v) {
	if(typeof(v)=='undefined') return '';
	if(v.length>100) return v.substr(0,100);
	return v;
	}
	<!-- End Contextual Targeting -->

googletag.defineSlot('/7176/Tamil/TML_Home/TML_Home_Home/TML_HP_ATF_300', [[300, 250], [300, 1050]], 'div-gpt-ad-1436513313820-0').addService(googletag.pubads());
googletag.defineSlot('/7176/Tamil/TML_Home/TML_Home_Home/TML_HP_BTF_300', [300, 250], 'div-gpt-ad-1436513313820-1').addService(googletag.pubads());
googletag.defineSlot('/7176/Tamil/TML_Home/TML_Home_Home/TML_HP_BTF_728', [728, 90], 'div-gpt-ad-1436513313820-2').addService(googletag.pubads());
googletag.defineSlot('/7176/Tamil/TML_Home/TML_Home_Home/TML_HP_MTF_300', [300, 250], 'div-gpt-ad-1436513313820-3').addService(googletag.pubads());
googletag.defineSlot('/7176/Tamil/TML_Home/TML_Home_Home/TML_HP_OP_Innov1', [1, 1], 'div-gpt-ad-1436513313820-4').addService(googletag.pubads());
googletag.defineSlot('/7176/Tamil/TML_Home/TML_Home_Home/TML_HP_OP_Inter', [1, 1], 'div-gpt-ad-1436513313820-5').addService(googletag.pubads());
googletag.defineSlot('/7176/Tamil/TML_Home/TML_Home_Home/TML_HP_OP_Pop', [1, 1], 'div-gpt-ad-1436513313820-6').addService(googletag.pubads());
googletag.defineSlot('/7176/Tamil/TML_Home/TML_Home_Home/TML_HP_OP_Shosh', [1, 1], 'div-gpt-ad-1436513313820-7').addService(googletag.pubads());
googletag.pubads().setTargeting('sg', _auds).setTargeting('HDL', _tval(_HDL)).setTargeting('ARC1', _tval(_ARC1)).setTargeting('Hyp1', _tval(_Hyp1)).setTargeting('article', _tval(_article));
googletag.pubads().enableSingleRequest();
googletag.pubads().collapseEmptyDivs();
TimesGDPR.common.consentModule.gdprCallback(function(data) {
	if( TimesGDPR.common.consentModule.isEUuser() ) {
		googletag.pubads().setRequestNonPersonalizedAds(1);
	}
	googletag.enableServices();
} );
});
</script>";
$my_settings['google_ad_top'] = "<!-- Tamil/TML_Home/TML_Home_Home/TML_HP_BTF_728 -->
<div id='div-gpt-ad-1436513313820-2' style='width:728px; height:90px;'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1436513313820-2'); });
</script>
</div>";
$my_settings['google_ad_right_1'] = "<!-- Tamil/TML_Home/TML_Home_Home/TML_HP_ATF_300 -->
<div id='div-gpt-ad-1436513313820-0'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1436513313820-0'); });
</script>
</div>";
$my_settings['google_ad_right_2'] = "<!-- Tamil/TML_Home/TML_Home_Home/TML_HP_BTF_300 -->
<div id='div-gpt-ad-1436513313820-1' style='width:300px; height:250px;'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1436513313820-1'); });
</script>
</div>";
$my_settings['visual_revenue_reader_response_tracking_script'] = "<!--Visual Revenue Reader Response Tracking Script (v6) -->
        <script type='text/javascript'>
            var _vrq = _vrq || [];
            _vrq.push(['id', 624]);
            _vrq.push(['automate', false]);
            _vrq.push(['track', function(){}]);
            (function(d, a){
                var s = d.createElement(a),
                x = d.getElementsByTagName(a)[0];
                s.async = true;
            s.src = 'http://a.visualrevenue.com/vrs.js';
            x.parentNode.insertBefore(s, x);
            })(document, 'script');
        </script>
        <!-- End of VR RR Tracking Script - All rights reserved -->";
$my_settings['visual_revenue_reader_response_tracking_script_for_not_singular'] = "<!--Visual Revenue Reader Response Tracking Script (v6) -->
        <script type='text/javascript'>
            var _vrq = _vrq || [];
            _vrq.push(['id', 624]);
            _vrq.push(['automate', true]);
            _vrq.push(['track', function(){}]);
            (function(d, a){
                var s = d.createElement(a),
                x = d.getElementsByTagName(a)[0];
                s.async = true;
            s.src = 'http://a.visualrevenue.com/vrs.js';
            x.parentNode.insertBefore(s, x);
            })(document, 'script');
        </script>
        <!-- End of VR RR Tracking Script - All rights reserved -->";
$my_settings['not_found_heading'] = ' கிடைக்கவில்லை';
$my_settings['not_found_txt'] = 'தேடல் தோல்வி... மீண்டும் முயற்சிக்கவும்...';
$my_settings['search_placeholder_txt'] = '3 எழுத்துக்களைப் பதிய';
$my_settings['go_to_the_profile_of_txt'] = '';
$my_settings['go_to_txt'] = '';
$my_settings['share_link_fb_txt'] = 'பேஸ்புக்கில் பகிர... ';
$my_settings['share_link_twitter_txt'] = 'ட்விட்டரில் பகிர... ';
$my_settings['share_link_linkedin_txt'] = 'LinkedIn-ல் பகிர ';
$my_settings['mail_link_txt'] = 'மின்னஞ்சல் இணைப்பு';
$my_settings['read_complete_article_here_txt'] = 'முழு கட்டுரை வாசிக்க ';
$my_settings['featured_txt'] = ' பிரத்யேக';
$my_settings['disclaimer_txt'] = 'மறுப்பு ';
$my_settings['disclaimer_on_txt'] = 'இது டைம்ஸ் ஆஃப் இந்தியா பத்திரிகையில் பதிவான கருத்து ';
$my_settings['disclaimer_off_txt'] = "மேலே கூறிய கருத்துக்கள் ஆசிரியரின் சொந்தக் கருத்து";
$my_settings['most discussed_txt'] = 'அதிகம் விவாதிக்கப்பட்டது';
$my_settings['more_txt'] = 'மேலும்';
$my_settings['less_txt'] = ' குறைவான';
//$my_settings['login_txt'] = 'உள்நுழைய ';
//$my_settings['logout_txt'] = 'வெளியேற ';
$my_settings['login_txt'] = 'Log In';
$my_settings['logout_txt'] = 'Log Out';
$my_settings['view_all_posts_in_txt'] = '';
$my_settings['home_txt'] = 'முகப்பு';
$my_settings['blogs_txt'] = 'ப்ளாக்ஸ்';
$my_settings['search_results_for_txt'] = 'தேடல் முடிவுகள்';
$my_settings['most_read_txt'] = 'அதிகம் வாசித்தவை';
$my_settings['popular_tags_txt'] = 'பிரபல டேக்ஸ்';
$my_settings['recently_joined_authors_txt'] = 'சமீபத்தில் இணைந்த எழுத்தாளர்கள்';
$my_settings['like_us_txt'] = 'எங்களை ஆதரிக்க';
$my_settings['author_txt'] = ' ஆசிரியர்';
$my_settings['popular_from_author_txt'] = 'பிரபலமான பதிவுகள் ஆசிரியர்';
$my_settings['search_authors_by_name_txt'] = 'பெயர் மூலம் ஆசிரியர்களைத் தேட';
$my_settings['search_txt'] = ' தேடல்';
$my_settings['back_to_authors_page_txt'] = 'மீண்டும் ஆசிரியர் பக்கத்திற்குச் செல்ல...';
$my_settings['no_authors_found_txt'] = 'ஆசிரியர்கள் கண்டறியப்படவில்லை ';
$my_settings['further_commenting_is_disabled_txt'] = 'மேலும் கருத்துக்கள் ஏற்கப்படாது ';
$my_settings['comments_on_this_post_are_closed_now_txt'] = 'இந்தப் பதிவு தொடர்பான கருத்துக்கள் முடிந்தது.';
$my_settings['add_your_comment_here_txt'] = 'உங்கள் கருத்துக்கு ';
$my_settings['characters_remaining_txt'] = 'மீதமுள்ள எழுத்துக்கள்';
$my_settings['share_on_fb_txt'] = 'பேஸ்புக்கில் பகிர';
$my_settings['share_on_twitter_txt'] = 'ட்விட்டரில் பகிர ';
$my_settings['sort_by_txt'] = 'வகைப்படுத்து ';
$my_settings['newest_txt'] = ' புதியது';
$my_settings['oldest_txt'] = 'பழமையான...';
$my_settings['discussed_txt'] = 'கலந்துரையாடப்பட்டது ';
$my_settings['up_voted_txt'] = 'ஓட்டுப் பதிவு';
$my_settings['down_voted_txt'] = 'ஓட்டு நிராகரிப்பு ';
$my_settings['be_the_first_one_to_review_txt'] = 'மறுபரிசீலனை செய்ய ';
$my_settings['more_points_needed_to_reach_next_level_txt'] = 'அடுத்த நிலையை அடைய இன்னும் அதிக புள்ளிகள் தேவை';
$my_settings['know_more_about_times_points_txt'] = 'டைம்ஸ் புள்ளிகள் பற்றி மேலும் அறிய';
$my_settings['know_more_about_times_points_link'] = 'http://www.timespoints.com/about/8606871469114261504';
$my_settings['badges_earned_txt'] = 'ஈட்டிய பேட்ஜ்கள் ';
$my_settings['just_now_txt'] = 'இப்பொழுது ';
$my_settings['follow_txt'] = 'பின்தொடர...';
$my_settings['reply_txt'] = ' பதில்';
$my_settings['flag_txt'] = ' கொடி';
$my_settings['up_vote_txt'] = 'ஓட்டுப் பதிவு ';
$my_settings['down_vote_txt'] = 'ஓட்டு நிராகரிப்பு';
$my_settings['mark_as_offensive_txt'] = 'குற்றம் என பதிய ';
$my_settings['find_this_comment_offensive_txt'] = 'இந்தக் கருத்தை குற்றம் என கண்டறிய';
$my_settings['reason_submitted_to_admin_txt'] = 'உங்களது காரணம் நிர்வாகத்திடம் சமர்ப்பிக்கப்பட்டது ';
$my_settings['choose_reason_txt'] = 'கீழே கொடுக்கப்பட்டுள்ள காரணத்தைக் கண்டறிந்து சமர்ப்பிக்கவும். நடவடிக்கை எடுக்க இது எங்களை எச்சரிக்கைப்படுத்தும் ';
$my_settings['reason_for_reporting_txt'] = 'அறிக்கைக்கான காரணம்';
$my_settings['foul_language_txt'] = 'தகாத வார்த்தைகள்';
$my_settings['defamatory_txt'] = 'அவதூறானது';
$my_settings['inciting_hatred_against_certain_community_txt'] = 'குறிப்பிட்ட சமூகத்திற்கு எதிராக வெறுப்பைத் தூண்டுதல் ';
$my_settings['out_of_context_spam_txt'] = 'சூழலுக்கு பொருந்தாதவை ';
$my_settings['others_txt'] = 'மற்றவை ';
$my_settings['report_this_txt'] = 'அறிக்கை சமர்ப்பிக்க';
$my_settings['close_txt'] = 'க்ளோஸ் ';
$my_settings['already_marked_as_offensive'] = 'குற்றம் என ஏற்கனவே குறிக்கப்பட்டுள்ளது';
$my_settings['flagged_txt'] = ' குறியிடப்பட்டது ';
$my_settings['blogauthor_link'] = 'http://blogauthors.tamil.samayam.com/';
$my_settings['old_blogauthor_link'] = 'http://blogauthors.tamil.samayam.com/';
$my_settings['blog_link'] = 'http://blogs.tamil.samayam.com/';
$my_settings['common_cookie_domain'] = 'samayam.com';
$my_settings['quill_lang'] = 'tamil';
$my_settings['quill_link_1'] = 'தமிழில் பதியவும் (INSCRIPT)';
$my_settings['quill_link_2'] = 'ஆங்கிலத்தில் பதியவும்(English characters)';
$my_settings['quill_link_3'] = 'Write in English';
$my_settings['quill_link_4'] = 'கீபோர்டு பயன்படுத்த';
$my_settings['footer'] = '<div class="container footercontainer"><div class="insideLinks"><h2>உங்களது பார்வைக்கு </h2><ul><li><a href="http://tamil.samayam.com/cinema/articlelist/48237837.cms" pg="fotkjlnk1" target="_blank">சினிமா </a></li><li><a href="http://tamil.samayam.com/social/articlelist/45939352.cms" target="_blank" pg="fotkjlnk2">சமூகம் </a></li><li><a href="http://tamil.samayam.com/news/articlelist/48237907.cms" target="_blank" pg="fotkjlnk10">செய்திகள்  </a></li><li><a href="http://tamil.samayam.com/lifestyle/articlelist/45939394.cms" pg="fotkjlnk3" target="_blank">லைஃப் ஸ்டைல்  </a></li><li><a href="http://tamil.samayam.com/sports/articlelist/47766652.cms" target="_blank" pg="fotkjlnk4">விளையாட்டு </a></li><li><a href="http://tamil.samayam.com/spiritual/articlelist/45939389.cms" pg="fotkjlnk5" target="_blank">ஆன்மிகம் </a></li><li><a href="http://tamil.samayam.com/jokes/articlelist/48213869.cms" target="_blank" pg="fotkjlnk6">ஜோக்ஸ் </a></li><li><a href="http://tamil.samayam.com/education/articlelist/48213890.cms" pg="fotkjlnk7" target="_blank">கல்வி </a></li><li><a href="http://tamil.samayam.com/video/videolist/47344128.cms" target="_blank" pg="fotkjlnk8">வீடியோ </a></li><li><a href="http://tamil.samayam.com/photogallery/photolist/47344662.cms" pg="fotkjlnk9" target="_blank">புகைப்படம் </a></li></ul></div><div class="newsletter"><div class="fottershareLinks"><h4>
						எப்பொழுதும் தமிழ் சமயம் App இணைப்பில் இருக்க 
					</h4><div class="fotterApplinks"><a href="https://play.google.com/store" target="_blank" class="andriod"></a></div></div></div><div class="timesotherLinks"><h2>எங்களது பிற இணையதளங்கள்</h2><a href="http://timesofindia.indiatimes.com/" target="_blank" pg="fottgwslnk1">Times of India</a>| <a target="_blank" href="http://economictimes.indiatimes.com/" pg="fottgwslnk2">Economic Times</a> | <a href="http://itimes.com/" target="_blank" pg="fottgwslnk16">iTimes</a>|<br><a href="http://vijaykarnataka.indiatimes.com/" target="_blank" pg="fottgwslnk4">Vijay Karnataka</a>| <a href="http://eisamay.indiatimes.com/" target="_blank" pg="fottgwslnk5">Ei Samay</a> | <a href="http://navgujaratsamay.indiatimes.com/" target="_blank" pg="fottngslnk5">Navgujarat Samay</a> | <a href="http://maharashtratimes.com/" target="_blank" pg="fottgwslnk6">महाराष्ट्र टाइम्स</a> |<a href="http://www.businessinsider.in/" target="_blank" pg="fottgwslnk7">Business Insider</a>| <a href="http://zoomtv.indiatimes.com/" target="_blank" pg="fottgwslnk8">ZoomTv</a> | <a href="http://boxtv.com/" target="_blank" pg="fottgwslnk11">BoxTV</a>| <a href="http://www.gaana.com/" target="_blank" pg="fottgwslnk12">Gaana</a> | <a href="http://shopping.indiatimes.com/" target="_blank" pg="fottgwslnk13">Shopping</a> | <a href="http://www.idiva.com/" target="_blank" pg="fottgwslnk14">IDiva</a> | <a href="http://www.astrospeak.com/" target="_blank" pg="fottgwslnk15">Astrology</a> |
				<a target="_blank" href="http://www.simplymarry.com/" pg="fottgwslnk17">Matrimonial</a><span class="footfbLike">பேஸ்புக்கில் தமிழ் சமயம்</span><iframe width="260px" height="35" frameborder="0" src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FTamilSamayam&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35&amp;colorscheme=dark" scrolling="no" allowtransparency="true"></iframe></div><div class="timesLinks"><a target="_blank" href="http://www.timesinternet.in/" pg="fotlnk1">About Us</a>&nbsp;|&nbsp;
				<a target="_blank" href="http://advertise.indiatimes.com/" pg="fotlnk2">Advertise with Us</a>&nbsp;|&nbsp;
				<a target="_blank" href="http://www.indiatimes.com/termsandcondition" pg="fotlnk3">Terms of Use and Grievance Redressal Policy</a>&nbsp;|&nbsp;
				<a target="_blank" href="http://www.indiatimes.com/privacypolicy" pg="fotlnk4">Privacy Policy</a>&nbsp;|&nbsp;
						<a target="_blank" href="http://tamil.samayam.com/sitemap.cms" pg="fotlnk6">Sitemap</a></div><div class="copyText">Copyright &copy;&nbsp; '.date('Y').' &nbsp;Bennett Coleman &amp; Co. Ltd. All rights reserved. For reprint rights: <a target="_blank" href="http://timescontent.com/" pg="fotlnk7">Times Syndication Service</a></div></div>';
$my_settings['footer_css'] = '<style type="text/css">
.footercontainer{background:#333333; clear:both; overflow:hidden; padding:10px 0;}
.footercontainer a{color:#FDFDFD;}
.footercontainer h2{ color:#fff; font-size:20px; line-height:30px; padding:0 0 10px 2px;font-family: Arial, Helvetica, sans-serif;}

.insideLinks{ width:36%; padding-left:2%; float:left;}
.insideLinks ul{ list-style:none; display: inline-block; padding: 0; margin: 0}
.insideLinks ul li{ float:left; margin:0 10px 0 0; width:90px; }
.insideLinks ul li a{ font-family:Arial, Helvetica, sans-serif;color:#B5B3B4;font-size:14px;font-weight:bold;line-height:25px;}

.newsletter{width:32%; padding:0 2%; float:left;}
.newsletterpost {background:#CCCCCC; padding:14px; margin:10px 0 10px 0;float:left; width:300px;}

.newsletterpost input[type="text"]{background:#fff; border:1px #B7B7B7 solid; color:#333;height:21px; width:205px; font-size:14px;font-weight:bold;color:#999;float:left; padding:2px 10px;}
.newsletterpost input[type="submit"]{ background:#f5cc10; color:#000; padding:0 5px; *padding:0 4px; height:26px; cursor:pointer; border:none;font-weight:bold;float:left;}
.fottershareLinks span{ font-size:14px; color:#fff; width:100%; float:left; margin-bottom:10px;}
.fottershareLinks img{ vertical-align:middle;margin-top:5px;}
.fottershareLinks a{margin: 0 2px;}

.fotterApplinks{padding: 0 0 15px 0;}
.fotterApplinks a{padding: 0 10px 0 0; background-image:url(http://navbharattimes.indiatimes.com/photo/42711833.cms); float:left; width:47px; height:65px;background-repeat: no-repeat;}
.fotterApplinks a.andriod{background-position:0 4px}
.fotterApplinks a.andriod:hover{background-position:0 -68px}

.fotterApplinks a.ios{background-position: 0 -144px;}
.fotterApplinks a.ios:hover{background-position:0 -219px}

.fotterApplinks a.java{background-position:0 -296px;}
.fotterApplinks a.java:hover{background-position:0 -378px;}

.fotterApplinks a.win{background-position:0 -447px}
.fotterApplinks a.win:hover{background-position:0 -526px}

.fottershareLinks{clear: both;}
.fottershareLinks h4{color:#fff; font-size:16px;line-height: 23px;}
.fottershareLinks strong{color:#f5cc10; font-weight:normal}

.footfbLike{display: block;margin: 25px 0 12px 0;font-size: 14px;color: #fff;}

.timesotherLinks{width:32%; float:left; padding-right:2%; color:#CCCCCC;font-size:12px;}
.timesotherLinks a{ font-size:11px; padding:0 1px; color:#CCCCCC; line-height:18px;}

.timesLinks{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:10px 0; border-top:1px solid #474747;}
.timesLinks a {color:#FDFDFD; font-family:Arial;}

.copyText{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:0 35px;}
.copyText a {color:#FDFDFD; font-family:Arial;}

.seoLinks{clear:both; margin:10px 0; background:#585757; overflow:hidden;padding-left: 11px;}
.seoLinks ul{list-style:none; padding:0;}
.seoLinks ul li{float:left;}
.seoLinks ul li a{font-family:arial;padding:0 7px;color:#fff;font-size:13px; line-height:32px; display:block; float:left; border-left:#4a4a4a 1px solid;  border-right:#696969 1px solid;}
@media (max-width: 767px){.insideLinks,.newsletter,.timesotherLinks,.seoLinks,.timesLinks{display: none;}}
</style>';
