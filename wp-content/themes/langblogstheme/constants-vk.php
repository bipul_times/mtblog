<?php 

define('Z_IMAGE_PLACEHOLDER', get_template_directory_uri()."/images/placeholder.png");
define('FB_PLACEHOLDER', get_template_directory_uri()."/images/blog-vk-facebook.png");
define('Z_AUTHOR_PLACEHOLDER', get_template_directory_uri()."/images/50.jpg");
define('RECO_URL','http://reco.indiatimes.com/Reco/RecoIndexer?eventType=update&hostid=1b&contentType=blog&msid=');
define('COMMENTS_URL','http://www.vijaykarnatakablogcmtapi.indiatimes.com/');
define('RCHID', '2147478037');
define('MYT_COMMENTS_API_KEY', 'VKB');
define('MYTIMES_URL','http://myt.indiatimes.com/');

global $my_settings;
$my_settings['site_lang'] = 'kn';
$my_settings['og_locale'] = 'kn_IN';
$my_settings['google_site_verification'] = 'ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw';
$my_settings['fblikeurl'] = 'https://www.facebook.com/VijayKarnataka';
$my_settings['favicon'] = 'https://vijaykarnataka.indiatimes.com/icons/VK_favicon.ico';
$my_settings['ga'] = "<script>

TimesGDPR.common.consentModule.gdprCallback(function(data) {
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-29031733-2', 'auto', {'allowLinker': true});
  var userCookie = document.cookie.match(/(?:\s)?ssoid=(\w+);?/);
  if(!!(userCookie)) {
	ga('set', 'userId', userCookie[1]);
  }
  if( TimesGDPR.common.consentModule.isEUuser() ){
  ga('set', 'anonymizeIp', true); 
  }
  ga('require', 'linker');
  ga('linker:autoLink', ['superpacer.in', 'vijayanextepaper.com', 'vijaykarnatakaepaper.com', 'mobilevk.com'] );
  ga('send', 'pageview');
  });
  </script>";
$my_settings['fbappid'] = '117787264903013';
$my_settings['channel_url_part'] = 'channel=vk';
$my_settings['main_site_url'] = 'https://vijaykarnataka.indiatimes.com';
$my_settings['main_site_txt'] = 'Vijay Karnataka Blog';
$my_settings['fb_url'] = 'https://www.facebook.com/VijayKarnataka';
$my_settings['twitter_url'] = 'https://twitter.com/Vijaykarnataka';
$my_settings['twitter_handle'] = 'Vijaykarnataka';
$my_settings['google_url'] = 'https://plus.google.com/104429515049980312291/';
$my_settings['rss_url'] = 'https://vijaykarnataka.indiatimes.com/rssfeeds/11193745.cms';
$my_settings['logo_title'] = 'Vijay Karnataka Blogs';
$my_settings['logo_url'] = 'https://vijaykarnataka.indiatimes.com/photo/49732906.cms';
$my_settings['footer_logo_txt'] = 'ವಿಜಯ ಕರ್ನಾಟಕ';
$my_settings['comscore_tag'] = '<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  var objComScore = { c1: "2", c2: "6036484" };
  TimesGDPR.common.consentModule.gdprCallback(function(data) {
    if( TimesGDPR.common.consentModule.isEUuser() ){
      objComScore["cs_ucfr"] = 0;
    }
  _comscore.push(objComScore);  
  (function() {
	var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
	s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
	el.parentNode.insertBefore(s, el);
  })();
  }); 
</script>
<noscript><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6036484&amp;cv=2.0&amp;cj=1"></noscript>
<!-- End comScore Tag -->';
$my_settings['ibeat_channel'] = 'VkBlog';
$my_settings['ibeat_host'] = 'blogs.vijaykarnataka.indiatimes.com';
$my_settings['ibeat_key'] = '9fcd21b769be6672e419d29c2ee8';
$my_settings['ibeat_domain'] = 'vijaykarnataka.indiatimes.com';
$my_settings['footer_dmp_tag'] = 'https://static.clmbtech.com/ase/8359/19/aa.js';
$my_settings['footer_google_tag'] = '';
$my_settings['google_tag_js_head'] = "";
$my_settings['google_ad_top'] = "";
$my_settings['google_ad_right_1'] = "";
$my_settings['google_ad_right_2'] = "";
$my_settings['visual_revenue_reader_response_tracking_script'] = "";
$my_settings['visual_revenue_reader_response_tracking_script_for_not_singular'] = "";
$my_settings['not_found_heading'] = 'ಕಂಡು ಬಂದಿಲ್ಲ';
$my_settings['not_found_txt'] = 'ಈ ಸ್ಥಳದಲ್ಲಿ ಏನೂ ಕಾಣಸಿಗಲಿಲ್ಲವೆಂದು ತೋರುತ್ತಿದೆ. ಹುಡುಕಲು ಪ್ರಯತ್ನಿಸೋಣವೇ?';
$my_settings['search_placeholder_txt'] = '3 ಅಕ್ಷರಗಳನ್ನು ಟೈಪ್ ಮಾಡಿ...';
$my_settings['go_to_the_profile_of_txt'] = '';
$my_settings['go_to_txt'] = '';
$my_settings['share_link_fb_txt'] = 'ಫೇಸ್‌ಬುಕ್‌ನಲ್ಲಿ ಲಿಂಕ್ ಶೇರ್ ಮಾಡಿ';
$my_settings['share_link_twitter_txt'] = 'ಟ್ವಿಟರ್‌ನಲ್ಲಿ ಲಿಂಕ್ ಶೇರ್ ಮಾಡಿ';
$my_settings['share_link_linkedin_txt'] = 'ಲಿಂಕ್ಡ್‌ಇನ್‌ನಲ್ಲಿ ಲಿಂಕ್ ಶೇರ್ ಮಾಡಿ';
$my_settings['mail_link_txt'] = 'ಲಿಂಕ್ ಮೇಲ್ ಮಾಡಿ';
$my_settings['read_complete_article_here_txt'] = 'ಪೂರ್ಣ ಲೇಖನ ಇಲ್ಲಿ ಓದಿ';
$my_settings['featured_txt'] = 'ಹೆಚ್ಚು ಮೆಚ್ಚುಗೆಯಾದವು';
$my_settings['disclaimer_txt'] = 'ಹಕ್ಕು ನಿರಾಕರಣೆ';
$my_settings['disclaimer_on_txt'] = "ಇದು ಟೈಮ್ಸ್ ಆಫ್ ಇಂಡಿಯಾ ಮುದ್ರಿತ ಆವೃತ್ತಿಯಲ್ಲಿ ಸಂಪಾದಕೀಯ ಅಭಿಮತವಾಗಿ ಪ್ರಕಟವಾಗಿದೆ.";
$my_settings['disclaimer_off_txt'] = 'ಮೇಲೆ ಮಂಡಿಸಲಾದ ಅಭಿಪ್ರಾಯವು ಲೇಖಕರದ್ದು';
$my_settings['most discussed_txt'] = 'ಹೆಚ್ಚು ಚರ್ಚೆಗೊಳಗಾದವು';
$my_settings['more_txt'] = 'ಹೆಚ್ಚು';
$my_settings['less_txt'] = 'ಕಡಿಮೆ';
//$my_settings['login_txt'] = '';
//$my_settings['logout_txt'] = '';
$my_settings['login_txt'] = 'Log In';
$my_settings['logout_txt'] = 'Log Out';
$my_settings['view_all_posts_in_txt'] = '';
$my_settings['home_txt'] = 'ಮುಖಪುಟ';
$my_settings['blogs_txt'] = 'ಬ್ಲಾಗ್ಸ್';
$my_settings['search_results_for_txt'] = 'ಈ ವಿಷಯದ ಹುಡುಕಾಟ ಫಲಿತಾಂಶಗಳು';
$my_settings['most_read_txt'] = 'ಹೆಚ್ಚು ಓದಿದವು';
$my_settings['popular_tags_txt'] = 'ಜನಪ್ರಿಯ ಟ್ಯಾಗ್‌ಗಳು';
$my_settings['recently_joined_authors_txt'] = 'ಇತ್ತೀಚೆಗೆ ಸೇರಿಕೊಂಡ ಲೇಖಕರು';
$my_settings['like_us_txt'] = 'ನಮ್ಮನ್ನು ಲೈಕ್ ಮಾಡಿ';
$my_settings['author_txt'] = ' ಲೇಖಕರು';
$my_settings['popular_from_author_txt'] = 'ಈ ಲೇಖಕರಲ್ಲಿ ಜನಪ್ರಿಯವಾದವು';
$my_settings['search_authors_by_name_txt'] = 'ಹೆಸರಿನ ಪ್ರಕಾರ ಲೇಖಕರನ್ನು ಹುಡುಕಿ';
$my_settings['search_txt'] = 'ಹುಡುಕಿ';
$my_settings['back_to_authors_page_txt'] = 'ಲೇಖಕರ ಪುಟಕ್ಕೆ ಮರಳಿ';
$my_settings['no_authors_found_txt'] = 'ಯಾವುದೇ ಲೇಖಕರಿಲ್ಲ';
$my_settings['further_commenting_is_disabled_txt'] = 'ಕಾಮೆಂಟ್ ಮಾಡುವುದನ್ನು ನಿರ್ಬಂಧಿಸಲಾಗಿದೆ';
$my_settings['comments_on_this_post_are_closed_now_txt'] = 'ಈ ಪೋಸ್ಟ್‌ಗೆ ಕಾಮೆಂಟ್ ಮಾಡುವುದನ್ನು ಸ್ಥಗಿತಗೊಳಿಸಲಾಗಿದೆ';
$my_settings['add_your_comment_here_txt'] = 'ನಿಮ್ಮ ಕಾಮೆಂಟ್ ಇಲ್ಲಿ ಸೇರಿಸಿ';
$my_settings['characters_remaining_txt'] = 'ಬಾಕಿ ಇರುವ ಅಕ್ಷರಗಳು';
$my_settings['share_on_fb_txt'] = 'ಫೇಸ್‌ಬುಕ್‌ನಲ್ಲಿ ಹಂಚಿಕೊಳ್ಳಿ';
$my_settings['share_on_twitter_txt'] = 'ಟ್ವಿಟರ್‌ನಲ್ಲಿ ಹಂಚಿಕೊಳ್ಳಿ';
$my_settings['sort_by_txt'] = 'ಹೀಗೆ ವಿಂಗಡಿಸಿ';
$my_settings['newest_txt'] = 'ಹೊಸತು';
$my_settings['oldest_txt'] = 'ಹಳೆಯದು';
$my_settings['discussed_txt'] = 'ಚರ್ಚೆಯಾದದ್ದು';
$my_settings['up_voted_txt'] = 'ಪರವಾದ ಮತಗಳು';
$my_settings['down_voted_txt'] = 'ವಿರುದ್ಧ ಮತಗಳು';
$my_settings['be_the_first_one_to_review_txt'] = 'ನೀವೇ ಮೊದಲ ವಿಮರ್ಶಕರಾಗಿ';
$my_settings['more_points_needed_to_reach_next_level_txt'] = 'ಮುಂದಿನ ಹಂತ ತಲುಪಲು ಮತ್ತಷ್ಟು ಪಾಯಿಂಟ್ಸ್ ಅಗತ್ಯವಿದೆ';
$my_settings['know_more_about_times_points_txt'] = 'ಟೈಮ್ಸ್ ಪಾಯಿಂಟ್ಸ್ ಬಗ್ಗೆ ಮತ್ತಷ್ಟು ತಿಳಿಯಿರಿ';
$my_settings['know_more_about_times_points_link'] = 'http://www.timespoints.com/about/8606871469114261504';
$my_settings['badges_earned_txt'] = 'ಗಳಿಸಿದ ಬ್ಯಾಡ್ಜ್‌ಗಳು';
$my_settings['just_now_txt'] = 'ಇದೀಗ';
$my_settings['follow_txt'] = 'ಫಾಲೋ ಮಾಡಿ';
$my_settings['reply_txt'] = 'ಉತ್ತರಿಸಿ';
$my_settings['flag_txt'] = 'ಫ್ಲ್ಯಾಗ್ ಮಾಡಿ';
$my_settings['up_vote_txt'] = 'ಪರ ಮತ ಹಾಕಿ';
$my_settings['down_vote_txt'] = 'ವಿರುದ್ಧ ಮತ ಹಾಕಿ';
$my_settings['mark_as_offensive_txt'] = 'ನಿಂದನಾತ್ಮಕ ಎಂದು ಮಾರ್ಕ್ ಮಾಡಿ';
$my_settings['find_this_comment_offensive_txt'] = 'ಈ ಕಾಮೆಂಟ್ ನಿಂದನಾತ್ಮಕ ಅನಿಸಿದೆಯೇ?';
$my_settings['reason_submitted_to_admin_txt'] = 'ನಿಮ್ಮ ಕಾರಣವನ್ನು ಆಡ್ಮಿನ್‌ಗೆ ಸಲ್ಲಿಸಬೇಕಾಗುತ್ತದೆ';
$my_settings['choose_reason_txt'] = 'ಕೆಳಗೆ ನಿಮ್ಮ ಕಾರಣವನ್ನು ಆಯ್ದುಕೊಳ್ಳಿ ಮತ್ತು ಸಲ್ಲಿಸಿ ಎಂಬ ಬಟನ್ ಕ್ಲಿಕ್ ಮಾಡಿ. ಈ ಮೂಲಕ, ಕ್ರಮ ಕೈಗೊಳ್ಳಲು ನಮ್ಮ ಮಾಡರೇಟರ್‌ಗೆ ಸಂದೇಶ ರವಾನೆಯಾಗುತ್ತದೆ.';
$my_settings['reason_for_reporting_txt'] = 'ದೂರು ನೀಡಿದ್ದಕ್ಕೆ ಕಾರಣ';
$my_settings['foul_language_txt'] = 'ಕೆಟ್ಟ ಭಾಷೆ';
$my_settings['defamatory_txt'] = 'ಅವಮಾನಕರ';
$my_settings['inciting_hatred_against_certain_community_txt'] = 'ನಿರ್ದಿಷ್ಟ ಸಮುದಾಯದ ವಿರುದ್ಧ ದ್ವೇಷ ಪ್ರಚೋದನೆ';
$my_settings['out_of_context_spam_txt'] = 'ಸಂದರ್ಭಕ್ಕೆ ತಕ್ಕುದಲ್ಲ/ಸ್ಪ್ಯಾಮ್';
$my_settings['others_txt'] = 'ಇತರ';
$my_settings['report_this_txt'] = 'ಈ ಬಗ್ಗೆ ದೂರು ನೀಡಿ!';
$my_settings['close_txt'] = 'ಮುಚ್ಚಿರಿ';
$my_settings['already_marked_as_offensive'] = 'ಈಗಾಗಲೇ ನಿಂದನಾತ್ಮಕ ಎಂದು ಮಾರ್ಕ್ ಮಾಡಲಾಗಿದೆ';
$my_settings['flagged_txt'] = 'ಫ್ಲ್ಯಾಗ್ ಮಾಡಲಾಗಿದೆ';
$my_settings['blogauthor_link'] = 'http://author.blogs.vijaykarnataka.indiatimes.com/';
$my_settings['old_blogauthor_link'] = 'http://author.blogs.vijaykarnataka.indiatimes.com/';
$my_settings['blog_link'] = 'http://blogs.vijaykarnataka.indiatimes.com/';
$my_settings['common_cookie_domain'] = 'indiatimes.com';
$my_settings['quill_lang'] = 'kannada';
$my_settings['quill_link_1'] = 'ಕನ್ನಡದಲ್ಲಿ ಬರೆಯಿರಿ (ಇನ್‌ಸ್ಕ್ರಿಪ್ಟ್)';
$my_settings['quill_link_2'] = 'ಕನ್ನಡದಲ್ಲಿ ಬರೆಯಿರಿ (ಇಂಗ್ಲಿಷ್ ಅಕ್ಷರಗಳ ಮೂಲಕ)';
$my_settings['quill_link_3'] = 'Write in English';
$my_settings['quill_link_4'] = 'ವರ್ಚುವಲ್ ಕೀಬೋರ್ಡ್';
$my_settings['footer'] = '<div class="container footercontainer">
  <div class="insideLinks">
    <h2>ಸುದ್ದಿಗಳ ಮೇಲೊಂದು ನೋಟ</h2>
    <ul>
      <li id="nav10753874"><a href="http://vijaykarnataka.indiatimes.com/news/articlelist/10753874.cms" pg="" target="_blank">ದೇಶ-ವಿದೇಶ </a></li>
      <li id="nav10738526"><a href="http://vijaykarnataka.indiatimes.com/state/articlelist/10738526.cms" pg="" target="_blank">ಕರ್ನಾಟಕ </a></li>
      <li id="nav10738522"><a href="http://vijaykarnataka.indiatimes.com/district/articlelist/10738522.cms" pg="" target="_blank">ನಿಮ್ಮ ಜಿಲ್ಲೆ </a></li>
      <li id="nav10738520"><a href="http://vijaykarnataka.indiatimes.com/sporthome/10738520.cms" pg="" target="_blank">ಕ್ರೀಡೆ-ಕ್ರಿಕೆಟ್ </a></li>
      <li id="nav10738518"><a href="http://vijaykarnataka.indiatimes.com/astrology/10738518.cms" pg="" target="_blank">ಧರ್ಮ-ಜ್ಯೋತಿಷ್ಯ </a></li>
      <li id="nav10738512"><a href="http://vijaykarnataka.indiatimes.com/entertainmentlist/10738512.cms" pg="" target="_blank">ಸಿನಿಮಾ </a></li>
      <li id="nav10738503"><a href="http://vijaykarnataka.indiatimes.com/businesslist/10738503.cms" pg="" target="_blank">ವಾಣಿಜ್ಯ </a></li>
      <li id="nav10738497"><a href="http://vijaykarnataka.indiatimes.com/edit/articlelist/10738497.cms" pg="" target="_blank">ವಿಚಾರ ಮಂಟಪ </a></li>
      <li><a target="_blank" pg="fotkjlnk16" href="http://photogallery.vijaykarnataka.indiatimes.com/"> ಫೋಟೋ ಗ್ಯಾಲರಿ </a></li>
    </ul>
  </div>
  <div class="newsletter">
    <div class="fottershareLinks">
      <h4 style="color:#fff">ವಿಕ ಅಪ್ಲಿಕೇಶನ್ ಬಳಸಿ, <br>
        <strong>ತಾಜಾ ಸುದ್ದಿ </strong>ಪಡೆಯುತ್ತಿರಿ!</h4>
      <div class="fotterApplinks"><a class="ios" target="_blank" href="https://itunes.apple.com/in/app/kannada-news-vijay-karnataka/id848154881"></a><a class="andriod" target="_blank" href="https://play.google.com/store/apps/details?id=com.vk.reader"></a></div>
    </div>
  </div>
  <div class="timesotherLinks">
    <h2>ನಮ್ಮ ಇತರ ಸೈಟ್‌ಗಳು</h2>
    <a pg="fottgwslnk1" target="_blank" href="http://timesofindia.indiatimes.com/">Times of India</a>| 
    <a pg="fottgwslnk2" href="http://economictimes.indiatimes.com/" target="_blank">Economic Times</a> | 
    <a pg="fottgwslnk3" target="_blank" href="http://itimes.com/">iTimes</a>| 
    <a pg="fottgwslnk5" target="_blank" href="http://maharashtratimes.indiatimes.com/">Marathi News</a> | 
    <a pg="fottgwslnk6" target="_blank" href="http://eisamay.indiatimes.com/">Bangla News</a> | 
    <a pg="fottgwslnk7" target="_blank" href="http://navgujaratsamay.indiatimes.com/">Gujarati News</a>| 
    <a pg="fottgwslnk9" target="_blank" href="http://tamil.samayam.com/">Tamil News</a> | 
    <a pg="fottgwslnk10" target="_blank" href="http://telugu.samayam.com/">Telugu News</a> | 
    <a pg="fottgwslnk11" target="_blank" href="http://malayalam.samayam.com/">Malayalam News</a> | 
    <a pg="fottgwslnk12" target="_blank" href="http://www.businessinsider.in/">Business Insider</a>| 
    <a pg="fottgwslnk13" target="_blank" href="http://zoomtv.indiatimes.com/">ZoomTv</a> | 
    <a pg="fottgwslnk16" target="_blank" href="http://boxtv.com/">BoxTV</a>| 
    <a pg="fottgwslnk17" target="_blank" href="http://www.gaana.com/">Gaana</a> | 
    <a pg="fottgwslnk18" target="_blank" href="http://shopping.indiatimes.com/">Shopping</a> | 
    <a pg="fottgwslnk19" target="_blank" href="http://www.idiva.com/">IDiva</a> | 
    <a pg="fottgwslnk20" target="_blank" href="http://www.astrospeak.com/">Astrology</a> | 
    <a pg="fottgwslnk21" href="http://www.simplymarry.com/" target="_blank">Matrimonial</a><span class="footfbLike">ಫೇಸ್‌ಬುಕ್‌ನಲ್ಲಿ ವಿಕ</span>
    <div data-share="false" data-show-faces="false" data-action="like" data-layout="button_count" data-href="https://www.facebook.com/VijayKarnataka" class="fb-like" style="height: 20px;overflow: hidden;"></div>
  </div>
  <div class="seoLinks">
    <ul>
      <li><a href="http://vijaykarnataka.indiatimes.com/" target="_blank" title="Kannada News" style="border-left:none !important"> Kannada News </a></li>
      <li><a href="http://vijaykarnataka.indiatimes.com/news/india/articlelist/11193745.cms" target="_blank"> National News </a></li>
      <li><a href="http://vijaykarnataka.indiatimes.com/news/world/articlelist/11181729.cms" target="_blank">World News</a></li>
      <li><a href="http://vijaykarnataka.indiatimes.com/state/karnataka/articlelist/10765233.cms" target="_blank">Karnataka News</a></li>
      <li><a href="http://vijaykarnataka.indiatimes.com/sporthome/10738520.cms" target="_blank">Sports News</a> </li>
      <li> <a href="http://vijaykarnataka.indiatimes.com/business/articlelist/10738503.cms" target="_blank"> Business News </a> </li>
      <li> <a href="http://vijaykarnataka.indiatimes.com/astrology/10738518.cms" target="_blank"> Astrology </a> </li>
      <li> <a href="http://navbharattimes.indiatimes.com/" target="_blank"> Hindi News </a> </li>
    </ul>
  </div>
  <div class="timesLinks"><a pg="fotlnk1" href="http://www.timesinternet.in/" target="_blank">About Us</a>&nbsp;|&nbsp; <a href="https://www.ads.timesinternet.in/expresso/selfservice/loginSelfService.htm" target="_blank">Create Your Own Ad</a>&nbsp;|&nbsp; <a pg="fotlnk2" href="http://advertise.indiatimes.com/" target="_blank">Advertise with Us</a>&nbsp;|&nbsp; <a pg="fotlnk3" href="http://vijaykarnataka.indiatimes.com/termsandcondition.cms" target="_blank">Terms of Use and Grievance Redressal Policy</a>&nbsp;|&nbsp; <a pg="fotlnk4" href="http://vijaykarnataka.indiatimes.com/privacypolicy.cms" target="_blank">Privacy Policy</a>&nbsp;|&nbsp; <span class="feedbackeu"><a pg="fotlnk5" href="mailto:grievance.vk@timesinternet.in" target="_blank">Feedback</a>&nbsp;|&nbsp;</span> <a pg="fotlnk6" href="http://vijaykarnataka.indiatimes.com/sitemap.cms" target="_blank">Sitemap</a></div><div class="copyText">Copyright &copy;&nbsp; '.date('Y').'
    &nbsp;Bennett Coleman &amp; Co. Ltd. All rights reserved. For reprint rights: <a pg="fotlnk7" href="http://timescontent.com/" target="_blank">Times Syndication Service</a>
  </div>
</div>';
$my_settings['footer_css'] = '<style type="text/css">
.footercontainer{background:#333333; clear:both; overflow:hidden; padding:10px 0;}
.footercontainer a{color:#FDFDFD;}
.footercontainer h2{ color:#fff; font-size:20px; line-height:30px; padding:0 0 10px 2px;font-family: Arial, Helvetica, sans-serif;}

.insideLinks{ width:36%; padding-left:2%; float:left;}
.insideLinks ul{ list-style:none; display: inline-block; padding: 0; margin: 0}
.insideLinks ul li{ float:left; margin:0 10px 0 0; width:90px; }
.insideLinks ul li a{ font-family:Arial, Helvetica, sans-serif;color:#B5B3B4;font-size:14px;font-weight:bold;line-height:25px;}

.newsletter{width:32%; padding:0 2%; float:left;}
.newsletterpost {background:#CCCCCC; padding:14px; margin:10px 0 10px 0;float:left; width:300px;}

.newsletterpost input[type="text"]{background:#fff; border:1px #B7B7B7 solid; color:#333;height:21px; width:205px; font-size:14px;font-weight:bold;color:#999;float:left; padding:2px 10px;}
.newsletterpost input[type="submit"]{ background:#f5cc10; color:#000; padding:0 5px; *padding:0 4px; height:26px; cursor:pointer; border:none;font-weight:bold;float:left;}
.fottershareLinks span{ font-size:14px; color:#fff; width:100%; float:left; margin-bottom:10px;}
.fottershareLinks img{ vertical-align:middle;margin-top:5px;}
.fottershareLinks a{margin: 0 2px;}

.fotterApplinks{padding: 0 0 15px 0;}
.fotterApplinks a{padding: 0 10px 0 0; background-image:url(https://navbharattimes.indiatimes.com/photo/42711833.cms); float:left; width:47px; height:65px;background-repeat: no-repeat;}
.fotterApplinks a.andriod{background-position:0 4px}
.fotterApplinks a.andriod:hover{background-position:0 -68px}

.fotterApplinks a.ios{background-position: 0 -144px;}
.fotterApplinks a.ios:hover{background-position:0 -219px}

.fotterApplinks a.java{background-position:0 -296px;}
.fotterApplinks a.java:hover{background-position:0 -378px;}

.fotterApplinks a.win{background-position:0 -447px}
.fotterApplinks a.win:hover{background-position:0 -526px}

.fottershareLinks{clear: both;}
.fottershareLinks h4{color:#fff; font-size:16px;line-height: 23px;}
.fottershareLinks strong{color:#ed1c24; font-weight:normal}

.footfbLike{display: block;margin: 25px 0 12px 0;font-size: 14px;color: #fff;}

.timesotherLinks{width:32%; float:left; padding-right:2%; color:#CCCCCC;font-size:12px;}
.timesotherLinks a{ font-size:11px; padding:0 1px; color:#CCCCCC; line-height:18px;}

.timesLinks{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:10px 0;}
.timesLinks a {color:#FDFDFD; font-family:Arial;}

.copyText{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:0 35px;}
.copyText a {color:#FDFDFD; font-family:Arial;}

.seoLinks{clear:both; margin:10px 0; background:#585757; overflow:hidden;padding-left: 11px;}
.seoLinks ul{list-style:none; padding:0;}
.seoLinks ul li{float:left;}
.seoLinks ul li a{font-family:arial;padding:0 7px;color:#fff;font-size:12px; line-height:32px; display:block; float:left; border-left:#4a4a4a 1px solid;  border-right:#696969 1px solid;}
@media (max-width: 767px){.insideLinks,.newsletter,.timesotherLinks,.seoLinks,.timesLinks{display: none;}}
</style>';


// esi ads setting

$my_settings['esi_status'] = 'LIVE';// TEST, LIVE, NONE

if(isMobile()){
	                             
	$esiHeaderStr = '<!--esi 
           <esi:eval src="/fetch_native_content/?fpc=$url_encode($(HTTP_COOKIE{\'_col_uuid\'}))&ab=$(HTTP_COOKIE{\'ce_mapvk\'})&id=$url_encode(\'##ESIIDS##\')&_t=4&_u=$url_encode($(HTTP_HOST)+$(REQUEST_PATH))&ua=$url_encode($(HTTP_USER_AGENT))&ip=$(REMOTE_ADDR)&_v=0&dpv=1&r=$rand()" dca="esi"/> 
           -->';

        $esiHeaderStr .= <<<'ESISTR'
<!--esi           
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_uuid'}))">
                		        $add_header('Set-Cookie', $(native_content{'_col_uuid'}))
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_script'}))">
                		        $(native_content{'_col_script'})
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_ab_call'}))">
                		        $(native_content{'_col_ab_call'})
                            </esi:when>
                        </esi:choose>
                    -->
ESISTR;
$my_settings['esi_header'] = $esiHeaderStr;

	$my_settings['esi_homepage_ids'] = '312381~1~0';
	
	/*HP_CTN_NAT*/
	$my_settings['esi_homepage'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'312381~1~0\'}))">
								$(native_content{\'312381~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	/*HP_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_homepage_rhs'] = "";
	
	$my_settings['esi_article_list_ids'] = '312382~1~0';
	
	/*ROS_AL_CTN_NAT*/
	$my_settings['esi_article_list'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'312382~1~0\'}))">
								$(native_content{\'312382~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*ROS_AL_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_list_rhs'] = "";
	
	$my_settings['esi_article_show_ids'] = '312383~1~0';
	
	/*ROS_AS_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_rhs'] = "";
	
	/*ROS_AS_EOA_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_1'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'312383~1~0\'}))">
								$(native_content{\'312383~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*ROS_AS_EOA1_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_2'] = ""; 
	
	/*Mweb_AS_BLY_VDO_CTN_NAT*/                               
	$my_settings['ctn_article_show_mid_article_video'] = '';
	
}else{

	$esiHeaderStr = '<!--esi 
           <esi:eval src="/fetch_native_content/?fpc=$url_encode($(HTTP_COOKIE{\'_col_uuid\'}))&ab=$(HTTP_COOKIE{\'ce_apvk\'})&id=$url_encode(\'##ESIIDS##\')&_t=4&_u=$url_encode($(HTTP_HOST)+$(REQUEST_PATH))&ua=$url_encode($(HTTP_USER_AGENT))&ip=$(REMOTE_ADDR)&_v=0&dpv=1&r=$rand()" dca="esi"/> 
           -->';

        $esiHeaderStr .= <<<'ESISTR'
<!--esi           
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_uuid'}))">
                		        $add_header('Set-Cookie', $(native_content{'_col_uuid'}))
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_script'}))">
                		        $(native_content{'_col_script'})
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_ab_call'}))">
                		        $(native_content{'_col_ab_call'})
                            </esi:when>
                        </esi:choose>
                    -->
ESISTR;
$my_settings['esi_header'] = $esiHeaderStr;


	$my_settings['esi_homepage_ids'] = '312372~1~0,312373~1~0';
	
	/*HP_CTN_NAT*/
	$my_settings['esi_homepage'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'312372~1~0\'}))">
								$(native_content{\'312372~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*HP_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_homepage_rhs'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'312373~1~0\'}))">
								$(native_content{\'312373~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	$my_settings['esi_article_list_ids'] = '312375~1~0,312376~1~0';
	
	/*ROS_AL_CTN_NAT*/
	$my_settings['esi_article_list'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'312375~1~0\'}))">
								$(native_content{\'312375~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*ROS_AL_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_list_rhs'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'312376~1~0\'}))">
								$(native_content{\'312376~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	$my_settings['esi_article_show_ids'] = '312377~1~0,312378~1~0';
	
	/*ROS_AS_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_rhs'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'312377~1~0\'}))">
								$(native_content{\'312377~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	/*ROS_AS_EOA_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_1'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'312378~1~0\'}))">
								$(native_content{\'312378~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	/*ROS_AS_EOA1_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_2'] = '';          
	
	/*AS_BLY_VDO_CTN_NAT*/  
	$my_settings['ctn_article_show_mid_article_video'] = '';                                                
}
