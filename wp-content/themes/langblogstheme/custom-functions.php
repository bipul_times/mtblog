<?php
$lang = isLang();
if(!empty($lang)){
	if (strpos($_SERVER['HTTP_HOST'], 'readerblogs.navbharattimes.indiatimes.com') !== false || strpos($_SERVER['HTTP_HOST'], 'vsp1nbtreaderblogs.indiatimes.com') !== false ) {
        require get_template_directory() . '/constants-readernbt.php';
    }else{
        require get_template_directory() . '/constants-'.$lang.'.php';
    }
}
require get_template_directory() . '/bloghooks.php';

/*function blogsThemeInit($oldname, $oldtheme=false) {
    remove_role('subscriber');
    remove_role('editor');           
}

add_action("after_switch_theme", "blogsThemeInit", 10 ,  2);

//restrict authors to only being able to view media that they've uploaded

add_filter('parse_query', 'restrict_posts');
function restrict_posts($wp_query)
{
    //are we looking at the Media Library or the Posts list?
    if (strpos($_SERVER['REQUEST_URI'], '/wp-admin/upload.php') !== false || strpos($_SERVER['REQUEST_URI'], '/wp-admin/edit.php') !== false) {
        //user level 5 converts to Editor
        if (!current_user_can('level_5')) {
            //restrict the query to current user
            global $current_user;
            $wp_query->set('author', $current_user->id);
        }
    }
}

//restrict authors to only being able to view media that they've uploaded

add_action('pre_get_posts', 'restrict_media_library');
function restrict_media_library($wp_query_obj)
{
    global $current_user, $pagenow;
    if (!is_a($current_user, 'WP_User'))
        return;
    if ('admin-ajax.php' != $pagenow || $_REQUEST['action'] != 'query-attachments')
        return;
    if (!current_user_can('manage_media_library'))
        $wp_query_obj->set('author', $current_user->ID);
    return;
}*/


function z_add_style()
{
    echo '<style type="text/css" media="screen">
    th.column-thumb {width:60px;}
    .form-field img.taxonomy-image {border:1px solid #eee;max-width:300px;max-height:300px;}
    .inline-edit-row fieldset .thumb label span.title {width:48px;height:48px;border:1px solid #eee;display:inline-block;}
    .column-thumb span {width:48px;height:48px;border:1px solid #eee;display:inline-block;}
    .inline-edit-row fieldset .thumb img,.column-thumb img {width:48px;height:48px;}
    </style>';
}

// add image field in add form
function z_add_texonomy_field()
{
    wp_enqueue_media();
    echo '<div class="form-field">
    <label for="taxonomy_image">' . __('Image', 'zci') . '</label>
    <input type="text" name="taxonomy_image" id="taxonomy_image" value="" />
    <br/>
    <button class="z_upload_image_button button">' . __('Upload/Add image', 'zci') . '</button>
    </div>' . z_script();
    
}

// add image field in edit form
function z_edit_texonomy_field($taxonomy)
{
    wp_enqueue_media();
    if (z_taxonomy_image_url($taxonomy->term_id, NULL, TRUE) == Z_IMAGE_PLACEHOLDER)
        $image_text = "";
    else
        $image_text = z_taxonomy_image_url($taxonomy->term_id, NULL, TRUE);
    echo '<tr class="form-field">
    <th scope="row" valign="top"><label for="taxonomy_image">' . __('Image', 'zci') . '</label></th>
    <td><img class="taxonomy-image" src="' . z_taxonomy_image_url($taxonomy->term_id, NULL, TRUE) . '"/><br/><input type="text" name="taxonomy_image" id="taxonomy_image" value="' . $image_text . '" /><br />
    <button class="z_upload_image_button button">' . __('Upload/Add image', 'zci') . '</button>
    <button class="z_remove_image_button button">' . __('Remove image', 'zci') . '</button>
    </td>
    </tr>' . z_script();
    ?>
    <tr class="form-field">
        <th> Add/Remove authors </th>
        <td> 
          <div>
              <p> <input type="text" id="select-user" name="select-user" class="newtag form-input-tip" size="16" autocomplete="off" value="">
                <input type="hidden" id="author-input" name="author-input" />
                <div id="selector-dd" style="display:none;min-width:50px;z-index: 500;float: left;position: absolute;background: white;border: 1px solid;opacity: .8;margin-left: 1px;padding: 2px;">
                </div>
                <p class="howto">Type at least 3 letters and select user from dropdown. Hit update button when done</p>
                <div class="tagchecklist">
                </div>
            </div>
        </td>
    </tr>
    <?php
    echo '<script type="text/javascript">';
    echo 'var taxonomyId ="' . $taxonomy->term_id . '";';
    echo 'var authorNames = []; var authorIds = [];';
    $blogAuthorIds = get_option("blogAuthors_" . $taxonomy->term_id);
    if ($blogAuthorIds && !empty($blogAuthorIds)) {
        foreach ($blogAuthorIds as $authorId) {
            if (!empty($authorId)) {
                $author = get_user_by('id', $authorId);
                echo 'authorIds.push("' . $authorId . '");';                
                echo 'authorNames.push("' . $author->user_nicename . '");';                
            }
        }
    }
    echo addAuthorAjaxScript();
}

// upload using wordpress upload
function addAuthorAjaxScript()
{
    return '
    jQuery(document).ready(function($) {

     $("#author-input").val(authorIds.join("-"))
     fillSpan();

     $("#select-user").keyup(function(){ 
        var inputValue = $("#select-user").val().trim();
        if(inputValue.length>2){
          jQuery.ajax({
              type: "POST",
              url: "' . site_url() . '/wp-admin/admin-ajax.php",
              dataType: "json",
              data: {
                action: "autoSuggestUsers",
                input: $("#select-user").val(),
            },
            success: function(data, textStatus, XMLHttpRequest){
                $("#selector-dd").empty();
                for (key in data.data ){
                  $("#selector-dd").append("<a id=user-"+key+">"+data.data[key]+"</a><br>");
                  $("#selector-dd").show();
              }
          },
          error: function(MLHttpRequest, textStatus, errorThrown){
            alert("hello"+errorThrown);
        }
    });
}
});

$("#selector-dd > a").live("click",function(e){
  var userId = e.target.getAttribute("id").split("-")[1];
  if(authorIds.indexOf(userId)==-1){
        //new author id added
    var userName = e.target.innerHTML;
    authorIds.push(userId);
    authorNames.push(userName);
    $("#author-input").val(authorIds.join("-"))
    fillSpan();
}
$("#selector-dd").hide();
$("#select-user").val("");  
});

function fillSpan(){
  $(".tagchecklist").empty();
  for (var key in authorIds ){
      $(".tagchecklist").append("<span><a id=author-"+authorIds[key]+" class=\'ntdelbutton\'>X</a>"+authorNames[key]+"</span>");      
  }
}

$(".ntdelbutton").live("click", function(e){
  var userId = e.target.getAttribute("id").split("-")[1];
  var index =  authorIds.indexOf(userId);      
  authorIds.splice(index,1);
  authorNames.splice(index,1);
  $("#author-input").val(authorIds.join("-"))
  fillSpan();
  this.remove();
});
});
</script>';
}

// upload using wordpress upload
function z_script()
{
    return '<script type="text/javascript">
    jQuery(document).ready(function($) {
      var wordpress_ver = "' . get_bloginfo("version") . '", upload_button;
      $(".z_upload_image_button").click(function(event) {
        upload_button = $(this);
        var frame;
        if (wordpress_ver >= "3.5") {
          event.preventDefault();
          if (frame) {
            frame.open();
            return;
        }
        frame = wp.media();
        frame.on( "select", function() {
            // Grab the selected attachment.
            var attachment = frame.state().get("selection").first();
            frame.close();
            if (upload_button.parent().prev().children().hasClass("tax_list")) {
              upload_button.parent().prev().children().val(attachment.attributes.url);
              upload_button.parent().prev().prev().children().attr("src", attachment.attributes.url);
          }
          else
              $("#taxonomy_image").val(attachment.attributes.url);
      });
frame.open();
}
else {
  tb_show("", "media-upload.php?type=image&amp;TB_iframe=true");
  return false;
}
});

$(".z_remove_image_button").click(function() {
    $("#taxonomy_image").val("");
    $(this).parent().siblings(".title").children("img").attr("src","' . Z_IMAGE_PLACEHOLDER . '");
    $(".inline-edit-col :input[name=\'taxonomy_image\']").val("");
    return false;
});

if (wordpress_ver < "3.5") {
    window.send_to_editor = function(html) {
      imgurl = $("img",html).attr("src");
      if (upload_button.parent().prev().children().hasClass("tax_list")) {
        upload_button.parent().prev().children().val(imgurl);
        upload_button.parent().prev().prev().children().attr("src", imgurl);
    }
    else
        $("#taxonomy_image").val(imgurl);
    tb_remove();
}
}

$(".editinline").live("click", function(){  
  var tax_id = $(this).parents("tr").attr("id").substr(4);
  var thumb = $("#tag-"+tax_id+" .thumb img").attr("src");
  if (thumb != "' . Z_IMAGE_PLACEHOLDER . '") {
      $(".inline-edit-col :input[name=\'taxonomy_image\']").val(thumb);
  } else {
      $(".inline-edit-col :input[name=\'taxonomy_image\']").val("");
  }
  $(".inline-edit-col .title img").attr("src",thumb);
  return false;  
});  
});
</script>';
}

add_action('blog_add_form_fields', 'z_add_texonomy_field');
add_action('blog_edit_form_fields', 'z_edit_texonomy_field');
add_filter('manage_edit-blog_columns', 'z_taxonomy_columns');
add_filter('manage_blog_custom_column', 'z_taxonomy_column', 10, 3);

// save our taxonomy image while edit or save term
add_action('edit_term', 'z_save_taxonomy_image');
add_action('create_term', 'z_save_taxonomy_image');
function z_save_taxonomy_image($term_id)
{
    if (isset($_POST['taxonomy_image']))
        update_option('z_taxonomy_image' . $term_id, $_POST['taxonomy_image'], false);
    if (isset($_POST['author-input'])) {
        $authorIds = explode('-', $_POST['author-input']);
        $previousAuthorIds = get_option("blogAuthors_".$term_id);
        foreach ($authorIds as $id) {
            if(!in_array($id, $previousAuthorIds)){
                // this id was newly added
                $authoringBlogIds = get_user_meta($id,'authoringBlogIds',true);
                if (!in_array($term_id, $authoringBlogIds)) {
                    array_push($authoringBlogIds, $term_id);
                    update_user_meta($id, 'authoringBlogIds', $authoringBlogIds);
                }
            }
        }
        foreach ($previousAuthorIds as $id) {
            if(!in_array($id, $authorIds)){
                // this id was removed   
                $authoringBlogIds = get_user_meta($id,'authoringBlogIds',true);
                array_splice($authoringBlogIds, array_search($term_id, $authoringBlogIds), 1);
                update_user_meta($id, 'authoringBlogIds', $authoringBlogIds);
            }
        }
        update_option("blogAuthors_".$term_id, $authorIds, false);
    }
}

// get attachment ID by image url
function z_get_attachment_id_by_url($image_src)
{
    global $wpdb;
    $query = "SELECT ID FROM {$wpdb->posts} WHERE guid = '$image_src'";
    $id    = $wpdb->get_var($query);
    return (!empty($id)) ? $id : NULL;
}

// get taxonomy image url for the given term_id (Place holder image by default)
function z_taxonomy_image_url($term_id = NULL, $size = NULL)
{
    if (!$term_id) {
        if (is_category())
            $term_id = get_query_var('cat');
        elseif (is_tax()) {
            $current_term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
            $term_id      = $current_term->term_id;
        }
    }
    
    $taxonomy_image_url = get_option('z_taxonomy_image' . $term_id);
    $attachment_id      = z_get_attachment_id_by_url($taxonomy_image_url);
    if (!empty($attachment_id)) {
        if (empty($size))
            $size = 'full';
        $taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
        $taxonomy_image_url = $taxonomy_image_url[0];
    }else {
        //check in backup
        $upload_dir = wp_upload_dir();
        $src = $upload_dir['basedir'].'/backup/'.$taxonomy_image_url;
        if (file_exists($src)) {
            $taxonomy_image_url = $upload_dir['baseurl'].'/backup/'.$taxonomy_image_url;
        }
    }
    return ($taxonomy_image_url != '') ? $taxonomy_image_url : Z_IMAGE_PLACEHOLDER;
}

function z_quick_edit_custom_box($column_name, $screen, $name)
{
    if ($column_name == 'thumb')
    echo '<fieldset>
    <div class="thumb inline-edit-col">
    <label>
    <span class="title"><img src="" alt="Thumbnail"/></span>
    <span class="input-text-wrap"><input type="text" name="taxonomy_image" value="" class="tax_list" /></span>
    <span class="input-text-wrap">
    <button class="z_upload_image_button button">' . __('Upload/Add image', 'zci') . '</button>
    <button class="z_remove_image_button button">' . __('Remove image', 'zci') . '</button>
    </span>
    </label>
    </div>
    </fieldset>';
}

/**
 * Thumbnail column added to category admin.
 *
 * @access public
 * @param mixed $columns
 * @return void
 */
function z_taxonomy_columns($columns)
{
    $new_columns            = array();
    $new_columns['cb']      = $columns['cb'];
    $new_columns['thumb']   = __('Image', 'zci');
    $new_columns['authors'] = __('Authors', 'zci');
    
    unset($columns['cb']);
    
    return array_merge($new_columns, $columns);
}

/**
 * Thumbnail column value added to category admin.
 *
 * @access public
 * @param mixed $columns
 * @param mixed $column
 * @param mixed $id
 * @return void
 */
function z_taxonomy_column($columns, $column, $id)
{
    if ($column == 'thumb')
        $columns = '<span><img src="' . z_taxonomy_image_url($id, NULL, TRUE) . '" alt="' . __('Thumbnail', 'zci') . '" class="wp-post-image" /></span>';
    else if ($column == 'authors') {
        $blogAuthorIds = get_option("blogAuthors_" . $id);
        if ($blogAuthorIds) {
            $authors     = get_users(array(
                'include' => $blogAuthorIds,
                'fields' => array(
                    'user_nicename'
                    )
                ));
            $authorNames = array();
            foreach ($authors as $author) {
                array_push($authorNames, $author->user_nicename);
            }
            // print_r($authorNames);
            $columns = implode(',', $authorNames);
        }
    }
    return $columns;
}

// change 'insert into post' to 'use this image'
function z_change_insert_button_text($safe_text, $text)
{
    return str_replace("Insert into Post", "Use this image", $text);
}

// style the image in category list
if (strpos($_SERVER['SCRIPT_NAME'], 'edit-tags.php') > 0) {
    add_action('admin_head', 'z_add_style');
    add_action('quick_edit_custom_box', 'z_quick_edit_custom_box', 10, 3);
    add_filter("attribute_escape", "z_change_insert_button_text", 10, 2);
}

// Validating options
function z_options_validate($input)
{
    return $input;
}

// creating Ajax call for autosuggesting users in the add to Add To Blog field  
add_action('wp_ajax_autoSuggestUsers', 'autoSuggestUsers');
function autoSuggestUsers()
{
    //get the data from ajax() call  
    $input     = $_POST['input'];
    $users     = get_users('search=*' . $input . '*');
    $userArray = array();
    foreach ($users as $user) {
        if($user->ID!='1'){
            $userArray[$user->ID] = $user->user_login;
        }
    }
    $result['status'] = "success";
    $result['data']   = $userArray;
    echo json_encode($result);
    die();
}

function get_user_avatar($id){
    if(!function_exists('get_wp_user_avatar_src')){
        return null;
    }
    //check if avatar is set
    $avatar_new = get_wp_user_avatar_src($id);
    $avatar_new = str_replace( $my_settings['blogauthor_link'],'../../',$avatar_new);
    $avatar_new = str_replace( $my_settings['old_blogauthor_link'],'../../',$avatar_new);
    if( strpos($avatar_new,'default') === false){
        //uploaded image exists
        return $avatar_new;
    }else{
        $avatar = get_user_meta($id, 'avatar',true);
        if(empty($avatar)){
			return $avatar_new;
        }
        $upload_dir = wp_upload_dir();
        $src = $upload_dir['basedir'].'/backup/'.$avatar;
        
        if (file_exists($src)) {
            return $upload_dir['baseurl'].'/backup/'.$avatar;
        }else{    
			// check for et source
			return $avatar_new;
        }
    }
}

add_action('admin_head', 'hide_menu');
function hide_menu() {
    if ( wp_get_current_user()->ID != 1 ) {
		// To remove the whole Appearance admin menu you would use;
		remove_menu_page( 'themes.php' );
		remove_menu_page( 'plugins.php' );
		remove_menu_page( 'options-general.php' );
		remove_menu_page( 'wp-user-avatar' );
		remove_menu_page( 'w3tc_dashboard' );
		remove_menu_page( 'wp_stream' );
		remove_menu_page( 'edit.php?post_type=page' );
		remove_menu_page( 'edit-comments.php' );
		remove_menu_page( 'tools.php' );
		remove_submenu_page( 'index.php', 'update-core.php' );
		remove_submenu_page( 'themes.php', 'theme-editor.php' );
		remove_submenu_page( 'themes.php', 'widgets.php' );
		remove_submenu_page( 'themes.php', 'nav-menus.php' );
		remove_submenu_page( 'themes.php', 'customize.php' );
		remove_submenu_page( 'themes.php', 'custom-background' );
	}
}

// remove links/menus from the admin bar
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );
function mytheme_admin_bar_render() {
    if ( wp_get_current_user()->ID != 1 ) {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('themes');
		$wp_admin_bar->remove_menu('customize');
		$wp_admin_bar->remove_menu('widgets');
		$wp_admin_bar->remove_menu('menus');
		$wp_admin_bar->remove_menu('view-site');
		$wp_admin_bar->remove_node( 'wp-logo');
		$wp_admin_bar->remove_node( 'new-page');
		$wp_admin_bar->remove_node( 'w3tc' );
		$wp_admin_bar->remove_node( 'comments' );
	}
}

add_action( 'admin_menu', 'remove_meta_boxes' );
function remove_meta_boxes() {
	if ( wp_get_current_user()->ID != 1 ) {
		remove_meta_box('tagsdiv-blog', 'post', 'normal');
		remove_meta_box('formatdiv', 'post', 'normal');
		remove_meta_box('commentsdiv','post','normal');
		remove_meta_box('postcustom','post','normal');
		remove_meta_box('slugdiv','post','normal');
		remove_meta_box('trackbacksdiv','post','normal');
	}
}

add_action('admin_menu', 'remove_the_dashboard');
function remove_the_dashboard () {
    if (current_user_can('level_10')) {
        return;
    }
    else {
        global $menu, $submenu, $user_ID;
        $the_user = new WP_User($user_ID);
        reset($menu); $page = key($menu);
        while ((__('Dashboard') != $menu[$page][0]) && next($menu))
            $page = key($menu);
            if (__('Dashboard') == $menu[$page][0]) unset($menu[$page]);
            reset($menu); $page = key($menu);
            while (!$the_user->has_cap($menu[$page][1]) && next($menu))
                $page = key($menu);
            if (preg_match('#wp-admin/?(index.php)?$#',$_SERVER['REQUEST_URI']) && ('index.php' != $menu[$page][2]))
                wp_redirect(get_option('siteurl') . '/wp-admin/post-new.php');
   }
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function custom_excerpt_length( $length ) {
    return 35;
}

add_filter('excerpt_more', 'new_excerpt_more');
function new_excerpt_more( $more ) {
    return '...';
}

function getTrendingByCat($cat){
        global $my_settings;
        $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '2 months ago',
                                ),
                            ),
                        'posts_per_page' => 10,
                        'orderby' => 'comment_count',
                        'category_name' =>$cat
                        );
             $popular = new WP_Query($args);
                $i=0;             
                while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li>
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
}

function getPopularByCat($cat){
        global $my_settings;
        $args = array(
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count',
                        'category_name' =>$cat
                        );
             $popular = new WP_Query($args);
        while ($popular->have_posts()) : $popular->the_post(); ?>   
        <div class="media">
        <img class="pull-left media-object img-circle" src="<?php echo get_user_avatar(get_the_author_meta('ID')); ?>" >
        <div class="media-body">
          <?php the_title( '<h4 class="media-heading"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
          <span class="post-date"><?php the_time('F j, Y') ?></span>
        </div>
        </div>
    <?php endwhile; 
}


function getTrending(){
    global $my_settings;
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '2 weeks ago',
                                ),
                            ),
                        'posts_per_page' => 10,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            echo '<div class="panel"><h3 class="panel-title">'.$my_settings['most discussed_txt'].'</h3><ul class="panel-body list-unstyled trending" data-vr-zone="Most Discussed">';
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
            echo '</ul></div>';
            ?> 
    <?php
}

function getSideWidget(){
    global $my_settings;
    echo '<div class="panel"><h3 class="panel-title">'.$my_settings['most discussed_txt'].'</h3><ul class="panel-body list-unstyled trending" data-vr-zone="Most Discussed">';            
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '2 weeks ago',
                                ),
                            ),
                        'posts_per_page' => 10,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
    echo '</ul></div>';      
}

function getPopular($popular){
    global $my_settings;
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '6 months ago',
                                ),
                            ),
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count'
                        );
    $popular = new WP_Query($args); 
    while ($popular->have_posts()) : $popular->the_post(); ?>   
        <div class="media">
        <img class="pull-left media-object img-circle" src="<?php echo get_user_avatar(get_the_author_meta('ID')); ?>" >
        <div class="media-body">
          <?php the_title( '<h4 class="media-heading"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
          <span class="post-date"><?php the_time('F j, Y') ?></span>
        </div>
        </div>
    <?php endwhile; ?> 
<?php
}


function getMostDiscussedToday(){
    global $my_settings;
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '1 day ago',
                                ),
                            ),
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
            ?> 
    <?php
}

function getMostDiscussedWeek(){
    global $my_settings;
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '1 week ago',
                                ),
                            ),
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
            ?> 
    <?php
}

function getMostDiscussedMonth(){
    global $my_settings;
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '1 month ago',
                                ),
                            ),
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
            ?> 
    <?php
}

function getRecentlyBlogged(){
    global $my_settings;
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '1 month ago',
                                ),
                            ),
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
            ?> 
    <?php
}

function getRecentlyJoinedAuthors(){
	$recentlyauthorslist = array();
	$recentlyauthorslist['count'] = 0;
	$recentlyauthorslist['html'] = '';
	$recent_users = new WP_User_Query( 
      array( 
        'meta_query'   => array(
			'relation' => 'OR',
			array(
			  'compare' => '!=',
			  'key' => 'hide_on_front',
			  'value' => 'yes',
			),
			array(
			  'compare' => 'NOT EXISTS',
			  'key' => 'hide_on_front'
			)
		  ),
		'query_id' => 'authors_with_posts',
		'orderby' => 'user_registered',
		'order' => 'DESC',
        'offset' => 0 ,
        'number' => 10,
        'fields' => 'ids',
        'exclude' => array('1')
      ));
    $users = $recent_users->get_results();
	$recentlyauthorslist['html'] = $recentlyauthorslist['html'] . '<ul class="panel-body list-inline recentauthors">';
	foreach($users as $user) { 
		$user_info = get_userdata($user);
		$recentlyauthorslist['html'] = $recentlyauthorslist['html'] . '<li><a href="'.get_author_posts_url( $user ).'" target="_blank"><img data-src="'.get_user_avatar($user).'" width="55" height="55" alt="'.$user_info->display_name.'" title="'.$user_info->display_name.'" class="avatar avatar-50 wp-user-avatar wp-user-avatar-50 alignnone photo"></a></li>';
		$recentlyauthorslist['count'] = $recentlyauthorslist['count'] + 1;
	}
	$recentlyauthorslist['html'] = $recentlyauthorslist['html'] . '</ul>';
	
	return $recentlyauthorslist;
}

add_action( 'admin_head', 'showhiddencustomfields' );

function showhiddencustomfields() {
    echo "<style type='text/css'>#misc-publishing-actions #visibility {
        display: none;
    }
#wp-admin-bar-view-site{display:none}
    </style>";
}

add_action( 'user_register', 'addMetaFields', 10, 1 );

function addMetaFields( $user_id ) {
    update_user_meta($user_id, 'authoringBlogIds', array());
}


/** 
* Commenting Systems code
**/
add_action('wp_ajax_postComment', 'postComment');
add_action('wp_ajax_nopriv_postComment', 'postComment');

function postComment(){
    //close connection
    $result['status'] = "success";
    $result['data']   = "dummy";
    $status = json_encode($result);
    die();
}

add_action('wp_ajax_validatecomment', 'validatecomment');
add_action('wp_ajax_nopriv_validatecomment', 'validatecomment');
function html2txt($document){
    $search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
                   '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
                   '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
                   '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
    );
    $text = preg_replace($search, '', $document);
    return $text;
} 
function vinput($ip, $dt) 
{   
    $newip = $ip;
    switch ($dt) {
        case 'i':
            $newip = (int)$ip;
            break;
        case 'b':
            $newip = (boolean)$ip;
            break;
        case 's':
            $ip = strip_tags($ip, '<br/>');
            $ip = html2txt($ip);
            //$ip = htmlentities($ip, ENT_QUOTES);
            $newip = (string)$ip;
            break;
    }
    return $newip;
}

function validatecomment(){
    $appKeyForMyTimes = getAppKeyForMyTimes();        
    $postVars = $_POST;
    //echo '<pre>';print_r($postVars); exit;
    $postVars['fromname'] = vinput($_POST['fromname'], 's');
    $postVars['fromaddress'] = vinput($_POST['fromaddress'], 's');
    $postVars['userid'] = vinput($_POST['userid'], 's');
    $postVars['location'] = vinput($_POST['location'], 's');
    $postVars['imageurl'] = vinput($_POST['imageurl'], 's');
    $postVars['loggedstatus'] = vinput($_POST['loggedstatus'], 'i');
    $postVars['message'] = (urlencode($postVars['message']));
    $postVars['roaltdetails'] = vinput($_POST['roaltdetails'], 's');
    $postVars['ArticleID'] = vinput($_POST['ArticleID'], 'i');
    $postVars['msid'] = vinput($_POST['msid'], 'i');
    $postVars['parentid'] = vinput($_POST['parentid'], 'i');
    $postVars['rootid'] = vinput($_POST['rootid'], 'i');
    
    $logFileName = WP_CONTENT_DIR . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'postcommentlogs' . DIRECTORY_SEPARATOR . $postVars['ArticleID'] . '.txt';
    $logFileData = 'Request Time : ' . date( 'Y-m-d H:i:s', current_time( 'timestamp', 0 ) ) . PHP_EOL;
    $logFileData = $logFileData . 'params : ' . $fields_string . PHP_EOL;
    //open connection
    if($postVars['rootid'] >0 && $postVars['parentid']>0)
    {
        $objectType  = "A";
        $activityType = "Replied";

    }else{
        $objectType  = "B";
        $activityType = "Commented";
    }
    $postXML = '<roaltdetails><fromaddress>'.($_POST['fromaddress']).'</fromaddress><location>'.vinput($_POST['location'], 's').'</location><fromname>'.(vinput($_POST['fromname'], 's')).'</fromname><loggedstatus>1</loggedstatus><ssoid>'.vinput($_POST['userid'], 's').'</ssoid><url>'.vinput($_POST['url'], 's').'</url></roaltdetails>';

    $url = MYTIMES_URL.'mytimes/commentIngestion';
    //$curl_headers = array('Content-Type: application/x-www-form-urlencoded;','Accept-Encoding: gzip; deflate');
    
    $url  = $url."/?".$getString;
    $curlPostData = array('eroaltdetails'=>$postXML,
                          'exCommentTxt'=>$postVars['message'],
                          'objectType'  =>$objectType,
                          'activityType'=>$activityType,
                          'appKey'      =>$appKeyForMyTimes,
                          'uniqueAppID' =>vinput($_POST['ArticleID'], 'i'),
                          'baseEntityType'=>'ARTICLE',
                          'ssoId'         =>($_POST['fromaddress']),
                          'userName'      =>(vinput($_POST['fromname'], 's')),
                          'uuId'          =>vinput($_POST['userid'], 's'),
                          'isCommentMigrate'=>true,
                          'isCommentMailMigrate'=>false    
                          );
    if($postVars['rootid'] >0 && $postVars['parentid']>0)
    {
        $curlPostData['objectId'] = $postVars['parentid'];
    }

    $curlPost = http_build_query($curlPostData);

    error_log("Mytimes comment post Data::: ".$curlPost);
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$curlPost);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
    //curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
    $ch_result = curl_exec($ch);
    curl_close($ch);
    $logFileData = $logFileData  .'response : ' . $result . PHP_EOL . PHP_EOL;
    $status = json_decode($ch_result);

    if( $status == null || !isset($status->Error)){
        global $wpdb;
        $wpdb->query('UPDATE wp_posts SET comment_count= comment_count+1 WHERE ID = '.$postVars['ArticleID'].';');
        echo json_encode($status);
    }elseif(isset($status->Error) && isset($status->Error) && ($status->Error=='Message Duplicate')){
        header('Content-type: text/html');
        echo 'duplicatecontent';
    }elseif( isset($status->Error) && isset($status->Error) && ($status->Error=='Comment contains abusive text')){
        header('Content-type: text/html');
        echo 'abusivecontent';
    }else{
        $fp = fopen($logFileName, 'a');
        fwrite($fp, $logFileData);
        fclose($fp);
        echo json_encode($status);
    }
    die();
}

add_action('wp_ajax_ratecomment', 'ratecomment');
add_action('wp_ajax_nopriv_ratecomment', 'ratecomment');

function ratecomment(){
    $params = array();
    $param['callback'] = 'test';
    $param['actorType'] = 'U';
    if($_GET['typeid']=='101')
    {
        $param['activityType'] = 'Disagreed';
    }
    else
    {
        $param['activityType'] = 'Agreed';
    }
    $param['objectId'] = $_GET['opinionid'];
    $param['objectType'] = 'A';
    $param['fromMyTimes'] = true;
    if(isset($_COOKIE['ssoid']))
    {
        $param['uuId']        = $_COOKIE['ssoid'];    
    }
    
   // $url = "http://myt.indiatimes.com/mytimes/addActivity";
	$url = "http://mytpvt.indiatimes.com/mytimes/addActivity";
    foreach( $param as $key => $value )
    {
        $fields_string .= $key.'='.$value.'&';
    }
    $fields_string = rtrim( $fields_string, '&' );
    $url = $url."?".$fields_string; 
    
    //open connection
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
    header('Content-type: text/html');
    echo $result;
    die();
}

add_action('wp_ajax_offensive', 'offensive');
add_action('wp_ajax_nopriv_offensive', 'offensive');

function offensive(){
    
    $params = array();
    $param['callback'] = 'test';
    $param['actorType'] = 'U';
    $param['activityType'] = 'Offensive';
    $param['objectId'] = $_GET['ofcommenteroid'];
    $param['offenText']= $_GET['ofreason'];
    $param['objectType'] = 'A';
    $param['isCommentMigrate'] = true;
    $param['fromMyTimes'] = true;
    if(isset($_COOKIE['ssoid']))
    {
        $param['uuId']        = $_COOKIE['ssoid'];    
    }
    
    
    //$url = "http://myt.indiatimes.com/mytimes/addActivity";
	$url = "http://mytpvt.indiatimes.com/mytimes/addActivity";
    foreach( $param as $key => $value )
    {
        $fields_string .= $key.'='.$value.'&';
    }
    $fields_string = rtrim( $fields_string, '&' );
    echo $url = $url."?".$fields_string;    

    //open connection
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
    header('Content-type: text/html');
    echo $result;
    die();
}


function getBadges($badges,$uid){
    if(is_array ($badges) ){
        foreach($badges as $badge){
            if ($badge->userId == $uid){
                return $badge;
            }
        }
    }elseif (is_object($badges)) {
        if($badges->userId && $badges->userId==$uid){
            return $badges;
        }
    }
    return null;
}

function getRewards($rewards,$uid){
    if(is_array ($rewards) ){
        foreach($rewards as $reward){
            if ($reward->userid == $uid){
                return $reward;
            }
        }
    }elseif (is_object($rewards)) {
        if($rewards->userid && $rewards->userid==$uid){
            return $rewards;
        }
    }
    return null;
}

add_action('wp_ajax_commentsdata', 'commentsdata');
add_action('wp_ajax_nopriv_commentsdata', 'commentsdata');
function extraDataFromAPI($value,$isreply=false){

    $indi = new stdClass();
    //$commetDetails[''] = '';
    if($isreply==true){
        $indi->level = "2";
        $indi->rootid = $value->O_ID;
        $indi->leveldepth =  "2";
    }else{
        $indi->istoplevel = "1";
        $indi->level = "1";
        $indi->leveldepth =  "1";
        $indi->rootid = $value->_id;
    }
    
    $indi->displaylocation = "1";
    $indi->mailidpresent =  "1";
    $indi->optext =  $value->C_T;
    //echo "<pre>";
   //print_r($value);
    if(is_object($value->user_detail))
    {
    $indi->roaltdetails =  array( 'configid' =>$value->_id,
                                'imageurl'=> $value->user_detail->profile,
                                'rotype'=> "0",
                                'url'=>$value->teu,
                                'fromaddress'=> $value->user_detail->sso,
                                'urs'=>$value->teu,
                                'loggedstatus'=> "1",
                                'extid'=> $value->msid,
                                'rchid'=>$value->user_detail->_id,
                                'location'=> $value->user_detail->CITY,
                                'fromname'=> $value->user_detail->FL_N,
                                'ssoid'=> $value->user_detail->uid,
                                'useripaddress'=> "192.168.99.10"
                                );
    }else{
        $details = json_decode($value->ERO_D);
        $details = $details->roaltdetails;
        //echo "<pre>";
        //print_r($details);
    
        $indi->roaltdetails =  array( 'configid' =>$value->_id,
                                'imageurl'=> $details->imageurl,
                                'rotype'=> "0",
                                'url'=>$details->url,
                                'fromaddress'=> $details->fromaddress,
                                'urs'=>$value->teu,
                                'loggedstatus'=> "1",
                                'extid'=> $details->msid,
                                'rchid'=>$details->rchid,
                                'location'=> $details->location,
                                'fromname'=> $details->fromname,
                                'ssoid'=> $details->ssoid,
                                'useripaddress'=> $details->useripaddress
                                );
    }

    $indi->msid =  $value->msid;
    //$indi->rodate =  "05 Jul, 2017 11:35 AM";
    $indi->rodate =  date('d M, Y h:i A', (($value->C_D_Minute*60)+(330*60)));
    $indi->messageid =  $value->_id;
    $indi->parentid =  $value->O_ID;
    $indi->agree = $value->AC_A_C;
    $indi->disagree = $value->AC_D_C;
    $indi->recommended = "0";
    $indi->offensive = $value->AC_O_C;;
    $indi->uid = $value->A_ID;
    
    if($isreply==true){
        $indi->roaltdetails['rootid'] = $value->O_ID;
        $indi->parentusername =  $value->PA_U_D_N_U;
        $indi->parentuid =  $value->PA_A_I;
    }
    

    return $indi;
}

function formatChildForReplyShow(&$childIndi,$value)
{
    if(isset($value->CHILD) && count($value->CHILD)>0)
    {
        foreach ($value->CHILD as $key => $childValue) 
        {
           if(isset($childValue->CHILD) && count($childValue->CHILD)>0)
           {
               $childIndi[] = extraDataFromAPI($childValue,true);
               formatChildForReplyShow($childIndi,$childValue);
           }else{
                $childIndi[] = extraDataFromAPI($childValue,true);
           }
        }
    }
    //var_dump($childIndi);
    //return $childIndi;
}

function commentsdata(){
    $postId = $_GET['msid'];
    $curpg = $_GET['curpg'];
    $appKeyForMyTimes = getAppKeyForMyTimes();
   // $url =  COMMENTS_URL.'showcomments.cms?extid='.$postId.'&perpage=15&feedtype=json&curpg='.$curpg;
    $ordertype = 'desc';
    if(isset($_GET['ordertype']) && !empty($_GET['ordertype'])){
        $ordertype=$_GET['ordertype'];
    }

    $commenttype  = 'CreationDate';
    if(isset($_GET['commenttype']) && !empty($_GET['commenttype'])){
        $commenttype =$_GET['commenttype'];
    }
    

    switch($commenttype){
        
        case "disagree":
        $commenttype= 'disagree';
        $sortcriteria= 'DisagreeCount';
        $order = 'desc';
        break;

        case 'discussed':
        $commenttype = 'mostdiscussed'; 
        $sortcriteria = 'discussed';
        $order = 'desc';
        break;

        case 'agree':
        $commenttype='agree';
        $sortcriteria='AgreeCount';
        $order='desc';
        break;

        default:
        $sortcriteria = 'CreationDate';
        $order       = ($ordertype=="asc")?"desc":"asc";
        break;
    }

    $queryStringData = 'msid='.$postId.'&curpg='.$curpg.'&appkey='.$appKeyForMyTimes.'&commenttype='.$commenttype.'&sortcriteria='.$sortcriteria.'&order='.$order.'&size=15&pagenum='.$curpg.'&withReward=true';
    //MYTIMES_URL/mytimes/getFeed/Activity?msid=".$postId."&curpg=1&appkey=FEMINA&sortcriteria=CreationDate&order=asc&size=10&pagenum=1&withReward=true
    $url = MYTIMES_URL.'mytimes/getFeed/Activity';
    $curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Accept-Encoding: gzip; deflate', 'Accept-Language: en-US');
	$curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8',  'Accept-Language: en-US');    

    $url  = $url."/?".$queryStringData;
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url); 
    //curl_setopt($ch, CURLOPT_POST, 1);
    //curl_setopt($ch, CURLOPT_POSTFIELDS,array());
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,30);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
    $ch_result = curl_exec($ch);
    curl_close($ch);
    $rawComments = json_decode($ch_result);
    //echo "<pre>";
    //var_dump($rawComments);
    $comments = new stdClass();
    $comments->totalcount = $rawComments[0]->totalcount;
    $comments->ActionParams = array();
    $comments->rothrd= new stdClass();
    $comments->rothrd->op = array();
    $comments->rothrd->potime = "96";
    $comments->rothrd->opctr = $rawComments[0]->totalcount;
    $comments->rothrd->opctrtopcnt = $rawComments[0]->cmt_c;
    $comments->rothrd->recommendcount = $rawComments[0]->cmt_c;
    $comments->rothrd->agreecount = $rawComments[0]->cmt_c;
    $comments->rothrd->disagreecount = $rawComments[0]->cmt_c;
    $comments->rothrd->loggedopctr = "0";
    unset($rawComments[0]);
    foreach ($rawComments as $key => $value) 
    {
        
        $indi = extraDataFromAPI($value);
        array_push($comments->rothrd->op,$indi);

        $childIndi = array();
        formatChildForReplyShow($childIndi,$value);
        

        foreach ($childIndi as $key => $childValue) 
        {
            array_push($comments->rothrd->op,$childValue);
        }
        //echo "<pre>";
        //print_r($comments->rothrd->op);

    }
    //echo "<pre>";
    //print_r($comments);
    //echo json_encode($comments);

    if(empty($comments->totalcount)){
        if(is_object($ch)){
            curl_close($ch);
        }
    $response = array();
    $response['new_cmtofart2_nit_v1'] = new stdClass();
    $response['new_cmtofart2_nit_v1']->mytuserdata = new stdClass();
    $response['new_cmtofart2_nit_v1']->ActionParams =  $comments->ActionParams;
    $response['new_cmtofart2_nit_v1']->rothrd =  $comments->rothrd;
    $response['new_cmtofart2_nit_v1']->tdate =  new stdClass();
    $response['new_cmtofart2_nit_v1']->tdate->potime =  0;
    //date_default_timezone_set('UTC');
    $response['new_cmtofart2_nit_v1']->tdate->date = date('D M d Y H:i:s', mktime(date("H")+5, date("i")+30, date("s"), date("m")  , date("d"), date("Y")));    
    //print_r($response);
    echo json_encode($response,JSON_HEX_TAG);
        die();
    }
    //var_dump($comments);
    if($comments->totalcount >0){
        //unset($rawComments[0]);
        //echo "<pre>";
        //print_r($comments);

        $userIds = '';
        /*if($comments->rothrd->opctr=='1'){
            $singleComment = $comments->rothrd->op;
            $comments->rothrd->op = array();
            array_push($comments->rothrd->op,$singleComment);
        }*/
        foreach ($comments->rothrd->op as $value){
            $value->optext = html_entity_decode(stripslashes(strip_tags($value->optext)), ENT_QUOTES);
            $userIds .= urlencode($value->roaltdetails['ssoid']).',';
        }
        $userIds = rtrim($userIds,',');
        //$userIds = urlencode("13pmqrqgfa4mvep66cjwcbh5i");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
        //$userUrl = 'http://myt.indiatimes.com/mytimes/getUsersInfo?ssoids='.$userIds;
	$userUrl = 'http://mytpvt.indiatimes.com/mytimes/getUsersInfo?ssoids='.$userIds;

        curl_setopt($ch, CURLOPT_URL, $userUrl); #set the url and get string together    
        $users = json_decode(curl_exec($ch));
        //var_dump($users);
        //die;
        $badgesUrl = 'http://rewards.indiatimes.com/bp/api/urs/mubhtry?format=json&pcode=TOI&uid='.$userIds;       
        curl_setopt($ch, CURLOPT_URL, $badgesUrl); #set the url and get string together    
        $badges = json_decode(curl_exec($ch));
        $badges = $badges->output;
        //print_r($badges);
        $rewardsUrl = 'http://rewards.indiatimes.com/bp/api/urs/ups?format=json&pcode=TOI&uid='.$userIds;
        curl_setopt($ch, CURLOPT_URL, $rewardsUrl); #set the url and get string together    
        $rewards = json_decode(curl_exec($ch));
        //print_r($rewards);
        $rewards = $rewards->output->user;
        foreach ($users as $user){
            $userReward = getRewards($rewards,$user->sso);
            $user->reward = new stdClass();
            $user->reward->user = $userReward;
            //$userBadges = new stdClass();
            $userBadges = getBadges($badges->userbadgehistory[0]->userbadges,$user->sso);            
            $user->rewardpoint = new stdClass(); 
            //$user->rewardpoint
            $user->rewardpoint->userbadges = $userBadges;
        }
        //print_r($users);
    }
    curl_close($ch);   
    $response = array();
    $response['new_cmtofart2_nit_v1'] = new stdClass();
    $response['new_cmtofart2_nit_v1']->mytuserdata = new stdClass();
    $response['new_cmtofart2_nit_v1']->mytuserdata->array = $users;
    $response['new_cmtofart2_nit_v1']->ActionParams =  $comments->ActionParams;
    $response['new_cmtofart2_nit_v1']->rothrd =  $comments->rothrd;
    $response['new_cmtofart2_nit_v1']->tdate =  new stdClass();
    $response['new_cmtofart2_nit_v1']->tdate->potime =  0;
    //date_default_timezone_set('UTC');
    $response['new_cmtofart2_nit_v1']->tdate->date = date('D M d Y H:i:s', mktime(date("H")+5, date("i")+30, date("s"), date("m")  , date("d"), date("Y")));    
    //print_r($response);
    echo json_encode($response,JSON_HEX_TAG);
    die();
}

//add_filter('screen_options_show_screen', '__return_false');    
add_action( 'admin_head-post-new.php','hide_ping_track');
add_action( 'admin_head-post.php', 'hide_ping_track' );
function hide_ping_track() {
    if( get_post_type() === "post" ){
            // only for non-admins
            echo "<style>.meta-options label[for=ping_status], #ping_status{display:none !important;} </style>";
    }
}

add_filter( 'user_has_cap', 'user_has_cap', 10, 3 );
function user_has_cap( $all_caps, $cap, $args ){
if ( isset( $all_caps['contributor'] ) && $all_caps['contributor'] )
        {
            $all_caps['upload_files'] = TRUE;
            $all_caps['edit_published_posts'] = TRUE;
        }
    return $all_caps;
}

//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
    global $my_settings;
    return ' lang="'.$my_settings['site_lang'].'" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
}
add_filter('language_attributes', 'add_opengraph_doctype');

function catch_first_image($content) {
	global $my_settings;
	$first_img = '';
	ob_start();
	ob_end_clean();
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
	$first_img = $matches[1][0];

	if(empty($first_img)) {
		$first_img = FB_PLACEHOLDER;
	} else if(substr_count($first_img, '/backup/') >= 1) {
		$first_img = FB_PLACEHOLDER;
	} else {
		$first_img = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $first_img );
		global $wpdb;
		$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $first_img ));
		$first_img_arr = wp_get_attachment_image_src($attachment[0], 'full');
		if(isset($first_img_arr[0]) && !empty($first_img_arr[0]) && $first_img_arr[1] > 200 && $first_img_arr[2] > 200) {
			$first_img = $first_img_arr[0];
			$first_img = str_replace( $my_settings['blogauthor_link'],'../../',$first_img);
            $first_img = str_replace( $my_settings['old_blogauthor_link'],'../../',$first_img);
			$first_img = str_replace('../..', WP_HOME, $first_img);
		} else {
			$first_img = FB_PLACEHOLDER;
		}
	}
	return $first_img;
}

//Lets add Open Graph Meta Info
function insert_fb_in_head() {
    global $my_settings, $page, $paged, $post;
    $meta_part = '';
    if(!empty($my_settings['google_site_verification'])) {
		$meta_part = $meta_part . '<meta name="google-site-verification" content="'.$my_settings['google_site_verification'].'" />';
	}
    if ( get_previous_posts_link() ) {
        $meta_part = $meta_part . '<link rel="prev" href="'. get_pagenum_link( $paged - 1 ).'" />';
    }

    if ( get_next_posts_link() ) {
        $meta_part = $meta_part . '<link rel="next" href="'.get_pagenum_link( $paged +1 ).'" />';
    }

    if ( !is_singular()){ //if it is not a post or a page
        //$meta_part = $meta_part . $my_settings['visual_revenue_reader_response_tracking_script_for_not_singular']; 
        //return;
    }
    
    //$meta_part = $meta_part . $my_settings['visual_revenue_reader_response_tracking_script'];
    
    remove_filter( 'the_title', '_show_local_title' );
	remove_filter( 'the_terms', '_show_local_terms_name' );
	remove_filter( 'get_the_terms', '_show_local_terms_name' );
	remove_filter( 'get_terms', '_show_local_terms_name' );
	remove_filter( 'get_term', '_show_local_term_name' );
    
    $meta_part = $meta_part . '<meta property="og:locale" content="'.$my_settings['og_locale'].'" />';
    $meta_part = $meta_part . '<link href="' . $my_settings['google_url'] . '" rel="Publisher" title="' . $my_settings['main_site_txt'] . '">';
    $meta_part = $meta_part . '<meta property="og:site_name" content="' . $my_settings['main_site_txt'] . '"/>';
    $current_url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    if ( is_singular()){
		setup_postdata( $post );
		$authorName = get_the_author_meta('display_name');
		$canUrl = get_post_meta(get_the_ID(),'canurl', true);
		if(empty($canUrl)){
			$canUrl = get_permalink();
		}
		$og_title = get_the_title() . ' - ' . $my_settings['main_site_txt'];
		$og_desc = get_the_excerpt();
		$og_url = get_permalink();
		$og_image = catch_first_image(get_the_content());
		$og_type = 'article';
		
		$meta_part = $meta_part . '<meta property="article:published_time" content="'.esc_attr( get_the_date() ).'">';
		$meta_part = $meta_part . '<meta property="vr:type" content="text">';
		$category = get_the_category();
		$meta_part = $meta_part . '<meta property="vr:category" content="'.$category[0]->cat_name.'">';
		$meta_part = $meta_part . '<meta property="vr:author" content="'.$authorName.'">';
	} elseif(is_home() || is_front_page()) {
		//$canUrl = WP_HOME;
		$canUrl = $current_url;
		$og_title = 'News Expert Blog, '.ucfirst($my_settings['quill_lang']).' Blog, Top Indian Bloggers - ' . $my_settings['main_site_txt'];
		$og_desc = get_bloginfo('description');
		$og_url = $current_url;
		$og_image = FB_PLACEHOLDER;
		$og_type = 'website';
	} elseif( is_tag() ){
		$term_id = get_query_var('tag_id');
		$taxonomy = 'post_tag';
		$args ='include=' . $term_id;
		$terms = get_terms( $taxonomy, $args );
		$tag_name = $terms[0]->name;
		//$tag_link = get_tag_link($terms[0]->term_taxonomy_id);
		$tag_link = $current_url;
		$canUrl = $tag_link;
		$og_title = $tag_name . ' Tag Blog Post - ' . $my_settings['main_site_txt'];
		$og_desc = 'Get all the blog post with the '.$tag_name.' tag from our '.$my_settings['main_site_txt'].'.';
		$og_url = $tag_link;
		$og_image = FB_PLACEHOLDER;
		$og_type = '';
	} elseif( is_category() ){
		$the_cat = get_the_category();
		$category_name = $the_cat[0]->name;
		//$category_link = get_category_link( $the_cat[0]->cat_ID );
		$category_link = $current_url;
		$canUrl = $category_link;
		$og_title = $category_name . ' Blog in '.ucfirst($my_settings['quill_lang']).', Top '.$category_name.' Blog - ' . $my_settings['main_site_txt'];
		$og_desc = $my_settings['main_site_txt']. ' provides you one of the top '.$category_name.' blog in '.$my_settings['quill_lang'].' with very interesting blog post by the expert bloggers.';
		$og_url = $category_link;
		$og_image = FB_PLACEHOLDER;
		$og_type = '';
	} elseif( is_tax( 'blog' ) ){
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		if($term->slug == 'authors'){
			//$blog_link = get_term_link($term);
			$blog_link = $current_url;
			$canUrl = $blog_link;
			$og_title = ucfirst($my_settings['quill_lang']) . ' Blog New Authors, Blog Authors Profile List'.' - ' . $my_settings['main_site_txt'];
			$og_desc = 'Find out the blog new authors list, top blog authors in '.ucfirst($my_settings['quill_lang']).' who have expressed their views, opinions and stories on politics, social issues, sports and other topics at '.$my_settings['main_site_txt'].'.';
			$og_url = $blog_link;
			$og_image = FB_PLACEHOLDER;
			$og_type = '';
		} else {
			$blog_name = $term->name;
			//$blog_link = get_term_link($term);
			$blog_link = $current_url;
			$canUrl = $blog_link;
			$og_title = $blog_name . ' Post by best bloggers in '.ucfirst($my_settings['quill_lang']).' - ' . $my_settings['main_site_txt'];
			$og_desc = 'Read the bloggers opinion and stories on '.$blog_name.' by best bloggers and news experts in '.ucfirst($my_settings['quill_lang']).'.';
			$og_url = $blog_link;
			$og_image = FB_PLACEHOLDER;
			$og_type = '';
		}
	} elseif( is_author() ){
		//$author_url = get_author_posts_url(get_the_author_meta( 'ID' ));
		$author_url = $current_url;
		$author_name = get_the_author_meta( 'first_name' ) . ' ' . get_the_author_meta( 'last_name' );
		$canUrl = $author_url;
		$og_title = $author_name . ' Blog Post - ' . $my_settings['main_site_txt'];
		$og_desc = get_the_author_meta( 'description' );
		$og_url = $author_url;
		$og_image = FB_PLACEHOLDER;
		$og_type = '';
	} elseif( is_search() ){
		$canUrl = $current_url;
		$og_title = $my_settings['main_site_txt'] . ' Post Search';
		$og_desc = 'Search result of '.get_search_query().' in '.$my_settings['main_site_txt'].'.';
		$og_url = $current_url;
		$og_image = FB_PLACEHOLDER;
		$og_type = '';
	}
	
	if ( $paged >= 2 || $page >= 2 ){
		$og_title = sprintf( 'Page %s : %s', max( $paged, $page ),  $og_title);
		$og_desc = sprintf( 'Page %s : %s', max( $paged, $page ),  $og_desc);
	}
	
	if(!defined('WP_ADMIN')) {
		add_filter( 'the_title', '_show_local_title', 10, 2 );
		add_filter( 'the_terms', '_show_local_terms_name', 10, 2 );
		add_filter( 'get_the_terms', '_show_local_terms_name', 10, 2 );
		add_filter( 'get_terms', '_show_local_terms_name', 10, 2 );
		add_filter( 'get_term', '_show_local_term_name', 10, 2 );
	}
	
	if(!empty($canUrl)){
		$meta_part = $meta_part . '<link rel=canonical href="'.$canUrl.'" />';
	}
	$meta_part = $meta_part . '<meta name="description" content="' .$og_desc. '"/>';
	$meta_part = $meta_part . '<meta property="og:url" content="' . $og_url . '"/>';
	if ( is_singular()){
		$meta_part = $meta_part . '<meta property="og:title" content="' . get_the_title() . '"/>';
	} else {
		$meta_part = $meta_part . '<meta property="og:title" content="' . $og_title . '"/>';
	}
	$meta_part = $meta_part . '<meta property="og:description" content="' . $og_desc .'"/>';
	$meta_part = $meta_part . '<meta property="og:image" content="' . $og_image . '"/>';
	if(!empty($og_type)){
		$meta_part = $meta_part . '<meta property="og:type" content="' . $og_type . '"/>';
	}
	$meta_part = $meta_part . '<meta name="twitter:card" content="summary" />';
	if ( is_singular()){
		$meta_part = $meta_part . '<meta name="twitter:title" content="' . get_the_title() . '" />';
	} else {
		$meta_part = $meta_part . '<meta name="twitter:title" content="' . $og_title . '" />';
	}
	$meta_part = $meta_part . '<meta property="twitter:description" content="' . $og_desc .'">';
	$meta_part = $meta_part . '<meta name="twitter:url" content="' . $og_url . '" >';
	$meta_part = $meta_part . '<meta name="twitter:domain" content="' . WP_HOME . '">';
	
	echo '<title>' . $og_title . '</title>' . $meta_part;
	echo '<script type="text/javascript" src="'.get_template_directory_uri() . '/js/quill-compiled.js?ver=1.2"></script>';
}
add_action( 'wp_head', 'insert_fb_in_head', 5 );

add_action('wp_ajax_getIbeatData', 'getIbeatData');
add_action('wp_ajax_nopriv_getIbeatData', 'getIbeatData');

function getIbeatData()
{
    global $my_settings;
    //echo 'here';
    $url = $_GET['url'];

$url = str_replace('IBEAT_HOST', $my_settings['ibeat_host'], $url);
    $url = str_replace('IBEAT_KEY', $my_settings['ibeat_key'], $url);
    $url = str_replace('IBEAT_CONTENT_TYPE', '21', $url);

    //echo $url;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,10); # timeout after 10 seconds, you can increase it
    curl_setopt($ch, CURLOPT_URL, $url ); #set the url and get string together    
    $result = curl_exec($ch);
    $res = json_decode ($result);
    $html = '';
    $i = 1;
    foreach ($res->DATA as $key => $value){
        if($i==6){
            break;
        }
        $postTitle = get_the_title($key);
        $postSubtitle = get_post_meta( $key, 'wps_subtitle', true );
		$postTitle = $postSubtitle ? $postSubtitle : $postTitle;
        $html .= '<li data-vr-contentbox="">';
        $html .= '<span class="pull-left number">'.$i.'.</span>';
        $blogurl = get_permalink($key);
        $html .= '<h4><a pg="MR_Pos#'.$i.'" href="' . $blogurl . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.$postTitle.'">'.$postTitle.' </a></h4>';    
        $html .= '</li>'  ;
        $i++;
    }
    wp_reset_query(); 
    header("Cache-Control: max-age=900"); //30days (60sec * 60min * 24hours * 30days)
    echo json_encode($html);
    die();
}

add_action('wp_ajax_getPlusones', 'getPlusones');
add_action('wp_ajax_nopriv_getPlusones', 'getPlusones');

function getPlusones1()  {
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, "https://clients6.google.com/rpc");
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"'.rawurldecode($_GET['url']).'","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
$curl_results = curl_exec ($curl);
curl_close ($curl);
$json = json_decode($curl_results, true);
$res =  isset($json[0]['result']['metadata']['globalCounts']['count'])?intval( $json[0]['result']['metadata']['globalCounts']['count'] ):0;
$response = json_encode($res);
echo $response;
die();
}

function getPlusones()  {
/* get source for custom +1 button */
    $contents = file_get_contents( 'https://plusone.google.com/_/+1/fastbutton?url=' .  rawurldecode($_GET['url']) );
    //echo $contents;
    /* pull out count variable with regex */
    preg_match( '/window\.__SSR = {c: ([\d]+)/', $contents, $matches );
 
    /* if matched, return count, else zed */
    if( isset( $matches[0] ) ) 
        echo (int) str_replace( 'window.__SSR = {c: ', '', $matches[0] );
    else
        echo 0;
    die();
}

add_action('wp_ajax_getSearchData', 'getSearchData');
add_action('wp_ajax_nopriv_getSearchData', 'getSearchData');

function getSearchData (){
    global $wpdb;
    $input = $_GET['input'];
    // fetch authors
    $queryStringUsers = "SELECT display_name as name,CONCAT('".site_url()."/author/',user_nicename) as link FROM {$wpdb->prefix}users u WHERE u.display_name LIKE '%" . $input . "%' LIMIT 0, 5";
    $tempTerms  = $wpdb->get_results( $queryStringUsers );
    //print_r($tempTerms);
    //echo $wpdb->last_query;
    $queryStringTaxonomies = 'SELECT term.name as name, CONCAT("'.site_url().'/",term.slug) as link FROM ' . $wpdb->term_taxonomy . ' tax ' .
                    'LEFT JOIN ' . $wpdb->terms . ' term ON term.term_id = tax.term_id WHERE 1 = 1 ' .
                    'AND term.name LIKE "%' . $input . '%" ' .
                    'AND ( tax.taxonomy = "blog") ' .
                    'ORDER BY tax.count DESC ' .
                    'LIMIT 0, 5';
    //echo $wpdb->last_query;
    $tempTerms2  = $wpdb->get_results( $queryStringTaxonomies );
    $result = array_merge($tempTerms,$tempTerms2);
    echo json_encode($result);
    die();
}


add_action('wp_ajax_getAuthors', 'getAuthors');
add_action('wp_ajax_getAuthors', 'getAuthors');

function getAuthors(){
    global $wpdb;
    $input = $_GET['input'];
    // fetch authors
    $queryStringUsers = "SELECT display_name as name,CONCAT('".site_url()."/author/',user_nicename) as link FROM {$wpdb->prefix}users u WHERE u.display_name LIKE '%" . $input . "%' LIMIT 0, 5";
    $tempTerms  = $wpdb->get_results( $queryStringUsers );
    //print_r($tempTerms);
    //echo $wpdb->last_query;
    $queryStringTaxonomies = 'SELECT term.name as name, CONCAT("'.site_url().'/",term.slug) as link FROM ' . $wpdb->term_taxonomy . ' tax ' .
                    'LEFT JOIN ' . $wpdb->terms . ' term ON term.term_id = tax.term_id WHERE 1 = 1 ' .
                    'AND term.name LIKE "%' . $input . '%" ' .
                    'AND ( tax.taxonomy = "blog") ' .
                    'ORDER BY tax.count DESC ' .
                    'LIMIT 0, 5';
    //echo $wpdb->last_query;
    $tempTerms2  = $wpdb->get_results( $queryStringTaxonomies );
    $result = array_merge($tempTerms,$tempTerms2);
    echo json_encode($result);
    die();
}


add_action('do_meta_boxes', 'customauthor_image_box');

function customauthor_image_box() {
    remove_meta_box('postimagediv', 'post', 'side');
    add_meta_box('postimagediv', __('Custom Author Image'), 'post_thumbnail_meta_box', 'post', 'side', 'low');
}

function new_title( $title, $sep ) {
    $title = '';
    if ( is_singular()){ //if it is not a post or a page
        global $post;
        setup_postdata( $post );
        $title.= get_the_title().' '.$sep.' ';
    }
    $title .= get_bloginfo( 'name' );
    /*global $paged, $page;

    if ( is_feed() )
        return $title;

    // Add the site name.
    $title .= get_bloginfo( 'name' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        $title = "$title $sep $site_description";

    // Add a page number if necessary.
    if ( $paged >= 2 || $page >= 2 )
        $title = "$title $sep " . sprintf( __( 'Page %s', 'twentytwelve' ), max( $paged, $page ) );
    */
    return $title;
}
//add_filter( 'wp_title', 'new_title', 10, 2 );

require get_template_directory() . '/apis.php';

function add_theme_menu_item() {
    add_options_page( 'Interstitial Settings', 'Interstitial', 'manage_options', 'interstitial-settings', 'interstitial_settings_page' );
    add_options_page( 'Ad Blocker Settings', 'Ad Blocker', 'manage_options', 'ad-blocker-settings', 'adblocker_settings_page' );
}

add_action("admin_menu", "add_theme_menu_item");

function adblocker_settings_page(){
    ?>
	<div class="wrap">
	    <h1>Ad Blocker Settings</h1>
	    <form method="post" action="options.php">
	        <?php
	            settings_fields("ad-section");
	            do_settings_sections("ad-blocker-options");      
	            submit_button(); 
	        ?>          
	    </form>
	</div>
    <?php
}

function interstitial_settings_page(){
    ?>
	<div class="wrap">
	    <h1>Interstitial Settings</h1>
	    <form method="post" action="options.php">
	        <?php
	            settings_fields("section");
	            do_settings_sections("theme-options");      
	            submit_button(); 
	        ?>          
	    </form>
	</div>
    <?php
}

function display_webadcontent_element(){
    ?><textarea name="web_ad_content" rows="10" cols="50" id="web_ad_content" class="large-text code"><?php echo get_option('web_ad_content'); ?></textarea><?php
}

function display_wapadcontent_element(){
    ?><textarea name="wap_ad_content" rows="10" cols="50" id="wap_ad_content" class="large-text code"><?php echo get_option('wap_ad_content'); ?></textarea><?php
}

function display_showad_element(){
    ?><fieldset>
    <label><input type="radio" name="show_ad" value="WEB" <?php checked('WEB', get_option('show_ad'), true); ?> />WEB</label><br />
    <label><input type="radio" name="show_ad" value="WAP" <?php checked('WAP', get_option('show_ad'), true); ?> />WAP</label><br />
    <label><input type="radio" name="show_ad" value="Both" <?php checked('Both', get_option('show_ad'), true); ?> />Both</label><br />
    <label><input type="radio" name="show_ad" value="None" <?php checked('None', get_option('show_ad'), true); ?> />None</label>
    </fieldset><?php
}

function display_ad_blocker_js_code_element(){
	 ?><textarea name="ad_blocker_js_code" rows="10" cols="50" id="ad_blocker_js_code" class="large-text code"><?php echo get_option('ad_blocker_js_code'); ?></textarea><?php
}

function display_ad_blocker_content_element(){
	?><textarea name="ad_blocker_content" rows="10" cols="50" id="ad_blocker_content" class="large-text code"><?php echo get_option('ad_blocker_content'); ?></textarea><?php
}

function display_theme_panel_fields(){
    add_settings_section("section", "", null, "theme-options");
    add_settings_field("web_ad_content", "WEB Ad Code", "display_webadcontent_element", "theme-options", "section");
    add_settings_field("wap_ad_content", "WAP Ad Code", "display_wapadcontent_element", "theme-options", "section");
    add_settings_field("show_ad", "Where you want show interstitial?", "display_showad_element", "theme-options", "section");

    register_setting("section", "web_ad_content");
    register_setting("section", "wap_ad_content");
    register_setting("section", "show_ad");
    
    add_settings_section("ad-section", "", null, "ad-blocker-options");
    add_settings_field("ad_blocker_js_code", "JS Code", "display_ad_blocker_js_code_element", "ad-blocker-options", "ad-section");
    add_settings_field("ad_blocker_content", "Page Content", "display_ad_blocker_content_element", "ad-blocker-options", "ad-section");

    register_setting("ad-section", "ad_blocker_js_code");
    register_setting("ad-section", "ad_blocker_content");
}

add_action("admin_init", "display_theme_panel_fields");

add_filter('upload_mimes','restrict_mime');

function restrict_mime($mimes) {
	$mimes = array(
					'jpg|jpeg|jpe' => 'image/jpeg',
					//'gif' => 'image/gif',
					'png' => 'image/png',
	);
	return $mimes;
}


function getAppKeyForMyTimes()
{
    $appKeyForMyTimes = MYT_COMMENTS_API_KEY;    
    return $appKeyForMyTimes; 
}

add_action('wp_ajax_mytimeEntityMigration', 'mytimeEntityMigration');
add_action('wp_ajax_nopriv_mytimeEntityMigration', 'mytimeEntityMigration');

function mytimeEntityMigration()
{
    ignore_user_abort(true);
    set_time_limit(0);
    $appKeyForMyTimes = getAppKeyForMyTimes();
    $perPageRecord = 2000;
    if(isset($_GET['page'])&& $_GET['page']>0){
        $offset = $_GET['page'];
    }else{
        $offset = 1;
    }
    $offset = $perPageRecord*($offset-1);
    $args = array('numberposts' => $perPageRecord,
                  'offset' => $offset  
                 );
    $postList = get_posts($args);
    //var_dump($postList);
    //die;
    foreach ($postList as $key => $postDetails) 
    {
        
        echo $post_id = $postDetails->ID;
        echo "<br/>";
        //continue;
        if(has_post_thumbnail($post_id)) 
        { 
                $photo = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' )[0];
        }
        else
        {
            $post_author = get_post_field( 'post_author', $post_id );
            $photo = get_user_avatar($post_author);
        }

        $title = $postDetails->post_title;
        $subtitle = get_post_meta($post_id, 'wps_subtitle', true );
        $headline = $subtitle ? $subtitle : $title;

        $specialCharToRemove = array('&lsquo;','&rsquo;','&zwj;','&zwnj;','!&nbsp;','&nbsp;','&mdash;','&ndash;','&amp;');
        $synopsys = strip_tags(get_the_excerpt($post_id));
        foreach ($specialCharToRemove as $k=>$value) 
        {
            $synopsys = str_replace($value,'', $synopsys);
        }

        global $my_settings;
        $photo = str_replace($my_settings['blogauthor_link'], $my_settings['blog_link'], $photo);
        $photo = str_replace($my_settings['old_blogauthor_link'], $my_settings['blog_link'], $photo);
        $articleLink = get_permalink( $post_id );
        $articleLink = str_replace($my_settings['blogauthor_link'], $my_settings['blog_link'], $articleLink);
        $articleLink = str_replace($my_settings['old_blogauthor_link'], $my_settings['blog_link'], $articleLink);

        $categories = get_the_category($post_id);
        $catIds = array();
        if($categories){
            foreach($categories as $category) {
                $catIds[] = $category->cat_ID;
            }
        }
        $catIdsList = implode(',', $catIds);
        $catName    = trim($categories[0]->cat_name);

        $xmlPost = array(
                    'name'=>($headline),
                    'id' => $post_id,
                    'synopsys' =>$synopsys, 
                    'url' =>$articleLink,
                    'type' =>'ARTICLE',
                    'appKey'=>$appKeyForMyTimes,
                    'cat'=>$catName,
                    'scat'=>'',
                    'parentmsidlist'=>$catIdsList,
                    'imageUrl'=>$photo
                );
        //$str = 'cmsObjectDetail=<entity><msid>'.$post_id.'</msid><Mstype>2</Mstype><Mssubtype>0</Mssubtype><title><![CDATA['.urlencode(get_the_title( $post_id )).']]></title><uppdt_date>10/18/2016 12:36:06 PM</uppdt_date><appKey>'.$appKeyForMyTimes.'</appKey><synopsys>'.urlencode(get_the_excerpt($post_id)).'</synopsys><url>'.get_permalink( $post_id ).'</url><topicNames></topicNames><parentmsidlist></parentmsidlist></entity>';
        $str = "entityDetail=".json_encode($xmlPost);
        //$url = MYTIMES_URL.'mytimes/cms/add';
        $url = MYTIMES_URL.'mytimes/entity/add';
        //die;
       // $curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Accept-Encoding: gzip; deflate', 'Accept-Language: en-US');
       
	$curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8',  'Accept-Language: en-US');
	 $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$str);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
        $ch_result = curl_exec($ch);
        //var_dump($ch_result);
        curl_close($ch);
    }
    die();   
}

add_action('wp_ajax_mytimeEntityVerification', 'mytimeEntityVerification');
add_action('wp_ajax_nopriv_mytimeEntityVerification', 'mytimeEntityVerification');

function mytimeEntityVerification()
{
    ignore_user_abort(true);
    set_time_limit(0);
    $appKeyForMyTimes = getAppKeyForMyTimes();
    $perPageRecord = 2000;
    if(isset($_GET['page'])&& $_GET['page']>0){
        $offset = $_GET['page'];
    }else{
        $offset = 1;
    }
    $offset = $perPageRecord*($offset-1);
    $args = array('numberposts' => $perPageRecord,
                  'offset' => $offset  
                 );
    $postList = get_posts($args);

    //$curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Content-Length: ' . (strlen($str)), 'Accept-Encoding: gzip; deflate', 'Accept-Language: en-US');

	$curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Content-Length: ' . (strlen($str)),  'Accept-Language: en-US');

    foreach ($postList as $key => $postDetails) 
    {
        //$url = "http://myt.indiatimes.com/mytimes/entity?msid=".$postDetails->ID."&appKey=".$appKeyForMyTimes;
	$url = "http://mytpvt.indiatimes.com/mytimes/entity?msid=".$postDetails->ID."&appKey=".$appKeyForMyTimes;
        //echo "<br/>";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
        $ch_result = curl_exec($ch);
        //var_dump($ch_result);
        if($ch_result=="[]"){
            echo "Failed for postId ==".$postDetails->ID." and appkey=".$appKeyForMyTimes."<br/>";
        }else{
            //echo "Success for postId ==".$postDetails->ID." and appkey=".$appKeyForMyTimes."<br/>";
        }
        curl_close($ch);   
    }
    die();
}
