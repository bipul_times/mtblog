<?php

  /**
   * The main template file
   *
   * This is where we create the a custom taxonomy blogs and all SEO friendly URLs
   * TODO : Move this code to a plugin so that it can be shared across different blogs
   * 
   * @link http://codex.wordpress.org/Taxonomies, http://www.sitepoint.com/guide-url-rewriting/
   *
   * @author Shikhar Sachan
   */
  
//One Time Functions 
add_action('init', 'initialize');
function initialize()
{
    if (!taxonomy_exists('blog')) {
        register_blog_taxonomy();
    }
    no_category_base_permastruct();
    if (strpos($_SERVER['HTTP_HOST'],'author') !== false) {
        $currentPage = $_SERVER['REQUEST_URI'];
        if($currentPage == '/'){
            wp_redirect(admin_url());
            exit;
        }
    }   
}

function register_blog_taxonomy()
{
    // create a new taxonomy
    $labels = array(
        'name' => _x('Blogs', 'taxonomy general name'),
        'singular_name' => _x('Blog', 'taxonomy singular name'),
        'search_items' => __('Search Blogs'),
        'popular_items' => __('Popular Blogs'),
        'all_items' => __('All Blogs'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Edit Blog'),
        'update_item' => __('Update Blog'),
        'add_new_item' => __('Add New Blog'),
        'new_item_name' => __('New Blog Name'),
        'separate_items_with_commas' => __('Separate blogs with commas'),
        'add_or_remove_items' => __('Add or remove blogs'),
        'choose_from_most_used' => __('Choose from the most used blogs'),
        'not_found' => __('No blogs found.'),
        'menu_name' => __('Blogs')
        );
    
    $args = array(
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'rewrite' => array(
            'slug' => 'blog'
            )
        );
    register_taxonomy('blog', 'post', $args);
}

/**
 * Code for custom URLs
 */
add_action('created_category', 'no_category_base_refresh_rules');
add_action('edited_category', 'no_category_base_refresh_rules');
add_action('delete_category', 'no_category_base_refresh_rules');

add_action('created_blog', 'no_category_base_refresh_rules');
add_action('edited_blog', 'no_category_base_refresh_rules');
add_action('delete_blog', 'no_category_base_refresh_rules');

function no_category_base_refresh_rules()
{
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}


function no_category_base_permastruct()
{
    global $wp_rewrite;
    //print_r($wp_rewrite -> extra_permastructs);
    $wp_rewrite->extra_permastructs['category']['struct'] = '%category%';
    $wp_rewrite->extra_permastructs['blog']['struct']     = '%blog%';
}

// Add our custom category rewrite rules
add_filter('category_rewrite_rules', 'no_category_base_rewrite_rules');
function no_category_base_rewrite_rules($category_rewrite)
{
    //var_dump($category_rewrite); // For Debugging
    $category_rewrite = array();
    $categories       = get_categories(array(
        'hide_empty' => false
        ));
    foreach ($categories as $category) {
        $category_nicename = $category->slug;
        if ($category->parent == $category->cat_ID) // recursive recursion
        $category->parent = 0;
        elseif ($category->parent != 0)
            $category_nicename = get_category_parents($category->parent, false, '/', true) . $category_nicename;
        $category_rewrite['(' . $category_nicename . ')/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?category_name=$matches[1]&feed=$matches[2]';
        $category_rewrite['(' . $category_nicename . ')/page/?([0-9]{1,})/?$']                  = 'index.php?category_name=$matches[1]&paged=$matches[2]';
        $category_rewrite['(' . $category_nicename . ')/?$']                                    = 'index.php?category_name=$matches[1]';
    }
    // Redirect support from Old Category Base
    global $wp_rewrite;
    $old_category_base                               = get_option('category_base') ? get_option('category_base') : 'category';
    $old_category_base                               = trim($old_category_base, '/');
    $category_rewrite[$old_category_base . '/(.*)$'] = 'index.php?category_redirect=$matches[1]';
    
    $taxonomy   = 'blog';
    $categories = get_terms($taxonomy, array(
        'hide_empty' => false
        ));
    
    foreach ($categories as $category) {
        $slug                                                               = $category->slug;
        $feed_rule                                                          = sprintf('index.php?taxonomy=%s&term=%s&feed=$matches[1]', $taxonomy, $slug);
        $paged_rule                                                         = sprintf('index.php?taxonomy=%s&term=%s&paged=$matches[1]', $taxonomy, $slug);
        $base_rule                                                          = sprintf('index.php?taxonomy=%s&term=%s', $taxonomy, $slug);
        $category_rewrite[$slug . '/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$'] = $feed_rule;
        $category_rewrite[$slug . '/page/?([0-9]{1,})/?$']                  = $paged_rule;
        $category_rewrite[$slug . '/?$']                                    = $base_rule;
    }
    $category_rewrite['blog/(.*)$'] = 'index.php?category_redirect=$matches[1]';
    //var_dump($category_rewrite); // For Debugging
    return $category_rewrite;
}

add_filter('query_vars', 'no_category_base_query_vars');
function no_category_base_query_vars($public_query_vars)
{
    $public_query_vars[] = 'category_redirect';
    return $public_query_vars;
}

// Redirect if 'category_redirect' is set
add_filter('request', 'no_category_base_request');
function no_category_base_request($query_vars)
{
    // print_r($query_vars); // For Debugging
    if (isset($query_vars['category_redirect'])) {
        $catlink = trailingslashit(get_option('home')) . user_trailingslashit($query_vars['category_redirect'], 'category');
        status_header(301);
        header("Location: $catlink");
        exit();
    }
    return $query_vars;
}


add_filter('post_link', 'change_permalink', 10, 3);
add_filter('post_type_link', 'change_permalink', 10, 3);
function change_permalink($permalink, $post_id, $leavename)
{
    $slugName = apply_filters( 'editable_slug', $post_id->post_name, $post_id );
    if(empty($slugName)){
		return wp_get_shortlink($post_id->ID, 'post');
	}
    if (strpos($permalink, '%blog%') === FALSE)
        return $permalink;
    // Get post
    $post = get_post($post_id);
    if (!$post)
        return $permalink;
    // Get taxonomy terms
    $terms = wp_get_object_terms($post->ID, 'blog');
    if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0]))
        $taxonomy_slug = $terms[0]->slug;
    else
        $taxonomy_slug = 'not-categorized';
    $link = str_replace('%blog%', $taxonomy_slug, $permalink);
    return $link;
}
// end code for custom URLs

/**
 * Adds a box to the main column on the Post and Page edit screens.
 */
function add_custom_box()
{
    add_meta_box('sectionid', __('Add to Blog', 'textdomain'), 'blog_selection_box', 'post');
}
add_action('add_meta_boxes', 'add_custom_box');

// get blogs for a specific author to be displayed in the create post  
add_action('wp_ajax_getAuthoringBlogs', 'getAuthoringBlogs');
function getAuthoringBlogs(){
    $aid = $_POST['input'];
    $authoringBlogIds = get_user_meta($aid, 'authoringBlogIds', true);
    if(!empty($authoringBlogIds)){
    $authoringBlogs   = get_terms('blog', array(
        'include' => $authoringBlogIds,
        'hide_empty' => false,
        'fields' => 'names'
        ));
        echo json_encode($authoringBlogs);
    }else{
       echo json_encode(array());
    }
    die();
}
/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post.
 */
function blog_selection_box($post)
{
    // Add an nonce field so we can check for it later.
    wp_nonce_field('blog_selection_box', 'blog_selection_box_nonce');
    // extract the blogs the current user can contribute to 
    $authoringBlogIds = get_user_meta($post->post_author, 'authoringBlogIds', true);
    if(empty($authoringBlogIds)){
        echo '<select name="add_to_blog" id="add_to_blog"></select>';
        echo '<script type="text/javascript">document.getElementById("publish").disabled = true</script>';
    }else{
        $authoringBlogs   = get_terms('blog', array(
            'order' => 'ASC',
            'include' => $authoringBlogIds,
            'hide_empty' => false,
            'fields' => 'names'
            ));
        echo '<select name="add_to_blog" id="add_to_blog">';
        $blogTerm  = array_values(get_the_terms(get_the_ID(), 'blog' ))[0]; //[0];  
        if(!empty($blogTerm)){
            $selected = false;
            foreach ($authoringBlogs as $blog) {
                if($blogTerm->name == $blog){
                        echo '<option value="' . $blog . '" selected>' . $blog . '</option>';
                        $selected = true;
                } else {
                    echo '<option value="' . $blog . '">' . $blog . '</option>';
                }
            }   
        }else{
            $selected = false;
            foreach ($authoringBlogs as $blog) {
                if (!$selected) {
                    echo '<option value="' . $blog . '" selected>' . $blog . '</option>';
                    $selected = true;
                } else {
                    echo '<option value="' . $blog . '">' . $blog . '</option>';
                }
            }
        }
        echo '</select>';
        echo '<script type="text/javascript">document.getElementById("publish").disabled = false</script>';
    }
    if(current_user_can('administrator') || current_user_can('editor')){
        echo '<script type="text/javascript"> 
        jQuery( document ).ready(function() {
        var d = document.getElementById("authordiv");
        d.className = "postbox";
        var el = document.getElementById("post_author_override");
        el.addEventListener("change", function(e) { 
            var selectedAuthor = document.getElementById("post_author_override").value;
            jQuery.ajax({
              type: "POST",
              url: "' . site_url() . '/wp-admin/admin-ajax.php",
              dataType: "json",
              data: {
                action: "getAuthoringBlogs",
                input: selectedAuthor,
            },
            success: function(data, textStatus, XMLHttpRequest){
                document.getElementById("add_to_blog").innerHTML = "";
                var a = [];
                var selected = false;
                for (key in data ){
                    //alert(data[key]);
                    if(!selected){
                        a.push(\'<option value="\'+data[key]+\'" selected>\'+data[key]+\'</option>\');
                        selected = true;
                    }else{
                        a.push(\'<option value="\'+data[key]+\'">\'+data[key]+\'</option>\');
                    }
                }      
                document.getElementById("add_to_blog").innerHTML = a.join("");
                if(selected == false){
                    document.getElementById("publish").disabled = true;
                }else{
                    document.getElementById("publish").disabled = false;
                }
            },
            error: function(MLHttpRequest, textStatus, errorThrown){
                //TODO: do something;
            }
        })});
        });
        </script>';
    
    $hidedisclaimer = get_post_meta(get_the_ID(),'hidedisclaimer', true);
    if(isset($hidedisclaimer) && $hidedisclaimer=='1')
        echo '
        <p class="meta-options">
        <label for="hide_disclaimer" class="selectit"><input name="hide_disclaimer" type="checkbox" id="hide_disclaimer" value="open" checked="checked"> Hide Disclaimer Notice.</label><br>
        </p>';
    else 
        echo '
        <p class="meta-options">
        <label for="hide_disclaimer" class="selectit"><input name="hide_disclaimer" type="checkbox" id="hide_disclaimer" value="open"> Hide Disclaimer Notice.</label><br>
        </p>';

    $canUrl = get_post_meta(get_the_ID(),'canurl', true);
    if(!empty($canUrl))
        echo '
        <p class="meta-options">
        <label for="canurl" class="selectit">Canonical Url </label><input style="width: 600px;" name="canurl" type="text" id="canurl" value='.$canUrl.' /> <br>
        </p>';
    else 
        echo '
        <p class="meta-options">
        <label for="canurl" class="selectit">Canonical Url </label><input style="width: 600px;" name="canurl" type="text" id="canurl" /><br>
        </p>';
    
    }
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
// Comment migration to mytimes  
function save_post_as_entity_in_mytimes($post_id) {

    // If this is just a revision, don't send the add entity call.
    if ( wp_is_post_revision( $post_id ) ){
        //return;
    }
    
    $appKeyForMyTimes = MYT_COMMENTS_API_KEY;
    
    if ( has_post_thumbnail($post_id)) 
    { 
            $photo = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' )[0];
    }
    else
    {
        $post_author = get_post_field( 'post_author', $post_id );
        $photo = get_user_avatar($post_author);
    }

    $title = get_the_title( $post_id );
    $subtitle = get_post_meta($post_id, 'wps_subtitle', true );
    $headline = $subtitle ? $subtitle : $title;

    $specialCharToRemove = array('&lsquo;','&rsquo;','&zwj;','&zwnj;','!&nbsp;','&nbsp;','&mdash;','&ndash;','&amp;');
    $synopsys = strip_tags(get_the_excerpt($post_id));
    foreach ($specialCharToRemove as $k=>$value) 
    {
        $synopsys = str_replace($value,'', $synopsys);
    }
    
    global $my_settings;
    $photo = str_replace($my_settings['blogauthor_link'], $my_settings['blog_link'], $photo);
    $photo = str_replace($my_settings['old_blogauthor_link'], $my_settings['blog_link'], $photo);
    $articleLink = get_permalink( $post_id );
    $articleLink = str_replace($my_settings['blogauthor_link'], $my_settings['blog_link'], $articleLink);
    $articleLink = str_replace($my_settings['old_blogauthor_link'], $my_settings['blog_link'], $articleLink);

    $categories = get_the_category($post_id);
    $catIds = array();
    if($categories){
        foreach($categories as $category) {
            $catIds[] = $category->cat_ID;
        }
    }
    $catIdsList = implode(',', $catIds);
    $catName   = trim($categories[0]->cat_name);

    $xmlPost = array(
                'name'=>($headline),
                'id' => $post_id,
                'synopsys' =>$synopsys,
                'url' =>$articleLink,
                'type' =>'ARTICLE',
                'appKey'=>$appKeyForMyTimes,
                'cat'=>$catName,
                'scat'=>'',
                'parentmsidlist'=>$catIdsList,
                'imageUrl'=>$photo
            );
    $str = "entityDetail=".json_encode($xmlPost);
    $url = MYTIMES_URL.'mytimes/entity/add';
    //die;
    $curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Content-Length: ' . (strlen($str)), 'Accept-Encoding: gzip; deflate', 'Accept-Language: en-US');
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$str);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
    $ch_result = curl_exec($ch);
    //var_dump($ch_result);
    curl_close($ch);

    //Get Status for currently added Article Id
    /*$url = MYTIMES_URL."mytimes/entity?msid=".$post_id."&appKey=".$appKeyForMyTimes;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
    $search_result = curl_exec($ch);
    curl_close($ch);
    
    $resultArr = reset(json_decode($search_result,true));
    //var_dump($resultArr);
    if(count($resultArr)>0 && $resultArr['_id']!=''){
         update_post_meta( $post_id, 'mytimesId', $resultArr['_id']);
    }
    die;*/

}



add_action('save_post', 'save_postdata');
function save_postdata($post_id)
{
    // We need to verify this came from the our screen and with proper authorization,
    // Check if our nonce is set.
    if (!isset($_POST['blog_selection_box_nonce']))
        return $post_id;
    
    $nonce = $_POST['blog_selection_box_nonce'];
    
    // Verify that the nonce is valid.
    if (!wp_verify_nonce($nonce, 'blog_selection_box'))
        return $post_id;
    
    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
    
    // Check the user's permissions.
    if (!current_user_can('edit_post', $post_id))
        return $post_id;

    /* OK, its safe for us to save the data now. */
    if ( !empty( $_POST['add_to_blog']) ) {
        wp_set_post_terms($post_id, $_POST['add_to_blog'], 'blog');
        if(isset($_POST['hide_disclaimer']))
            update_post_meta( $post_id, 'hidedisclaimer', '1' );
        else
            update_post_meta( $post_id, 'hidedisclaimer', '0' );
        if(isset($_POST['canurl']) && !empty($_POST['canurl']))
            update_post_meta( $post_id, 'canurl', $_POST['canurl'] );
        else
            update_post_meta( $post_id, 'canurl', '' );
        if(isset($_POST['wps_subtitle']) && !empty($_POST['wps_subtitle']))
            update_post_meta( $post_id, 'wps_subtitle', wp_kses_post( $_POST['wps_subtitle'] ) );
        else
            update_post_meta( $post_id, 'wps_subtitle', '' );

        // now update 
        /*$ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,10); # timeout after 10 seconds, you can increase it
        curl_setopt($ch, CURLOPT_URL, RECO_URL.$post_id ); #set the url and get string together    
        $result = curl_exec($ch);
        //close connection
        curl_close($ch);*/    
    }
    save_post_as_entity_in_mytimes($post_id);
    return $post_id;
}
