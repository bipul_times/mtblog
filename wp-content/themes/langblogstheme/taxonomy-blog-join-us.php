<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

require_once(ABSPATH . WPINC . '/registration.php');  

$successmsg = "Thank you for reaching out to us. We'll get back to you shortly";

function validateAndSave(){
	global $_POST,$wpdb, $user_ID,$successmsg;  
	$msg = '';
	//Check whether the user is already logged in 
	//if (!$user_ID) {  
	if(isset($_POST['submit'])){  
        //We shall SQL escape all inputs  
		//echo 'here';
		$username = $wpdb->escape($_POST['username']);  
        //echo $username;
		if(!empty($_POST['username'])){
			if(!preg_match("/^[a-zA-Z]*$/", $username)) {  
				$msg = "Error : Username should contain only letters. No whitespaces or special characters allowed.";  
				return $msg;
			}
		}else{
			$msg = "Error : Please enter a desired username.";  
			return $msg;
		}  
		$email = $wpdb->escape($_POST['email']);  
		if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", $email)) {  
			$msg = "Error : Please enter a valid email.";  
			return $msg;
		}
		$user_id = username_exists( $username );
		if ( !$user_id and email_exists($email) == false ) {
			if(!empty($_POST['url'])){
				if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$_POST['url'])) {
 					$msg  = "Error : Invalid blog url"; 
					return $msg;
				}
			}
			if(!empty($_POST['firstname'])){
				if(!preg_match("/^[a-zA-Z]*$/", $_POST['firstname'])) {  
					$msg = "Error : Firstname should contain only letters. No whitespaces or special characters allowed.";  
					return $msg;
				}
			}else{
				$msg = "Error : Firstname should not be empty.";  
				return $msg;
			}

			if(!empty($_POST['lastname'])){
				if(!preg_match("/^[a-zA-Z]*$/", $_POST['lastname'])) {  
					$msg = "Error : Lastname should contain only letters. No whitespaces or special characters allowed.";  
					return $msg;
				}
			}else{
				$msg = "Error : Lastname name should not be empty.";  
				return $msg;
			}
			if(empty($_POST['description'])) {  
				$msg = "Error : More about you section should not be empty."; 
				return $msg;
			}
			$userdata = array(
				'user_login'  =>  $username ,
				'first_name' => $_POST['firstname'],
				'last_name' => $_POST['lastname'],
				'description' => $_POST['description'],
				'user_url'    =>  $_POST['url'],
				'user_email' => $email,
    			'user_pass'   =>  NULL,  // When creating an user, `user_pass` is expected
    		);
			$status = wp_insert_user( $userdata ) ;
		//$status = wp_create_user( $username, $random_password, $email );  
			if ( is_wp_error($status) )  
				$msg = "Error : Something went wrong. Please email us on Anshu.Tandon@timesinternet.in.";  
			else {  
				$msg = $successmsg;  
			}
        //exit();
		}else{
			$msg = "Error : This username/email is already registered. Please try another combination.";
		}
	}
	return $msg;
}
$msg = validateAndSave();
 /*}else{
 	wp_redirect( home_url() ); exit;  
 } */
 get_header();
 ?>  

 <div class="container">
 	<div class="row">
 		<div class="col-md-8">
 			<h1 class="blog-heading"> Join Us </h1>
 			<p class="description">TOI Blogs is an amazing platform to share quick takes, analysis and both macro and micro level views on things happening around us. Want to contribute and strengthen your online presence, reach out to us </p>
 			<div class="feeds">
 				<?php if(!empty($msg)) { if($msg == $successmsg) echo '<div class="msg">'.$msg.'</div>';else echo '<div class="msg errorm">'.$msg.'</div>'; } ?>	
 				<form action="" method="post">
 					<table class="form-table">
 						<tbody>
 							<tr class="form-field form-required">
 								<th scope="row"><label for="user_login">Desired username <span class="description">*</span></label></th>
 								<td><input name="username" type="text" id="username" value="<?php echo $_POST['username']; ?>" aria-required="true"></td>
 							</tr>
 							<tr class="form-field form-required">
 								<th scope="row"><label for="email">E-mail <span class="description">*</span></label></th>
 								<td><input name="email" type="text" id="email" value="<?php echo $_POST['email']; ?>"></td>
 							</tr>
 							<tr class="form-field">
 								<th scope="row"><label for="firstname">First Name <span class="description">*</span></label></th>
 								<td><input name="firstname" type="text" id="first_name" value="<?php echo $_POST['firstname']; ?>"></td>
 							</tr>
 							<tr class="form-field">
 								<th scope="row"><label for="lastname">Last Name <span class="description">*</span></label></th>
 								<td><input name="lastname" type="text" id="last_name" value="<?php echo $_POST['lastname']; ?>"></td>
 							</tr>
 							<tr class="form-field">
 								<th scope="row"><label for="url">Existing Blog Url </label></th>
 								<td><input name="url" type="text" id="url" class="code" value="<?php echo $_POST['url']; ?>"></td>
 							</tr>
 							<tr class="form-field">
 								<th scope="row"><label for="description">About Yourself <span class="description">*</span></label></th>
 								<td><textarea name="description" type="text" id="description"><?php echo $_POST['description']; ?></textarea></td>
 							</tr>
 						</tbody>
 					</table>
 					<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Submit Details"></p>
 				</form>
 			</div>
 		</div>
 		<div class="col-md-4 sidebar">
 			<div class="panel">
 				<div class="addwrapper">
 					<?php echo $my_settings['google_ad_right_1']; ?>
 				</div>
 			</div>
 			<div class="panel">
 				<div class="addwrapper">
 					<?php echo $my_settings['google_ad_right_2']; ?>
 				</div>
 			</div>
 		</div>
 	</div>
 </div> <!--container -->
 <?php
 get_footer();
