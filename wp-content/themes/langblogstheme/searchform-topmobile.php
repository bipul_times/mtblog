<?php global $my_settings; ?>
<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<label style=float:left;>
		<span class="screen-reader-text">Search for:</span>
		<input type="text" id="search-form-topmobile-field" class="search-field dropdown" placeholder="<?php //echo $my_settings['search_placeholder_txt']; ?>" value="" name="s" title="Search for:" autocomplete="off" style="height:30px;" />
		<ul class="dropdown-menu searchdd" role="menu">
		</ul>
	</label>
	<input type="submit" class="search-submit" value="<?php echo $my_settings['search_txt']; ?>" />
</form>
