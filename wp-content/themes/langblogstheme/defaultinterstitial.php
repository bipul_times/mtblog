<?php global $my_settings; ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<HTML>
   <HEAD>
      <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <TITLE><?php echo $my_settings['main_site_txt']; ?></TITLE>
      <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
      <meta content="IE=EmulateIE7" http-equiv="X-UA-Compatible">
      <style>.footer{font-size:11px; height:50px; border-top:1px dotted #343434}
         a{text-decoration:none; color:#039;font-size:11px;font-family:arial;}
         a:hover{text-decoration:underline; color:#039;font-size:11px;font-family:arial;}
         .copyright{font-size:11px;font-family:arial;}
      </style>
      <?php echo $my_settings['ga'];?>
      <script type="text/javascript">
		  function _getCookie(c_name) {
			var i,x,y;
			var cookieStr = document.cookie.replace("UserName=", "UserName&");
			var ARRcookies=cookieStr.split(";");

			for (i=0;i<ARRcookies.length;i++)
			{
				x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
				y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
				x=x.replace(/^\s+|\s+$/g,"");
				if (x==c_name)
				{
					return unescape(y);
				}
			 }
		  }

		  function _setCookie(cname, cvalue, exdays) {
			var d = new Date();
			d.setTime(d.getTime() + (exdays*24*60*60*1000));
			var expires = "expires="+ d.toUTCString();
			document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
		  }
		  var prevURL = _getCookie('prevurl');
	  </script>
   </HEAD>
   <body style="padding:0; margin:0;" bottommargin="10" align="center">
      <table align="center" border="0" cellspacing="0" cellpadding="0" width="850">
         <tr>
            <td>
               <table align="center" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td align="left"><a href="/"><img border="0" src="<?php echo $my_settings['logo_url']; ?>"></a></td>
                     <td style="padding-left:25px;font-family: Arial;font-size: 12px;" align="left"><?php echo $my_settings['main_site_txt']; ?> will load in a few seconds&nbsp;&nbsp;&nbsp;&nbsp;
                        |&nbsp;&nbsp;<a style="padding-left:12px;font-family: Arial;font-size: 12px;color:#000000" href="/">click here to go to <?php echo $my_settings['main_site_txt']; ?></a>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td height="2" colspan="2"></td>
         </tr>
         <tr>
            <td>
               <table align="center" width="728" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                     <td style="font-family:Arial;font-size:12px;" bgcolor="#dbdbdb" valign="middle" height="17" colspan="2" width="728" align="center">Advertisement</td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td height="3" colspan="2"></td>
         </tr>
         <tr>
            <td align="center" colspan="2"><?php echo get_option('web_ad_content'); ?></td>
         </tr>
         <tr>
            <td>
               <table align="center" cellpadding="0" cellspacing="0" border="0" width="728">
                  <tr>
                     <td height="5"></td>
                  </tr>
                  <div style="height:10px"></div>
                  <div style="padding-top:2px;" class="footer" align="center"><a href="http://info.indiatimes.com/terms/aboutus.html">About Us</a>&nbsp;|&nbsp;<a href="http://advertise.indiatimes.com/">Advertise with Us</a> 
                     | <a href="http://www.timesinternet.in/apply.cms">Careers @ TIL</a>&nbsp;|&nbsp;<a href="http://info.indiatimes.com/terms/tou.html">Terms 
                     of Use</a>&nbsp;|&nbsp;<a href="http://info.indiatimes.com/terms/privacy.html">Privacy 
                     Policy</a>&nbsp;|&nbsp;<a href="http://info.indiatimes.com/feedback/itfeedback.html">Feedback</a>&nbsp;|&nbsp;<a href="/sitemap.cms">Sitemap</a><br><span class="copyright">Copyright &#169;&nbsp;2016&nbsp;Bennett Coleman &amp; Co. Ltd. All 
                     rights reserved. For reprint rights: <a href="http://syndication.indiatimes.com/">Times Syndication Service</a><br>This site is best viewed with Internet Explorer 6.0 or higher; Firefox 2.0 or higher at a minimum screen resolution of 1024x768</span>
                  </div>
               </table>
            </td>
         </tr>
      </table>
      <script type="text/javascript">                
		if(_getCookie('intAd') === '1'){
			window.location.replace(prevURL);
		}
		else{
			_setCookie('intAd','1',1);
			// set interval
			setTimeout(function(){
				window.location.href = prevURL; 
			},9000)
		}
	  </script>
   </body>
</HTML>
