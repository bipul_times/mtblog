<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Blogs_Theme
 * @since Blogs Theme 1.0
 */
?>
<?php global $my_settings; ?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/css/gdpr.css?ver=1.1' ;?>" />		
<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/gdpr.js?ver=1.1"></script>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<?php
    $seconds_to_cache = 1800;
    if(is_single()){
        $seconds_to_cache = 3600;
    }
    $ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
    header("Cache-Control: public, max-age=$seconds_to_cache, must-revalidate");
    header("Connection: Keep-Alive");
    header("Keep-Alive: timeout=15, max=98");    
    header("Expires: $ts");
	header("pragma: cache");
	?>
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	
	

	<meta name="robots" content="NOODP" />
	<?php if($_SERVER['REQUEST_URI'] == '/blocked') { ?><META CONTENT="NOINDEX, NOFOLLOW" NAME="ROBOTS"><?php } ?>
	<?php /*<title><?php wp_title('-', true, 'right'); ?></title>*/ ?>
	<link type="image/x-icon" href="<?php echo $my_settings['favicon'];?>" rel="shortcut icon">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri()?>/js/html5shiv.js"></script>
    <script src="<?php echo get_template_directory_uri()?>/js/respond.min.js"></script>
    <![endif]-->
	<?php echo $my_settings['ga'];?>
	<script type="text/javascript">
		var $ = jQuery.noConflict();
		var site_url = '<?php echo site_url(); ?>';
		var curPage = '';
		var pgname = '';
		var _gaq = Object();
		var isMobile = window.matchMedia("only screen and (max-width: 760px)");
		var isQuillEnabled = true;
		if (isMobile.matches) {
			isQuillEnabled = false;
		}
	</script>
	<?php if(!isInitAd()) { echo $my_settings['google_tag_js_head']; } ?>
	<script type="text/javascript">
	function _getCookie(c_name) {
		var i,x,y;
		var cookieStr = document.cookie.replace("UserName=", "UserName&");
		var ARRcookies=cookieStr.split(";");

		for (i=0;i<ARRcookies.length;i++)
		{
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name)
			{
				return unescape(y);
			}
		 }
	}

	function _setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+ d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
	}
	</script>
</head>

<?php 
	$currentPage = (get_query_var('paged')) ? get_query_var('paged') : 1;
	if(array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'LIVE'){
		if (is_front_page() && $currentPage == 1) {
			//echo $my_settings['ctn_homepage_rhs'];
			if(array_key_exists('esi_header',$my_settings)){
				echo str_replace('##ESIIDS##',$my_settings['esi_homepage_ids'],$my_settings['esi_header']);
			}
		} else if(is_single()){
			//echo $my_settings['ctn_article_show_rhs'];
			if(array_key_exists('esi_header',$my_settings)){
				echo str_replace('##ESIIDS##',$my_settings['esi_article_show_ids'],$my_settings['esi_header']);
			}
		} else {
			//echo $my_settings['ctn_article_list_rhs'];
			if(array_key_exists('esi_header',$my_settings)){
				echo str_replace('##ESIIDS##',$my_settings['esi_article_list_ids'],$my_settings['esi_header']);
			}
		}
	}
?>

<body <?php body_class(); ?>>
<?php if ( !is_front_page() && $_SERVER['REQUEST_URI'] != '/blocked') { ?>
	<?php echo get_option('ad_blocker_js_code'); ?>
<?php } ?>
<?php if(!isInitAd()) { ?>
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    // init the FB JS SDK
    FB.init({appId: '<?php echo $my_settings['fbappid'];?>',oauth:true, status: true, cookie: true, xfbml: true});
    // Additional initialization code such as adding Event Listeners goes here
  };
  // Load the SDK asynchronously
  (function(){
     // If we've already installed the SDK, we're done
     if (document.getElementById('facebook-jssdk')) {return;}

     // Get the first script element, which we'll use to find the parent node
     var firstScriptElement = document.getElementsByTagName('script')[0];

     // Create a new script element and set its id
     var facebookJS = document.createElement('script'); 
     facebookJS.id = 'facebook-jssdk';

     // Set the new script's source to the source of the Facebook JS SDK
     facebookJS.src = '//connect.facebook.net/en_US/all.js';

     // Insert the Facebook JS SDK into the DOM
     firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
   }());
   
function checkLogin(){
	TimesGDPR.common.consentModule.gdprCallback(function(){
            if(window._euuser){
	require(["login"],function(login){
		login.init();
	});
	}});	
}

function callfunctiononload(){
	/*if(isQuillEnabled){
		var scrt=document.createElement('script');
		scrt.type = 'text/javascript';
		scrt.src='<?php echo get_template_directory_uri(); ?>/js/quill-compiled.js?ver=1';
		document.getElementsByTagName("head")[0].appendChild(scrt);
		
		// all cool browsers
		try { scrt.onload = applyingQuill; } catch(ex){}
		// IE 6 & 7
		scrt.onreadystatechange = function() {
			if (this.readyState == 'complete') {
				try { applyingQuill(); } catch(ex){}
			}
		}
	} else {
		try { applyingComments(); } catch(ex){}
	}*/
	if(isQuillEnabled){
		try { applyingQuill(); } catch(ex){}
	} else {
		try { applyingComments(); } catch(ex){}
	}
}
function applyingQuill(){
	try { applyingQuillComments(); } catch(ex){}
	if($('#search-form-field').length > 0){
		Quill.init('search-form-field', false, null, null, '<?php echo $my_settings['quill_lang']; ?>');
		Quill.setLanguage('search-form-field', '<?php echo $my_settings['quill_lang']; ?>');
	}
	if($('#search-form-top-field').length > 0){
		Quill.init('search-form-top-field', false, null, null, '<?php echo $my_settings['quill_lang']; ?>');
		Quill.setLanguage('search-form-top-field', '<?php echo $my_settings['quill_lang']; ?>');
	}
	/*if($('#search-form-topmobile-field').length > 0){
		Quill.init('search-form-topmobile-field', false, null, null, '<?php echo $my_settings['quill_lang']; ?>');
		Quill.setLanguage('search-form-topmobile-field', '<?php echo $my_settings['quill_lang']; ?>');
	}*/
}
$(function(){
	callfunctiononload();
	require(["tiljs/event"],function(event){
		event.subscribe("user.login",function(){
			$( '#timespointframe' ).attr( 'src', function () { return $( this )[0].src; } );
		});
		event.subscribe("user.logout",function(){
			$( '#timespointframe' ).attr( 'src', function () { return $( this )[0].src; } );
		});
	})
	
});
$(window).load(function() {
	var themeClass = '<?php echo implode(' ', get_body_class()); ?>';
	$('body').addClass(themeClass);
});

var showInitAd = '<?php echo get_option('show_ad'); ?>';
var initAdUrl = '';
if(_getCookie('intAd') !== '1'){
	if(isMobile.matches){
		if(showInitAd == 'Both' || showInitAd == 'WAP'){
			initAdUrl = '<?php bloginfo('url'); ?>/defaultinterstitial?v=mob';
		}
	} else {
		if(showInitAd == 'Both' || showInitAd == 'WEB'){
			initAdUrl = '<?php bloginfo('url'); ?>/defaultinterstitial';
		}
	}
}
if(initAdUrl){
	_setCookie('prevurl',window.location.href,1);
	window.location.replace(initAdUrl);
}
</script>
<?php
	if(isset($_GET['source']) && $_GET['source'] == 'app'){
		return;
	}else if(isset($_GET['frmapp']) && $_GET['frmapp'] == 'yes'){
            return;            
        } else {
		include 'custom-header.php';
	}
}
?>
