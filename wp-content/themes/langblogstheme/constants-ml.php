<?php 

define('Z_IMAGE_PLACEHOLDER', get_template_directory_uri()."/images/placeholder.png");
define('FB_PLACEHOLDER', get_template_directory_uri()."/images/blog-ml-facebook.png");
define('Z_AUTHOR_PLACEHOLDER', get_template_directory_uri()."/images/50.jpg");
define('RECO_URL','http://reco.indiatimes.com/Reco/RecoIndexer?eventType=update&hostid=1b&contentType=blog&msid=');
define('COMMENTS_URL','http://www.malayalamblogcmtapi.indiatimes.com/');
define('RCHID', '2147478027');
define('MYT_COMMENTS_API_KEY', 'MALB');
define('MYTIMES_URL','http://myt.indiatimes.com/');

global $my_settings;
$my_settings['site_lang'] = 'ml';
$my_settings['og_locale'] = 'ml_IN';
$my_settings['google_site_verification'] = 'zU5qA2HPdN2PFPyZd70q25_5NHXI8AE8AbqWNwXsoFA';
$my_settings['fblikeurl'] = 'https://www.facebook.com/pages/Malayalam-Samayam/1618522865080130';
$my_settings['favicon'] = 'https://malayalam.samayam.com/icons/malyalam.ico';
$my_settings['ga'] = "<script>

TimesGDPR.common.consentModule.gdprCallback(function(data) {
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-29031733-10', 'auto');
  var userCookie = document.cookie.match(/(?:\s)?ssoid=(\w+);?/);
  if(!!(userCookie)) {
	ga('set', 'userId', userCookie[1]);
  }
  if( TimesGDPR.common.consentModule.isEUuser() ){
  ga('set', 'anonymizeIp', true); 
  }
  ga('require', 'displayfeatures');
  ga('send', 'pageview');
  });
  </script>";
$my_settings['fbappid'] = '1452558595050529';
$my_settings['channel_url_part'] = 'channel=ml';
$my_settings['main_site_url'] = 'https://malayalam.samayam.com';
$my_settings['main_site_txt'] = 'Malayalam Samayam Blog';
$my_settings['fb_url'] = 'https://www.facebook.com/pages/Malayalam-Samayam/1618522865080130';
$my_settings['twitter_url'] = 'https://twitter.com/MSamayam';
$my_settings['twitter_handle'] = 'MSamayam';
$my_settings['google_url'] = 'https://plus.google.com/b/116736781617448904002/dashboard/overview/getstarted?hl=en&service=PLUS';
$my_settings['rss_url'] = 'https://malayalam.samayam.com/rssfeedsdefault.cms';
$my_settings['logo_title'] = 'Malayalam Samayam';
$my_settings['logo_url'] = 'https://malayalam.samayam.com/photo/48050800.cms';
$my_settings['footer_logo_txt'] = 'സമയം';
$my_settings['comscore_tag'] = '<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  var objComScore = { c1: "2", c2: "6036484" };
  TimesGDPR.common.consentModule.gdprCallback(function(data) {
    if( TimesGDPR.common.consentModule.isEUuser() ){
      objComScore["cs_ucfr"] = 0;
    }
  _comscore.push(objComScore);
  (function() {
	var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
	s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
	el.parentNode.insertBefore(s, el);
  })();
  });
</script>
<noscript><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6036484&amp;cv=2.0&amp;cj=1"></noscript>
<!-- End comScore Tag -->';
$my_settings['ibeat_channel'] = 'MalayalamBlog';
$my_settings['ibeat_host'] = 'blogs.malayalam.samayam.com';
$my_settings['ibeat_key'] = 'a28eb8efe7219fd665ca26ccf44c444';
$my_settings['ibeat_domain'] = 'malayalam.samayam.com';
$my_settings['footer_google_tag'] = "<!-- /7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_OP_Innov1 -->
<div id='div-gpt-ad-8337392800902-4'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-8337392800902-4'); });
</script>
</div>
<!-- /7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_OP_Inter -->
<div id='div-gpt-ad-8337392800902-5'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-8337392800902-5'); });
</script>
</div>
<!-- /7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_OP_Pop -->
<div id='div-gpt-ad-8337392800902-6'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-8337392800902-6'); });
</script>
</div>
<!-- /7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_OP_Shosh -->
<div id='div-gpt-ad-8337392800902-7'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-8337392800902-7'); });
</script>
</div>";
$my_settings['google_tag_js_head'] = "<script type='text/javascript'>
	var googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];
	(function() {
	var gads = document.createElement('script');
	gads.async = true;
	gads.type = 'text/javascript';
	var useSSL = 'https:' == document.location.protocol;
	gads.src = (useSSL ? 'https:' : 'http:') + 
	'//www.googletagservices.com/tag/js/gpt.js';
	var node = document.getElementsByTagName('script')[0];
	node.parentNode.insertBefore(gads, node);
	})();
	</script>
	
	<script type='text/javascript'>
	googletag.cmd.push(function() {

	<!-- Audience Segment Targeting -->
	var _auds = new Array();
	if(typeof(_ccaud)!='undefined') {
	for(var i=0;i<_ccaud.Profile.Audiences.Audience.length;i++)
	if(i<200)
	_auds.push(_ccaud.Profile.Audiences.Audience[i].abbr);
	}
	<!-- End Audience Segment Targeting -->
	
	<!-- Contextual Targeting -->
	var _HDL = '';
	var _ARC1 = '';
	var _Hyp1 = '';
	var _article = '';
	var _tval = function(v) {
	if(typeof(v)=='undefined') return '';
	if(v.length>100) return v.substr(0,100);
	return v;
	}
	<!-- End Contextual Targeting -->


googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_ATF_300', [300,250], 'div-gpt-ad-8337392800902-0').addService(googletag.pubads());
googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_BTF_300', [300,250], 'div-gpt-ad-8337392800902-1').addService(googletag.pubads());
googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_BTF_728', [728,90], 'div-gpt-ad-8337392800902-2').addService(googletag.pubads());
googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_MTF_300', [300,250], 'div-gpt-ad-8337392800902-3').addService(googletag.pubads());
googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_OP_Innov1', [1,1], 'div-gpt-ad-8337392800902-4').addService(googletag.pubads());
googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_OP_Inter', [1,1], 'div-gpt-ad-8337392800902-5').addService(googletag.pubads());
googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_OP_Pop', [1,1], 'div-gpt-ad-8337392800902-6').addService(googletag.pubads());
googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_OP_Shosh', [1,1], 'div-gpt-ad-8337392800902-7').addService(googletag.pubads());

googletag.pubads().setTargeting('sg', _auds).setTargeting('HDL', _tval(_HDL)).setTargeting('ARC1', _tval(_ARC1)).setTargeting('Hyp1', _tval(_Hyp1)).setTargeting('article', _tval(_article));
googletag.pubads().enableSingleRequest();
googletag.pubads().collapseEmptyDivs();
TimesGDPR.common.consentModule.gdprCallback(function(data) {
	if( TimesGDPR.common.consentModule.isEUuser() ) {
		googletag.pubads().setRequestNonPersonalizedAds(1);
	}
	googletag.enableServices();
} );
});
</script>";
$my_settings['google_ad_top'] = "<!-- /7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_BTF_728 -->
<div id='div-gpt-ad-8337392800902-2'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-8337392800902-2'); });
</script>
</div>";
$my_settings['google_ad_right_1'] = "<!-- /7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_ATF_300 -->
<div id='div-gpt-ad-8337392800902-0'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-8337392800902-0'); });
</script>
</div>";
$my_settings['google_ad_right_2'] = "<!-- /7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_BTF_300 -->
<div id='div-gpt-ad-8337392800902-1'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-8337392800902-1'); });
</script>
</div>";
$my_settings['visual_revenue_reader_response_tracking_script'] = "<!--Visual Revenue Reader Response Tracking Script (v6) -->
        <script type='text/javascript'>
            var _vrq = _vrq || [];
            _vrq.push(['id', 624]);
            _vrq.push(['automate', false]);
            _vrq.push(['track', function(){}]);
            (function(d, a){
                var s = d.createElement(a),
                x = d.getElementsByTagName(a)[0];
                s.async = true;
            s.src = 'http://a.visualrevenue.com/vrs.js';
            x.parentNode.insertBefore(s, x);
            })(document, 'script');
        </script>
        <!-- End of VR RR Tracking Script - All rights reserved -->";
$my_settings['visual_revenue_reader_response_tracking_script_for_not_singular'] = "<!--Visual Revenue Reader Response Tracking Script (v6) -->
        <script type='text/javascript'>
            var _vrq = _vrq || [];
            _vrq.push(['id', 624]);
            _vrq.push(['automate', true]);
            _vrq.push(['track', function(){}]);
            (function(d, a){
                var s = d.createElement(a),
                x = d.getElementsByTagName(a)[0];
                s.async = true;
            s.src = 'http://a.visualrevenue.com/vrs.js';
            x.parentNode.insertBefore(s, x);
            })(document, 'script');
        </script>
        <!-- End of VR RR Tracking Script - All rights reserved -->";
$my_settings['not_found_heading'] = ' കണ്ടെത്തിയില്ല';
$my_settings['not_found_txt'] = 'ഈ ലൊക്കേഷനിൽ ഒന്നും കണ്ടെത്താനായില്ല. സെർച്ച് ചെയ്താലോ?';
$my_settings['search_placeholder_txt'] = '3 അക്ഷരങ്ങൾ ടൈപ്പ് ചെയ്യുക';
$my_settings['go_to_the_profile_of_txt'] = '';
$my_settings['go_to_txt'] = '';
$my_settings['share_link_fb_txt'] = 'ലിങ്ക് ഫെയ്സ്ബുക്കിൽ ഷെയർ ചെയ്യുക';
$my_settings['share_link_twitter_txt'] = 'ലിങ്ക് ട്വിറ്ററിൽ ഷെയർ ചെയ്യുക';
$my_settings['share_link_linkedin_txt'] = 'ലിങ്ക് ലിങ്ക്ഡ് ഇന്നിൽ ഷെയർ ചെയ്യുക';
$my_settings['mail_link_txt'] = 'ലിങ്ക് മെയിൽ ചെയ്യുക';
$my_settings['read_complete_article_here_txt'] = 'പൂർണ്ണമായ ലേഖനം ഇവിടെ വായിക്കുക';
$my_settings['featured_txt'] = 'ഫീച്ചർ';
$my_settings['disclaimer_txt'] = ' നിരാകരണവ്യവസ്ഥ';
$my_settings['disclaimer_on_txt'] = 'ഈ ആർട്ടിക്കിൾ ടൈംസ് ഓഫ് ഇന്ത്യയുടെ മുഖപ്രസംഗത്തിൽ വന്നതാണ്';
$my_settings['disclaimer_off_txt'] = "മേൽപ്പറഞ്ഞ അഭിപ്രായങ്ങൾ എഴുത്തുകാരൻറേതു മാത്രമാണ്";
$my_settings['most discussed_txt'] = 'ഏറ്റവും ചർച്ച ചെയ്യപ്പെട്ടവ';
$my_settings['more_txt'] = 'കൂടുതൽ';
$my_settings['less_txt'] = 'കുറവ്';
//$my_settings['login_txt'] = 'ലോഗിൻ';
//$my_settings['logout_txt'] = 'ലോഗ് ഔട്ട്';
$my_settings['login_txt'] = 'Log In';
$my_settings['logout_txt'] = 'Log Out';
$my_settings['view_all_posts_in_txt'] = '';
$my_settings['home_txt'] = 'ഹോം';
$my_settings['blogs_txt'] = 'ബ്ലോഗുകൾ';
$my_settings['search_results_for_txt'] = 'തിരയൽ ഫലങ്ങൾ';
$my_settings['most_read_txt'] = 'ഏറ്റവും വായിക്കപ്പെട്ടവ';
$my_settings['popular_tags_txt'] = 'പോപ്പുലർ ടാഗുകൾ';
$my_settings['recently_joined_authors_txt'] = 'പുതുതായി ചേർന്ന എഴുത്തുകാർ';
$my_settings['like_us_txt'] = 'ലൈക്ക് ചെയ്യൂ';
$my_settings['author_txt'] = ' രചയിതാവ്';
$my_settings['popular_from_author_txt'] = 'ജനപ്രിയ പോസ്റ്റുകളുടെ സ്രഷ്ടാവ്';
$my_settings['search_authors_by_name_txt'] = 'എഴുത്തുകാരെ പേരുപയോഗിച്ച് തിരയുക';
$my_settings['search_txt'] = 'തിരയുക';
$my_settings['back_to_authors_page_txt'] = 'എഴുത്തുകാരുടെ പേജിലേക്ക് തിരികെ പോകാം';
$my_settings['no_authors_found_txt'] = 'എഴുത്തുകാർ ഇല്ല';
$my_settings['further_commenting_is_disabled_txt'] = 'കൂടുതൽ പ്രതികരണങ്ങൾ അസാധുവാക്കിയിരിക്കുകയാണ്';
$my_settings['comments_on_this_post_are_closed_now_txt'] = 'ഈ പോസ്റ്റിലെ അഭിപ്രായങ്ങൾ അവസാനിപ്പിച്ചിരിക്കുകയാണ്';
$my_settings['add_your_comment_here_txt'] = 'നിങ്ങളുടെ അഭിപ്രായം ഇവിടെ ചേർക്കുക';
$my_settings['characters_remaining_txt'] = 'ശേഷിക്കുന്ന പ്രതീകങ്ങൾ';
$my_settings['share_on_fb_txt'] = 'ഫെയ്സ്ബുക്കിൽ ഷെയർ ചെയ്യുക';
$my_settings['share_on_twitter_txt'] = 'ട്വിറ്ററിൽ ഷെയർ ചെയ്യുക';
$my_settings['sort_by_txt'] = 'സോർട്ട് ചെയ്യുക';
$my_settings['newest_txt'] = 'ഏറ്റവും പുതിയ';
$my_settings['oldest_txt'] = ' പഴയത്';
$my_settings['discussed_txt'] = 'ചർച്ച ചെയ്യപ്പെട്ട';
$my_settings['up_voted_txt'] = 'അപ്പ് വോട്ട് ചെയ്തു';
$my_settings['down_voted_txt'] = 'ഡൗൺ വോട്ട് ചെയ്തു';
$my_settings['be_the_first_one_to_review_txt'] = 'റിവ്യൂ ചെയ്യുന്ന ആദ്യ വ്യക്തിയാവൂ';
$my_settings['more_points_needed_to_reach_next_level_txt'] = 'അടുത്ത തലത്തിലേക്ക് എത്താൻ  കൂടുതൽ പോയിൻറ് ആവശ്യമാണ്';
$my_settings['know_more_about_times_points_txt'] = 'ടൈംസ് പോയിൻറുകളെക്കുറിച്ച് കൂടുതൽ അറിയുക';
$my_settings['know_more_about_times_points_link'] = 'http://www.timespoints.com/about/8606871469114261504';
$my_settings['badges_earned_txt'] = 'നേടിയ ബാഡ്ജുകൾ';
$my_settings['just_now_txt'] = 'ഇപ്പോൾ';
$my_settings['follow_txt'] = 'പിന്തുടരുക';
$my_settings['reply_txt'] = 'മറുപടി';
$my_settings['flag_txt'] = 'ഫ്ലാഗ്';
$my_settings['up_vote_txt'] = 'വോട്ട് അപ്പ്';
$my_settings['down_vote_txt'] = 'വോട്ട് ഡൗൺ';
$my_settings['mark_as_offensive_txt'] = 'പ്രകോപനപരമെന്ന് അടയാളപ്പെടുത്തുക';
$my_settings['find_this_comment_offensive_txt'] = 'ഈ പ്രതികരണം പ്രകോപനപരമാണോ?';
$my_settings['reason_submitted_to_admin_txt'] = 'നിങ്ങളുടെ കാരണം അഡ്മിന് സമർപ്പിച്ചു';
$my_settings['choose_reason_txt'] = 'നിങ്ങളുടെ കാരണം തെരഞ്ഞെടുത്ത് സബ്മിറ്റ് ബട്ടൺ അമർത്തുക. വേണ്ട നടപടിയെടുക്കാൻ മോഡറേറ്റർമാർക്ക് മുന്നറിയിപ്പു നൽകും';
$my_settings['reason_for_reporting_txt'] = 'റിപ്പോർട്ട് ചെയ്യാനുള്ള കാരണം';
$my_settings['foul_language_txt'] = 'മോശം ഭാഷ';
$my_settings['defamatory_txt'] = 'അപകീർത്തികരമായ';
$my_settings['inciting_hatred_against_certain_community_txt'] = 'ഒരു വിഭാഗത്തെ പ്രകോപിപ്പിക്കുന്നത്';
$my_settings['out_of_context_spam_txt'] = 'സന്ദര്ർഭത്തിന് യോജിക്കാത്തത്/സ്പാം';
$my_settings['others_txt'] = ' മറ്റുള്ളവ';
$my_settings['report_this_txt'] = 'ഇത് റിപ്പോർട്ട് ചെയ്യുക';
$my_settings['close_txt'] = 'ക്ലോസ് ചെയ്യുക';
$my_settings['already_marked_as_offensive'] = 'പ്രകോപനപരമെന്ന് നേരത്തേ മാർക്ക് ചെയ്തിരുന്നു';
$my_settings['flagged_txt'] = 'ഫ്ലാഗ് ചെയ്തു';
$my_settings['blogauthor_link'] = 'http://blogauthors.malayalam.samayam.com/';
$my_settings['old_blogauthor_link'] = 'http://blogauthors.malayalam.samayam.com/';
$my_settings['blog_link'] = 'http://blogs.malayalam.samayam.com/';
$my_settings['common_cookie_domain'] = 'samayam.com';
$my_settings['quill_lang'] = 'malayalam';
$my_settings['quill_link_1'] = 'ടൈപ്പ് ഇന്  മലയാളം ഇന് സ്ക്രിപ്റ്റ്';
$my_settings['quill_link_2'] = 'ടൈപ്പ് ഇന്  മംഗ്ലീഷ്';
$my_settings['quill_link_3'] = 'Write in English';
$my_settings['quill_link_4'] = 'വിര് ച്വല്  കീ ബോര് ഡ്';
$my_settings['footer'] = '<div class="container footercontainer"><div class="insideLinks"><h2>വാര് ത്തകള്  ഒറ്റനോട്ടത്തില് </h2><ul><li><a target="_blank" href="http://malayalam.samayam.com/cinema/articlelist/48237399.cms">സിനിമ </a></li><li><a target="_blank" pg="" href="http://malayalam.samayam.com/social/articlelist/47161637.cms">വൈറല്  </a></li><li><a href="http://malayalam.samayam.com/news/articlelist/48237651.cms" target="_blank" pg="fotkjlnk2">വാര് ത്തകള്  </a></li><li><a href="http://malayalam.samayam.com/lifestyle/articlelist/47096498.cms" target="_blank" pg="fotkjlnk3">ലൈഫ്സ്റ്റൈല്  </a></li><li><a href="http://malayalam.samayam.com/sports/articlelist/47337812.cms" target="_blank" pg="fotkjlnk4">സ്പോര് ട്സ് </a></li><li><a href="http://malayalam.samayam.com/spirituality/articlelist/47093814.cms" target="_blank" pg="fotkjlnk4">ആത്മീയം </a></li><li><a href="http://malayalam.samayam.com/jokes/articlelist/48213974.cms" target="_blank" pg="fotkjlnk5">ജോക്സ് </a></li><li><a href="http://malayalam.samayam.com/education/articlelist/47337849.cms" target="_blank" pg="fotkjlnk6">വിദ്യാഭ്യാസം </a></li><li><a href="http://malayalam.samayam.com/photo-gallery/photolist/47354467.cms" target="_blank" pg="fotkjlnk6">ഫോട്ടോ ഗ്യാലറി </a></li><li><a href="http://malayalam.samayam.com/video/videolist/47329248.cms" target="_blank" pg="fotkjlnk8">വീഡിയോ ഗ്യാലറി </a></li></ul></div><div class="newsletter"><div class="fottershareLinks"><h4>കണക്ട് ചെയ്തു കൊണ്ടിരിക്കൂ<br><strong>മലയാളം സമയം  </strong>ആപ്പിനൊപ്പം</h4><div class="fotterApplinks"><a href="https://play.google.com/store" target="_blank" class="andriod"></a></div></div></div><div class="timesotherLinks"><h2>ഞങ്ങളുടെ മറ്റു സൈറ്റുകള് </h2><a href="http://timesofindia.indiatimes.com/" target="_blank" pg="fottgwslnk1">Times of India</a>| <a target="_blank" href="http://economictimes.indiatimes.com/" pg="fottgwslnk2">Economic Times</a> | <a href="http://itimes.com/" target="_blank" pg="fottgwslnk16">iTimes</a>|<br><a href="http://vijaykarnataka.indiatimes.com/" target="_blank" pg="fottgwslnk4">Vijay Karnataka</a>| <a href="http://eisamay.indiatimes.com/" target="_blank" pg="fottgwslnk5">Ei Samay</a> | <a href="http://navgujaratsamay.indiatimes.com/" target="_blank" pg="fottngslnk5">Navgujarat Samay</a> | <a href="http://maharashtratimes.com/" target="_blank" pg="fottgwslnk6">महाराष्ट्र टाइम्स</a> |<a href="http://www.businessinsider.in/" target="_blank" pg="fottgwslnk7">Business Insider</a>| <a href="http://zoomtv.indiatimes.com/" target="_blank" pg="fottgwslnk8">ZoomTv</a> | <a href="http://boxtv.com/" target="_blank" pg="fottgwslnk11">BoxTV</a>| <a href="http://www.gaana.com/" target="_blank" pg="fottgwslnk12">Gaana</a> | <a href="http://shopping.indiatimes.com/" target="_blank" pg="fottgwslnk13">Shopping</a> | <a href="http://www.idiva.com/" target="_blank" pg="fottgwslnk14">IDiva</a> | <a href="http://www.astrospeak.com/" target="_blank" pg="fottgwslnk15">Astrology</a> |
				<a target="_blank" href="http://www.simplymarry.com/" pg="fottgwslnk17">Matrimonial</a><span class="footfbLike">മലയാളം സമയം ഫെയ്സ്ബുക്കില് </span><iframe width="260px" height="35" frameborder="0" src="http://www.facebook.com/plugins/like.php?href=https%3a%2f%2fwww.facebook.com%2fpages%2fMalayalam-Samayam%2f1618522865080130&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35&amp;colorscheme=dark" scrolling="no" allowtransparency="true"></iframe></div><div class="timesLinks"><a target="_blank" href="http://www.timesinternet.in/" pg="fotlnk1">About Us</a>&nbsp;|&nbsp;
				<a target="_blank" href="https://www.ads.timesinternet.in/expresso/selfservice/loginSelfService.htm">Create Your Own Ad</a>|
				<a target="_blank" href="http://advertise.indiatimes.com/" pg="fotlnk2">Advertise with Us</a>|
				<a target="_blank" href="http://www.indiatimes.com/termsandcondition" pg="fotlnk3">Terms of Use and Grievance Redressal Policy</a>&nbsp;|&nbsp;
				<a target="_blank" href="http://www.indiatimes.com/privacypolicy" pg="fotlnk4">Privacy Policy</a>&nbsp;|&nbsp;
				
				<a target="_blank" href="http://malayalam.samayam.com/sitemap.cms" pg="fotlnk6">Sitemap</a></div><div class="copyText">Copyright &copy;&nbsp; '.date('Y').'
        &nbsp;Bennett Coleman &amp; Co. Ltd. All rights reserved. For reprint rights: <a target="_blank" href="http://timescontent.com/" pg="fotlnk7">Times Syndication Service</a></div></div>';
$my_settings['footer_css'] = '<style type="text/css">
.footercontainer{background:#333333; clear:both; overflow:hidden; padding:10px 0;}
.footercontainer a{color:#FDFDFD;}
.footercontainer h2{ color:#fff; font-size:20px; line-height:30px; padding:0 0 10px 2px;font-family: Arial, Helvetica, sans-serif;}

.insideLinks{ width:36%; padding-left:2%; float:left;}
.insideLinks ul{ list-style:none; display: inline-block; padding: 0; margin: 0}
.insideLinks ul li{ float:left; margin:0 10px 0 0; width:90px; }
.insideLinks ul li a{ font-family:Arial, Helvetica, sans-serif;color:#B5B3B4;font-size:14px;font-weight:bold;line-height:25px;}

.newsletter{width:32%; padding:0 2%; float:left;}
.newsletterpost {background:#CCCCCC; padding:14px; margin:10px 0 10px 0;float:left; width:300px;}

.newsletterpost input[type="text"]{background:#fff; border:1px #B7B7B7 solid; color:#333;height:21px; width:205px; font-size:14px;font-weight:bold;color:#999;float:left; padding:2px 10px;}
.newsletterpost input[type="submit"]{ background:#f5cc10; color:#000; padding:0 5px; *padding:0 4px; height:26px; cursor:pointer; border:none;font-weight:bold;float:left;}
.fottershareLinks span{ font-size:14px; color:#fff; width:100%; float:left; margin-bottom:10px;}
.fottershareLinks img{ vertical-align:middle;margin-top:5px;}
.fottershareLinks a{margin: 0 2px;}

.fotterApplinks{padding: 0 0 15px 0;}
.fotterApplinks a{padding: 0 10px 0 0; background-image:url(http://navbharattimes.indiatimes.com/photo/42711833.cms); float:left; width:47px; height:65px;background-repeat: no-repeat;}
.fotterApplinks a.andriod{background-position:0 4px}
.fotterApplinks a.andriod:hover{background-position:0 -68px}

.fotterApplinks a.ios{background-position: 0 -144px;}
.fotterApplinks a.ios:hover{background-position:0 -219px}

.fotterApplinks a.java{background-position:0 -296px;}
.fotterApplinks a.java:hover{background-position:0 -378px;}

.fotterApplinks a.win{background-position:0 -447px}
.fotterApplinks a.win:hover{background-position:0 -526px}

.fottershareLinks{clear: both;}
.fottershareLinks h4{color:#fff; font-size:16px;line-height: 23px;}
.fottershareLinks strong{color:#f5cc10; font-weight:normal}

.footfbLike{display: block;margin: 25px 0 12px 0;font-size: 14px;color: #fff;}

.timesotherLinks{width:32%; float:left; padding-right:2%; color:#CCCCCC;font-size:12px;}
.timesotherLinks a{ font-size:11px; padding:0 1px; color:#CCCCCC; line-height:18px;}

.timesLinks{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:10px 0; border-top:1px solid #474747;}
.timesLinks a {color:#FDFDFD; font-family:Arial;}

.copyText{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:0 35px;}
.copyText a {color:#FDFDFD; font-family:Arial;}

.seoLinks{clear:both; margin:10px 0; background:#585757; overflow:hidden;padding-left: 11px;}
.seoLinks ul{list-style:none; padding:0;}
.seoLinks ul li{float:left;}
.seoLinks ul li a{font-family:arial;padding:0 7px;color:#fff;font-size:13px; line-height:32px; display:block; float:left; border-left:#4a4a4a 1px solid;  border-right:#696969 1px solid;}
@media (max-width: 767px){.insideLinks,.newsletter,.timesotherLinks,.seoLinks,.timesLinks{display: none;}}
</style>';
