
<style>
.user-controls {
display: inline;
}
.toi_nav_sprite {
  background-image: url(https://timesofindia.indiatimes.com/photo/26906357.cms);
  background-repeat: no-repeat;
}
.search-submit{
  /*display: none;*/
  margin-bottom:5px !important;
  margin-left:5px;
  padding:5px !important;
}
.search-form{
  overflow: hidden;
}
.search-field{
height:20px;
border: 1px solid rgba(0, 0, 0, 0.1);
border-radius: 2px;
color: #2b2b2b;
padding: 0px 10px;
-webkit-box-sizing: border-box;
-moz-box-sizing: border-box;
box-sizing: border-box;
font-size: 80%;
margin: 0;
vertical-align: baseline;
max-width: 150px;
}
#navigation {
background: #ebeedd;
position: relative;
display: none;
}

.navbar-btn {
display: block;
background: #3d9ae8 !important;
height: 35px;
}

.style_nbt .navbar-btn{background: #f5cc10 !important;}
.navbar-btn a {
background-color: #3d9ae8 !important;
width: 40px;
display: block;
text-indent: -2000px;
overflow: hidden;
padding: 5px 0px 0px;
margin: 0;
float: left;
}
.style_nbt .navbar-btn a{background: #f5cc10 !important;}
.navbar-btn hr{
  height: 3px;
  background: white;
  border: white;
  margin: 4px 6px;
}
.style_nbt .navbar-btn hr{background: #000;}
nav ul, nav li {
margin: 0;
list-style: none;
list-style-image: none;
}
#navigation ul li {
border-right: 1px solid #cad2a7;
border-bottom: 1px solid #cad2a7;
border-left: 1px solid #cad2a7;
}

#navigation ul li a {
  display: inline-block;
  padding: 0 13px;
  line-height: 37px;
  color: #3d9ae8;
}
.style_nbt #navigation ul li a{color: #000 !important;}

#navigation .searchBox .txtBox {
border: 0;
font: 130% /35px 'Dosis',sans-serif;
height: 35px;
text-transform: uppercase;
padding: 0 4%;
width: 91%;
position: relative;
z-index: 1;
line-height: 16px;
}

#navigation .searchBox {
width: 37px;
height: 36px;
cursor: pointer;
right: 0;
background: url(/photo/28467421.cms) no-repeat right -513px;
border: 0 solid #555;
top: 0;
}
.s{
  cursor: pointer;
}
.s:before{
  color: #fff;
  content: '\f400';
  -webkit-font-smoothing: antialiased;
  font: normal 25px/1 'Genericons';
  position: relative;
  font-weight: bold;
}
.style_nbt .s:before{color:#000;}
.toi_nav_sprite {
  background-image: url(https://timesofindia.indiatimes.com/photo/26906357.cms);
  background-repeat: no-repeat;
}
.mobile_blog_head{color: white;padding-top: 3px;display: block;font-size: 20px;font-weight: bold;font-family: arial sans-serif;margin-left: 40px;width: 150px;}
.style_nbt .mobile_blog_head{color: #000;}
.mobile_loginout_container{float:left;color:white;font-size:13px;padding-top: 4px;}
.style_nbt .mobile_loginout_container{color: #000;}

.style_eisamay .navbar-btn{background: #f9a11b !important;}
.style_eisamay .navbar-btn a{background: #f9a11b !important;}
.style_eisamay .navbar-btn hr{background: #000;}
.style_eisamay #navigation ul li a{color: #000 !important;}
.style_eisamay .s:before{color:#000;}
.style_eisamay .mobile_blog_head{color: #000;}
.style_eisamay .mobile_loginout_container{color: #000;}

.style_vk .navbar-btn{background: #000 !important;}
.style_vk .navbar-btn a{background: #000 !important;}
.style_vk .navbar-btn hr{background: #fff;}
.style_vk #navigation ul li a{color: #000 !important;}
.style_vk .s:before{color:#fff;}
.style_vk .mobile_blog_head{color: #fff;}
.style_vk .mobile_loginout_container{color: #fff;}

.style_ml .navbar-btn{background: #00695c !important;}
.style_ml .navbar-btn a{background: #00695c !important;}
.style_ml .navbar-btn hr{background: #fff;}
.style_ml #navigation ul li a{color: #000 !important;}
.style_ml .s:before{color:#fff;}
.style_ml .mobile_blog_head{color: #fff;}
.style_ml .mobile_loginout_container{color: #fff;}

.style_tm .navbar-btn{background: #bf4707 !important;}
.style_tm .navbar-btn a{background: #bf4707 !important;}
.style_tm .navbar-btn hr{background: #fff;}
.style_tm #navigation ul li a{color: #000 !important;}
.style_tm .s:before{color:#fff;}
.style_tm .mobile_blog_head{color: #fff;}
.style_tm .mobile_loginout_container{color: #fff;}

.style_tl .navbar-btn{background: #c00200 !important;}
.style_tl .navbar-btn a{background: #c00200 !important;}
.style_tl .navbar-btn hr{background: #fff;}
.style_tl #navigation ul li a{color: #000 !important;}
.style_tl .s:before{color:#fff;}
.style_tl .mobile_blog_head{color: #fff;}
.style_tl .mobile_loginout_container{color: #fff;}

.style_ngs .navbar-btn{background: #003d76 !important;}
.style_ngs .navbar-btn a{background: #003d76 !important;}
.style_ngs .navbar-btn hr{background: #fff;}
.style_ngs #navigation ul li a{color: #000 !important;}
.style_ngs .s:before{color:#fff;}
.style_ngs .mobile_blog_head{color: #fff;}
.style_ngs .mobile_loginout_container{color: #fff;}
</style>

<div class="container" id="navigation">
  <div class="row">
    <nav class="clearfix" style='max-width: 1003px;margin: 0 auto;overflow:hidden;position:relative;'>
    <div class="navbar-btn">
      <a id="openNavbarMenu" onClick='showTabs(this)'>
        <hr><hr><hr>
      </a>
      <span class="mobile_blog_head"><?php echo $my_settings['blogs_txt']; ?></span>
    </div>
  <div style="position: absolute;right: 50px;top: 4px">
    <div id="tpres" class="mobile_loginout_container">
      <span data-plugin="user-isloggedin" class="hide">  <!-- span visible when user is not logged in -->
        <span data-plugin="user-name"></span> | 
        <span data-plugin="user-logout" style="cursor:pointer"><?php echo $my_settings['logout_txt']; ?></span>
      </span>
      <span data-plugin="user-notloggedin" class="hide pointer"> <!-- span visible when user is not logged in -->
        <span data-plugin="user-login" style="cursor:pointer"><?php echo $my_settings['login_txt']; ?></span>
      </span>
    </div>
  </div>
  <a class="s" style="position: absolute;right: 4px;top:6px;z-index: 100;" onClick='showSearch2(this)'></a>
  <ul class="clearfix" id="menutabs" style="display:none;padding: 0px;">
    <li><a id="mobileBloglink" href="<?php echo home_url();?>"><?php echo $my_settings['blogs_txt']; ?></a></li>
    <?php
		foreach($main_menus as $menu){
			echo '<li><a href="'.esc_url( $menu->url ).'" title="'.$my_settings['view_all_posts_in_txt'].' '.$menu->title.'">'.$menu->title.'</a> </li>';
		}
    ?>
  </ul>
  </nav>
  <div id='sform2' class="mobile-search-form hide-top-search" pg="Blog_MobNav_Pos#search">
        <?php get_template_part( 'searchform', 'topmobile' ); ?>
  </div>
  </div>
</div>

<script>
  var menuHidden = true;
  function showTabs(e){
    var evt = e ? e:window.event;
    if (evt.preventDefault) evt.preventDefault();
    if(menuHidden){
      menuHidden = false;
      document.getElementById('menutabs').style.display = 'block'; 
    }else{
      menuHidden = true;
      document.getElementById('menutabs').style.display = 'none';
    }
  }
  function showSearch(e){
    var evt = e ? e:window.event;
    if (evt.preventDefault) evt.preventDefault();
    if($('#sform').hasClass('hide-top-search')){
      $('#sform').removeClass('hide-top-search');
      //document.getElementById('search-form-top-field').focus()
      //Quill.setFocus('search-field');
    }else{
      $('#sform').addClass('hide-top-search');
    }
  }

  function showSearch2(e){
    var evt = e ? e:window.event;
    if (evt.preventDefault) evt.preventDefault();
    if($('#sform2').hasClass('hide-top-search')){
      $('#sform2').removeClass('hide-top-search');
    }else{
      $('#sform2').addClass('hide-top-search');
    }
  }
</script>
