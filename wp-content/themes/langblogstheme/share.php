<?php
	global $my_settings;
	$lang = isLang();
	
	$sites = $_GET['sites'];
	$status = $_GET['status'];
	
	if(strpos($sites, ',') !== false){
		$siteArr = explode(',', $sites);
		$site = $siteArr[0];
	} else if(strlen($sites) > 0){
		$site = $sites;
	} else {
		$site = $_GET['site'];
	}
	
	$content = $_GET['content'];
	$link = $_GET['link'];
	$title = $_GET['title'];
	$domain = $_GET['domain'];
	$desc = $_GET['desc'];
?>
<!DOCTYPE HTML >
    <html>
        <head>
            <title>Share your comment - <?php echo $my_settings['main_site_txt'];?></title>
            <meta content="width=device-width, minimum-scale=1" name="viewport"/>
            <style type="text/css">
                body{font-family:Tahoma;margin:0;padding:0;color:#7b706f;}
                .clear{clear:both}
                
                #facebookShare .header{
                    background-color: #6b84b5;
                    padding: 10px 0 8px 11px;
                    font-size: 12px;
                    color: #ffffff;
                    font-weight: bold;
                }
                
                #facebookShare .header .facebook;Icon{margin:0 5px 0 0;}
                
                #facebookShare .content{overflow:auto;padding:15px;}
                #facebookShare .content .imgBlock{float:left; margin-right:5px;}
                #facebookShare .content .imgBlock img{width:50px;height:50px;}
                #facebookShare .content .rightBlock{float:left;margin:7px;}
                #facebookShare .content .rightBlock h2{font-size: 12px; padding: 0;margin:0;font-weight: bold;}
                
                #facebookShare .content .contentText{width: 455px; height: 40px;resize:none;}
                
                #facebookShare .content .link{font-weight:bold;}
                #facebookShare .content .title{font-weight:bold;}
                #facebookShare .content .article{font-size:11px;}
                 #facebookShare .content .article p span{padding-left:5px;}
                #facebookShare .content .articleMeta{width: 455px; height: auto;}
                
                #facebookShare .footer{background-color: #eff7ef;padding: 7px 6px 9px 0;border-top: 1px solid #cccccc;text-align: right;position: fixed;bottom: 0px;width: 100%;}
                #facebookShare .footer img{margin-right:10px;}
                
                
                
                #twitterShare .header{background-color:#172A31;}
                
                #twitterShare .content{background-color:#d8ecf7;padding:25px;}
                #twitterShare .content .twitter_name{float:right;margin-bottom:10px;font-size:12px;}
                #twitterShare .content .lurl{font-size:12px;}
                #twitterShare .content .link{font-weight:bold;margin-left:10px;}
                #twitterShare .content .contentTextWrapper{padding: 15px 12px;text-decoration: none;width: 446px;height: 63px;background: url(https://timesofindia.indiatimes.com/photo/6566859.cms) no-repeat;margin: 0 0 13px 30px;}
                #twitterShare .content .contentText{width: 446px;height: 63px;border:none;resize:none;outline:none;}
                
                #twitterShare .footer{background-color:#d8ecf7;overflow:auto;padding:19px 0 18px 0;}
                #twitterShare .footer img{float:right;margin-right:20px;}
                
                
                .status{margin:10px;}
                /*.status .close{text-decoration:underline;color:#1A0DAB;cursor:pointer;}*/
                
                @media screen and (max-width:567px){
                #facebookShare .content .articleMeta{width:auto;}
                #facebookShare .content .contentText{width:374px;}
                #twitterShare .content .contentTextWrapper{background:none; width:426px; height:auto; padding:0; margin-left:0;}
                #twitterShare .content .contentText{width:100%; border:2px solid #3ca2c8;}
                #twitterShare .header img{width:100%; min-height:38px;}
                #twitterShare .link{word-break:break-all; display:block;}
                
                }
                
                @media screen and (max-width:479px){
                #facebookShare .content .rightBlock{float:none;}
                #facebookShare .content .contentText{width:220px;}
                #twitterShare .content .contentTextWrapper{width:290px;}
                #twitterShare .content{padding:25px 10px;}
                }
                
            </style>
        </head>
        <body>
            <script type="text/javascript">
                document.domain = "<?php echo $my_settings['common_cookie_domain']; ?>";
                window.resizeTo(600,420);
            </script>

            <?php if($status == 'success') { ?>
				<div class="status">
					<!-- Your comment was posted successfully.
					<br/><button class="close">Close</button> -->
				</div>
			<?php } else if($status == 'failure') { ?>
				<div class="status">
					<!-- Failed to post your comment.
					<br/><button class="close">Close</button> -->
				</div>
			<?php } else if($status == 'parammissing') { ?>
				<div class="status">
					<!-- Failed to post your comment.
					<br/><button class="close">Close</button> -->
				</div>
			<?php } ?>
			<?php if($site == 'facebook') { ?>
				<div id="facebookShare">
					<div class="header">
						<img class="facebookIcon" align="left" title="Facebook" alt="Facebook" src="https://timesofindia.indiatimes.com/photo/6559656.cms" />
						<span>Publish to your wall and your friends' home pages?</span>
					</div>
					<div class="content">
						<div class="imgBlock">
							<img class="facebook_image" src="https://timesofindia.indiatimes.com/photo/6559750.cms" />
						</div>
						<div class="rightBlock">
							<h2>What's on your mind?</h2>
							<textarea class="contentText" >
								<?php echo $content; ?>
							</textarea>
							<div class="article">
								<div class="imgBlock">
									<img class="picture" src="<?php echo FB_PLACEHOLDER; ?>" />
								</div>
								<div class="articleMeta">
									<div >
										<a class="title link">
											<a href="<?php echo $link; ?>"><?php echo $title; ?></a>
										</a>
									</div>
									<div class="domain"><?php echo $domain; ?></div>
									<div class="desc"><?php echo $desc; ?></div>
								</div>
								<p class="clear">
									<?php echo $my_settings['favicon'];?>
									<span>Not Published Yet via <?php echo $my_settings['main_site_txt'];?></span>
								</p>
								<p class="clear">By publishing, you are agreeing to the <a class="sml" target="_blank" href="http://www.facebook.com/terms.php">Facebook Terms of Use.</a></p>
							</div>
						</div>
					</div>
					<div class="footer">
						<img class="submit" title="Publish" alt="Publish" src="https://timesofindia.indiatimes.com/photo/6559660.cms" />
						<img class="close" title="Skip" alt="Skip" src="https://timesofindia.indiatimes.com/photo/6559662.cms" />
					</div>
				</div>
			<?php } else if($site == 'twitter') { ?>
				<div id="twitterShare">
                        
					<script>
						(function(){
							var content = window.opener.newShareData?window.opener.newShareData.message:'';
							var link = window.opener.newShareData?window.opener.newShareData.link:'';
							if(window.opener && window.opener.newLogin){
								location.href = 'https://twitter.com/intent/tweet?text='+content+' - '+link+'&original_referer=http://'+location.hostname+'/phpshare';
								document.getElementById("twitterShare").style.display="none";
							}
						}())
					</script>

					<div class="header">
						<img src="https://timesofindia.indiatimes.com/photo/6566828.cms" />
					</div>
					<div class="content">
						<div class="twitter_name"></div>
						<div class="contentTextWrapper clear">
							<textarea class="contentText" maxlength="116">
								<?php echo $content; ?> - <?php echo $link; ?>
							</textarea>
						</div>
						<div class="lurl">
							<span>You are sharing</span>
							<span class="link"><?php echo $link; ?></span>
						</div>
					</div>
					<div class="footer">
						<img class="close" title="Skip" alt="Skip" src="https://timesofindia.indiatimes.com/photo/8178937.cms" />
						<img class="submit" title="Post to Twitter" alt="Post to Twitter" src="https://timesofindia.indiatimes.com/photo/6566936.cms" />
					</div>
				</div>
			<?php } ?>
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                window.page_params = {
                    sites:"<?php echo $sites; ?>",
                    site:"<?php echo $site; ?>"
                   };
            </script>
            <script type="text/javascript">
                $(function(){
                    function post(url,data){
                        //Create the form in a jQuery object
                        var form = $("<form action='"+url+"' method='post'></form>");
                        //Get the value from the asp textbox with the ID txtBox1 and POST it as b1
                        $.each(data, function(k,v){
                            form.append($("<input type='hidden' name='"+k+"' />").attr('value',v));
                        });
                
                        //Add the form to the body
                        form.appendTo('body')
                            //Submit the form which posts the variables into the iframe
                            .submit();
                    }
                
               
            
                    $("#facebookShare .submit").on("click",function(){
                        var message =$("#facebookShare .contentText").val();
                        
                         var params = {
                            channel: "<?php echo $lang; ?>",
                            message: message,
                            link:$("#facebookShare .link").attr("href"),
                            picture:$("#facebookShare .picture").attr("src"),
                            name:$("#facebookShare .title").text(),
                            caption:$("#facebookShare .domain").text(),
                            source:$("#facebookShare .link").attr("href"),//todo verify if this is correct
                            description:$("#facebookShare .desc").text(),
                            popupenabled:"1"
                        };
                        
                        //document.location.href = "http://socialappsintegrator.indiatimes.com/socialsite/facebookpost?" + $.param(params);
                        
                        post("http://socialappsintegrator.indiatimes.com/socialsite/facebookpost",params);
                    });
                    
                     $("#twitterShare .submit").on("click",function(){
                        var message =$("#twitterShare .contentText").val();
                        var link =$("#twitterShare .link").text();
                        
                        var params = $.param({
                            //channel: "<?php echo $lang; ?>",
                            message: message.substring(0,116),
                            link:link
                        });
                        
                        document.location.href = "http://socialappsintegrator.indiatimes.com/socialsite/twitterpost?" + params;
                    });
                    
                    window.selfClose = function(){
                        var sites = window.opener && window.opener.newShareData?window.opener.newShareData.sites:null;
                        var site = window.page_params?window.page_params.site:null;
                        
                        if(sites && sites.indexOf(",") > 0){
                            var sites_next = sites.replace(site + ",", "");
                            window.opener.newShareData.sites = sites_next; // todo find better way to get sites
                            window.location.href = "http://"+location.hostname+"/phpshare/?sites=" + sites_next;
                        }else{
                            window.opener.closeWindow(window.name,true);
                        }
                    }
                    
                    /*
                    setInterval(function(){
                        var closeCount = parseInt($(".status .closeCount").text(),10);
                        if(closeCount <=0){
                            selfClose();
                        }else{
                            $(".status .closeCount").text(--closeCount);
                        }
                        
                    },1000);
                    */
                    
                    $(".close").on("click",function(){
                        selfClose();
                    })
                    
                    $( window ).resize(function() {
                        window.resizeTo(600,420);
                    });
                    
                    
                    if(window.opener && window.opener.newShareData){
                        var data = window.opener.newShareData;
                    
                        $("#facebookShare .contentText").text(data.message);
                        $("#twitterShare .contentText").text(data.message.substr(0,116));
                        $("#facebookShare .link").attr("href",data.link);
                        $("#twitterShare .link").text(data.link);
                        $("#facebookShare .picture").attr("src",data.picture);
                        $("#twitterShare .title,#facebookShare .title").text(data.title);
                        $("#twitterShare .domain,#facebookShare .domain").text(data.domain);
                        $("#twitterShare .desc,#facebookShare .desc").text(data.desc);
                        
                        if(window.opener.newShareData.facebook){
                            var facebook = window.opener.newShareData.facebook;
                            if(facebook.image){
                                $("#facebookShare .facebook_image").attr("src",facebook.image);
                            }
                            if(facebook.name){
                                $("#facebookShare .facebook_name").text(facebook.name);
                            }
                        }
                        
                        if(window.opener.newShareData.twitter){
                            var twitter = window.opener.newShareData.twitter;
                            if(twitter.image){
                                $("#twitterShare .twitter_image").attr("src",twitter.image);
                            }
                            if(twitter.name){
                                $("#twitterShare .twitter_name").text("signed in as @" + twitter.name);
                            }
                        }
                    }
                    
                    
                    
                });
            </script>
            
            <?php if($status == 'success' || $status == 'failure' || $status == 'signinfailure' || $status == 'parammissing') { ?>
                <script type="text/javascript">
                    $(function(){
                        window.selfClose();
                    });
                </script>
            <?php } ?>
            
        </body>
    </html>
