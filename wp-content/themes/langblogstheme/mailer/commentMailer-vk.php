<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>

<head>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Indiatimes Vijay Karnataka Newsletter</title>
    <style>
        body,
        div,
        img,
        p {
            margin: 0;
            padding: 0;
            font-family: ARIAL UNICODE MS, mangal, raghu8;
            font-size: 17px;
        }
        
        body {
            background: #f2f2f2;
        }
        
        .newsltr_cont {
            width: 700px;
            margin: 0 auto;
        }
        
        .newsltr_message {
            text-align: center;
            width: 650px;
            font-size: 11px;
            margin: 0 auto;
            padding: 10px 0;
        }
        
        .logo {
            width: 700px;
            height: 48px;
        }
        
        #topbar_outer {
            background: #fff url(http://navbharattimes.indiatimes.com/photo/18792467.cms) repeat-x center 30px;
            position: fixed;
            top: 0;
            z-index: 10000;
        }
        
        #topnav_outer {
            width: 700px;
            float: left;
            background-image: url('http://navbharattimes.indiatimes.com/photo/19526379.cms');
            background-repeat: repeat-x;
            height: 33px
        }
        
        #topNav {
            width: 700px;
            margin: 0 auto;
        }
        
        .tabs {
            margin: 0;
            padding: 0;
            list-style: none;
            float: left;
            width: 700px;
        }
        
        .tabs ul {
            padding: 0;
            margin: 0;
        }
        
        .tabs li {
            float: left;
            display: block;
            padding: 0;
            margin: 0;
        }
        
        .tabs a {
            float: left;
            padding: 0 14px;
            text-decoration: none !important;
            color: #fff;
            height: 33px;
            line-height: 33px;
            font-size: 17px;
        }
        
        .clear {
            clear: both;
        }
        
        .newsltr_body {
            background: #fff;
            float: left;
            width: 668px;
            padding: 20px 15px 15px 15px;
            border-left: 1px solid #dadada;
            border-right: 1px solid #dadada
        }
        
        .lftpart {
            float: left;
            width: 655px;
            margin-right: 13px;
        }
        .title-wrap {
            height: 27px;
            border-bottom: solid #9b3202;
            border-width: 1px;
            color: #a4511f;
        }
        
        .title-wrap a {
            text-decoration: none
        }
        
        .txt_style {
            color: #a1a1a1;
        }
        
        .txt_deco {
            color: #4e80ba;
            font-size: 15px;
        }
        
        .txt_deco a {
            padding-bottom: 5px;
            color: #4e80ba;
            font-size: 14px;
            text-decoration: none;
            line-height: 23px;
        }
        
        .clr {
            clear: both;
            height: 20px;
            background: #ffffff
        }
        
        .footer {
            font-size: 11px;
            height: 50px;
            padding-bottom: 10px;
            clear: both;
        }
        
        .footer span.copyright {
            font-size: 11px;
            font-family: ARIAL UNICODE MS, mangal, raghu8
        }
        
        .footer a {
            text-decoration: none;
        }
        
        img {
            border: 0px;
        }
    </style>
</head>

<body>
    <table width="700" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td>
                <div class="newsltr_cont">
                    <div class="newsltr_body">
                        <div class="lftpart">
                            <p><font style="font-weight:bold">
									&#3206;&#3236;&#3277;&#3246;&#3264;&#3247; &#3219;&#3238;&#3265;&#3223;&#3248;&#3271;,</font>
                                <br>
                                <br>&#3253;&#3263;&#3228;&#3247; &#3221;&#3248;&#3277;&#3240;&#3262;&#3231;&#3221; &#3228;&#3236;&#3270;&#3223;&#3270; &#3256;&#3271;&#3248;&#3263;&#3221;&#3274;&#3202;&#3233;&#3263;&#3248;&#3265;&#3253;&#3265;&#3238;&#3221;&#3277;&#3221;&#3270; &#3240;&#3263;&#3246;&#3223;&#3270; &#3257;&#3267;&#3236;&#3277;&#3242;&#3266;&#3248;&#3277;&#3253;&#3221; &#3239;&#3240;&#3277;&#3247;&#3253;&#3262;&#3238;&#3223;&#3251;&#3265;!
                                <br>
                                <br>&#3240;&#3263;&#3246;&#3277;&#3246; &#3221;&#3262;&#3246;&#3270;&#3202;&#3231;&#3277; <a href="https://blogs.vijaykarnataka.indiatimes.com" style="text-decoration:none;">blogs.vijaykarnataka.indiatimes.com</a> &#3208;&#3223; &#3250;&#3272;&#3253;&#3277; &#3206;&#3223;&#3263;&#3238;&#3270;. &#3205;&#3238;&#3240;&#3277;&#3240;&#3265; &#3240;&#3275;&#3233;&#3250;&#3265; <a style="text-decoration:none;" href="https://blogs.vijaykarnataka.indiatimes.com/?p=[#article_id]&comments=show&amp;commentid=[#usrcommentid]&amp;type=[#commenttype]">&#3207;&#3250;&#3277;&#3250;&#3263;</a> &#3221;&#3277;&#3250;&#3263;&#3221;&#3277; &#3246;&#3262;&#3233;&#3263;.
                                <br>
                                <br>&#3253;&#3263;&#3228;&#3247; &#3221;&#3248;&#3277;&#3240;&#3262;&#3231;&#3221; &#3228;&#3236;&#3270;&#3223;&#3270; &#3240;&#3264;&#3253;&#3266; &#3256;&#3271;&#3248;&#3263;&#3221;&#3274;&#3251;&#3277;&#3251;&#3263; &#3246;&#3262;&#3236;&#3277;&#3248;&#3253;&#3250;&#3277;&#3250;&#3238;&#3270; &#3240;&#3263;&#3246;&#3277;&#3246; &#3256;&#3277;&#3240;&#3271;&#3257;&#3263;&#3236;&#3248;&#3240;&#3277;&#3240;&#3266; &#3256;&#3271;&#3248;&#3263;&#3256;&#3263;&#3221;&#3274;&#3251;&#3277;&#3251;&#3263;&#3248;&#3263;.
                                <br>
                                <br><font style="font-weight:bold">&#3239;&#3240;&#3277;&#3247;&#3253;&#3262;&#3238;&#3223;&#3251;&#3265;,</font>
                                <br>&#3256;&#3202;&#3242;&#3262;&#3238;&#3221;&#3248;&#3265;
                                <br>
                            </p>
                        </div>
                    </div>
                    <div class="footer" align="center"><a href="https://www.indiatimes.com/aboutus" target="_blank">About Us</a>| <a href="http://advertise.indiatimes.com/" target="_blank">Advertise with Us</a> | <a href="http://www.indiatimes.com/termsandcondition" target="_blank">Terms of Use</a> | <a href="http://www.indiatimes.com/privacypolicy" target="_blank">Privacy 
								    Policy</a> | <a href="https://vijaykarnataka.indiatimes.com/sitemap.cms" target="_blank">Sitemap</a>
                        <br><span class="copyright">Copyright &copy; <?php echo date('Y');?> Bennett Coleman &amp; Co. Ltd. All rights reserved. For reprint rights: <a href="http://timescontent.com/" target="_blank">Times Syndication Service</a></span>
                        <br><span style="padding-top:5px;">If you want to unsubscribe this service, please <a href="#" target="_blank">click here</a></span></div>
                </div>
            </td>
        </tr>
    </table>
</body>

</html>