<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>

<head>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>NBT Newsletter</title>
    <style>
        body,
        div,
        img,
        p {
            margin: 0;
            padding: 0;
            font-family: ARIAL UNICODE MS, mangal, raghu8;
            font-size: 17px;
        }
        
        body {
            background: #f2f2f2;
        }
        
        .newsltr_cont {
            width: 700px;
            margin: 0 auto;
        }
        
        .newsltr_message {
            text-align: center;
            width: 650px;
            font-size: 11px;
            margin: 0 auto;
            padding: 10px 0;
        }
        
        .logo {
            width: 700px;
            height: 48px;
        }
        
        #topbar_outer {
            background: #fff url(http://navbharattimes.indiatimes.com/photo/18792467.cms) repeat-x center 30px;
            position: fixed;
            top: 0;
            z-index: 10000;
        }
        
        #topnav_outer {
            width: 700px;
            float: left;
            background-image: url('http://navbharattimes.indiatimes.com/photo/19526379.cms');
            background-repeat: repeat-x;
            height: 33px
        }
        
        #topNav {
            width: 700px;
            margin: 0 auto;
        }
        
        .tabs {
            margin: 0;
            padding: 0;
            list-style: none;
            float: left;
            width: 700px;
        }
        
        .tabs ul {
            padding: 0;
            margin: 0;
        }
        
        .tabs li {
            float: left;
            display: block;
            padding: 0;
            margin: 0;
        }
        
        .tabs a {
            float: left;
            padding: 0 14px;
            text-decoration: none !important;
            color: #fff;
            height: 33px;
            line-height: 33px;
            font-size: 17px;
        }
        
        .clear {
            clear: both;
        }
        
        .newsltr_body {
            background: #fff;
            float: left;
            width: 668px;
            padding: 20px 15px 15px 15px;
            border-left: 1px solid #dadada;
            border-right: 1px solid #dadada
        }
        
        .lftpart {
            float: left;
            width: 655px;
            margin-right: 13px;
        }
        
        .title-wrap {
            height: 27px;
            border-bottom: solid #9b3202;
            border-width: 1px;
            color: #a4511f;
        }
        
        .title-wrap a {
            text-decoration: none
        }
    
        .txt_style {
            color: #a1a1a1;
        }
        
        .txt_deco {
            color: #4e80ba;
            font-size: 15px;
        }
        
        .txt_deco a {
            padding-bottom: 5px;
            color: #4e80ba;
            font-size: 14px;
            text-decoration: none;
            line-height: 23px;
        }
        
        .clr {
            clear: both;
            height: 20px;
            background: #ffffff
        }
        
        .footer {
            font-size: 11px;
            height: 50px;
            padding-bottom: 10px;
            clear: both;
        }
        
        .footer span.copyright {
            font-size: 11px;
            font-family: ARIAL UNICODE MS, mangal, raghu8
        }
        
        .footer a {
            text-decoration: none;
        }
        
        img {
            border: 0px;
        }
    </style>
    <link href="//plus.google.com" rel="dns-prefetch">
    <link href="//connect.facebook.net" rel="dns-prefetch">
    <link href="//static.clmbtech.com" rel="dns-prefetch">
    <link href="//static.chartbeat.com" rel="dns-prefetch">
    <link href="//ade.clmbtech.com" rel="dns-prefetch">
    <link href="//ajax.googleapis.com" rel="dns-prefetch">
    <link href="http://blogs.navbharattimes.indiatimes.com" rel="dns-prefetch">
    <link href="http://readerblogs.navbharattimes.indiatimes.com" rel="dns-prefetch">
    <link href="//www.facebook.com" rel="dns-prefetch">
    <link href="http://photogallery.navbharattimes.indiatimes.com" rel="dns-prefetch">
    <link href="//image.timespoints.iimg.in" rel="dns-prefetch">
    <link href="//googleadservices.com" rel="dns-prefetch">
    <link href="//google-analytics.com" rel="dns-prefetch">
    <script type="text/javascript">
        document.domain = 'indiatimes.com';
        var domainname = document.location.host;
        var hrefpath = document.location.href;
        var domainpath = window.location.origin;
        var __activity = [];
    </script>
    <link title="Navbharat Times" rel="publisher" href="https://plus.google.com/+navbharattimes/posts">
    <script type="text/javascript">
        var _sf_startpt = (new Date()).getTime()
    </script>
    <link href="/icons/nbtfavicon.ico" rel="shortcut icon">
    <script language="javascript">
        document.domain = 'indiatimes.com';
    </script>
    <script>
        function Get_Ckie(name) {
            var value = "; " + document.cookie;
            var parts = value.split("; " + name + "=");
            if (parts.length == 2) return parts.pop().split(";").shift();
        }

        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-429254-3', 'auto', {
            'allowLinker': true
        });
        if (typeof Get_Ckie == "function" && Get_Ckie('ssoid')) {
            ga('set', 'userId', Get_Ckie('ssoid'));
        }
        ga('require', 'displayfeatures');
        ga('require', 'linker');
        ga('linker:autoLink', ['navbharattimes.com', 'superpacer.in', 'm.nbt.in']);
        ga('send', 'pageview');
    </script>
    <script>
        var _comscore = _comscore || [];
        _comscore.push({
            c1: "2",
            c2: "6036484"
        });
        (function() {
            var s = document.createElement("script"),
                el = document.getElementsByTagName("script")[0];
            s.async = true;
            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
            el.parentNode.insertBefore(s, el);
        })();
    </script>
    <noscript><img src="https://sb.scorecardresearch.com/p?c1=2&amp;c2=6036484&amp;cv=2.0&amp;cj=1"></noscript>
</head>

<body>
    <table width="100%" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td>
                <div class="newsltr_cont">
                    <div class="newsltr_body">
                        <div class="lftpart">
                            <p><font style="font-weight:bold">
									&#2337;&#2367;&#2351;&#2352; &#2346;&#2366;&#2336;&#2325;,</font>
                                <br>
                                <br>'&#2344;&#2357;&#2349;&#2366;&#2352;&#2340; &#2335;&#2366;&#2311;&#2350;&#2381;&#2360;' &#2360;&#2375; &#2332;&#2369;&#2337;&#2364;&#2344;&#2375; &#2325;&#2375; &#2354;&#2367;&#2319; &#2310;&#2346;&#2325;&#2366; &#2348;&#2361;&#2369;&#2340;-&#2348;&#2361;&#2369;&#2340; &#2358;&#2369;&#2325;&#2381;&#2352;&#2367;&#2351;&#2366;!
                                <br>
                                <br>&#2310;&#2346;&#2325;&#2366; &#2325;&#2377;&#2350;&#2375;&#2306;&#2335; <a href="http://readerblogs.navbharattimes.indiatimes.com" style="text-decoration:none;">readerblogs.navbharattimes.indiatimes.com</a> &#2346;&#2352; &#2354;&#2366;&#2311;&#2357; &#2361;&#2376;&#2404; &#2311;&#2360;&#2375; &#2342;&#2375;&#2326;&#2344;&#2375; &#2325;&#2375; &#2354;&#2367;&#2319; <a style="text-decoration:none;" href="http://readerblogs.navbharattimes.indiatimes.com/?p=[#article_id]&comments=show&amp;commentid=[#usrcommentid]&amp;type=[#commenttype]">&#2351;&#2361;&#2366;&#2306;</a> &#2325;&#2381;&#2354;&#2367;&#2325; &#2325;&#2352;&#2375;&#2306;&#2404;
                                <br>
                                <br>'&#2344;&#2357;&#2349;&#2366;&#2352;&#2340; &#2335;&#2366;&#2311;&#2350;&#2381;&#2360;' &#2360;&#2375; &#2326;&#2369;&#2342; &#2349;&#2368; &#2332;&#2369;&#2337;&#2364;&#2375; &#2352;&#2361;&#2367;&#2319; &#2324;&#2352; &#2342;&#2379;&#2360;&#2381;&#2340;&#2379;&#2306; &#2325;&#2379; &#2349;&#2368; &#2332;&#2379;&#2337;&#2364;&#2340;&#2375; &#2330;&#2354;&#2367;&#2319;&#2404;
                                <br>
                                <br><font style="font-weight:bold">&#2343;&#2344;&#2381;&#2351;&#2357;&#2366;&#2342;,</font>
                                <br>&#2360;&#2306;&#2346;&#2366;&#2342;&#2325;
                                <br>
                            </p>
                        </div>
                    </div>
                    <div class="footer" align="center"><a href="https://www.timesinternet.in/" target="_blank">About Us</a>| <a href="http://advertise.indiatimes.com/" target="_blank">Advertise with Us</a> | <a href="http://navbharattimes.indiatimes.com/termsandcondition.cms" target="_blank">Terms of Use</a> | <a href="http://navbharattimes.indiatimes.com/privacypolicy.cms" target="_blank">Privacy 
								    Policy</a> | <a href="http://navbharattimes.indiatimes.com/feedback.cms" target="_blank">Feedback</a> | <a href="http://navbharattimes.indiatimes.com/sitemap.cms" target="_blank">Sitemap</a>
                        <br><span class="copyright">Copyright &copy; <?php echo date('Y');?> Bennett Coleman &amp; Co. Ltd. All rights reserved. For reprint rights: <a href="http://timescontent.com/" target="_blank">Times Syndication Service</a></span>
                        <br>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        var secname = "";
        var agename = "";
    </script>
    <script type="text/javascript">
        secname = '';
        agename = '';
        var ibur = "//static.chartbeat.com/js/chartbeat.js";

        var _sf_async_config = {};
        /** CONFIGURATION START **/
        _sf_async_config.uid = 10538;
        _sf_async_config.domain = 'navbharattimes.indiatimes.com';
        _sf_async_config.useCanonical = true;
        _sf_async_config.sections = 'Change this to your Section name'; //CHANGE THIS
        _sf_async_config.authors = 'Change this to your Author name'; //CHANGE THIS
        /** CONFIGURATION END **/
        (function() {
            function loadChartbeat() {
                window._sf_endpt = (new Date()).getTime();
                var e = document.createElement('script');
                e.setAttribute('language', 'javascript');
                e.setAttribute('type', 'text/javascript');
                e.setAttribute('src', ibur);
                document.body.appendChild(e);
            }
            var oldonload = window.onload;
            window.onload = (typeof window.onload != 'function') ?
                loadChartbeat : function() {
                    oldonload();
                    loadChartbeat();
                };
        })();
    </script>
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=117787264903013&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <script>
        var usersessionkey = "";
        var langUsed = "";
        var userFbNameLang = '';
        FB.init({
            appId: '117787264903013',
            oauth: true,
            status: true,
            cookie: true,
            xfbml: true
        });

        function getSessionFB() {

            FB.getLoginStatus(function(response) {
                if (response.authResponse) {
                    usersessionkey = response.authResponse.accessToken;
                    var query = FB.Data.query('select name, locale from user where uid = me()', sessionkey);
                    query.wait(function(rows) {
                        langUsed = rows[0].locale;

                        if (langUsed == 'hi_IN') {
                            userFbNameLang = rows[0].name;
                        }
                    });
                } else {

                }
            });
        }

        setTimeout("getSessionFB()", 2000);
    </script>
    <div><img height="1" width="1" id="imgLogout"></div>
    <script type="text/javascript">
        var _mfq = _mfq || [];
        (function() {
            var mf = document.createElement("script");
            mf.type = "text/javascript";
            mf.async = true;
            mf.src = "//cdn.mouseflow.com/projects/0031008a-3cec-4f54-b5d5-c600553afcd1.js";
            document.getElementsByTagName("head")[0].appendChild(mf);
        })();
    </script>
</body>

</html>