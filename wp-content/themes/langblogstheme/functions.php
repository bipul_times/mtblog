<?php
/**
 * Blogs Theme functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link http://codex.wordpress.org/Theme_Development
 * @link http://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * @link http://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage Blogs_Theme
 * @since Blogs Theme 1.0
 */

/**
 * Set up the content width value based on the theme's design.
 *
 * @see twentyfourteen_content_width()
 *
 * @since Blogs Theme 2.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 474;
}

if ( ! function_exists( 'twentyfourteen_setup' ) ) :
/**
 * Blogs Theme setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 * @since Blogs Theme 1.0
 */
function twentyfourteen_setup() {

	// This theme styles the visual editor to resemble the theme style.
	add_editor_style( array( 'css/editor-style.css', twentyfourteen_font_url() ) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list',
	) );

	// Add support for featured content.
	add_theme_support( 'featured-content', array(
		'featured_content_filter' => 'twentyfourteen_get_featured_posts',
		'max_posts' => 6,
	) );

	add_theme_support( 'post-thumbnails' ); 
	
	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );
	
	add_theme_support( 'menus' );
	
	remove_core_updates();
	
	 // Filters for WP-API version 1.x
	add_filter('json_enabled', '__return_false');
	add_filter('json_jsonp_enabled', '__return_false');

	// Filters for WP-API version 2.x
	add_filter('rest_enabled', '__return_false');
	add_filter('rest_jsonp_enabled', '__return_false');
}
endif; // twentyfourteen_setup
add_action( 'after_setup_theme', 'twentyfourteen_setup' );

/**
 * Getter function for Featured Content Plugin.
 *
 * @since Blogs Theme 1.0
 *
 * @return array An array of WP_Post objects.
 */
function twentyfourteen_get_featured_posts() {
	/**
	 * Filter the featured posts to return in Blogs Theme.
	 *
	 * @since Blogs Theme 1.0
	 *
	 * @param array|bool $posts Array of featured posts, otherwise false.
	 */
	return apply_filters( 'twentyfourteen_get_featured_posts', array() );
}

/**
 * A helper conditional function that returns a boolean value.
 *
 * @since Blogs Theme 1.0
 *
 * @return bool Whether there are featured posts.
 */
function twentyfourteen_has_featured_posts() {
	return ! is_paged() && (bool) twentyfourteen_get_featured_posts();
}


/**
 * Register Lato Google font for Blogs Theme.
 *
 * @since Blogs Theme 1.0
 *
 * @return string
 */
function twentyfourteen_font_url() {
	//$font_url = '';
	$font_url = get_template_directory_uri() . '/css/fonts.css';
	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Lato, translate this to 'off'. Do not translate into your own language.
	 */
	/*if ( 'off' !== _x( 'on', 'Lato font: on or off', 'twentyfourteen' ) ) {
		$font_url = add_query_arg( 'family', urlencode( 'Lato:300,400,700,900,300italic,400italic,700italic' ), "//fonts.googleapis.com/css" );
	}*/

	return $font_url;
}

function isLang(){
    if (strpos($_SERVER['HTTP_HOST'], 'navbharattimes.indiatimes.com') !== false || strpos($_SERVER['HTTP_HOST'], 'vsp1nbtblogs.indiatimes.com') !== false || strpos($_SERVER['HTTP_HOST'], 'vsp1nbtreaderblogs.indiatimes.com') !== false) {
        return 'nbt';
    }elseif (strpos($_SERVER['HTTP_HOST'], 'maharashtratimes.indiatimes.com') !== false || strpos($_SERVER['HTTP_HOST'], 'vsp1mtblogs.indiatimes.com') !== false ) {
        return 'mt';
    }elseif (strpos($_SERVER['HTTP_HOST'], 'eisamay.indiatimes.com') !== false  || strpos($_SERVER['HTTP_HOST'], 'vsp1eisamayblogs.indiatimes.com') !== false ) {
        return 'eisamay';
    }elseif (strpos($_SERVER['HTTP_HOST'], 'vijaykarnataka.indiatimes.com') !== false  || strpos($_SERVER['HTTP_HOST'], 'vsp1vkblogs.indiatimes.com') !== false ) {
        return 'vk';
    }elseif (strpos($_SERVER['HTTP_HOST'], 'navgujaratsamay.indiatimes.com') !== false ) {
        return 'ngs';
    }elseif (strpos($_SERVER['HTTP_HOST'], 'malayalam.samayam.com') !== false ) {
        return 'ml';
    }elseif (strpos($_SERVER['HTTP_HOST'], 'tamil.samayam.com') !== false ) {
        return 'tm';
    }elseif (strpos($_SERVER['HTTP_HOST'], 'telugu.samayam.com') !== false ) {
        return 'tl';
    }else{
        return '';
    }
}

function isMobile(){
	//$_SERVER['HTTP_X_AKAMAI_DEVICE_CHARACTERISTICS'] = 'is_mobile=false;is_tablet=false';
	if(isset($_SERVER['HTTP_X_AKAMAI_DEVICE_CHARACTERISTICS']) && !empty($_SERVER['HTTP_X_AKAMAI_DEVICE_CHARACTERISTICS'])){
		 if (strpos($_SERVER['HTTP_X_AKAMAI_DEVICE_CHARACTERISTICS'],'is_mobile=true') !== false && strpos($_SERVER['HTTP_X_AKAMAI_DEVICE_CHARACTERISTICS'],'is_tablet=false') !== false) {
			 return true;
		 } else {
			 return false;
		 }
	} else {
		return wp_is_mobile();
	}
}

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Blogs Theme 1.0
 *
 * @return void
 */
function twentyfourteen_scripts() {
	$currentLangTerm = isLang();
	// Add Lato font, used in the main stylesheet.
	//wp_enqueue_style( 'twentyfourteen-lato', twentyfourteen_font_url(), array(), null );

	// Add Genericons font, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '1.0' );
	wp_enqueue_style( 'twentyfourteen-style', get_stylesheet_uri(), array(),'6.0' );
	//wp_enqueue_style( 'new-nav', get_template_directory_uri() . '/css/newnavcss.css', array(), '1.7' );
	wp_enqueue_style( 'comment-css', get_template_directory_uri() . '/css/comments.css', array(), '2.2' );
	wp_enqueue_style( 'ctnads-css', get_template_directory_uri() . '/css/ctnads.css', array(), '1.4' );
	
	/*if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_style( 'comment-css', get_template_directory_uri() . '/css/comments.css', array(), '1.1' );
	}*/

	// Load our main scripts.
	//wp_enqueue_script( 'jquery', false );
	wp_enqueue_script( 'ctnads-script', get_template_directory_uri() . '/js/ctnads.js', array( 'jquery' ), '1.2' );
	wp_enqueue_script( 'functions-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '2.10', true );
	//wp_enqueue_script( 'lotame-behave-toi', 'http://tags.crwdcntrl.net/c/2801/cc_af.js', array(), '1', true );
	//wp_enqueue_script( 'lotame-ad-toi', 'http://ad.crwdcntrl.net/5/c=2800/pe=y/var=_ccaud', array(), '1', true );
	//wp_enqueue_script( 'ad-script', get_template_directory_uri() . '/js/ad.js', array(), '1', false );
	wp_enqueue_script( 'sharer-script', get_template_directory_uri() . '/js/social-likes.js', array( 'jquery' ), '1.3', true );
	//wp_enqueue_script( 'require-script', get_template_directory_uri() . '/js/require.js', array('jquery'), '2.1', true );
	wp_enqueue_script( 'config-script', 'https://timesofindia.indiatimes.com/jsrender.cms', array('jquery'), '2.2', true );
	if (strpos($_SERVER['HTTP_HOST'], 'readerblogs.navbharattimes.indiatimes.com') !== false ) {
        wp_enqueue_script( 'times-script', get_template_directory_uri() . '/js/blogs_readernbt.js', array('config-script'), '4.1', true );
    }else{
		wp_enqueue_script( 'times-script', get_template_directory_uri() . '/js/blogs_'.$currentLangTerm.'.js', array('config-script'), '4.1', true );
	}
	
	// Load the Internet Explorer specific stylesheet.
	/*wp_enqueue_style( 'twentyfourteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentyfourteen-style', 'genericons' ), '20131205' );
	wp_style_add_data( 'twentyfourteen-ie', 'conditional', 'lt IE 9' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'twentyfourteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20130402' );
	}

	if ( is_active_sidebar( 'sidebar-3' ) ) {
		wp_enqueue_script( 'jquery-masonry' );
	}

	if ( is_front_page() && 'slider' == get_theme_mod( 'featured_content_layout' ) ) {
		wp_enqueue_script( 'twentyfourteen-slider', get_template_directory_uri() . '/js/slider.js', array( 'jquery' ), '20131205', true );
		wp_localize_script( 'twentyfourteen-slider', 'featuredSliderDefaults', array(
			'prevText' => __( 'Previous', 'twentyfourteen' ),
			'nextText' => __( 'Next', 'twentyfourteen' )
		) );
	}*/
	/*if ( is_singular() ) {
		wp_enqueue_script( 'sharer-script', get_template_directory_uri() . '/js/social-likes.min.js', array( 'jquery' ), '1', true );
	}*/
}
add_action( 'wp_enqueue_scripts', 'twentyfourteen_scripts' );

/**
 * Enqueue Google fonts style to admin screen for custom header display.
 *
 * @since Blogs Theme 1.0
 *
 * @return void
 */
function twentyfourteen_admin_fonts() {
	wp_enqueue_style( 'twentyfourteen-lato', twentyfourteen_font_url(), array(), null );
}
add_action( 'admin_print_scripts-appearance_page_custom-header', 'twentyfourteen_admin_fonts' );

if ( ! function_exists( 'twentyfourteen_the_attached_image' ) ) :
/**
 * Print the attached image with a link to the next attached image.
 *
 * @since Blogs Theme 1.0
 *
 * @return void
 */
function twentyfourteen_the_attached_image() {
	$post                = get_post();
	/**
	 * Filter the default Blogs Theme attachment size.
	 *
	 * @since Blogs Theme 1.0
	 *
	 * @param array $dimensions {
	 *     An array of height and width dimensions.
	 *
	 *     @type int $height Height of the image in pixels. Default 810.
	 *     @type int $width  Width of the image in pixels. Default 810.
	 * }
	 */
	$attachment_size     = apply_filters( 'twentyfourteen_attachment_size', array( 810, 810 ) );
	$next_attachment_url = wp_get_attachment_url();

	/*
	 * Grab the IDs of all the image attachments in a gallery so we can get the URL
	 * of the next adjacent image in a gallery, or the first image (if we're
	 * looking at the last image in a gallery), or, in a gallery of one, just the
	 * link to that image file.
	 */
	$attachment_ids = get_posts( array(
		'post_parent'    => $post->post_parent,
		'fields'         => 'ids',
		'numberposts'    => -1,
		'post_status'    => 'inherit',
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'order'          => 'ASC',
		'orderby'        => 'menu_order ID',
	) );

	// If there is more than 1 attachment in a gallery...
	if ( count( $attachment_ids ) > 1 ) {
		foreach ( $attachment_ids as $attachment_id ) {
			if ( $attachment_id == $post->ID ) {
				$next_id = current( $attachment_ids );
				break;
			}
		}

		// get the URL of the next image attachment...
		if ( $next_id ) {
			$next_attachment_url = get_attachment_link( $next_id );
		}

		// or get the URL of the first image attachment.
		else {
			$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
		}
	}

	printf( '<a href="%1$s" rel="attachment">%2$s</a>',
		esc_url( $next_attachment_url ),
		wp_get_attachment_image( $post->ID, $attachment_size )
	);
}
endif;

if ( ! function_exists( 'twentyfourteen_list_authors' ) ) :
/**
 * Print a list of all site contributors who published at least one post.
 *
 * @since Blogs Theme 1.0
 *
 * @return void
 */
function twentyfourteen_list_authors() {
	$contributor_ids = get_users( array(
		'fields'  => 'ID',
		'orderby' => 'post_count',
		'order'   => 'DESC',
		'who'     => 'authors',
	) );

	foreach ( $contributor_ids as $contributor_id ) :
		$post_count = count_user_posts( $contributor_id );

		// Move on if user has not published a post (yet).
		if ( ! $post_count ) {
			continue;
		}
	?>

	<div class="contributor">
		<div class="contributor-info">
			<div class="contributor-avatar"><?php echo get_avatar( $contributor_id, 132 ); ?></div>
			<div class="contributor-summary">
				<h2 class="contributor-name"><?php echo get_the_author_meta( 'display_name', $contributor_id ); ?></h2>
				<p class="contributor-bio">
					<?php echo get_the_author_meta( 'description', $contributor_id ); ?>
				</p>
				<a class="contributor-posts-link" href="<?php echo esc_url( get_author_posts_url( $contributor_id ) ); ?>">
					<?php printf( _n( '%d Article', '%d Articles', $post_count, 'twentyfourteen' ), $post_count ); ?>
				</a>
			</div><!-- .contributor-summary -->
		</div><!-- .contributor-info -->
	</div><!-- .contributor -->

	<?php
	endforeach;
}
endif;

/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Presence of header image.
 * 3. Index views.
 * 4. Full-width content layout.
 * 5. Presence of footer widgets.
 * 6. Single views.
 * 7. Featured content layout.
 *
 * @since Blogs Theme 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function twentyfourteen_body_classes( $classes ) {
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	if ( get_header_image() ) {
		$classes[] = 'header-image';
	} else {
		$classes[] = 'masthead-fixed';
	}

	if ( is_archive() || is_search() || is_home() ) {
		$classes[] = 'list-view';
	}

	if ( ( ! is_active_sidebar( 'sidebar-2' ) )
		|| is_page_template( 'page-templates/full-width.php' )
		|| is_page_template( 'page-templates/contributors.php' )
		|| is_attachment() ) {
		$classes[] = 'full-width';
	}

	if ( is_active_sidebar( 'sidebar-3' ) ) {
		$classes[] = 'footer-widgets';
	}

	if ( is_singular() && ! is_front_page() ) {
		$classes[] = 'singular';
	}

	if ( is_front_page() && 'slider' == get_theme_mod( 'featured_content_layout' ) ) {
		$classes[] = 'slider';
	} elseif ( is_front_page() ) {
		$classes[] = 'grid';
	}
	
	$classes[] = 'style_' . isLang();

	return $classes;
}
add_filter( 'body_class', 'twentyfourteen_body_classes' );

/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 *
 * @since Blogs Theme 1.0
 *
 * @param array $classes A list of existing post class values.
 * @return array The filtered post class list.
 */
function twentyfourteen_post_classes( $classes ) {
	if ( ! post_password_required() && has_post_thumbnail() ) {
		$classes[] = 'has-post-thumbnail';
	}

	return $classes;
}
//add_filter( 'post_class', 'twentyfourteen_post_classes' );

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Blogs Theme 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function twentyfourteen_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'twentyfourteen' ), max( $paged, $page ) );
	}
	return $title;
}
add_filter( 'wp_title', 'twentyfourteen_wp_title', 10, 2 );

// Custom template tags for this theme.
require get_template_directory() . '/inc/template-tags.php';

/*
 * Add Featured Content functionality.
 *
 * To overwrite in a plugin, define your own Featured_Content class on or
 * before the 'setup_theme' hook.
 */
if ( ! class_exists( 'Featured_Content' ) && 'plugins.php' !== $GLOBALS['pagenow'] ) {
	require get_template_directory() . '/inc/featured-content.php';
}

require get_template_directory() . '/custom-functions.php';

/* remove parent sidebars */
function aquatify_remove_fourteen_sidebars() {
  unregister_sidebar( 'sidebar-1' ); // primary on left
}
add_action( 'widgets_init', 'aquatify_remove_fourteen_sidebars', 11 );


/* Added by Amit & Bharat */
function wpbeginner_remove_version() {
return '';
}
add_filter('the_generator', 'wpbeginner_remove_version');

function custom_admin_js() {
    echo '<script type="text/javascript" src="https://www.google.com/jsapi"></script>';
    echo '<script type="text/javascript">
      // Load the Google Transliteration API
      google.load("elements", "1", {
        packages: "transliteration"
      });
      var transliterationControl;
      function onLoad() {
        var options = {
            sourceLanguage: "en",
            destinationLanguage: ["ml", "ta", "te"],
            transliterationEnabled: true,
            shortcutKey: "ctrl+g"
        };
        // Create an instance on TransliterationControl with the required
        // options.
        transliterationControl =
          new google.elements.transliteration.TransliterationControl(options);

        // Enable transliteration in the textfields with the given ids.
        if(jQuery("#content").length){
			transliterationControl.makeTransliteratable(["content"]);
		}
		if(jQuery("#title").length){
			transliterationControl.makeTransliteratable(["title"]);
		}
		if(jQuery("#content_ifr").length){
			transliterationControl.makeTransliteratable(["content_ifr"]);
		}
		if(jQuery("#new-tag-post_tag").length){
			transliterationControl.makeTransliteratable(["new-tag-post_tag"]);
		}
		if(jQuery("#newcategory").length){
			transliterationControl.makeTransliteratable(["newcategory"]);
		}
		if(jQuery("#tag-name").length){
			transliterationControl.makeTransliteratable(["tag-name"]);
		}
		if(jQuery("#tag-slug").length){
			transliterationControl.makeTransliteratable(["tag-slug"]);
		}
		if(jQuery("#tag-description").length){
			transliterationControl.makeTransliteratable(["tag-description"]);
		}
		if(jQuery("#name").length){
			transliterationControl.makeTransliteratable(["name"]);
		}
		if(jQuery("#slug").length){
			transliterationControl.makeTransliteratable(["slug"]);
		}
		if(jQuery("#description").length){
			transliterationControl.makeTransliteratable(["description"]);
		}

        // Add the STATE_CHANGED event handler to correcly maintain the state
        // of the checkbox.
        transliterationControl.addEventListener(
            google.elements.transliteration.TransliterationControl.EventType.STATE_CHANGED,
            transliterateStateChangeHandler);

        // Add the SERVER_UNREACHABLE event handler to display an error message
        // if unable to reach the server.
        transliterationControl.addEventListener(
            google.elements.transliteration.TransliterationControl.EventType.SERVER_UNREACHABLE,
            serverUnreachableHandler);

        // Add the SERVER_REACHABLE event handler to remove the error message
        // once the server becomes reachable.
        transliterationControl.addEventListener(
            google.elements.transliteration.TransliterationControl.EventType.SERVER_REACHABLE,
            serverReachableHandler);

        // Set the checkbox to the correct state.
        document.getElementById("checkboxId").checked =
          transliterationControl.isTransliterationEnabled();

        // Populate the language dropdown
        var destinationLanguage =
          transliterationControl.getLanguagePair().destinationLanguage;
        var languageSelect = document.getElementById("languageDropDown");
        var supportedDestinationLanguages =
          google.elements.transliteration.getDestinationLanguages(
            google.elements.transliteration.LanguageCode.ENGLISH);
        for (var lang in supportedDestinationLanguages) {
          if(supportedDestinationLanguages[lang] != "ml" && supportedDestinationLanguages[lang] != "ta" && supportedDestinationLanguages[lang] != "te"){
			continue;
		  }
          var opt = document.createElement("option");
          opt.text = lang;
          opt.value = supportedDestinationLanguages[lang];
          if (destinationLanguage == opt.value) {
            opt.selected = true;
          }
          try {
            languageSelect.add(opt, null);
          } catch (ex) {
            languageSelect.add(opt);
          }
        }
      }

      // Handler for STATE_CHANGED event which makes sure checkbox status
      // reflects the transliteration enabled or disabled status.
      function transliterateStateChangeHandler(e) {
        document.getElementById("checkboxId").checked = e.transliterationEnabled;
      }

      // Handler for checkbox click event.  Calls toggleTransliteration to toggle
      // the transliteration state.
      function checkboxClickHandler() {
        transliterationControl.toggleTransliteration();
      }

      // Handler for dropdown option change event.  Calls setLanguagePair to
      // set the new language.
      function languageChangeHandler() {
        var dropdown = document.getElementById("languageDropDown");
        transliterationControl.setLanguagePair(
            google.elements.transliteration.LanguageCode.ENGLISH,
            dropdown.options[dropdown.selectedIndex].value);
      }

      // SERVER_UNREACHABLE event handler which displays the error message.
      function serverUnreachableHandler(e) {
        alert("Transliteration server unreachable!");
      }

      // SERVER_UNREACHABLE event handler which clears the error message.
      function serverReachableHandler(e) {
        // do something
      }
      google.setOnLoadCallback(onLoad);
    </script>';
}
function add_my_media_button() {
    echo '<input type="checkbox" id="checkboxId" onclick="javascript:checkboxClickHandler()"></input> Type in <select id="languageDropDown" onchange="javascript:languageChangeHandler()"></select>';
}

/*add_action('admin_footer', 'custom_admin_js');
add_action('media_buttons', 'add_my_media_button', 15);
add_action('blog_add_form_fields', 'add_my_media_button', 10, 2);
add_action('blog_edit_form_fields', 'add_my_media_button', 10, 2);
add_action('category_add_form_fields', 'add_my_media_button', 10, 2);
add_action('category_edit_form_fields', 'add_my_media_button', 10, 2);
add_action('post_tag_add_form_fields', 'add_my_media_button', 10, 2);
add_action('post_tag_edit_form_fields', 'add_my_media_button', 10, 2);*/

function get_cat_tree($parent = 0) {
	$categories = get_categories();
	$result = array();
	foreach($categories as $category){
		$taxonomy = 'category';
		$t_id = $category->cat_ID;
		$cat_meta =  get_option( "$taxonomy_$t_id" );
		if(isset($cat_meta['wps_subtitle']) && !empty($cat_meta['wps_subtitle'])){
			$category->name = $cat_meta['wps_subtitle'];
		}
		if ($parent == $category->category_parent) {
			$category->children = get_cat_tree($category->cat_ID,$categories);
			$result[] = $category;
		}
	}
	return $result;
}

add_action( 'admin_init', '_admin_init' );

if(!defined('WP_ADMIN')) {
	add_filter( 'the_title', '_show_local_title', 10, 2 );
	add_filter( 'the_terms', '_show_local_terms_name', 10, 2 );
	add_filter( 'get_the_terms', '_show_local_terms_name', 10, 2 );
	add_filter( 'get_terms', '_show_local_terms_name', 10, 2 );
	add_filter( 'get_term', '_show_local_term_name', 10, 2 );
}

function _show_local_title($title, $id) {
	if ($id == get_the_ID()) {
		$subtitle = get_post_meta( get_the_ID(), 'wps_subtitle', true );
		$title = $subtitle ? $subtitle : $title;
	}
	return $title;
}

function _show_local_terms_name($term_list, $taxonomy) {
    foreach($term_list as $key => $val){
		$taxonomy = $val->taxonomy;
		$t_id = $val->term_id;
		$term_meta =  get_option( "$taxonomy_$t_id" );
		if(isset($term_meta['wps_subtitle']) && !empty($term_meta['wps_subtitle'])){
			$val->name = $term_meta['wps_subtitle'];
		}
	}

    return $term_list;
}

function _show_local_term_name($term_list, $taxonomy) {
	$t_id = $term_list->term_id;
	$term_meta =  get_option( "$taxonomy_$t_id" );
	if(isset($term_meta['wps_subtitle']) && !empty($term_meta['wps_subtitle'])){
		$term_list->name = $term_meta['wps_subtitle'];
	}

    return $term_list;
}

function _admin_init() {

	global $pagenow;

	// Get post type
	$post_type = '';

	if ( isset( $_GET['post_type'] ) ) {
		$post_type = $_GET['post_type'];
	} elseif ( isset( $_GET['post'] ) ) {
		$post_type = get_post_type( $_GET['post'] );
	} elseif ( in_array( $pagenow, array( 'post-new.php', 'edit.php' ) ) ) {
		$post_type = 'post';
	}
	
	add_action( 'admin_head', '_add_admin_styles' );

	if($post_type != ''){
		if ( ! apply_filters( 'wps_subtitle_use_meta_box', false, $post_type ) ) {
			if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
				add_action( 'edit_form_after_title', '_add_subtitle_field' );
			} else {
				add_action( 'edit_form_before_permalink', '_add_subtitle_field' );
			}
		} else {
			add_action( 'add_meta_boxes', '_add_meta_boxes' );
		}

		add_filter( 'manage_edit-' . $post_type . '_columns', 'manage_subtitle_columns' );
		add_action( 'manage_' . $post_type . '_posts_custom_column', 'manage_subtitle_columns_content', 10, 2 );
	}
	
	add_action('blog_add_form_fields', '_add_taxonomy_subtitle_field');
	add_action('blog_edit_form_fields', '_edit_taxonomy_subtitle_field');
	add_action('category_add_form_fields', '_add_taxonomy_subtitle_field');
	add_action('category_edit_form_fields', '_edit_taxonomy_subtitle_field');
	add_action('post_tag_add_form_fields', '_add_taxonomy_subtitle_field');
	add_action('post_tag_edit_form_fields', '_edit_taxonomy_subtitle_field');
	
	add_action( 'create_blog', '_save_term_blog' );
	add_action( 'edited_blog', '_save_term_blog' );
	add_action( 'create_category', '_save_term_category' );
	add_action( 'edited_category', '_save_term_category' );
	add_action( 'create_post_tag', '_save_term_post_tag' );
	add_action( 'edited_post_tag', '_save_term_post_tag' );
	
	add_filter('manage_edit-blog_columns', '_term_blog_columns');
	add_filter('manage_blog_custom_column', '_term_blog_column', 10, 3);
	add_filter('manage_edit-category_columns', '_term_category_columns');
	add_filter('manage_category_custom_column', '_term_category_column', 10, 3);
	add_filter('manage_edit-post_tag_columns', '_term_post_tag_columns');
	add_filter('manage_post_tag_custom_column', '_term_post_tag_column', 10, 3);
}

function _add_taxonomy_subtitle_field( $term ) {
	echo '<div class="form-field form-required term-wps_subtitle-wrap">';
	echo '<label for="wpsubtitle">Local Name</label>';
	echo '<input id="wpsubtitle" value="" size="40" type="text" name="wps_subtitle" autocomplete="off" placeholder="' . esc_attr( apply_filters( 'wps_subtitle_field_placeholder', __( 'Enter local name here', 'wps_subtitle' ) ) ) . '" />';
	echo '<p class="description">Name in local language</p>';
	echo '</div>';
	echo '<script>jQuery("div.term-wps_subtitle-wrap").insertAfter("form#addtag div.form-field:first");jQuery("#tag-name").parents("div.form-field").find("p").html("Name in english language");</script>';
}

function _edit_taxonomy_subtitle_field( $term ){
	$termID = $term->term_id;
	$termMeta = get_option( "$taxonomyName_$termID" );    
	$wps_subtitle = $termMeta['wps_subtitle'];
	
	echo '<tr class="form-field form-required term-wps_subtitle-wrap">';
	echo '<th scope="row"><label for="wpsubtitle">Local Name</label></th>';
	echo '<td><input id="wpsubtitle" value="' . esc_attr( $wps_subtitle ) . '" size="40" type="text" name="wps_subtitle" autocomplete="off" placeholder="' . esc_attr( apply_filters( 'wps_subtitle_field_placeholder', __( 'Enter local name here', 'wps_subtitle' ) ) ) . '" />';
	echo '<p class="description">Name in local language</p>';
	echo '<script>jQuery("tr.term-wps_subtitle-wrap").insertAfter("form#edittag tr.form-field:first");jQuery("#name").parents("td").find("p.description").html("Name in english language");</script>';
	echo "</td></tr>";
}

function _save_term_blog( $termID ){
	_save_term( $termID, 'blog' );
}

function _save_term_category( $termID ){
	_save_term( $termID, 'category' );
}

function _save_term_post_tag( $termID ){
	_save_term( $termID, 'post_tag' );
}

function _save_term( $termID, $taxonomyName ){
	if ( isset( $_POST['wps_subtitle'] ) ) {
		
		// get options from database - if not a array create a new one
		$termMeta = get_option( "$taxonomyName_$termID" );
		if ( !is_array( $termMeta ))
			$termMeta = array();
		
		// get value and save it into the database - maybe you have to sanitize your values (urls, etc...)
		$termMeta['wps_subtitle'] = isset( $_POST['wps_subtitle'] ) ? $_POST['wps_subtitle'] : '';
		update_option( "$taxonomyName_$termID", $termMeta, false );
	}
}

function _term_blog_columns( $columns ){
	$new_columns            = array();
    $new_columns['cb']      = $columns['cb'];
    $new_columns['thumb']   = $columns['thumb'];
    $new_columns['authors'] = $columns['authors'];
    $new_columns['name']    = $columns['name'];
	$new_columns['subtitle'] = 'Local Name';
	unset($columns['cb']);
	unset($columns['thumb']);
	unset($columns['authors']);
	unset($columns['name']);
	unset($columns['description']);
	return array_merge($new_columns, $columns);
}

function _term_category_columns( $columns ){
	$new_columns            = array();
    $new_columns['cb']      = $columns['cb'];
    $new_columns['name']    = $columns['name'];
	$new_columns['subtitle'] = 'Local Name';
	unset($columns['cb']);
	unset($columns['name']);
	unset($columns['description']);
	return array_merge($new_columns, $columns);
}

function _term_post_tag_columns( $columns ){
	$new_columns            = array();
    $new_columns['cb']      = $columns['cb'];
    $new_columns['name']    = $columns['name'];
	$new_columns['subtitle'] = 'Local Name';
	unset($columns['cb']);
	unset($columns['name']);
	unset($columns['description']);
	return array_merge($new_columns, $columns);
}

function _term_blog_column( $content, $column_name, $term_id ){
	return _term_column( $content, $column_name, $term_id, 'blog' );
}

function _term_category_column( $content, $column_name, $term_id ){
	return _term_column( $content, $column_name, $term_id, 'category' );
}

function _term_post_tag_column( $content, $column_name, $term_id ){
	return _term_column( $content, $column_name, $term_id, 'post_tag' );
}

function _term_column( $content, $column_name, $term_id, $taxonomyName ){
	$term_meta = get_option( "$taxonomyName_$term_id" );
	switch ($column_name) {
        case 'subtitle':
            $content = $term_meta['wps_subtitle'];
            break;
        default:
            break;
    }
    return $content;
}

function _add_admin_styles() {
	?>
	<style>
	#subtitlediv.top {
		margin-top: 5px;
		margin-bottom: 15px;
		position: relative;
	}
	#subtitlediv.top #subtitlewrap {
		border: 0;
		padding: 0;
	}
	#subtitlediv.top #wpsubtitle {
		background-color: #fff;
		font-size: 1.4em;
		line-height: 1em;
		margin: 0;
		outline: 0;
		padding: 3px 8px;
		width: 100%;
		height: 1.7em;
	}
	#subtitlediv.top #wpsubtitle::-webkit-input-placeholder { padding-top: 3px; }
	#subtitlediv.top #wpsubtitle:-moz-placeholder { padding-top: 3px; }
	#subtitlediv.top #wpsubtitle::-moz-placeholder { padding-top: 3px; }
	#subtitlediv.top #wpsubtitle:-ms-input-placeholder { padding-top: 3px; }
	#subtitlediv.top #subtitledescription {
		margin: 5px 10px 0 10px;
	}
	#wpsubtitle.error{border: 1px solid #cc0000 !important;}
	</style>
	<script>
	var vf = {
		valid 		: false,
		lastclick 	: false,
		reclick		: false,
		debug 		: false,
		drafts		: true,
	};

	(function($){

		// If a form value changes, mark the form as dirty
		$(document).on('change', '#wpsubtitle', function(){
			vf.valid = false;
			$(this).removeClass('error');
		});
		
		// When a .button is clicked we need to track what was clicked
		$(document).on('click', 'form#post .button, form#post input[type=submit] form#edittag .button, form#edittag input[type=submit]', function(){
			vf.lastclick = $(this);
			// The default 'click' runs first and then calls 'submit' so we need to retrigger after we have tracked the 'lastclick'
			if (vf.reclick){
				vf.reclick = false;
				vf.lastclick.trigger('click');
			}
		});
		
		// Intercept the form submission
		$(document).on('submit', 'form#post, form#edittag', function(){

			// If we don't have a 'lastclick' this is probably a preview where WordPress calls 'click' first
			if (!vf.lastclick){
				// We need to let our click handler run, then start the whole thing over in our handler
				vf.reclick = true;
				return false;
			}

			// We mith have already checked the form and vf.valid is set and just want all the other 'submit' functions to run, otherwise check the validation
			return vf.valid || do_validation($(this), vf.lastclick);
		});

		// Validate the ACF Validated Fields
		function do_validation(formObj, clickObj){
			// default the form validation to false
			vf.valid = false;
			// we have to know what was clicked to retrigger
			if (!clickObj) return false;
			// validate non-"publish" clicks unless vf.drafts is set to false
			if (!vf.drafts&&clickObj.attr('id')!='publish') return true;
			// gather form fields and values to submit to the server
			
			if($('#wpsubtitle').length ==  0 || $.trim($('#wpsubtitle').val()) != ''){
				vf.valid = true;
			}
			
			$('#ajax-loading').attr('style','');
			$('.submitbox .spinner').hide();
			$('.submitbox .button').removeClass('button-primary-disabled').removeClass('disabled');
			
			// if everything is good, reclick which will now bypass the validation
			if (vf.valid) {
				clickObj.click();
				return true;
			} else {
				// if it wasn't valid, show all the errors
				formObj.siblings('#subtitle_message, #message').remove();
				formObj.before('<div id="subtitle_message" class="error"><p>Validation Failed. See errors below.</p></div>');
				formObj.find('#wpsubtitle').addClass('error');
			}
			return false;
		}
	})(jQuery);
	</script>
	<?php
}

function _add_subtitle_field() {
	global $post;
	echo '<input type="hidden" name="wps_noncename" id="wps_noncename" value="' . wp_create_nonce( 'wp-subtitle' ) . '" />';
	echo '<div id="subtitlediv" class="top">';
		echo '<div id="subtitlewrap">';
			echo '<input type="text" id="wpsubtitle" name="wps_subtitle" value="' . esc_attr( get_post_meta( $post->ID, 'wps_subtitle', true ) ) . '" autocomplete="off" placeholder="' . esc_attr( apply_filters( 'wps_subtitle_field_placeholder', __( 'Enter local title here', 'wps_subtitle' ) ) ) . '" />';
		echo '</div>';

	// Description
	$description = apply_filters( 'wps_subtitle_field_description', '', $post );
	if ( ! empty( $description ) ) {
		echo '<div id="subtitledescription">' . $description . '</div>';
	}
	echo '</div>';
	echo '<script>jQuery("#title-prompt-text").text("Enter english title here");</script>';
}

function _add_meta_boxes() {
	add_meta_box( 'wps_subtitle_panel',  apply_filters( 'wps_meta_box_title', __( 'Local Title', 'wps_subtitle' ), 'post' ), '_add_subtitle_meta_box', 'post', 'normal', 'high' );
	add_meta_box( 'wps_subtitle_panel',  apply_filters( 'wps_meta_box_title', __( 'Local Title', 'wps_subtitle' ), 'page' ), '_add_subtitle_meta_box', 'page', 'normal', 'high' );
}

function _add_subtitle_meta_box() {
	global $post;
	echo '<input type="hidden" name="wps_noncename" id="wps_noncename" value="' . wp_create_nonce( 'wp-subtitle' ) . '" />';
	echo '<input type="text" id="wpsubtitle" name="wps_subtitle" value="' . esc_attr( get_post_meta( $post->ID, 'wps_subtitle', true ) ) . '" style="width:99%;" />';
	echo apply_filters( 'wps_subtitle_field_description', '', $post );
}

function manage_subtitle_columns( $columns ) {

	$new_columns = array();

	foreach ( $columns as $column => $value ) {
		$new_columns[ $column ] = $value;
		if ( 'title' == $column ) {
			$new_columns['wps_subtitle'] = __( 'Local Title', 'wps_subtitle' );
		}
	}

	return $new_columns;

}

function manage_subtitle_columns_content( $column_name, $post_id ) {

	if ( $column_name == 'wps_subtitle' ) {
		$post = get_post( $post_id );
		if ( $post ) {
			$subtitle = get_post_meta( $post->ID, 'wps_subtitle', true );
			echo apply_filters( 'wps_subtitle', $subtitle, $post_id );
		}
	}

}

function authors_with_posts( $query ) {

    if ( isset( $query->query_vars['query_id'] ) && 'authors_with_posts' == $query->query_vars['query_id'] ) {  
        $query->query_from = $query->query_from . ' LEFT OUTER JOIN (
                SELECT post_author, COUNT(*) as post_count
                FROM wp_posts
                WHERE post_type = "post" AND (post_status = "publish")
                GROUP BY post_author
            ) p ON (wp_users.ID = p.post_author)';
        $query->query_where = $query->query_where . ' AND post_count  > 0 ';  
    } 
}
add_action('pre_user_query','authors_with_posts');

function my_user_field( $user ) {
    $hideFront = get_the_author_meta( 'hide_on_front', $user->ID);
?>
    <table class="form-table">
        <th>
			<?php _e('Visibility'); ?>
		</th>
        <tr>
            <td><label><input type="checkbox" name="hide_on_front" <?php if ($hideFront == 'yes' ) { ?>checked="checked"<?php }?> value="yes" /> Do not show on recently joined authors list</label></td>
        </tr>
    </table>
<?php 
}


function my_save_custom_user_profile_fields( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) )
        return FALSE;

    update_usermeta( $user_id, 'hide_on_front', $_POST['hide_on_front'] );
}

add_action( 'show_user_profile', 'my_user_field' );
add_action( 'edit_user_profile', 'my_user_field' );
add_action( 'personal_options_update', 'my_save_custom_user_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_custom_user_profile_fields' );


function ordered_post_list_categories($thelist, $separator=' ')
{
    $categories = array();
    $unsort_categories = get_the_category();
    usort($unsort_categories, function($a, $b)
	{
		return strcmp($a->parent, $b->parent);
	});
    foreach ($unsort_categories as $category) {
        $categories[] = '<a href="' . get_category_link( $category->term_id ) . '" rel="category tag">' . $category->name . '</a>';
    }
    return implode($separator, $categories);
}

if(!defined('WP_ADMIN')) {
	add_filter('the_category','ordered_post_list_categories', 10, 2);
}

function remove_core_updates() {
	$user_ID = get_current_user_id();
	if($user_ID == 1){return;}
	add_action('init', create_function('$a',"remove_action( 'init', 'wp_version_check' );"),2);
	add_filter('pre_option_update_core','__return_null');
	add_filter('pre_site_transient_update_core','__return_null');
	remove_action('load-update-core.php','wp_update_plugins');
	add_filter('pre_site_transient_update_plugins','__return_null');
	add_filter('pre_site_transient_update_themes','__return_null');
}

function remove_quick_edit( $actions ) {
    unset($actions['inline hide-if-no-js']);
    return $actions;
}

if (is_admin()) {
	add_filter( 'post_row_actions', 'remove_quick_edit', 10, 2 );
	add_filter( 'tag_row_actions', 'remove_quick_edit', 10, 2 );
	add_filter( 'bulk_actions-' . 'edit-post', 'my_custom_bulk_actions' );
	add_filter( 'bulk_actions-' . 'edit-page', 'my_custom_bulk_actions' );
}

function my_custom_bulk_actions( $actions ){
	unset( $actions[ 'edit' ] );
	return $actions;
}

add_action( 'template_redirect', 'select_template' );

function select_template() {
	global $wp;
    $template = $wp->query_vars;
    if ( array_key_exists( 'blog', $template ) && 'main' == $template['blog'] ) {
		wp_redirect( get_bloginfo('url'), 301 );
		exit;
	}
    if ( array_key_exists( 'blog', $template ) && ( 'v1validateTicket' == $template['blog'] || 'phpshare' == $template['blog'] || 'phpsso' == $template['blog'] || 'defaultinterstitial' == $template['blog'] || 'blocked' == $template['blog'] || 'fetch_native_content' == $template['blog'] || 'commentMailer' == $template['blog']) ) {
		/* Make sure to set the 404 flag to false, and redirect to the page template. */
        global $wp_query;
        status_header( 200 );
        $wp_query->is_404 = false;
		if ( 'phpsso' == $template['blog'] ) {
			include( get_template_directory().'/sso.php' );
		} else if ( 'phpshare' == $template['blog'] ) {
			include( get_template_directory().'/share.php' );
		} else if ( 'v1validateTicket' == $template['blog'] ) {
			include( get_template_directory().'/v1validateTicket.php' );
		} else if ( 'defaultinterstitial' == $template['blog'] ) {
			if(isset($_GET['v']) && $_GET['v'] == 'mob'){
				include( get_template_directory().'/defaultinterstitial-wap.php' );
			} else {
				include( get_template_directory().'/defaultinterstitial.php' );
			}
		} else if ( 'blocked' == $template['blog'] ) {
			include( get_template_directory().'/blocked.php' );
		}else if ( 'fetch_native_content' == $template['blog'] ) {
			include( get_template_directory().'/fetch_native_content.php' );
		}else if ( 'commentMailer' == $template['blog'] ) {

			$lang = isLang();
			if(!empty($lang)){
				include( get_template_directory().'/mailer/commentMailer-'.$lang.'.php' );
			}	
		}
		exit;
	}
}

function get_post_excerpt_by_id( $post_id ) {
    setup_postdata( get_post( $post_id ) );
    $the_excerpt = get_the_excerpt();
    wp_reset_postdata();
    return $the_excerpt;
}

function blogs_custom_excerpt( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output = strip_tags($output);
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'blogs_custom_excerpt' );

function current_page_url() {
	$pageURL = 'http';
	if( isset($_SERVER["HTTPS"]) ) {
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

function my_cache_friendly_ajax_call_headers( $headers ) {
    if ( defined( 'DOING_AJAX' ) && isset( $_GET['action'] ) && ('getRecentArticlesJSONP' === $_GET['action'] || 'recentEntriesFeed' === $_GET['action'] || 'getAuthorsRecentArticle' === $_GET['action']) ) {
        $headers = array();
    }
    if ( ! is_admin() ) {
		global $wp;
		$template = $wp->query_vars;
		if ( is_array($template) && array_key_exists( 'blog', $template ) && ('defaultinterstitial' == $template['blog'] || 'blocked' == $template['blog'] ) ) {
			$headers = array();
		}
	}
    return $headers;
}

add_filter( 'nocache_headers', 'my_cache_friendly_ajax_call_headers', 9999 );

function isInitAd(){
	global $wp;
    $template = $wp->query_vars;
    if ( array_key_exists( 'blog', $template ) && 'defaultinterstitial' == $template['blog'] ) {
		return true;
	}
    return false;
}

function langblogs_mime_types($mime_types){
    //$mime_types['svg'] = 'image/svg+xml'; //Adding svg extension
    return $mime_types;
}
add_filter('upload_mimes', 'langblogs_mime_types', 1, 1);

// Disable use XML-RPC
add_filter( 'xmlrpc_enabled', '__return_false' );

add_action( 'wp', '_unhook_wp_head_footer' );
function _unhook_wp_head_footer(){
	global $wp_filter;
	$wp_head_blacklist = array( 'wp_generator', 'rsd_link', 'wlwmanifest_link', 'wp_shortlink_wp_head', 'feed_links', 'feed_links_extra', 'rest_output_link_wp_head', 'wp_oembed_add_discovery_links' );
	foreach ( $wp_filter['wp_head'] as $priority => $wp_head_hooks ) {
		if( is_array( $wp_head_hooks ) ){ // Check if this is an array
			foreach ( $wp_head_hooks as $wp_head_hook ) { // Loop the hook

				if( !is_array( $wp_head_hook['function'] ) && in_array( $wp_head_hook['function'], $wp_head_blacklist ) ){
					remove_action( 'wp_head', $wp_head_hook['function'], $priority ); // Remove the action from the hook
				}
			}
		}
	}
	remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
	remove_action( 'template_redirect', 'wp_shortlink_header', 11, 0 );
	header_remove( 'X-Pingback' );
}
