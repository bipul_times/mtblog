    <div class="tech_blue_nav">
    <script>var sectionid="5880659";</script>
    <a class="s web-search-icon" onclick="showSearch(this)"></a>
    <ul>
      <li class="home" id="home"><a pg="Blog_Nav_Pos#1" id="hlink" href="<?php echo $my_settings['main_site_url'];?>"><?php echo $my_settings['home_txt']; ?></a></li>
      <?php if (strpos($_SERVER['HTTP_HOST'], 'readerblogs.navbharattimes.indiatimes.com') !== false ) { ?>
      <li><a href="http://blogs.navbharattimes.indiatimes.com">ब्लॉग होम</a></li>
      <?php } ?>
      <li class="category<?php if(is_home()) { echo ' current'; }?>" id="bhome"><a pg="Blog_Nav_Pos#2" id="blink" href="<?php echo home_url(); ?>"><?php echo $my_settings['blogs_txt']; ?> </a></li>
      <?php
		  $current_url = current_page_url();
		  foreach($main_menus as $menu){
			  $liClassName = '';
			  if($current_url == $menu->url){
				  $liClassName = 'current';
			  }
			  if(is_array($menu->children) && count($menu->children) > 0){
				$menuId = ' id="menu_'.$menu->ID.'"';
				$arrowHTML = ' <span class="caret"></span>';
				$liClassName = $liClassName . ' showdrop';
				$dropDownHTML = '<ul class="dropdown-menu menu_'.$menu->ID.'menu" role="menu">';
				foreach($menu->children as $menu_child){
					$dropDownHTML = $dropDownHTML . '<li><a href="'.esc_url( $menu_child->url ).'" title="'.$my_settings['view_all_posts_in_txt'].' '.$menu_child->title.'">'.$menu_child->title.'</a></li>';
				}
				$dropDownHTML = $dropDownHTML . '</ul>';
			  } else {
				$menuId = '';
				$arrowHTML = '';
				$dropDownHTML = '';
			  }
			  echo '<li'.$menuId.' class="'.$liClassName.'"><a href="'.esc_url( $menu->url ).'" title="'.$my_settings['view_all_posts_in_txt'].' '.$menu->title.'">'.$menu->title.$arrowHTML.'</a>'.$dropDownHTML.'</li>';
		  }
      ?>
      <div id='sform' class="web-search-form hide-top-search" pg="Blog_Nav_Pos#search">
        <?php get_template_part( 'searchform', 'top' ); ?>
      </div>
    </ul>
  </div>

