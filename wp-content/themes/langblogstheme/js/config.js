require({
   paths: {
        jsrender: "https://timesofindia.indiatimes.com/jsrender.cms?"
    } ,
  config:{
     "times/api":{
comments : {url:site_url+"/wp-admin/admin-ajax.php?action=commentsdata"},
comments_newest : {url:site_url+"/wp-admin/admin-ajax.php?action=commentsdata"},
comments_oldest : {url:site_url+"/wp-admin/admin-ajax.php?action=commentsdata"},
comments_recommended : {url:site_url+"/wp-admin/admin-ajax.php?action=commentsdata"},
comments_discussed :{url:site_url+"/wp-admin/admin-ajax.php?action=commentsdata"},
comments_agree:{url:site_url+"/wp-admin/admin-ajax.php?action=commentsdata"},
comments_disagree:{url:site_url+"/wp-admin/admin-ajax.php?action=commentsdata"},
post_comment: {url:site_url+"/wp-admin/admin-ajax.php?action=postComment"},
validate_comment : {url:site_url+"/wp-admin/admin-ajax.php?action=validatecomment"},
rate_comment : {url:site_url+"/wp-admin/admin-ajax.php?action=ratecomment"},
 rate_comment_offensive : {url:site_url+"/wp-admin/admin-ajax.php?action=offensive"}
 },
"page":{
            channel:"TOI",
            siteId:"541cc79a8638cfd34bdc56d1a27c8cd7",
            domain:"indiatimes.com"
        },
         "social/facebook":{
            appid:117787264903013,
            load_js: true
        } 
 

  }
})
