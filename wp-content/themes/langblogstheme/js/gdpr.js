


/*
*** Defaults object have some variables defined based on projects (e.g.: 'logoutIframeSrc').

*** consentHTML variable have HTML snippet to be displayed and respective CSS is part of gdpr_css file
*** logoutIframeSrc variable have api path to logout user. Pass blank (e.g.: ''),if we don't need this functionality.

*** consentBannerClass variable have element class name, and it would be assigned to one div containing consentHTML.
*** consentAgreeButtonId varaible have element id of agree button, it is mentioned in consentHTML.

*** consentCookieName variable have cookie name w.r.t cookie consent is done or not. If cookie is present in browser. it means cookie consent is done by user.
*** consentCookieDefaultVal is '111' - To be checked
*** consentCookieAge is 'consentCookieName' cookie age ( no of days )

*** geoContCookieName variable have cookie name w.r.t geo-continent, instead of calling geoapi again, we are storing continent information in cookie.
*** geoContCookieAge is 'geoContCookieName' cookie age ( no of days )

*** consentReqCont variable contains list of continent where consent in required.
*** domainName for which we need to store cookie.
*** dynamicCssSrc  variable have path of CSS, if we want EU user specfic CSS. If not pass blank ('') in value.

*** TimesGDPR.common.consentModule.isConsentGiven(); will true/ false based on consent is given or not.
*** TimesGDPR.common.consentModule.isEUuser(); will give true/ false. Same as window._euuser
*** gdprCallback function - Sample form of getting callback
TimesGDPR.common.consentModule.gdprCallback(function(dataObj){
   console.log("User belongs to EU continent: ", dataObj.isEUuser);
});

*/  

if(window.location.hostname.indexOf("blogs.maharashtratimes.indiatimes.com") >=0){

    var propertyName = 'Maharashtra Times';
    var cookiesPolicyLink = 'https://maharashtratimes.indiatimes.com/cookiepolicy.cms';
    var privacyPolicyLink = 'https://maharashtratimes.indiatimes.com/privacypolicyeu.cms';
    var changeSettingLink = 'https://maharashtratimes.indiatimes.com/cookiepolicy.cms';
    var findOutMoreLink = 'https://maharashtratimes.indiatimes.com/cookiepolicy.cms';

}
else if(window.location.hostname.indexOf("blogs.navbharattimes.indiatimes.com") >=0){

    var propertyName = 'Navbharat Times';
    var cookiesPolicyLink = 'https://navbharattimes.indiatimes.com/cookiepolicy.cms';
    var privacyPolicyLink = 'https://navbharattimes.indiatimes.com/privacypolicyeu.cms';
    var changeSettingLink = 'https://navbharattimes.indiatimes.com/cookiepolicy.cms';
    var findOutMoreLink = 'https://navbharattimes.indiatimes.com/cookiepolicy.cms';

}
else if(window.location.hostname.indexOf("blogs.tamil.samayam.com") >=0){

    var propertyName = 'Tamil Samayam';
    var cookiesPolicyLink = 'https://tamil.samayam.com/cookiepolicy.cms';
    var privacyPolicyLink = 'https://tamil.samayam.com/privacypolicyeu.cms';
    var changeSettingLink = 'https://tamil.samayam.com/cookiepolicy.cms';
    var findOutMoreLink = 'https://tamil.samayam.com/cookiepolicy.cms';

}
else if(window.location.hostname.indexOf("blogs.eisamay.indiatimes.com") >=0){

    var propertyName = 'EiSamay';
    var cookiesPolicyLink = 'https://eisamay.indiatimes.com/cookiepolicy.cms';
    var privacyPolicyLink = 'https://eisamay.indiatimes.com/privacypolicyeu.cms';
    var changeSettingLink = 'https://eisamay.indiatimes.com/cookiepolicy.cms';
    var findOutMoreLink = 'https://eisamay.indiatimes.com/cookiepolicy.cms';

}else if(window.location.hostname.indexOf("blogs.telugu.samayam.com") >=0){

    var propertyName = 'Telugu Samayam';
    var cookiesPolicyLink = 'https://telugu.samayam.com/cookiepolicy.cms';
    var privacyPolicyLink = 'https://telugu.samayam.com/privacypolicyeu.cms';
    var changeSettingLink = 'https://telugu.samayam.com/cookiepolicy.cms';
    var findOutMoreLink = 'https://telugu.samayam.com/cookiepolicy.cms';
}
else if(window.location.hostname.indexOf("blogs.vijaykarnataka.indiatimes.com") >=0){

    var propertyName = 'Vijay Karnataka';
    var cookiesPolicyLink = 'https://vijaykarnataka.indiatimes.com/cookiepolicy.cms';
    var privacyPolicyLink = 'https://vijaykarnataka.indiatimes.com/privacypolicyeu.cms';
    var changeSettingLink = 'https://vijaykarnataka.indiatimes.com/cookiepolicy.cms';
    var findOutMoreLink = 'https://vijaykarnataka.indiatimes.com/cookiepolicy.cms';

}else if(window.location.hostname.indexOf("blogs.malayalam.samayam.com") >=0){

    var propertyName = 'Malayalam Samayam';
    var cookiesPolicyLink = 'https://malayalam.samayam.com/cookiepolicy.cms';
    var privacyPolicyLink = 'https://malayalam.samayam.com/privacypolicyeu.cms';
    var changeSettingLink = 'https://malayalam.samayam.com/cookiepolicy.cms';
    var findOutMoreLink = 'https://malayalam.samayam.com/cookiepolicy.cms';

}else if(window.location.hostname.indexOf("readerblogs.navbharattimes.indiatimes.com") >=0){

    var propertyName = 'Navbharat Times';
    var cookiesPolicyLink = 'https://navbharattimes.indiatimes.com/cookiepolicy.cms';
    var privacyPolicyLink = 'https://navbharattimes.indiatimes.com/privacypolicyeu.cms';
    var changeSettingLink = 'https://navbharattimes.indiatimes.com/cookiepolicy.cms';
    var findOutMoreLink = 'https://navbharattimes.indiatimes.com/cookiepolicy.cms';

}

var contentHTML = '<div class="consent-popup"><div class="consent-banner wrapper"><h2> Cookies on '+ propertyName +' website </h2>    <div class="column1" id="consent-prompt"><p>'+ propertyName +' has updated its Privacy and Cookie policy. We use cookies to ensure that we give you the better experience on our website. If you continue without changing your settings, we\'ll assume that you are happy to receive all cookies on the '+propertyName+' website. However, you can change your cookie setting at any time by clicking on our <a target="_blank" href="'+ cookiesPolicyLink +'"> Cookie Policy</a> at any time. You can also see our                 <a target="_blank" href="'+ privacyPolicyLink +'">Privacy Policy</a></property></p></div><div class="column2"><ul>      <li><button id="consent-continue-button" type="button">Agree &amp; Continue</button></li>       <li><a href="'+ changeSettingLink +'">Cookie Policy</a></li><li><a href="'+ findOutMoreLink +'" id="consent-more">Find out more</a></li>        </ul>   </div></div></div>';
    
function enablegdpr(){

    //$('.cmtbtn-wrapper, .bottom-comments, .mobile-comment, .articlecomment, .writeacomment').remove();
    if(window._euuser){
        $('.cmtbtn-wrapper .cmtbtn.view').width('99%');
        $('.cmtbtn-wrapper .cmtbtn.view').css('border-right','none');
        $('.cmtbtn-wrapper .cmtbtn.add').hide();
        if($('.cmtbtn-wrapper .cmtbtn.view').text()=='Viewcomments'){
            $('.cmtbtn-wrapper .cmtbtn.view').remove();
        }
        $('#comment-holder').remove();
        $('.footbar').hide();
        $('.comment-box a').each(function(){
            if($(this).hasClass("name")){
                var a = $(this).text();
                a = '<span class="name">'+a+'</span>';
                $(this).replaceWith(a);
            }
        });
        $('.comments-list').find("[data-plugin='comment-user-follow_wrapper']").hide();
        $('.blog-gray-btn.mobile, .blog-gray-btn').each(function(){  
            if($(this).text()=="Write for TOI Blogs" || $(this).text()=="Write for TOI Blogs" ){
                $(this).remove();
            }
        });
        $('.list-inline li a').each(function(){
            if($(this).text()=="Feedback" || $(this).text()=="Newsletter" ){
                $(this).parent().remove();
            }
        });
        $('.addacomment').remove();
        $('.feedbackeu').remove();
        $('.seoLinks li').each(function(){
            if($(this).text()=="Kundali matching"){
            $(this).remove();
            }
        });
    }
}
    
TimesGDPR = window.TimesGDPR || {};
TimesGDPR.common = window.TimesGDPR.common || {};
TimesGDPR.common.consentModule = (function () {
    
    var consentText = propertyName + ' has updated its Privacy and Cookie policy.';
    consentText += 'We use cookies to ensure that we give you the better experience on our' + propertyName + ' website.';
    consentText += 'If you continue without changing your settings, we\'ll assume that you are happy to receive all cookies on the Gadgets Now website.';
    consentText += 'However, you can change your cookie setting at any time by clicking on our Cookie Policy at any time.';
    // Keep this variable private inside this closure scope
    var defaults = {
            'consentHTML' : contentHTML,
            'consentText' : consentText,
            'consentBannerClass' : 'consent-popup',
            'consentAgreeButtonId' : 'consent-continue-button',
            'consentCookieName': 'ckns_policy',
            'consentCookieVal': '',
            'consentCookieAge': 365,
            'geoContCookieName' : 'geo_continent',
            'geoContCookieAge' : 1,
            'optOutCookieName' : 'optout',
            'optOutCookieDefaultVal' : 1,
            'optOutAcceptCookieVal' : 0,
            'optOutCookieAge' : 365,
            'consentReqCont' : ["EU"],
            'domainName' : '',
            'columbiaCoookie' : '_col_uuid',
            'dynamicCssSrc' : '',
            'productCode' : 'gn-web'
        };
        
    var apiRef = {
        'cbfn' : [],
        'domReady' : false,
        'displayPopupCalled' : false,
        'postConsent' : true,
        'consentAPIstatus' : '',
        'consentAPIretryCount' : 1
    };
    
    var consentAPIObj = {
        'consentAPIUrl' : 'https://etservices2.indiatimes.com/toi/consent/jsonp',
        'consentAPIinput' : {
                                "consent": {
                                   "consents": [
                                        {
                                        "agree": true,
                                        "created": "",
                                        "dataPoint": {
                                            "id": 5 // it is for cookie consent
                                        },
                                       "text": "",
                                       "updated": "",
                                     }
                                   ],
                                   "productCode": ""
                                 },
                                 "primaryId": "",
                            }
    };
    
    //This function would show pop-up for EU user, if consent is not received yet
    var displayPopup = function() {
        if(apiRef.domReady && (typeof defaults.geoDataAvailable != 'undefined') && !apiRef.displayPopupCalled){
            apiRef.displayPopupCalled = true;
            if(defaults.isEUuser){
                document.body.className += ' ' + 'euuser';
            }
            var _gdpr_ckns_policy_val = getCookie(defaults.consentCookieName);
            if((!_gdpr_ckns_policy_val) && defaults.isEUuser){
                renderHtml();
                attachEvents();
            }
        }
        
    };
    
    var attachEvents = function(){
        document.getElementById(defaults.consentAgreeButtonId).onclick = agreeButtonClickHandler;
    };

    var loadDynamicResource = function(){
        var s = document.createElement("link"), _head = document.getElementsByTagName("head")[0];
        s.rel  = 'stylesheet';
        s.type = 'text/css';
        s.href = defaults.dynamicCssSrc;
        if (typeof dynamicCssOnloadFn === 'function') { 
            if (typeof s.addEventListener != 'undefined') {
                s.addEventListener('load', dynamicCssOnloadFn, false);
            } 
            else if(typeof s.onreadystatechange != 'undefined') {
                s.onreadystatechange = function() {
                    ieCallback(s, dynamicCssOnloadFn);
                };
            }
            else{
                setTimeout(dynamicCssOnloadFn , 100);
            }
        }
        _head.appendChild(s);
    };
    
    var dynamicCssOnloadFn = function(){
        displayPopup();
    };
    
    //This function check geo cookie and geoinfo object and set cookie after getting geoinfo object (if required)
    var cbGeoLocation = function(){
        var _geo_contCookieVal = getCookie(defaults.geoContCookieName);
        if(typeof window.geoinfo == 'undefined' && (!_geo_contCookieVal)){
            var s = document.createElement("script"), _head = document.getElementsByTagName("head")[0];
            s.src = "https://geoapi.indiatimes.com/?cb=1";
            if (typeof geoapiOnloadFn === 'function') { 
                if (typeof s.addEventListener != 'undefined') {
                    s.addEventListener('load', geoapiOnloadFn, false);
                    s.addEventListener('error', geoapiOnloadFn, false);
                } 
                else if(typeof s.onreadystatechange != 'undefined'){
                    s.onreadystatechange = function() {
                        ieCallback(s, geoapiOnloadFn);
                    };
                }
                else{
                    setTimeout(geoapiOnloadFn, 100);
                }
            }
            _head.appendChild(s);
        }
        else if (typeof window.geoinfo != 'undefined' && (!_geo_contCookieVal)){
            geoapiOnloadFn();
        }
        else{
            setResource(_geo_contCookieVal);
        }
    };
    
    var geoapiOnloadFn = function(){
        if(typeof window.geoinfo != 'undefined' && !!window.geoinfo.Continent){
            var _geoContUpper = window.geoinfo.Continent.toUpperCase();
            setCookie(defaults.geoContCookieName, _geoContUpper, '' , defaults.geoContCookieAge);
            setResource(_geoContUpper);
        }
        else{
            var falioverContName = 'EU', falioverCookAge  = 0.25;
            setCookie(defaults.geoContCookieName, falioverContName, '', falioverCookAge);
            setResource(falioverContName);
        }
    };
    
    //This function loads EU user specific CSS and set some HTML class, JS object property to check, user is EU user or not
    var setResource = function(_geo_contCookieVal){
        defaults.geoDataAvailable = true;
        if (!Array.prototype.indexOf){
            var concatContinentData = defaults.consentReqCont.join(",");
        }
        else{
            var concatContinentData = defaults.consentReqCont;
        }
        if(concatContinentData.indexOf(_geo_contCookieVal.toUpperCase()) > -1){
            defaults.isEUuser = true;
            window._euuser = true;

            var _optout_val = getCookie(defaults.optOutCookieName);
            var _gdpr_ckns_policy_val = getCookie(defaults.consentCookieName);
            
            if(!_gdpr_ckns_policy_val && !_optout_val){
                setCookie(defaults.optOutCookieName, defaults.optOutCookieDefaultVal, defaults.domainName , defaults.optOutCookieAge);
            }
            executeCalbacks();
            
            if(!!defaults.dynamicCssSrc){
                loadDynamicResource();
            }
            else{
                displayPopup();
            }
        }
        else{
            defaults.isEUuser = false;
            executeCalbacks();
        }
    };
    
    var getReturnObject = function(){
        return { 
            'isEUuser' : TimesGDPR.common.consentModule.isEUuser(), 
            'consentGiven': TimesGDPR.common.consentModule.isConsentGiven()
        };
    }
    
    var executeCalbacks = function(){
        if(apiRef.cbfn.length){
            var isEUuserObj = getReturnObject();
            for( var i=apiRef.cbfn.length-1; i>= 0; i--){
                typeof apiRef.cbfn[i] == "function" && apiRef.cbfn[i](isEUuserObj);
                apiRef.cbfn[i] = null;
            }
                
        }
    }
    
    //This is continue/agree button event handler
    var agreeButtonClickHandler =  function(){
        var _consentBannerEles = document.querySelectorAll('.'+ defaults.consentBannerClass)
        if(_consentBannerEles.length > 0){
            _consentBannerEles[0].style.display = "none";    
        }
        var _columbiaCoookieVal = getCookie(defaults.columbiaCoookie);
        
        if(!!_columbiaCoookieVal){
            defaults.consentCookieVal = _columbiaCoookieVal;
        }
        else{
            defaults.consentCookieVal = getUuidv4();
        }
        
        if(!!apiRef.postConsent){
            consentAPIObj.consentAPIinput.primaryId = defaults.consentCookieVal;
            consentAPIObj.consentAPIinput.consent.consents[0].text = defaults.consentText;
            consentAPIObj.consentAPIinput.consent.productCode = defaults.productCode;
            TimesGDPR.common.ajax.post(consentAPIObj.consentAPIUrl, consentAPIObj.consentAPIinput, consentAPIResponse, consentAPIErrorCallback, true);
        }
        else{
            setConsentOptoutCookies();
        }
    };
    
    /*Dont pass days to set session cookie*/
    var setCookie = function(name, value, domainName, days){
        if(!!domainName){
            var domain = ";domain="+domainName;
        }
        if(!!days){
            //Below variable is one day time in mili seconds.
            var _oneDayinMS = 24*60*60*1000;
            var d = new Date();
            d.setTime(d.getTime() + (days*_oneDayinMS));
            var expires = ";expires="+d.toUTCString();
        }
        document.cookie = name+"=" +value + ( ( expires ) ? expires : '' ) + ( ( domain ) ? domain : '' ) + ";path=/";
    };
    
    var getCookie = function(name) {
        var re = new RegExp(name + "=([^;]+)");
        var value = re.exec(document.cookie);
        return (value != null) ? unescape(value[1]) : null;
    };
    
    var getDomain = function( url ) {
        return "." + ( url || document.location.host ).split( ":" )[ 0 ].split( "." ).reverse().slice( 0, 2 ).reverse().join( "." );
    };
    
    var getUuidv4 = function() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };
    
    var renderHtml = function() {
        var consentBannerCheck = document.querySelectorAll('.'+defaults.consentBannerClass);
        if(consentBannerCheck.length){
            //consent banner is already present
            return;
        }
        
        var _html= document.createElement('div');
        
        _html.innerHTML= defaults.consentHTML;
        _html.className= defaults.consentBannerClass;
        
        // Append into text.
        var body = document.getElementsByTagName("body")[0]; 
        body.insertBefore(_html, body.childNodes[0]);
    };
    
    var ieCallback = function(el, callback) {
        if(el.readyState === 'loaded' || el.readyState === 'complete'){
            callback();
        }
    };
    
    var gdprCallback = function(callbackFn) {
        if (typeof defaults.geoDataAvailable != 'undefined') {
            var _geo_contCookieVal = getCookie(defaults.geoContCookieName);
            var isEUuserObj = getReturnObject();
            callbackFn(isEUuserObj);
        } 
        else {
            apiRef.cbfn.push(callbackFn);
        }
    };
    
    var isEUuser = function() {
        if(typeof defaults.isEUuser != 'undefined'){
            return defaults.isEUuser;
        }
        return;
    };
    
    var isConsentGiven = function() {
        var _consentCookieVal =  getCookie(defaults.consentCookieName);
        if(!!_consentCookieVal){
            return true;
        }
        return false;
    };
    
    var consentAPIResponse = function(response) {
        if(typeof response != 'undefined' && response.status.toUpperCase() == 'SUCCESS'){
            setConsentOptoutCookies();
            return apiRef.consentAPIstatus = response;
        }
        else{
            consentAPIErrorCallback();
        }
        return apiRef.consentAPIstatus;
    };
    
    var setConsentOptoutCookies = function(){
        setCookie(defaults.consentCookieName, defaults.consentCookieVal, '' , defaults.consentCookieAge);
        //setCookie(defaults.optOutCookieName, defaults.optOutAcceptCookieVal, defaults.domainName , defaults.optOutCookieAge);
    }
    
    var consentAPIErrorCallback = function() {
            if(apiRef.consentAPIretryCount > 0){
                apiRef.consentAPIretryCount--;
                agreeButtonClickHandler();
            }
            else{
                return;
            }
    };
    
    // It starts the flow either to display consent pop-up or not for EU user based on consent acceptance status
    var start = function(){
        defaults.domainName = getDomain();
        cbGeoLocation();
        if(typeof document.addEventListener != 'undefined'){
            document.addEventListener('DOMContentLoaded', function(){
                apiRef.domReady = true;
                displayPopup();
            });
        }
        else if(typeof document.attachEvent != 'undefined'){
            document.attachEvent('onreadystatechange', function(){
                if(document.readyState === 'loaded' || document.readyState === 'complete'){
                    apiRef.domReady = true;
                    displayPopup();
                }
            });
        }
        else{
            setTimeout( function(){
                apiRef.domReady = true;
                displayPopup();
            }, 1000);
        }
    };

    // Explicitly reveal public pointers to the private functions 
    // that we want to reveal publicly
    return {
        agreeButtonClickHandler : agreeButtonClickHandler,
        isConsentGiven : isConsentGiven,
        gdprCallback : gdprCallback,
        isEUuser : isEUuser,
        start: start
    }
})();
TimesGDPR.common.ajax = {
                createRequest: function() {
                        var xhr;
                        var counter = 0;
                        var activexVersions = [
                            "MSXML2.XmlHttp.6.0",
                            "MSXML2.XmlHttp.5.0",
                            "MSXML2.XmlHttp.4.0",
                            "MSXML2.XmlHttp.3.0",
                            "MSXML2.XmlHttp.2.0",
                            "Microsoft.XmlHttp"
                        ];
                        
                        if (typeof XMLHttpRequest !== 'undefined') {
                            xhr = new XMLHttpRequest();
                        } else {
                            for (; counter < activexVersions.length; counter++) {
                                try {
                                    xhr = new ActiveXObject(activexVersions[counter]);
                                    break;
                                } catch (e) {}
                            }   
                        }
                        
                        if (!(xhr && "withCredentials" in xhr) && typeof XDomainRequest != "undefined") { 
                          xhr = new XDomainRequest();
                        }
                        
                        return xhr;
                   },
                   
                    post: function (url, data, callback, errorCallback, isJson) {
                        var oReq = this.createRequest();
                        oReq.open('POST', url, true);
                        if (!(!(oReq && "withCredentials" in oReq) && typeof XDomainRequest != "undefined")) {
                            oReq.onreadystatechange = function () {
                                var response = {};
                                if (oReq.readyState == 4) {
                                    if(oReq.status == 200){
                                        if(oReq.responseText) {
                                            try {
                                                response = JSON.parse(oReq.responseText);
                                            }
                                            catch(e){
                                            }
                                        }
                                        callback(response);
                                    }
                                    else{
                                        errorCallback();
                                    }
                                }
                                
                            };
                        } else {
                            // IE CORS
                            oReq.onload = function () {
                                var response = {};
                                try {
                                    response = JSON.parse(oReq.responseText);
                                }
                                catch(e){}
                                callback(response);
                            };
                        }
                        oReq.onerror = function () {
                            errorCallback();
                        }
                        oReq.send(JSON.stringify(data));
                    }
        };

TimesGDPR.common.consentModule.start();
