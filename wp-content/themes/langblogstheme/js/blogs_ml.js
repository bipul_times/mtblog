/*! 
 * samplejs Tue Oct 20 2015 16:35:21 GMT+0530 (IST) 
*/

/**
 * @license almond 0.3.1 Copyright (c) 2011-2014, The Dojo Foundation All Rights Reserved.
 * Available via the MIT or new BSD license.
 * see: http://github.com/jrburke/almond for details
 */
//Going sloppy to avoid 'use strict' string cost, but strict practices should
//be followed.
/*jslint sloppy: true */
/*global setTimeout: false */

var requirejs, require, define;
(function (undef) {
    var main, req, makeMap, handlers,
        defined = {},
        waiting = {},
        config = {},
        defining = {},
        hasOwn = Object.prototype.hasOwnProperty,
        aps = [].slice,
        jsSuffixRegExp = /\.js$/;

    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }

    /**
     * Given a relative module name, like ./something, normalize it to
     * a real name that can be mapped to a path.
     * @param {String} name the relative name
     * @param {String} baseName a real name that the name arg is relative
     * to.
     * @returns {String} normalized name
     */
    function normalize(name, baseName) {
        var nameParts, nameSegment, mapValue, foundMap, lastIndex,
            foundI, foundStarMap, starI, i, j, part,
            baseParts = baseName && baseName.split("/"),
            map = config.map,
            starMap = (map && map['*']) || {};

        //Adjust any relative paths.
        if (name && name.charAt(0) === ".") {
            //If have a base name, try to normalize against it,
            //otherwise, assume it is a top-level require that will
            //be relative to baseUrl in the end.
            if (baseName) {
                name = name.split('/');
                lastIndex = name.length - 1;

                // Node .js allowance:
                if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                    name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, '');
                }

                //Lop off the last part of baseParts, so that . matches the
                //"directory" and not name of the baseName's module. For instance,
                //baseName of "one/two/three", maps to "one/two/three.js", but we
                //want the directory, "one/two" for this normalization.
                name = baseParts.slice(0, baseParts.length - 1).concat(name);

                //start trimDots
                for (i = 0; i < name.length; i += 1) {
                    part = name[i];
                    if (part === ".") {
                        name.splice(i, 1);
                        i -= 1;
                    } else if (part === "..") {
                        if (i === 1 && (name[2] === '..' || name[0] === '..')) {
                            //End of the line. Keep at least one non-dot
                            //path segment at the front so it can be mapped
                            //correctly to disk. Otherwise, there is likely
                            //no path mapping for a path starting with '..'.
                            //This can still fail, but catches the most reasonable
                            //uses of ..
                            break;
                        } else if (i > 0) {
                            name.splice(i - 1, 2);
                            i -= 2;
                        }
                    }
                }
                //end trimDots

                name = name.join("/");
            } else if (name.indexOf('./') === 0) {
                // No baseName, so this is ID is resolved relative
                // to baseUrl, pull off the leading dot.
                name = name.substring(2);
            }
        }

        //Apply map config if available.
        if ((baseParts || starMap) && map) {
            nameParts = name.split('/');

            for (i = nameParts.length; i > 0; i -= 1) {
                nameSegment = nameParts.slice(0, i).join("/");

                if (baseParts) {
                    //Find the longest baseName segment match in the config.
                    //So, do joins on the biggest to smallest lengths of baseParts.
                    for (j = baseParts.length; j > 0; j -= 1) {
                        mapValue = map[baseParts.slice(0, j).join('/')];

                        //baseName segment has  config, find if it has one for
                        //this name.
                        if (mapValue) {
                            mapValue = mapValue[nameSegment];
                            if (mapValue) {
                                //Match, update name to the new value.
                                foundMap = mapValue;
                                foundI = i;
                                break;
                            }
                        }
                    }
                }

                if (foundMap) {
                    break;
                }

                //Check for a star map match, but just hold on to it,
                //if there is a shorter segment match later in a matching
                //config, then favor over this star map.
                if (!foundStarMap && starMap && starMap[nameSegment]) {
                    foundStarMap = starMap[nameSegment];
                    starI = i;
                }
            }

            if (!foundMap && foundStarMap) {
                foundMap = foundStarMap;
                foundI = starI;
            }

            if (foundMap) {
                nameParts.splice(0, foundI, foundMap);
                name = nameParts.join('/');
            }
        }

        return name;
    }

    function makeRequire(relName, forceSync) {
        return function () {
            //A version of a require function that passes a moduleName
            //value for items that may need to
            //look up paths relative to the moduleName
            var args = aps.call(arguments, 0);

            //If first arg is not require('string'), and there is only
            //one arg, it is the array form without a callback. Insert
            //a null so that the following concat is correct.
            if (typeof args[0] !== 'string' && args.length === 1) {
                args.push(null);
            }
            return req.apply(undef, args.concat([relName, forceSync]));
        };
    }

    function makeNormalize(relName) {
        return function (name) {
            return normalize(name, relName);
        };
    }

    function makeLoad(depName) {
        return function (value) {
            defined[depName] = value;
        };
    }

    function callDep(name) {
        if (hasProp(waiting, name)) {
            var args = waiting[name];
            delete waiting[name];
            defining[name] = true;
            main.apply(undef, args);
        }

        if (!hasProp(defined, name) && !hasProp(defining, name)) {
            throw new Error('No ' + name);
        }
        return defined[name];
    }

    //Turns a plugin!resource to [plugin, resource]
    //with the plugin being undefined if the name
    //did not have a plugin prefix.
    function splitPrefix(name) {
        var prefix,
            index = name ? name.indexOf('!') : -1;
        if (index > -1) {
            prefix = name.substring(0, index);
            name = name.substring(index + 1, name.length);
        }
        return [prefix, name];
    }

    /**
     * Makes a name map, normalizing the name, and using a plugin
     * for normalization if necessary. Grabs a ref to plugin
     * too, as an optimization.
     */
    makeMap = function (name, relName) {
        var plugin,
            parts = splitPrefix(name),
            prefix = parts[0];

        name = parts[1];

        if (prefix) {
            prefix = normalize(prefix, relName);
            plugin = callDep(prefix);
        }

        //Normalize according
        if (prefix) {
            if (plugin && plugin.normalize) {
                name = plugin.normalize(name, makeNormalize(relName));
            } else {
                name = normalize(name, relName);
            }
        } else {
            name = normalize(name, relName);
            parts = splitPrefix(name);
            prefix = parts[0];
            name = parts[1];
            if (prefix) {
                plugin = callDep(prefix);
            }
        }

        //Using ridiculous property names for space reasons
        return {
            f: prefix ? prefix + '!' + name : name, //fullName
            n: name,
            pr: prefix,
            p: plugin
        };
    };

    function makeConfig(name) {
        return function () {
            return (config && config.config && config.config[name]) || {};
        };
    }

    handlers = {
        require: function (name) {
            return makeRequire(name);
        },
        exports: function (name) {
            var e = defined[name];
            if (typeof e !== 'undefined') {
                return e;
            } else {
                return (defined[name] = {});
            }
        },
        module: function (name) {
            return {
                id: name,
                uri: '',
                exports: defined[name],
                config: makeConfig(name)
            };
        }
    };

    main = function (name, deps, callback, relName) {
        var cjsModule, depName, ret, map, i,
            args = [],
            callbackType = typeof callback,
            usingExports;

        //Use name if no relName
        relName = relName || name;

        //Call the callback to define the module, if necessary.
        if (callbackType === 'undefined' || callbackType === 'function') {
            //Pull out the defined dependencies and pass the ordered
            //values to the callback.
            //Default to [require, exports, module] if no deps
            deps = !deps.length && callback.length ? ['require', 'exports', 'module'] : deps;
            for (i = 0; i < deps.length; i += 1) {
                map = makeMap(deps[i], relName);
                depName = map.f;

                //Fast path CommonJS standard dependencies.
                if (depName === "require") {
                    args[i] = handlers.require(name);
                } else if (depName === "exports") {
                    //CommonJS module spec 1.1
                    args[i] = handlers.exports(name);
                    usingExports = true;
                } else if (depName === "module") {
                    //CommonJS module spec 1.1
                    cjsModule = args[i] = handlers.module(name);
                } else if (hasProp(defined, depName) ||
                           hasProp(waiting, depName) ||
                           hasProp(defining, depName)) {
                    args[i] = callDep(depName);
                } else if (map.p) {
                    map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                    args[i] = defined[depName];
                } else {
                    throw new Error(name + ' missing ' + depName);
                }
            }

            ret = callback ? callback.apply(defined[name], args) : undefined;

            if (name) {
                //If setting exports via "module" is in play,
                //favor that over return value and exports. After that,
                //favor a non-undefined return value over exports use.
                if (cjsModule && cjsModule.exports !== undef &&
                        cjsModule.exports !== defined[name]) {
                    defined[name] = cjsModule.exports;
                } else if (ret !== undef || !usingExports) {
                    //Use the return value from the function.
                    defined[name] = ret;
                }
            }
        } else if (name) {
            //May just be an object definition for the module. Only
            //worry about defining if have a module name.
            defined[name] = callback;
        }
    };

    requirejs = require = req = function (deps, callback, relName, forceSync, alt) {
        if (typeof deps === "string") {
            if (handlers[deps]) {
                //callback in this case is really relName
                return handlers[deps](callback);
            }
            //Just return the module wanted. In this scenario, the
            //deps arg is the module name, and second arg (if passed)
            //is just the relName.
            //Normalize module name, if it contains . or ..
            return callDep(makeMap(deps, callback).f);
        } else if (!deps.splice) {
            //deps is a config object, not an array.
            config = deps;
            if (config.deps) {
                req(config.deps, config.callback);
            }
            if (!callback) {
                return;
            }

            if (callback.splice) {
                //callback is an array, which means it is a dependency list.
                //Adjust args if there are dependencies
                deps = callback;
                callback = relName;
                relName = null;
            } else {
                deps = undef;
            }
        }

        //Support require(['a'])
        callback = callback || function () {};

        //If relName is a function, it is an errback handler,
        //so remove it.
        if (typeof relName === 'function') {
            relName = forceSync;
            forceSync = alt;
        }

        //Simulate async callback;
        if (forceSync) {
            main(undef, deps, callback, relName);
        } else {
            //Using a non-zero value because of concern for what old browsers
            //do, and latest browsers "upgrade" to 4 if lower value is used:
            //http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html#dom-windowtimers-settimeout:
            //If want a value immediately, use require('id') instead -- something
            //that works in almond on the global level, but not guaranteed and
            //unlikely to work in other AMD implementations.
            setTimeout(function () {
                main(undef, deps, callback, relName);
            }, 4);
        }

        return req;
    };

    /**
     * Just drops the config on the floor, but returns req in case
     * the config return value is used.
     */
    req.config = function (cfg) {
        return req(cfg);
    };

    /**
     * Expose module registry for debugging and tooling
     */
    requirejs._defined = defined;

    define = function (name, deps, callback) {
        if (typeof name !== 'string') {
            throw new Error('See almond README: incorrect module build, no module name');
        }

        //This module may not have dependencies
        if (!deps.splice) {
            //deps is not an array, so probably means
            //an object literal or factory function for
            //the value. Adjust args.
            callback = deps;
            deps = [];
        }

        if (!hasProp(defined, name) && !hasProp(waiting, name)) {
            waiting[name] = [name, deps, callback];
        }
    };

    define.amd = {
        jQuery: true
    };
}());

/**
 * 'cookie' module.
 *
 * @module cookie
 */
define( 'tiljs/cookie',[], function () {
	var mod_cookie = {};
	//    var default_config = {
	//          localstorage : false //use localstorage if available or else cookie
	//    };
	//
	//    var config = $.extend({}, default_config, module.config());

	/**
	 * Get value of a cookie
	 *
	 * @memberOf module:cookie#
	 * @function get
	 *
	 * @param name {String} name of the cookie for which value is required,
	 *                        if name is not provided an object with all cookies is returned
	 * @returns value {String | Array} value of the requested cookie / Array of all cookies
	 *
	 * @example
	 *
	 *  require(['cookie'],function(cookie){
	 *     var abc_cookie = cookie.get("abc");
	 *  });
	 */
	mod_cookie.get = function ( name ) {
		var result = name ? undefined : {};
		var cookies = document.cookie ? document.cookie.split( '; ' ) : [];
		for( var i = 0, l = cookies.length; i < l; i++ ) {
			var parts = cookies[ i ].split( '=' );
			var nameK = decodeURIComponent( parts.shift() );
			var cookie = parts.join( '=' );
			cookie = mod_cookie._parseCookieValue( cookie );
			if( name && name === nameK ) {
				result = cookie;
				break;
			}
			if( !name && cookie !== undefined ) {
				result[ nameK ] = cookie;
			}
		}
		return result;
	};

	/**
	 * Cookie Set,Get,Delete
	 */
	mod_cookie.getAll = function () {
		return mod_cookie.get();
	};
	/**
	 * Remove a cookie
	 *
 	 * @memberOf module:cookie#
	 * @function remove
	 *
	 * @param {String} name name of the cookie to be removed
	 * @param {String} [path] path of the cookie
	 * @param {String} [domain] domain of the cookie
	 *
	 * @example
	 *
	 *  require(['cookie'],function(cookie){
	 *     cookie.remove("abc");
	 *  });
	 */
	mod_cookie.remove = function ( name, path, domain ) {
		if( name ) {
			domain = ( domain || document.location.host ).split( ":" )[ 0 ];
			path = path || document.location.pathname;
			mod_cookie.set( name, null, -1, path, domain );
		}
	};
	/**
	 * Set a cookie
	 *
	 * @param {String} name name of the cookie to be set
	 * @param {String} value value of the cookie to be set
	 * @param {Number} days number of days for which the cookie is to be set
	 * @param {String} path path of the cookie to be set
	 * @param {String} domain domain of the cookie to be set
	 * @param {Boolean} secure true if the cookie is to be set on https only
	 */
	mod_cookie.set = function ( name, value, days, path, domain, secure ) {
		var expires = '';
		days = ( days !== undefined ) ? days : 30;
		var date = new Date();
		date.setTime( date.getTime() + ( days * 24 * 60 * 60 * 1000 ) );
		expires = '; expires=' + date.toGMTString();
		domain = ( domain || document.location.host ).split( ":" )[ 0 ]; //removing port
		path = path || document.location.pathname;
		//Removing file name, fix for IE11
		if( /\/.*\..*/.test( path ) ) { //if path contains file name
			path = path.split( "/" );
			path.pop();
			path = path.join( "/" );
		}
		document.cookie = name + '=' +
		value + expires +
		( ( path ) ? ';path=' + path : '' ) +
		( ( domain && domain !='localhost' ) ? ';domain=' + domain : '' ) +
		( ( secure ) ? ';secure' : '' );
	};
	mod_cookie._parseCookieValue = function ( s ) {
		if( s.indexOf( '"' ) === 0 ) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice( 1, -1 ).replace( /\\"/g, '"' ).replace( /\\\\/g, '\\' );
		}
		try {
			// If we can't decode the cookie, ignore it, it's unusable.
			// Replace server-side written pluses with spaces.
			return decodeURIComponent( s.replace( /\+/g, ' ' ) );
		} catch( e ) {}
	};
	return mod_cookie;
} );

/**
 * 'event' module.
 *
 * @module event
 */
define( 'tiljs/event',[], function () {
	var mod_event = {},
		pubsub = {},
		onsubscribe_prefix = "__on_",
		generateUid = ( function () {
			var id = 0;
			return function () {
				return id++;
			};
		} )();
	/**
	 * Publish subscribed events
	 *
	 * @param name Method name for the event to be published
	 * @param data data to be passed to the subscribed event
	 * @returns null
	 */
	mod_event.publish = function ( name, data ) {
		if( !name ) {
			return null;
		}
		if( pubsub[ name ] ) {
			for( var e in pubsub[ name ] ) {
				if( pubsub[ name ].hasOwnProperty( e ) ) {
					var eventCallback = pubsub[ name ][ e ];
					try { //try catch done to keep publisher running in case of error in event callback
						eventCallback( data );
					} catch( err ) {
						mod_event.publish( "logger.error", err.stack );
					}
				}
			}
		}
	};
	mod_event.subscribeAll = function ( name, eventCallback, options ) {
		if( name instanceof Array ) {
			var eventIds = [];
			var responses = [];
			for( var i = 0; i < name.length; i++ ) {
				eventIds.push( mod_event.subscribe( name[ i ], function ( response ) {
					responses.push( response );
					if( eventIds.length === responses.length ) { // todo fix, call even when first event is called twice
						if( eventCallback ) {
							eventCallback( responses );
						}
						responses = [];
					}
				}, options ) );
			}
			return eventIds;
		}
		return mod_event.subscribe( name, eventCallback, options );
	};
	/**
	 * Subscribe custom events
	 *
	 * @param name Method name for the event to be subscribed
	 * @param eventCallback(data) Function to be called when the event is published with the data
	 * @options options
	 * @returns eventId unique id generated for every subscription
	 */
	mod_event.subscribe = function ( name, eventCallback, options ) {
		if( name instanceof Array ) {
			var eventIds = [];
			for( var i = 0; i < name.length; i++ ) {
				eventIds.push( mod_event.subscribe( name[ i ], eventCallback, options ) );
			}
			return eventIds;
		}
		if( !name || !eventCallback ) {
			return null;
		}
		if( !pubsub[ name ] ) {
			pubsub[ name ] = {};
		}
		var eventId = name + ":" + generateUid();
		pubsub[ name ][ eventId ] = eventCallback;
		//TODO find better way
		//Setting callback in options so that it can be used in onsubscribe event.
		if( options ) {
			options.__callback = eventCallback;
		}
		//Calling onsubscribe events
		mod_event.publish( onsubscribe_prefix + name, options );
		return eventId;
	};
	/**
	 * Unsubscribe an event
	 *
	 * @param eventId id of the event to be unsubscribed
	 * @returns boolean true if event is successfully unsubscribed,else false
	 */
	mod_event.unsubscribe = function ( eventId ) {
		if( !eventId ) {
			return null;
		}
		var eventArr = eventId.split( ":" );
		if( eventArr.length === 2 ) {
			var eventName = eventArr[ 0 ];
			//            var eventNum = eventArr[1];
			if( pubsub[ eventName ][ eventId ] ) {
				delete pubsub[ eventName ][ eventId ];
				return true;
			}
		}
		return false;
	};
	/**
	 * Get all the subscribed events in an object
	 *
	 *
	 * @param name Event name for which all events are required
	 * @returns Event object for the provided event name
	 */
	mod_event.getSubscriptions = function ( name ) {
		if( !name ) {
			return pubsub; //todo return cloned object istead of original
		}
		return pubsub[ name ];
	};
	/**
	 * Used to subscribe to subscribe events, onsubscribe('method1') is called when subscribe('method1') is called
	 * This can be used to setup data / setup publish events
	 *
	 * @param name
	 * @param eventCallback
	 * @returns {string}
	 */
	mod_event.onsubscribe = function ( name, eventCallback ) {
		if( !name || !eventCallback ) {
			return null;
		}
		name = onsubscribe_prefix + name;
		if( !pubsub[ name ] ) {
			pubsub[ name ] = {};
		}
		var eventId = name + ":" + generateUid();
		pubsub[ name ][ eventId ] = eventCallback;
		return eventId;
	};
	return mod_event;
} );

define('jquery',[],function(){
    return jQuery;
});
/**
 * 'logger' module.
 *
 * @module logger
 * @requires event
 * @requires logger
 */
define( 'tiljs/logger',[ "./cookie", "module", "./event", "jquery" ], function ( cookie, module, event, $ ) {
	var mod_logger = {}, logCache = [];
	var types = [ "log", "debug", "info", "warn", "error" ];

	var default_config = {
		cookieName: "d",
		hashString: "#debugdsfgw456g", //change to url param  / use cookie
		prefix: "[times_log] ",
		//linenum: false,
		time: false, //todo to be implemented
		handleWindowError: true,
		handleJqueryError: true,
		logModuleLoad: true,
		log: false //can be overridden by cookie or hash when it is false
	};
	var config = $.extend( {}, default_config, module.config() ); //todo remove jquery dependency
	/**
	 * Returns the stack details for method that calls log
	 * @param stack
	 * @returns {string}
	 * @private
	 */
	function getStackDetails( stack ) {
		var regex = new RegExp( "\/(.*):([0-9]*)\:([0-9]*)", "g" );
		//done thrice to get the main calling method and line number
		var res = regex.exec( stack );
		res = regex.exec( stack );
		res = regex.exec( stack );
		if( res ) {
			res.shift();
		}
		return res ? res.join( ":" ).replace( config.hashString, "" ) : null;
	}

	/**
	 * times.debug
	 *
	 * times.log/info/error
	 */
	mod_logger = {};
	mod_logger.disable = function () {
		config.log = false;
		cookie.remove( config.cookieName, "/" );

		var i, type;
		for(i in types ) {
			if( types.hasOwnProperty( i ) ) {
				type = types[ i ];

				mod_logger[ type ] = (function(type) {
					return function() {
						logCache.push.apply(logCache,arguments);
					};
				})(type);
			}
		}

		return "Logging Disabled";
	};
	mod_logger.enable = function () {
		config.log = true;
		cookie.set( config.cookieName, config.hashString, 30, "/" );


		var i, type;
		for(i in types ) {
			if( types.hasOwnProperty( i ) ) {
				type = types[ i ];

				function fun(){
					console.log.apply(this,arguments);
				}

				mod_logger[ type ] = (function(type) {
					var noop = function() {};
					var log;
					var context = config.prefix;
					if (console.log.bind === 'undefined') { // IE < 10
						log = Function.prototype.bind.call(console[type], console, context);
					}
					else {
						log = console[type].bind(console, context);
					}

					//log = console[type].bind(console);
					//log = fun.bind(console);

					//log = (window.console === undefined) ? noop
					//	: (Function.prototype.bind !== undefined) ? Function.prototype.bind.call(console[type], console)
					//	: function() {Function.prototype.apply.call(console[type], console, arguments);};

					return log;

				})(type);

				(function ( type ) {
					event.subscribe( "logger." + type, function ( data ) {
						Function.prototype.apply.call(mod_logger[type],console, arguments);
					});
				}( type ));
			}
		}


		return "Logging Enabled";
	};
	mod_logger.isEnabled = function () {
		return config.log || ( window.location.hash.length > 0 && window.location.hash === ( config.hashString ) ) || ( cookie.get( config.cookieName ) === config.hashString );
	};
	mod_logger.handleWindowError = function () {
		window.onerror = function ( msg, url, linenumber, colno, error ) {
			mod_logger.error.apply(this,
				['Error message: ' + msg +
				'\n\tURL: ' + url + ( linenumber ? ":" + linenumber + ":" + colno : "" ) +
				'\n\tLine Number: ' + linenumber + ":" + colno +
				'\n\tError: ' + error] );
			return true;
		};
	};
	mod_logger.handleJqueryError = function () {
		$( document ).ajaxError( function ( event, jqxhr, settings, exception ) {
			if( exception === "timeout" ) {
				mod_logger.error.apply(this, [exception + ": " + settings.url] );
			} else {
				mod_logger.error.apply(this, [exception] );
			}
		} );
		$( document ).error( function ( event ) {
			mod_logger.error.apply(this, event );
		} );
	};
	/**
	 window.require.onResourceLoad = function (context, map, depArray) {
            console.log(map.name);
        };

	 */
	function init_log() {

		config.log = mod_logger.isEnabled();
		if(config.log){
			mod_logger.enable();
		}else{
			mod_logger.disable();
		}

		if( config.handleWindowError === true ) {
			mod_logger.handleWindowError();
			//Doing it again on window load
			event.subscribe( "window.load", function () {
				mod_logger.handleWindowError();
			});
		}
		if( config.handleJqueryError === true ) {
			mod_logger.handleJqueryError();
		}
	}

	/**
	 * As of date we are saving only logs which are not being cached.
	 *
	 * //TODO save all logs to cache
	 */
	mod_logger.getLogs = function(){
		return logCache.slice(0);//slice to create a new reference
	};
	mod_logger.getLogsStr = function(){
		return mod_logger.getLogs().join("\n");
	};


	init_log();
	return mod_logger;
});
/** 'is' module.
 * @module is
 * @exports is
 */
define( 'tiljs/is',[], function () {
	var mod_is = {};
	/**
	 * Checks if the param is a number.
	 *
	 * @memberOf module:is#
	 * @function number
	 * @param ele {object} Any element which is to be checked
	 * @returns {boolean}
	 * @example
	 *
	 * require(['is'],function(is){
	 *     is.number(1);        //returns true
	 *     is.number('abc');    //returns false
	 * });
	 */
	mod_is.number = function ( ele ) {
		return typeof ele === "number";
	};
	/**
	 * Checks if the param is a string.
	 *
	 * @memberOf module:is#
	 * @function string
	 * @param ele {object} Any element which is to be checked
	 * @returns {boolean}
	 * @example
	 *
	 * require(['is'],function(is){
	 *     is.number(1);        //returns false
	 *     is.number('abc');    //returns true
	 * });
	 */
	mod_is.string = function ( ele ) {
		return typeof ele === "string";
	};
	mod_is.funct = mod_is.method = function ( ele ) {
		return typeof ele === "function";
	};
	mod_is.object = function ( ele ) {
		return ele !== null && typeof ele === "object"; //&& !(ele instanceof Array);
	};
	mod_is.array = Array && Array.isArray ? Array.isArray : function ( ele ) {
		return ele instanceof Array;
	};
	mod_is.undefined = function ( ele ) {
		return typeof ele === "undefined";
	};
	mod_is.defined = function ( ele ) {
		return typeof ele !== "undefined";
	};
	mod_is.exists = function ( ele ) {
		return mod_is.defined( ele ) || ele === "";
	};
	mod_is.empty = function ( ele ) {
		if( !mod_is.defined( ele ) ) {
			return true;
		} else if( mod_is.string( ele ) || mod_is.array( ele ) ) {
			return ele.length === 0;
		} else if( mod_is.object( ele ) ) {
			var i = 0,
				e;
			for( e in ele ) {
				if( ele.hasOwnProperty( e ) ) {
					i++;
				}
			}
			return i === 0;
		} else if( mod_is.number( ele ) ) {
			return false;
		} else {
			return true;
		}
	};
	mod_is.alphaOnly = function ( str ) {
		return /^[A-z\s]+$/.test( str );
	};
	mod_is.numberOnly = function ( str ) {
		return /^[0-9]+$/.test( str );
	};
	mod_is.mobile = function () {
		return( function ( a ) {
			return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test( a ) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test( a.substr( 0, 4 ) );
		} )( navigator.userAgent || navigator.vendor || window.opera );
	};
	mod_is.tablet = function () {
		return( function ( a ) {
			return /(?:ipad|tab)/i.test( a );
		} )( navigator.userAgent || navigator.vendor || window.opera );
	};
	mod_is.desktop = function () {
		return !mod_is.mobile() && !mod_is.tablet();
	};
	mod_is.touch = function () {
		return (('ontouchstart' in window) || ('DocumentTouch' in window));
	};
	mod_is.IE = function () {
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf( "MSIE " );
		if( msie > 0 || !!navigator.userAgent.match( /Trident.*rv\:11\./ ) ) { // If Internet Explorer, return version number
			return true;
		} else { // If another browser, return 0
			return false;
		}
	};
	mod_is.IE11 = function () {
		return( !!navigator.userAgent.match( /Trident\/7\./ ) );
	};
	mod_is.visible = function ( ele ) {
		return ele && ele.is( ":visible" );
	};
	mod_is.iframe = function ( ele ) {
		return ele.tagName === "IFRAME";
	};
	mod_is.dateStr = function ( str ) {
		try {
			new Date( str );
			return true;
		} catch( e ) {
			return false;
		}
	};
	mod_is.email = function ( email ) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test( email );
	};
	mod_is.url = function ( str ) {
		var url_pattern = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-]*)?\??(?:[\-\+=&;%@\.\w]*)#?(?:[\.\!\/\\\w]*))?)/g;
		return url_pattern.test( str );
	};
	return mod_is;
} );

/**
 * 'string' module.
 *
 * @module string
 */
define( 'tiljs/string',[], function () {
	var mod_string = {};
	mod_string.camelCase = function ( str ) {
		return str.replace( /(?:^|\s)\w/g, function ( match ) {
			return match.toUpperCase();
		} ).replace( /(\s|-|_)/g, "" );
	};
	mod_string.splice = function ( str, idx, rem, s ) {
		return( str.slice( 0, idx ) + s + str.slice( idx + Math.abs( rem ) ) );
	};
	mod_string.startsWith = function ( haystack, needle ) {
		return haystack.indexOf( needle ) === 0;
	};
	return mod_string;
} );

define('json',[],function(){
    return JSON;
});
/**
 * 'util' module.
 *
 * @module util
 * @requires logger
 * @requires jquery
 * @requires is
 * @requires string
 * @requires json
 */
define( 'tiljs/util',[ "./logger", "jquery", "./is", "./string", "json" ], function ( logger, $, is, string, json ) {
	logger.log( "util loaded" );
	var mod_util = {};
	mod_util.extend = $.extend;
	/*

     function () {
     var arg = [true];
     for(var i=0;arguments[i];i++){
     arg.push(arguments[i]);
     }

     return mod_util.deepextend.apply(this,arg);
     };*/
	mod_util.deepextend = function () {
		//todo remove jquery dependency
		return $.extend.apply( this, arguments );
		//        for (var i = 1; i < arguments.length; i++)
		//            for (var key in arguments[i]){
		//                if (arguments[i].hasOwnProperty(key)){
		//                    if(arguments[i][key] instanceof Array || arguments[i][key] instanceof Object){
		////                        console.log(arguments[i][key]);
		//                        mod_util.extend(arguments[0][key],arguments[i][key]);
		//                    }else{
		//                        console.log(arguments[i][key]);
		//
		//                        arguments[0][key] = arguments[i][key];
		//                    }
		//                }
		//            }
		//        return arguments[0];
	};
	/**
	 * Get values in obj2 which are not there in obj1
	 *
	 * INPUT
	 * obj1 = {a:1,b:2,c:3}
	 * obj2 = {a:1,c:3,d:4}
	 *
	 * OUTPUT
	 * obj = {d:4}
	 *
	 * @param obj1
	 * @param obj2
	 * @returns {{}}
	 */
	mod_util.xor = function ( obj1, obj2 ) {
		var obj = {},o;
		for( o in obj2 ) {
			if( !obj1.hasOwnProperty( o ) ) {
				obj[ o ] = obj2[ o ];
			}
		}
		return obj;
	};
	mod_util.convertObj = function ( obj, callback ) {
		var objResult = {};
		util.each( obj, function ( k, v ) {
			objResult[ k ] = callback( k, v );
		} );
		return objResult;
	};
	mod_util.each = function ( obj, callback ) {
		var i,o;
		if( !callback ) {
			return null;
		}
		if( obj instanceof Array ) {
			for( i = 0; i < obj.length; i++ ) {
				if( callback( i, obj[ i ] ) === false ) {
					break;
				}
			}
		} else if( obj instanceof Object ) {
			for( o in obj ) {
				if( obj.hasOwnProperty( o ) ) {
					if( callback( o, obj[ o ] ) === false ) {
						break;
					}
				}
			}
		} else {
			//callback(0, obj);
		}
	};
	mod_util.wrapCallback = function ( callback, arga ) {
		try {
			if( callback ) {
				callback.apply( this, args );
			}
		} catch( e ) {
			logger.error( e );
		}
	};
	//    mod_util.update = function (obj) {
	//        config = mod_util.extend(true, config, obj);
	//    };
	mod_util.formatNumber = function ( num ) {
		if( num >= 1000000000 ) {
			return( num / 1000000000 ).toFixed( 1 ) + 'G';
		}
		if( num >= 1000000 ) {
			return( num / 1000000 ).toFixed( 1 ) + 'M';
		}
		if( num >= 1000 ) {
			return( num / 1000 ).toFixed( 1 ) + 'K';
		}
		return num;
	};
	mod_util.getJsonFromString = function ( str, entitySeparator, keyValueSeparator ) {
		entitySeparator = entitySeparator || "&";
		keyValueSeparator = keyValueSeparator || "=";
		var json = {};
		var entities = str.split( entitySeparator );
		mod_util.each( entities, function ( i, entity ) {
			var keyValue = entity.split( keyValueSeparator );
			json[ keyValue[ 0 ] ] = keyValue[ 1 ];
		} );
		return json;
	};
	mod_util.val = function ( val, options ) {
		if( is.funct( val ) ) {
			return val( options );
		}
		return val;
	};
	/**
	 * Throttles the number of times a function is called, ignores rest of calls
	 * <br/>
	 * function called normally        ##############################<br/>
	 * function called with throttle   #     #     #     #     #    #<br/>
	 * <br/>
	 * Assuming one space as one milisecond & delay=5<br/>
	 * <br/>
	 * Usage : 'scroll'/'move' functions where events are fired on every scroll/move, calling of event can be throttled
	 *
	 * @function throttle
	 * @memberOf module:util#
	 * @param delay time after which that method is to be called
	 * @param callback method to be called after specified delay
	 * @returns {function} throttled instance of function which can be called on throttle
	 * @example
	 *
	 * function run(i){
	 *     console.log("run called : "+i);
	 * }
	 *
	 * //Normal Call
	 * for(var i=0;i<500;i++){
	 *     run(i);  //called 500 times
	 * }
	 *
	 * //Throttled call
	 * require(['util'],function(util){
	 *   var th = util.throttle(5,run);
	 *   for(var i=0;i<500;i++){
	 *     th(i);  //called (500/time taken by function) times
	 *   }
	 * });
	 *
	 */
	mod_util.throttle = function ( delay, callback ) {
		var _timeout;
		var _exec = 0;
		return function callable() {
			var elapsed = +new Date() - _exec;
			//                console.log("callable:elapsed:"+_exec);
			var tthis = this;
			var args = arguments;

			function run() {
				_exec = +new Date();
				//                    console.log("run_exec:"+_exec);
				callback.apply( tthis, args );
			}
			_timeout && clearTimeout( _timeout );
			//                console.log("elapsed>run : "+(elapsed+">"+delay));
			if( elapsed > delay ) run();
			else _timeout = setTimeout( run, delay - elapsed );
		};
	};
	mod_util.getDate = function ( str ) {
		try {
			if( is.number( str ) ) {
				return new Date( str );
			} else {
				str = str.replace( /(\d)(A|P)M/, "$1 $2M" ); //Changes "22 Nov 2013, 10:19AM" -> "22 Nov 2013, 10:19 AM"
			}
			return new Date( str );
		} catch( e ) {
			logger.error( e );
			return null;
		}
	};
	var __uuidCnt = 0;
	/**
	 * Generates uuid.
	 * @returns {number} unique id for a system
	 */
	mod_util.uuid = function () {
		var d = new Date().getTime();
		var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace( /[xy]/g, function ( c ) {
			var r = ( d + Math.random() * 16 ) % 16 | 0;
			d = Math.floor( d / 16 );
			return( c == 'x' ? r : ( r & 0x7 | 0x8 ) ).toString( 16 );
		} );
		return uuid;
	};
	/**
	 * Make getter setters for heirerchy less object supplied as parameter
	 * @param objName
	 * @param json
	 * @param setter
	 */
	//    mod_util.jsonToObj = function(objName,json,setter){
	//        this[objName] = function(attr){
	//            var tthis = this;
	//            mod_util.each(attr,function(k,v){
	//                tthis[k] = v;
	//            });
	//        };
	//        var obj = new this[objName](json);
	////        if(!is.object(obj)){
	////            throw new Error("Paramater is not an 'Object'. Cannot apply Getter-Setter");
	////        }
	//
	//        mod_util.each(json,function(k,v){
	//            var kCamelCase = string.camelCase(k);
	//            if(setter === true){
	//                obj['set'+kCamelCase] = function(kk){return this[k] = kk;};
	//            }
	//            obj['get'+kCamelCase] = function(){return this[k]};
	//        });
	//        return obj;
	//    };
	/**
	 * Changes the key for an object using the maping.
	 *
	 * Eg :
	 *    obj = {id:123, name:"Joe"};
	 *    mapping = {idd:id}
	 *
	 *    result = {idd:123}
	 *
	 *
	 * @param mapping
	 * @param obj
	 * @returns {{}}
	 */
	mod_util.mapObj = function ( mapping, obj ) {
		if( !is.object( mapping ) && !is.object( obj ) ) {
			throw new Error( 'Invalid Parameters' ); //todo give proper message
		}
		var newObj = {};
		mod_util.each( mapping, function ( k, v ) {
			if( is.funct( v ) ) {
				newObj[ k ] = v( obj );
			} else {
				newObj[ k ] = obj[ v ];
			}
		} );
		return newObj;
	};
	mod_util.getDomainOnly = function ( url ) {
		//        var separate = window.location.host.split('.');
		//        separate.shift();
		//        return "." + separate.join('.');
		return "." + ( url || document.location.host ).split( ":" )[ 0 ].split( "." ).reverse().slice( 0, 2 ).reverse().join( "." );
	};
	mod_util.format = function ( text, data ) { //todo move to format.js
		var formattedText = text;
		mod_util.each( data, function ( i, v ) {
			formattedText = formattedText.replace( "{{:" + i + "}}", data[ i ] ); //todo use better way
		} );
		//Removing text which could not be formatted, todo log it
		formattedText = formattedText.replace( /{{:[a-zA_Z0-9]+}}/, "" );
		return formattedText;
	};
	mod_util.stringify = JSON ? JSON.stringify : null;
	mod_util.checkNoScript = function ( url ) {
		//        $("noscript")
	};
	mod_util.getClick = function () {
		return is.touch() ? "touchstart" : "click";
	};
	mod_util.getParam = function ( key ) {
		var sanitizedParams = window.location.search.replace( "?", "" );
		if( is.defined( key ) ) {
			return mod_util.deparam( sanitizedParams )[ key ];
		} else {
			return mod_util.deparam( sanitizedParams );
		}
	};
	mod_util.deparam = function ( text ) {
		var data = {}; //todo mot handled 'a[]' or 'a[b][c]'
		mod_util.each( text.replace( /\+/g, ' ' ).split( "&" ), function ( i, pair ) {
			var kv = pair.split( "=" );
			data[ decodeURIComponent( kv[ 0 ] ) ] = decodeURIComponent( kv[ 1 ] || "" ); //todo all values are string
		} );
		return data
	};
	//todo implement setting and getting each attr
	mod_util.data = function ( element ) {
		var data = {};
		var attrMap = $( element )[ 0 ].attributes;
		$.each( attrMap, function ( i, e ) {
			var prefix = e.nodeName.substr( 0, 5 );
			if( prefix === "data-" ) { //It is a data attribute
				data[ e.nodeName.replace( prefix, "" ) ] = e.nodeValue; //TODO nodeValue depricated use node
			}
		} );
		return data;
	};
	var ManagedError = function ( message ) {
		Error.prototype.constructor.apply( this, arguments );
		this.message = message;
	};
	ManagedError.prototype = new Error();
	mod_util.wrapCallback = function ( fn ) {
		return function () {
			try {
				return fn.apply( this, arguments );
			} catch( e ) {
				if( e instanceof ManagedError ) {
					// re-throw immediately
					throw e;
				}
				logger.error( e );
				// re-throw to halt execution
				throw e;
			}
		};
	};
	/**
	 * MD5 (Message-Digest Algorithm) by WebToolkit
	 * @param s
	 * @returns {string}
	 */
	mod_util.md5 = function ( s ) {
		function L( k, d ) {
			return( k << d ) | ( k >>> ( 32 - d ) )
		}

		function K( G, k ) {
			var I, d, F, H, x;
			F = ( G & 2147483648 );
			H = ( k & 2147483648 );
			I = ( G & 1073741824 );
			d = ( k & 1073741824 );
			x = ( G & 1073741823 ) + ( k & 1073741823 );
			if( I & d ) {
				return( x ^ 2147483648 ^ F ^ H )
			}
			if( I | d ) {
				if( x & 1073741824 ) {
					return( x ^ 3221225472 ^ F ^ H )
				} else {
					return( x ^ 1073741824 ^ F ^ H )
				}
			} else {
				return( x ^ F ^ H )
			}
		}

		function r( d, F, k ) {
			return( d & F ) | ( ( ~d ) & k )
		}

		function q( d, F, k ) {
			return( d & k ) | ( F & ( ~k ) )
		}

		function p( d, F, k ) {
			return( d ^ F ^ k )
		}

		function n( d, F, k ) {
			return( F ^ ( d | ( ~k ) ) )
		}

		function u( G, F, aa, Z, k, H, I ) {
			G = K( G, K( K( r( F, aa, Z ), k ), I ) );
			return K( L( G, H ), F )
		}

		function f( G, F, aa, Z, k, H, I ) {
			G = K( G, K( K( q( F, aa, Z ), k ), I ) );
			return K( L( G, H ), F )
		}

		function D( G, F, aa, Z, k, H, I ) {
			G = K( G, K( K( p( F, aa, Z ), k ), I ) );
			return K( L( G, H ), F )
		}

		function t( G, F, aa, Z, k, H, I ) {
			G = K( G, K( K( n( F, aa, Z ), k ), I ) );
			return K( L( G, H ), F )
		}

		function e( G ) {
			var Z;
			var F = G.length;
			var x = F + 8;
			var k = ( x - ( x % 64 ) ) / 64;
			var I = ( k + 1 ) * 16;
			var aa = Array( I - 1 );
			var d = 0;
			var H = 0;
			while( H < F ) {
				Z = ( H - ( H % 4 ) ) / 4;
				d = ( H % 4 ) * 8;
				aa[ Z ] = ( aa[ Z ] | ( G.charCodeAt( H ) << d ) );
				H++
			}
			Z = ( H - ( H % 4 ) ) / 4;
			d = ( H % 4 ) * 8;
			aa[ Z ] = aa[ Z ] | ( 128 << d );
			aa[ I - 2 ] = F << 3;
			aa[ I - 1 ] = F >>> 29;
			return aa
		}

		function B( x ) {
			var k = "",
				F = "",
				G, d;
			for( d = 0; d <= 3; d++ ) {
				G = ( x >>> ( d * 8 ) ) & 255;
				F = "0" + G.toString( 16 );
				k = k + F.substr( F.length - 2, 2 )
			}
			return k
		}

		function J( k ) {
			k = k.replace( /rn/g, "n" );
			var d = "";
			for( var F = 0; F < k.length; F++ ) {
				var x = k.charCodeAt( F );
				if( x < 128 ) {
					d += String.fromCharCode( x )
				} else {
					if( ( x > 127 ) && ( x < 2048 ) ) {
						d += String.fromCharCode( ( x >> 6 ) | 192 );
						d += String.fromCharCode( ( x & 63 ) | 128 )
					} else {
						d += String.fromCharCode( ( x >> 12 ) | 224 );
						d += String.fromCharCode( ( ( x >> 6 ) & 63 ) | 128 );
						d += String.fromCharCode( ( x & 63 ) | 128 )
					}
				}
			}
			return d
		}
		var C = Array();
		var P, h, E, v, g, Y, X, W, V;
		var S = 7,
			Q = 12,
			N = 17,
			M = 22;
		var A = 5,
			z = 9,
			y = 14,
			w = 20;
		var o = 4,
			m = 11,
			l = 16,
			j = 23;
		var U = 6,
			T = 10,
			R = 15,
			O = 21;
		s = J( s );
		C = e( s );
		Y = 1732584193;
		X = 4023233417;
		W = 2562383102;
		V = 271733878;
		for( P = 0; P < C.length; P += 16 ) {
			h = Y;
			E = X;
			v = W;
			g = V;
			Y = u( Y, X, W, V, C[ P + 0 ], S, 3614090360 );
			V = u( V, Y, X, W, C[ P + 1 ], Q, 3905402710 );
			W = u( W, V, Y, X, C[ P + 2 ], N, 606105819 );
			X = u( X, W, V, Y, C[ P + 3 ], M, 3250441966 );
			Y = u( Y, X, W, V, C[ P + 4 ], S, 4118548399 );
			V = u( V, Y, X, W, C[ P + 5 ], Q, 1200080426 );
			W = u( W, V, Y, X, C[ P + 6 ], N, 2821735955 );
			X = u( X, W, V, Y, C[ P + 7 ], M, 4249261313 );
			Y = u( Y, X, W, V, C[ P + 8 ], S, 1770035416 );
			V = u( V, Y, X, W, C[ P + 9 ], Q, 2336552879 );
			W = u( W, V, Y, X, C[ P + 10 ], N, 4294925233 );
			X = u( X, W, V, Y, C[ P + 11 ], M, 2304563134 );
			Y = u( Y, X, W, V, C[ P + 12 ], S, 1804603682 );
			V = u( V, Y, X, W, C[ P + 13 ], Q, 4254626195 );
			W = u( W, V, Y, X, C[ P + 14 ], N, 2792965006 );
			X = u( X, W, V, Y, C[ P + 15 ], M, 1236535329 );
			Y = f( Y, X, W, V, C[ P + 1 ], A, 4129170786 );
			V = f( V, Y, X, W, C[ P + 6 ], z, 3225465664 );
			W = f( W, V, Y, X, C[ P + 11 ], y, 643717713 );
			X = f( X, W, V, Y, C[ P + 0 ], w, 3921069994 );
			Y = f( Y, X, W, V, C[ P + 5 ], A, 3593408605 );
			V = f( V, Y, X, W, C[ P + 10 ], z, 38016083 );
			W = f( W, V, Y, X, C[ P + 15 ], y, 3634488961 );
			X = f( X, W, V, Y, C[ P + 4 ], w, 3889429448 );
			Y = f( Y, X, W, V, C[ P + 9 ], A, 568446438 );
			V = f( V, Y, X, W, C[ P + 14 ], z, 3275163606 );
			W = f( W, V, Y, X, C[ P + 3 ], y, 4107603335 );
			X = f( X, W, V, Y, C[ P + 8 ], w, 1163531501 );
			Y = f( Y, X, W, V, C[ P + 13 ], A, 2850285829 );
			V = f( V, Y, X, W, C[ P + 2 ], z, 4243563512 );
			W = f( W, V, Y, X, C[ P + 7 ], y, 1735328473 );
			X = f( X, W, V, Y, C[ P + 12 ], w, 2368359562 );
			Y = D( Y, X, W, V, C[ P + 5 ], o, 4294588738 );
			V = D( V, Y, X, W, C[ P + 8 ], m, 2272392833 );
			W = D( W, V, Y, X, C[ P + 11 ], l, 1839030562 );
			X = D( X, W, V, Y, C[ P + 14 ], j, 4259657740 );
			Y = D( Y, X, W, V, C[ P + 1 ], o, 2763975236 );
			V = D( V, Y, X, W, C[ P + 4 ], m, 1272893353 );
			W = D( W, V, Y, X, C[ P + 7 ], l, 4139469664 );
			X = D( X, W, V, Y, C[ P + 10 ], j, 3200236656 );
			Y = D( Y, X, W, V, C[ P + 13 ], o, 681279174 );
			V = D( V, Y, X, W, C[ P + 0 ], m, 3936430074 );
			W = D( W, V, Y, X, C[ P + 3 ], l, 3572445317 );
			X = D( X, W, V, Y, C[ P + 6 ], j, 76029189 );
			Y = D( Y, X, W, V, C[ P + 9 ], o, 3654602809 );
			V = D( V, Y, X, W, C[ P + 12 ], m, 3873151461 );
			W = D( W, V, Y, X, C[ P + 15 ], l, 530742520 );
			X = D( X, W, V, Y, C[ P + 2 ], j, 3299628645 );
			Y = t( Y, X, W, V, C[ P + 0 ], U, 4096336452 );
			V = t( V, Y, X, W, C[ P + 7 ], T, 1126891415 );
			W = t( W, V, Y, X, C[ P + 14 ], R, 2878612391 );
			X = t( X, W, V, Y, C[ P + 5 ], O, 4237533241 );
			Y = t( Y, X, W, V, C[ P + 12 ], U, 1700485571 );
			V = t( V, Y, X, W, C[ P + 3 ], T, 2399980690 );
			W = t( W, V, Y, X, C[ P + 10 ], R, 4293915773 );
			X = t( X, W, V, Y, C[ P + 1 ], O, 2240044497 );
			Y = t( Y, X, W, V, C[ P + 8 ], U, 1873313359 );
			V = t( V, Y, X, W, C[ P + 15 ], T, 4264355552 );
			W = t( W, V, Y, X, C[ P + 6 ], R, 2734768916 );
			X = t( X, W, V, Y, C[ P + 13 ], O, 1309151649 );
			Y = t( Y, X, W, V, C[ P + 4 ], U, 4149444226 );
			V = t( V, Y, X, W, C[ P + 11 ], T, 3174756917 );
			W = t( W, V, Y, X, C[ P + 2 ], R, 718787259 );
			X = t( X, W, V, Y, C[ P + 9 ], O, 3951481745 );
			Y = K( Y, h );
			X = K( X, E );
			W = K( W, v );
			V = K( V, g )
		}
		var i = B( Y ) + B( X ) + B( W ) + B( V );
		return i.toLowerCase()
	};
	/**
	 * Decode '<' to '&lt;'
	 * @param decodedText
	 * @returns {String}
	 */
	mod_util.encodeHTML = function ( text ) {
		return $( "<div/>" ).text( text ).html();
	}
	/**
	 * Decode '&lt;' to '<'
	 * @param decodedText
	 * @returns {String}
	 */
	mod_util.decodeHTML = function ( text ) {
		return $( "<div/>" ).html( text ).text();
	}
	/**
	 * reload iframe
	 * @param: iframe selector
	 * @usage: Used to reload iframe by passing
	 *         any selector
	 * @example: util.reloadIframe("#id");
	 *         util.reloadIframe(".class");
	 */
	mod_util.reloadIframe = function ( iframe ) {
		if ($(iframe).length > 0){
            $(iframe).attr("src", $(iframe).attr("src"));
            return true;
        }
        return false;
	}
	/**
	*trim text 
	*@param: cmt:text, charlen: character length
	*@usage: Used to cut the string to desired length
	*@example:util.trimText("text string",5)
	*/
	mod_util.trimText = function(cmt,charlen){
        var trimmedString = cmt.substr(0,charlen);
        if(cmt.length > charlen){
            trimmedString = cmt.replace(new RegExp("^(.{"+charlen+"}[^\\s]*).*"), "$1");
        }
        return trimmedString;
	}
	return mod_util;
} );

/**
 * 'timer' module.
 *
 * @module timer
 * @requires util
 * @requires is
 */
define( 'tiljs/timer',[ "./util", "./is" ], function ( util, is ) {
	var mod_timer = {};
	mod_timer.every = function ( time, callback ) {
		return setInterval( callback, time );
	};
	mod_timer.after = mod_timer.delay = function ( time, callback ) {
		return setTimeout( callback, time );
	};
	mod_timer.available = function ( varname, callback, time, trytimes ) {
		if( trytimes == 0 ) {
			callback( null );
		}
		if( window[ varname ] ) {
			if( callback ) {
				callback( window[ varname ] );
			}
		} else {
			time = time || 2000;
			setTimeout( function () {
				mod_timer.available( varname, callback, time, --trytimes );
			}, time );
		}
	};
	mod_timer.cancel = function ( id ) { //todo handle gracefully
		try {
			clearTimeout( id );
		} catch( e ) {}
		try {
			clearInterval( id );
		} catch( e ) {}
	};
	mod_timer.elapsedTime = function ( ctime, labels_config, last_only ) {
		if( !is.number( ctime ) ) {
			//            if(is.dateStr(ctime)){
			//                return mod_timer.elapsedTime(new Date(ctime).getTime(),labels_config, last_only);
			//            }
			try {
				ctime = parseInt( ctime, 10 );
				return mod_timer.elapsedTime( ctime, labels_config, last_only );
			} catch( e ) {
				return "";
			}
		}
		var labels_default = {
			year: "year",
			//            month:"month",  //todo implement months
			day: "day",
			hour: "hour",
			minute: "minute",
			second: "second",
			ago: "ago"
		};
		var labels = util.extend( true, {}, labels_default, labels_config );
		var timeparts = [
			{
				name: labels.year,
				div: 31536000000,
				mod: 10000
			},
 //            {name: labels.month, div: 86400000, mod: 365},
			{
				name: labels.day,
				div: 86400000,
				mod: 365
			},
			{
				name: labels.hour,
				div: 3600000,
				mod: 24
			},
			{
				name: labels.minute,
				div: 60000,
				mod: 60
			},
			{
				name: labels.second,
				div: 1000,
				mod: 60
			}
        ];
		var
			i = 0,
            l = timeparts.length,
            calc,
            values = [],
            currentTime = new Date(),
            currentOffset = currentTime.getTimezoneOffset(),
            ISTOffset = 330,
            ISTTime = currentTime.getTime() + (ISTOffset + currentOffset)*60000,
            interval = ISTTime - ctime; //todo use server time
		while( i < l ) {
			calc = Math.floor( interval / timeparts[ i ].div ) % timeparts[ i ].mod;
			if( calc && calc >= 0 ) {
				values.push( calc + ' ' + timeparts[ i ].name + ( calc > 1 ? 's' : '' ) );
			}
			i += 1;
		}
		if( values.length === 0 ) {
			values.push( '1 ' + labels.second );
		}
		if( last_only === true ) {
			return values[ 0 ] + ' ' + labels.ago;
		} else {
			return values.join( ', ' ) + ' ' + labels.ago;
		}
	};
	return mod_timer;
} );

/**
 * 'ajax' module.
 *
 * @module ajax
 * @requires timer
 * @requires util
 * @requires jquery
 * @requires is
 */
define( 'tiljs/ajax',[ "./timer", "./util", "jquery", "./is", "module" ], function ( timer, util, $, is, module ) {
	var default_config = {
			timeout: 60000 // 60 seconds
		},
		config = util.extend( true, {}, default_config, module.config()),
		mod_ajax = {};

	$.ajaxSetup( {
		timeout: config.timeout
	} );
	/**
	 * Checks if the javascript or css is loaded
	 *
	 * This is to be deprecated, use 'queue' instead.
	 *
	 * @memberOf module:ajax#
	 * @function getAll
	 *
	 * @param urlArr
	 * @param dataArr
	 * @param callback
	 * @param type
	 * @returns {boolean}
	 * @example
	 *
	 *  require(['ajax'],function(ajax){
	 *     ajax.getAll(['a.json','b.json'],[{a_param:'a'},[]],function(a_resp,b_resp){
	 *          console.log(a_resp);
	 *          console.log(b_resp);
	 *     })
	 *  });
	 */
	mod_ajax.getAll = function ( urlArr, dataArr, callback, type ) {
		var tthis = this,
			result = [],
			response = [],
			i, currDataArr;
		function process( data ) {
			response.push( data );
			if( response.length === urlArr.length ) {
				callback.apply( tthis, response );
			}
		}
		for( i = 0; i < urlArr.length; i++ ) {
			currDataArr = dataArr && dataArr.length > i ? dataArr[ i ] : {}; //todo use better method
			result.push( $.get( urlArr[ i ], currDataArr, process, type ) );
		}
		return result;
	};
	/**
	 * Creates a serialized representation of an array or object,
	 * suitable for use in a URL query string or Ajax request.
	 *
	 * @memberOf module:ajax#
	 * @function param
	 *
	 * @param {Array|Object} obj
	 * @param {Boolean} [traditional]
	 * @returns {String} Serialised array or object
	 * @example
	 *
	 *  require(['ajax'],function(ajax){
	 *
	 *  var myObject = {
	 *    a: {
	 *      one: 1,
	 *      two: 2,
	 *      three: 3
	 *    },
	 *    b: [ 1, 2, 3 ]
	 *  };
	 *  ajax.param(myObject);
	 *  //output : a%5Bone%5D=1&a%5Btwo%5D=2&a%5Bthree%5D=3&b%5B%5D=1&b%5B%5D=2&b%5B%5D=3
	 *  });
	 */
	mod_ajax.param = $.param; //todo override
	/**
	 * Perform an asynchronous HTTP (Ajax) request<br/>
	 * <br/>
	 * See {@link http://api.jquery.com/jQuery.ajax/|jQuery.ajax}
	 *
	 * @memberOf module:ajax#
	 * @function ajax
	 *
	 * @param {String} url A string containing the URL to which the request is sent.
	 * @param {Object} [settings] A set of key/value pairs that configure the Ajax request. All settings are optional.
	 *                            A default can be set for any option with $.ajaxSetup(). See jQuery.ajax( settings )
	 *                            below for a complete list of all settings.
	 * @returns {object}
	 * @example
	 *
	 *  require(['ajax'],function(ajax){
	 *     ajax.ajax('http://www.abc.com/ajax.json')
	 *  });
	 */
	/**
	 * Perform an asynchronous HTTP (Ajax) request<br/>
	 * <br/>
	 * See {@link http://api.jquery.com/jQuery.ajax/|jQuery.ajax}
	 *
	 * @memberOf module:ajax#
	 * @function ajax
	 *
	 * @param {Object} [settings] A set of key/value pairs that configure the Ajax request. All settings are optional.
	 *                            A default can be set for any option with $.ajaxSetup(). See jQuery.ajax( settings )
	 *                            below for a complete list of all settings.
	 * @returns {object}
	 * @example
	 *
	 *  require(['ajax'],function(ajax){
	 *     ajax.ajax({
	 *       url: "http://www.abc.com/data.json",
	 *       beforeSend: function( xhr ) {
	 *         xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
	 *       },
	 *       success:function(data){
	 *          console.log(data);   //data returned by ajax request
	 *       }
	 *     })
	 *  });
	 */
	mod_ajax.ajax = $.ajax; //todo override
	/**
     * Load data from the server using a HTTP GET request.<br/>
     * <br/>
     * See {@link http://api.jquery.com/jQuery.get/|jQuery.get}
     *
     * This is a shorthand Ajax function, which is equivalent to:
     *
     * <pre><code>
          ajax.ajax({
              url: url,
              data: data,
              success: success,
              dataType: dataType
            });
     * </code></pre>
     *
     * @memberOf module:ajax#
     * @function get
     *
     * @example
     *
     *  require(['ajax'],function(ajax){
     *     ajax.get("http://www.abc.com/data.json",{},function(data){
     *         console.log(data);   //data returned by ajax request
     *     });
     *  });
     * @param {String|Function} url A string containing the URL to which the request is sent.
     * @param {Object|String} [data] A plain object or string that is sent to the server with the request.
     * @param {Function} [callback] A callback function that is executed if the request succeeds.
     * @param {String} [type] The type of data expected from the server. Default: Intelligent Guess (xml, json, script, or html).
     * @returns {object}
     */
	mod_ajax.get = function ( url, data, callback, type ) {
		if( is.funct( url ) ) {
			return url( data, callback );
		} else {
			return $.get( url, data, callback, type );
		}
	};
	/**
     * Send/Load data to/from the server using a HTTP POST request.<br/>
     * <br/>
     * See {@link http://api.jquery.com/jQuery.get/|jQuery.get}
     *
     * This is a shorthand Ajax function, which is equivalent to:
     *
     * <pre><code>
     ajax.ajax({
              type: "POST",
              url: url,
              data: data,
              success: success,
              dataType: dataType
            });
     * </code></pre>
     *
     * @memberOf module:ajax#
     * @function post
     *
     * @example
     *
     *  require(['ajax'],function(ajax){
     *     ajax.post("http://www.abc.com/data.json",{},function(data){
     *         console.log(data);   //data returned by ajax request
     *     });
     *  });
     * @param {String} url A string containing the URL to which the request is sent.
     * @param {Object|String} [data] A plain object or string that is sent to the server with the request.
     * @param {Function} [callback] A callback function that is executed if the request succeeds.
     * @param {String} [type] The type of data expected from the server. Default: Intelligent Guess (xml, json, script, or html).
     * @returns {object}
     */
	mod_ajax.post = function ( url, data, callback, type ) {
		return $.post( url, data, callback, type );
	};
	mod_ajax.postCrossDomain = function ( url, data ) {
		var randomName = "postFrame" + new Date().getTime() + Math.floor( ( Math.random() * 1000 )),
			iframe = $( "<iframe></iframe>" ).attr( "name", randomName),
		//Create the form in a jQuery object
			form = $( "<form action='" + url + "' method='post' target='" + randomName + "'></form>" );
		//Get the value from the asp textbox with the ID txtBox1 and POST it as b1
		util.each( data, function ( k, v ) {
			form.append( $( "<input type='hidden' name='" + k + "' />" ).attr( 'value', v ) );
		} );
		//Add the iframe to the body
		iframe.appendTo( 'body' );
		//Add the form to the body
		form.appendTo( 'body' )
		//Submit the form which posts the variables into the iframe
		.submit()
		//Remove the form from the DOM (so subsequent requests won't keep expanding the DOM)
		.remove();
		timer.after( 10000, function () {
			iframe.remove();
		} );
		//        return $.post(url,data,callback,type);
	};
	mod_ajax.getJSONP = function ( url, data, callback ) {
		//        var req = mod_ajax.ajax({
		//            url : url,
		//            data : data,
		//            dataType : "jsonp",
		//            timeout : config.timeout
		//        });
		//
		//        req.success(callback);
		//        req.error(callback);
		return mod_ajax.get( url, data, callback, "jsonp" );
	};
	mod_ajax.getJSON = function ( url, data, callback ) {
		return mod_ajax.get( url, data, callback, "json" );
	};
	return mod_ajax;
} );

define( 'tiljs/user',[ "./util", "./ajax", "./cookie", "./event", "jquery", "./is", "./logger", "./string" ], function ( util, ajax, cookie, event, $, is, logger, string ) {
	var mod_user = {},
		/*__default_user = {
			uuid: util.uuid(),
			firstName: "",
			lastName: ""
	},*/
		User = function ( attr, mapping ) {
		var prvt = {},
			tthis = this;
		if( mapping && is.object( mapping ) ) {
			attr = util.mapObj( mapping, attr );
		}
		util.each( attr, function ( k, v ) {
			prvt[ k ] = v;
			var kCamelCase = string.camelCase( k );
			//            if(setter === true){
			//                tthis['set'+kCamelCase] = function(kk){return prvt[k] = kk;};
			//            }
			tthis[ 'get' + kCamelCase ] = function () {
				return prvt[ k ];
			};
		} );
		this.getMappedUser = function () {
			return prvt; //todo return clone.
		};
		this.getOriginalUser = function () {
			return attr; //todo return clone.
		};
		this.toString = this.toJson = function () {
			return util.stringify( prvt, true, "\t" );
		};
	};
	mod_user.getNewUser = function ( default_user, mapping ) {
		if( default_user instanceof User ) {
			return default_user;
		} else if( is.object( default_user ) ) {
			return new User( default_user, mapping );
		} else {
			return null;
		}
	};
	mod_user.isUser = function ( obj ) {
		return obj instanceof User;
	};
	return mod_user;
} );

/**
 * 'login' module.
 *
 * @module login
 * @requires util
 * @requires ajax
 * @requires cookie
 * @requires event
 * @requires jquery
 * @requires module
 * @requires user
 * @requires string
 * @requires logger
 */
define( 'tiljs/login',[ "./util", "./ajax", "./cookie", "./event", "jquery", "module", "./user", "./string", "./logger" ],
	function ( util, ajax, cookie, event, $, module, user, string, logger ) {
		var default_config,config,mod_login,userList,single_user_id;
		default_config = {
			autoinit: false,
			multiuser: false, //allows multiple users to login at a time
			login: "",
			logout: "",
			check_user_status: "",
			mapping: null,
			renderer: true //todo to be implemented
		};
		config = util.extend( true, {}, default_config, module.config() );
		mod_login = {};
		userList = {};
		single_user_id = "SINGLE_USER";
		/**
		 * Login method
		 *
		 * @memberOf module:login#
		 * @function login
		 * @param username
		 * @param password
		 * @param callback
		 */
		mod_login.login = function ( username, password, callback ) {
			if( !username ) {
				callback( null, "Username is required for login." );
			}
			if( !password ) {
				callback( null, "Password is required for login." );
			}
			ajax.get( config.login, {
				username: username,
				password: password
			}, function ( result, error ) {
				var _user = user.getNewUser( result, config.mapping );
				if( _user ) {
					mod_login.setUser( _user );
				} else {
					mod_login.removeUser();
				}
				if( error ) {
					event.publish( "login.error", error );
				}
				if( callback ) {
					callback( _user, error );
				}
			} );
		};
		mod_login.logout = function ( callback ) {
			ajax.get( config.logout, {}, function ( result ) {
				mod_login.removeUser();
				if( callback ) {
					callback( result );
				}
			} );
		};
		mod_login.register = function () {
			logger.info( "Register event called." );
		};
		mod_login.isLoggedIn = function ( callback ) {
			ajax.get( config.check_user_status, {}, function ( result ) {
				var _user = user.getNewUser( result, config.mapping );
				if( _user ) {
					_user.loginType = cookie.get( "LoginType" );
					_user.facebook = {
						name: cookie.get( "fbookname" ),
						location: cookie.get( "fbooklocation" ),
						image: cookie.get( "Fbimage" ),
						email: cookie.get( "FaceBookEmail" ),
						oauth: cookie.get( "Fboauthid" )
					};
					_user.twitter = {
						name: cookie.get( "TwitterUserName" ),
						//                location:cookie.get("fbooklocation"),
						image: cookie.get( "Twimage" ),
						//                email:cookie.get("FaceBookEmail"),
						oauth: cookie.get( "Twoauthid" )
					};
					mod_login.setUser( _user );
				} else {
					mod_login.removeUser();
				}
				if( callback ) {
					callback( _user );
				}
			} );
		};
		mod_login.removeUser = function ( userId ) {
			if( config.multiuser ) {
				if( userId ) {
					delete userList[ userId ];
				} else {
					throw new Error( "'userId' is required to remove a user." );
				}
			} else {
				delete userList[ single_user_id ];
			}
			mod_login.statusChange( null );
		};
		mod_login.setUser = mod_login.addUser = function ( _user ) {
			if( typeof _user !== 'undefined' && !user.isUser( _user ) ) {
				throw new Error( "Object is not an instance of User, use 'user.getNewUser()' to get a User object." );
			}
			if( config.multiuser ) {
				userList[ _user.id ]( _user );
			} else {
				userList[ single_user_id ] = _user;
			}
			mod_login.statusChange( _user );
		};
		mod_login.getUser = function ( userId ) {
			if( config.multiuser ) {
				return util.extend( true, {}, userList[ userId ] );
			} else {
				return userList[ single_user_id ];
			}
		};
		mod_login.statusChange = function ( user ) {
			logger.info( "User Status:" + ( user ? user.toString() : null ) );
			event.publish( "user.status", user );
		};
		mod_login.onStatusChange = function ( callback ) {
			event.subscribe( "user.status", callback );
		};
		mod_login.updateConfig = function ( init_config ) {
			if( init_config ) {
				config = util.extend( true, {}, config, init_config );
			}
		};
		mod_login.init = function ( init_config ) {
			mod_login.updateConfig( init_config );
			if( config.renderer === true ) {
				mod_login.onStatusChange( function ( _user ) {
					mod_login.renderPlugins( _user );
				} );
			}
			mod_login.isLoggedIn( function () {} );
			mod_login.initActions();
		};
		mod_login.initActions = function () {
			$( "[data-plugin='user-isloggedin']" )
				.on( "click", "[data-plugin='user-logout']", function () {
					mod_login.logout();
				} );
			$( "[data-plugin='user-notloggedin']" )
				.on( "click", "[data-plugin='user-login']", function () {
					mod_login.login();
				} )
				.on( "click", "[data-plugin='user-register']", function () {
					mod_login.register();
				} )
				.on( "click", "[data-plugin='user-login-facebook']", function () {
					mod_login.loginWithFacebook();
				} )
				.on( "click", "[data-plugin='user-login-twitter']", function () {
					mod_login.loginWithTwitter();
				} )
				.on( "click", "[data-plugin='user-login-google']", function () {
					mod_login.loginWithGoogle();
				} );
		};
		mod_login.renderPlugins = function ( _user ) {
			//        var _user = mod_login.getUser();
			var user_isLoggedIn = $( "[data-plugin='user-isloggedin']" ),
			user_isNotLoggedIn = $( "[data-plugin='user-notloggedin']" );
			if( user_isNotLoggedIn.length === 0 ) {
				logger.warn( "Cannot render plugins. data-plugin='user-isloggedin' not found" );
			}
			if( _user ) {
				user_isLoggedIn.show();
				user_isNotLoggedIn.hide();
				$( "[data-plugin='user-info']" ).each( function () {
					var tthis = $( this ),
					data = tthis.attr( "data-info" );
					try {
						tthis.text( _user[ "get" + string.camelCase( data ) ]() );
						logger.info( "user-info=" + data + ":" + _user[ "get" + string.camelCase( data ) ]() );
					} catch( e ) {
						logger.error( "user-info=" + data + ":" + e.message );
					}
				} );
			} else {
				user_isLoggedIn.hide();
				user_isNotLoggedIn.show();
			}
		};
		if( config.autoinit === true ) {
			mod_login.init();
		}
		return mod_login;
	} );

/**
 * 'page' module.
 * //todo find better name - it is not generic file
 * @module page
 * @requires util
 */
define( 'tiljs/page',[ "module", "./util", "jquery" ], function ( module, util, $ ) {
	var default_config = {
		msid: window.msid,
		channel: "",
		siteId: "",
		domain: ""
	};
	var config = util.extend( true, {}, default_config, module.config() );
	var mod_page = {};
	mod_page.getMsid = function ( url ) {
		try {
			if( !url || url.length === 0 || url === "#" ) {
				url = location.href;
				//                return /\/(\d*)\.cms/.exec(url)[1];
				return /(\d*)\.cms/.exec( url )[ 1 ];
			} else {
				return config.msid;
			}
		} catch( e ) {
			return config.msid;
		}
	}
	mod_page.getSiteId = function () {
		return config.siteId;
	}
	mod_page.getDomain = function () {
		return config.domain;
	}
	mod_page.getChannel = function () {
		return config.channel;
	}
	mod_page.getMeta = function ( property ) {
		if( property ) {
			return $( "meta[property='" + property + "']" ).attr( "content" );
		} else {
			var meta = {};
			$( "meta[property]" ).each( function ( k, v ) {
				var metaElement = $( v );
				meta[ metaElement.attr( "property" ) ] = metaElement.attr( "content" );
			} );
			return meta;
		}
	}
	return mod_page;
} );

define( 'tiljs/analytics/mytimes',[ "module", "../ajax", "../util" ], function ( module, ajax, util ) {
	var mod_mytimes = {};
	var default_config = {
		url: "http://myt.indiatimes.com/mytimes/",
		//        js_url: "http://mytest.indiatimes.com/mytimes/" //test url
		appKey: ""
	};
	var config = util.extend( true, {}, default_config, module.config() );
	mod_mytimes.log = function ( url, params ) {
		var req = new Image();
		req.src = url + "?" + ajax.param( params );
	};
	mod_mytimes.logActivity = function ( args ) {
		var _url = config.url + "addActivity";
		var params = util.extend( true, {
			//            appKey: config.appKey,
			//            uniqueAppID: 25416336,
			//            activityType: "Shared",
			//            baseEntityType: "ARTICLE",
			//            objectType: "B",
			//            url: url,
			//            via: "facebook"
		}, args );
		mod_mytimes.log( _url, params );
	};
	mod_mytimes.logCommentActivity = function ( args ) {
		var params = util.extend( true, {
			appKey: config.appKey,
			parentCommentId: 0,
			activityType: "", //Agreed, Disagreed, Reccomended
			baseEntityType: "ARTICLE",
			objectType: "A",
			url: document.location.href
		}, args );
		mod_mytimes.logActivity( params );
	};
	mod_mytimes.agreeComment = function ( msid ) {
		var params = {
			parentCommentId: msid,
			activityType: "Agreed"
		};
		mod_mytimes.logCommentActivity( params );
	};
	mod_mytimes.disagreeComment = function ( msid ) {
		var params = {
			parentCommentId: msid,
			activityType: "Disagreed"
		};
		mod_mytimes.logCommentActivity( params );
	};
	mod_mytimes.recommendComment = function ( msid ) {
		var params = {
			parentCommentId: msid,
			activityType: "Reccomended"
		};
		mod_mytimes.logCommentActivity( params );
	};
	mod_mytimes.logShareCount = function ( entityId, via ) {
		var url = config.url + "entityCount";
		var params = {
			appKey: config.appKey,
			via: via,
			entityId: entityId
		};
		mod_mytimes.log( url, params );
	};
	mod_mytimes.getShareCount = function ( msids, callback ) {
		var url = config.url + "sharedEntity";
		var params = {
			appKey: config.appKey,
			msids: msids
		};
		//        $.getJSON(url,params,callback);
		ajax.ajax( {
			type: 'GET',
			url: url,
			jsonpCallback: 'getShareCount' + Math.random() * 1000000000000000000,
			dataType: 'jsonp',
			data: params,
			success: function ( res ) {
				var result = res && res.length > 0 ? res[ 0 ] : null;
				if( result && callback ) {
					if( callback ) {
						callback( result );
					} else {
						callback( null );
					}
				}
			},
			error: callback
		} );
	};
	mod_mytimes.getNotifications = function ( callback ) {
		var url = config.url + "notification";
		var params = {
			appKey: config.appKey,
			openNetworkId: "sso",
			size: 100,
			lastSeenId: 0,
			after: true
		};
		//        $.getJSON(url,params,callback);
		ajax.ajax( {
			type: 'GET',
			url: url,
			jsonpCallback: 'getNotifications' + Math.random() * 1000000000000000000,
			dataType: 'jsonp',
			data: params,
			success: callback,
			error: callback
		} );
	};
	mod_mytimes.followUser = function ( userId, callback ) {
		ajax.getJSONP( config.url + "followuser", {
			userId: userId,
			fromMyTimes: true
		}, callback );
	};
	mod_mytimes.unfollowUser = function ( userId, callback ) {
		ajax.getJSONP( config.url + "unfollowuser", {
			userId: userId,
			fromMyTimes: true
		}, callback );
	};
	mod_mytimes.getFollowers = function ( callback ) {
		ajax.getJSONP( config.url + "activity/myfriends", {
			openNetworkId: "sso",
			/*appKey: "", */ size: -1,
			lastSeenId: 0,
			after: true,
			type: "follower"
		}, callback );
	};
	mod_mytimes.getFollowee = function ( callback ) {
		ajax.getJSONP( config.url + "activity/myfriends", {
			openNetworkId: "sso",
			/*appKey: "",*/ size: -1,
			lastSeenId: 0,
			after: true,
			type: "followee"
		}, callback );
	};
	mod_mytimes.followUser = function ( userId, callback ) {
		ajax.getJSONP( config.url + "followuser", {
			userId: userId,
			fromMyTimes: true
		}, callback );
	};
	mod_mytimes.updateUserCity = function (userCity, callback ) {
		ajax.getJSONP( config.url + "/profile/update?city="+userCity, {
			openNetworkId: "sso",
			/*appKey: "",*/ size: -1,
			lastSeenId: 0,
			after: true
		}, callback );
	};
	return mod_mytimes;
} );
//function log(e, t) {
//    var n = "http://rewards.indiatimes.com/bp";
//    var r = window._tp_async_data;
//    if (validate(r)) {
//        var i = n + "/api/alog/gal?uemail=" + encodeURI(r.email);
//        i += "&url=" + encodeURI(r.URL);
//        i += "&pcode=" + encodeURI(r.host);
//        i += "&scode=" + encodeURI(r.channel);
//        i += "&oid=" + encodeURI(t);
//        i += "&uid=" + encodeURI(r.userId);
//        i += "&ecode=" + encodeURI(e);
//        r.domain = window.location.hostname + window.location.port;
//        myDataStore.hideMiniNotification();
//        myDataStore.sendRequest(i, r)
//    } else {
//        var s = $.jStorage.get("credit");
//        if (s == null) {
//            myDataStore.checkValidity();
//            myDataStore.addActivity(e);
//            myDataStore.displayMiniNotification();
//            myDataStore.scroll()
//        }
//    }
//}
;
define('tiljs/apps/times/api',[
		"module",
		"../../util",
		"../../ajax",
		"../../cookie",
		"../../event",
		"../../string",
		"../../page",
		"../../analytics/mytimes"
], function ( module, util, ajax, cookie, event, string, page, mytimes ) { //todo remove mytimes dependency
	var mod_api = {};
	var default_config = {
		ticket: {
			url: "https://jsso.indiatimes.com/sso/crossdomain/getTicket"
		},
		usersInfo: {
			url: "http://myt.indiatimes.com/mytimes/profile/info/v1/", 
			params: {
				ssoid: "" // comma separated ssoids(emailid)
			}
		},
		badges: {
			url: "http://rewards.indiatimes.com/bp/api/urs/mubhtry",
			params: {
				format: "json",
				pcode: page.getChannel(),
				uid: "" // comma separated ssoids(emailid)
			}
		},
		rewards: {
			url: "http://rewards.indiatimes.com/bp/api/urs/ups",
			params: {
				format: "json",
				pcode: page.getChannel(),
				uid: "" // comma separated ssoids(emailid)
			}
		},
		comments: {
			//todo remove
			url: document.location.host == "test.indiatimes.com" || document.location.host == "test.happytrips.com" ? "lib/getComments.php" : "/json/new_cmtofart2_nit_v1.cms",
			type: "json",
			//            url: function(params){return "data/"+params.msid+".comments.json"},
			//            url: function(params){return "/json/test_comments/"+params.msid+".cms"},
			params: {
				msid: "",
				curpg: 1,
				pcode: page.getChannel()
				//                commenttype: "mostrecommended",
				//                sorttype: "bycount",
				//                ordertype: "asc"
				/*
                 &ordertype=asc
                 &commenttype=mostrecommended&sorttype=bycount
                 &commenttype=mostdiscussed
                 &commenttype=agree&sorttype=bycount
                 &commenttype=disagree&sorttype=bycount
                 */
			}
		}
	};
	default_config.comments_newest = default_config.comments;
	default_config.comments_oldest = {
		url: default_config.comments.url,
		type: default_config.comments.type,
		params: {
			msid: "",
			curpg: 1,
			ordertype: "asc",
			pcode: page.getChannel()
		}
	};
	default_config.comments_recommended = {
		url: default_config.comments.url,
		type: default_config.comments.type,
		params: {
			msid: "",
			curpg: 1,
			commenttype: "mostrecommended"
			//            ,sorttype: "bycount"
			,
			pcode: page.getChannel()
		}
	};
	default_config.comments_discussed = {
		url: default_config.comments.url,
		type: default_config.comments.type,
		params: {
			msid: "",
			curpg: 1,
			commenttype: "mostdiscussed"
			//            ,sorttype: "bycount"
			,
			pcode: page.getChannel()
		}
	};
	default_config.comments_agree = {
		url: default_config.comments.url,
		type: default_config.comments.type,
		params: {
			msid: "",
			curpg: 1,
			commenttype: "agree"
			//            ,sorttype: "bycount"
			,
			pcode: page.getChannel()
		}
	};
	default_config.comments_disagree = {
		url: default_config.comments.url,
		type: default_config.comments.type,
		params: {
			msid: "",
			curpg: 1,
			commenttype: "disagree"
			//            ,sorttype: "bycount"
			,
			pcode: page.getChannel()
		}
	};
	default_config.validate_comment = {
		url: "/validatecomment.cms",
		type: "html",
		params: {
			//hostid:83,//259:travel
			//rchid:-2128958273,//2147477992:travel
			fromname: null,
			fromaddress: null,
			userid: null,
			location: null,
			imageurl: null,
			loggedstatus: null,
			message: null,
			roaltdetails: null,
			ArticleID: null,
			msid: null,
			parentid: null,
			rootid: null
		}
	};
	default_config.post_comment = {
		url: "/postro.cms",
		type: "html",
		params: {
			//hostid:83,//259:travel
			//rchid:-2128958273,//2147477992:travel
			fromname: null,
			fromaddress: null,
			userid: null,
			location: null,
			imageurl: null,
			loggedstatus: null,
			message: null,
			roaltdetails: null,
			ArticleID: null,
			msid: null,
			parentid: null,
			rootid: null
		}
	};
	default_config.post_comment_withoutverification = {
		url: "/postro_nover.cms",
		type: "html",
		params: {
			//hostid:83,//259:travel
			//rchid:-2128958273,//2147477992:travel
			fromname: null,
			fromaddress: null,
			userid: null,
			location: null,
			imageurl: null,
			loggedstatus: null,
			message: null,
			roaltdetails: null,
			ArticleID: null,
			msid: null,
			parentid: null,
			rootid: null
		}
	};

	default_config.post_comment_withverification = {
		url: "/postroemailverification.cms",
		type: "html",
		params: {
			//hostid:83,//259:travel
			//rchid:-2128958273,//2147477992:travel
			fromname: null,
			fromaddress: null,
			userid: null,
			location: null,
			imageurl: null,
			loggedstatus: null,
			message: null,
			roaltdetails: null,
			ArticleID: null,
			msid: null,
			parentid: null,
			rootid: null
		}
	};
	default_config.rate = {
		//http://timesofindia.indiatimes.com/rate_techreview.cms?msid=44823267&getuserrating=1&criticrating=9&vote=8
		url: "/rate_techreview.cms",
		type: "html",
		params: {
			msid: null,
			getuserrating: null,
			vote: null,
			criticrating: null
		}
	};

	default_config.rate_comment = {
		url: "/ratecomment_new.cms",
		type: "html",
		params: {
			opinionid: null,
			typeid: null,
			rateid: null
		}
	};
	default_config.rate_comment_offensive = {
		url: "/offensive/mark",
		type: "html",
		params: {
			ofusername: null,
			ofreason: "NONE",
			ofcommenteroid: null,
			ofcommenthostid: 83,
			ofcommentchannelid: -2128958273,
			ofcommentid: null,
			ofuserisloggedin: null,
			ofuserssoid: null,
			ofuseremail: null
		}
	};
	var config = util.extend( true, {}, default_config, module.config() );
	mod_api.updateConfig = function ( new_config ) {
		config = util.extend( true, config, new_config );
	};
	mod_api.getConfig = function ( new_config ) {
		return config;
	};
	mod_api.getTicket = function ( _params, callback ) {
		return mod_api.api( "ticket", _params, callback );
	};
	mod_api.getUsersInfo = function ( _params, callback ) {
		return mod_api.api( "usersInfo", {ssoid:_params.ssoids}, callback );
	};
	mod_api.getBadges = function ( _params, callback ) {
		return mod_api.api( "badges", _params, callback );
	};
	mod_api.getRewards = function ( _params, callback ) {
		return mod_api.api( "rewards", _params, callback );
	};
	mod_api.getComments = mod_api.getComments_newest = function ( _params, callback ) {
		return mod_api.api( "comments", _params, callback );
	};
	mod_api.getComments_oldest = function ( _params, callback ) {
		return mod_api.api( "comments_oldest", _params, callback );
	};
	mod_api.getComments_recommended = function ( _params, callback ) {
		return mod_api.api( "comments_recommended", _params, callback );
	};
	mod_api.getComments_discussed = function ( _params, callback ) {
		return mod_api.api( "comments_discussed", _params, callback );
	};
	mod_api.getComments_agree = function ( _params, callback ) {
		return mod_api.api( "comments_agree", _params, callback );
	};
	mod_api.getComments_disagree = function ( _params, callback ) {
		return mod_api.api( "comments_disagree", _params, callback );
	};
	mod_api.validateComment = function ( _params, callback ) {
		return mod_api.post( "validate_comment", _params, callback );
	};
	mod_api.postComment = function ( _params, callback ) {
		return mod_api.post( "post_comment", _params, callback );
	};
	mod_api.postCommentWithoutVerification = function ( _params, callback ) {
		return mod_api.post( "post_comment_withoutverification", _params, callback );
	};
	mod_api.postCommentWithVerification = function ( _params, callback ) {
		return mod_api.post( "post_comment_withverification", _params, callback );
	};

	/**
	 *
	 *
	 *
	 * @param rating msid, user_rating, critic_rating, rating
	 * @param callback
	 * @param user
	 * @returns {*}
	 */
	mod_api.rate = function ( rating, callback, user ) {
		var rating_u = {
			msid: rating.msid,
			getuserrating: rating.user_rating,
			vote: rating.rating,
			criticrating: rating.critic_rating
		};
		return mod_api.api( "rate", rating_u, callback );
	};

	mod_api.rateComment = function ( rating, callback, user ) {
		if( user ) { //TODO move all calls to backend
			switch( rating.typeid ) {
			case 100: //Agree
				mytimes.agreeComment( rating.opinionid );
				break;
			case 101: //Disagree
				mytimes.disagreeComment( rating.opinionid );
				break;
			case 102: //Recommend
				mytimes.recommendComment( rating.opinionid );
				break;
			case 103: //Offensive
				mod_api.rateCommentOffensive( rating, user ); //todo check if mytimes request is to be sent
				break;
			}
		}
		//http://myt.indiatimes.com/mytimes/addActivity?activityType=Agreed&appKey=TOI&parentCommentId=28368882&baseEntityType=ARTICLE&objectType=A&url=
		//http://myt.indiatimes.com/mytimes/addActivity?activityType=Disagreed&appKey=TOI&parentCommentId=28368547&baseEntityType=ARTICLE&objectType=A&url=
		//http://myt.indiatimes.com/mytimes/addActivity?activityType=Reccomended&appKey=TOI&parentCommentId=28368541&baseEntityType=ARTICLE&objectType=A&url=
		return mod_api.api( "rate_comment", rating, callback );
	};
	mod_api.rateCommentOffensive = function ( rating, user, callback ) {
		//OLD http://timesofindia.indiatimes.com/offensiveService/offence.asmx/getOffencivecomment?ofusername=Del%20Sanic&ofreason=Others:%20testing&ofcommenteroid=28231992&ofcommenthostid=83&ofcommentchannelid=-2128958273&ofcommentid=30937855&ofuserisloggedin=1&ofuserssoid=delsanic@gmail.com&ofuseremail=delsanic@gmail.com
		//http://timesofindia.indiatimes.com/offensive/mark?ofusername=Del%20Sanic&ofreason=Others:%20testing&ofcommenteroid=28232011&ofcommenthostid=83&ofcommentchannelid=-2128958273&ofcommentid=30937855&ofuserisloggedin=1&ofuserssoid=delsanic@gmail.com&ofuseremail=delsanic@gmail.com
		if( user ) {
			var params = {};
			params.ofcommenteroid = rating.opinionid;
			params.ofreason = rating.ofreason;
			params.ofcommentid = window.msid;
			params.ofuserisloggedin = 1;
			params.ofuserssoid = params.ofuseremail = user.getEmail();
			params.ofusername = user.getFullName();
			//            params = util.extend(true,{}, params, rating) ;
			return mod_api.api( "rate_comment_offensive", params, callback );
		} else {
			event.publish( "logger.error", "Cannot rate comment offensive, user not available." );
		}
	};
	mod_api.api = function ( api, _params, callback ) {
		var value = config[ api ];
		return ajax.get( util.val( value.url, value.params ), util.extend( true, {}, value.params, _params ), function ( data ) {
			if( callback ) {
				try {
					callback( data );
				} catch( e ) {
					event.publish( "logger.error", e.stack );
				}
			}
		}, value.type || "jsonp" ).error( function () {
			if( callback ) {
				try {
					callback();
				} catch( e ) {
					event.publish( "logger.error", e.stack );
				}
			}
		} );
	};
	mod_api.post = function ( api, _params, callback ) {
		var value = config[ api ];
		return ajax.post( util.val( value.url, value.params ), util.extend( true, {}, value.params, _params ), function ( data ) {
			if( callback ) {
				try {
					callback( data );
				} catch( e ) {
					event.publish( "logger.error", e.stack );
				}
			}
		} ).error( function ( event, xhr, e ) {
			event.publish( "comment.post.error", e );
		} );
	};
	mod_api.get = function ( key, params ) { //todo change method name
		if( params ) {
			var api = config[ key ];
			var _params = util.extend( true, {}, api.params, params );
			return {
				url: util.val( api.url, _params ),
				params: _params
			};
		} else {
			return config[ key ];
		}
	};
	mod_api.init = function () {
		util.each( config, function ( key, value ) {
			var funName = string.camelCase( "get " + key );
			mod_api[ funName ] = function ( _params, callback ) {
				ajax.getJSONP( util.val( value.url, value.params ), util.extend( true, {}, value.params, _params ), function ( data ) {
					if( callback ) {
						callback( data );
					}
				} );
			};
		} );
	};
	//    mod_api.init();
	return mod_api;
} );

/**
 * 'ui' module.
 *
 * @module ui
 * @requires util
 * @requires logger
 * @requires event
 * @requires jquery
 * @requires is
 */
define('tiljs/ui',["./util", "./logger", "./event","jquery","./is", "./timer"], function (util, logger, event, $, is, timer) {
    logger.log("ui loaded");
    var mod_ui = {},
    open_window = {},
	mask,maskConfig;

    mod_ui.window = function (url, options) {
        logger.log("ui.window called");
		var default_options,opt,x,y,params,popup;
        default_options = {
            width: 300,
            height: 300,
            name: "Window" ,
            mask:true,
            resizable:false,
            disableScroll:false,
            closeCallback: function(){logger.log("Empty ui.window callback function.");}
        };
        opt = util.extend(true, {}, default_options, options);

        x = window.screen.width/2 -  opt.width/2;
        y = window.screen.height/2 - opt.height/2;

        params = ['width=' + opt.width,
            'height=' + opt.height,
            'left=' + (is.defined(opt.left)?opt.left:x),
            'top=' + (is.defined(opt.top)?opt.top:(y-20)),
            'scrollbars=' + (is.defined(opt.scrollbars) && (opt.scrollbars == 1)?opt.scrollbars:0),
            'resizable=' + opt.resizable];			

         if(opt.mask === true && is.desktop()){
             mod_ui.mask(null,{},false);
         }
        if(opt.disableScroll === true){
            mod_ui.disableScroll();
        }

        popup = window.open(url, opt.name, params.join(","));
        if(popup){
            popup.focus();
            logger.log("Popup opened: " + url);
            logger.log(popup);
        }

//        popup.onresize = function(){
//            popup.resizeTo(opt.width,opt.height);
//        };


        try{
            //Exception "Permission Denied" in IE, using try catch so that this functionality works in other browsers.
            popup.reposition = function(){
                var x = window.screen.width/2 -  opt.width/2,
                y = (window.screen.height/2 - opt.height/2);
                popup.moveTo(x,y);
            };
        }catch(e){
            logger.warn("Handled Exception in IE10.");
            logger.error(e);
        }

        if(!popup){
            alert("Popups are blocked. Please enable them.");//TODO find better way
            logger.error("Popups are blocked. Please enable them.");
            mod_ui.unmask();
            return popup;
        }
        //TODO use timer module, prevent recursive dependency
        (function(popup,url,opt){
            var interval = window.setInterval(function() {   //todo use timer
                try {
                    logger.log("Checking if popup is closed:"+url+":" + popup.closed);
                    if (popup === null || popup.closed !== false) {
                        mod_ui.window.close(opt.name);
                    }
                }
                catch (e) {
                    logger.warn("Handled exception while closing popup.");
                    logger.error(e.stack);
                    window.clearInterval(interval);
                    interval = null;
                }
            }, 500);

            open_window[opt.name] = {opt:opt, popup:popup, interval:interval, url:url};

        }(popup,url,opt));


        return popup;
    };

    mod_ui.window.close = function(name, all){
        if(!name){
            logger.error("Window name is required to close it.");
            return;
        }
        var popup = open_window[name];
        if(all === true){
              util.each(open_window,function(k){
                  mod_ui.window.close(k);
              });
        }else if(popup){
            logger.log("Closing popup is closed:" + popup.opt.name + ":" + popup.url);

            if(popup.opt.mask === true){
                mod_ui.unmask();
            }
            if(popup.opt.disableScroll === true){
                mod_ui.enableScroll();
            }

            window.clearInterval(popup.interval);
            popup.interval = null;
            popup.popup.close();
            open_window[popup.opt.name] = null;
            popup.opt.closeCallback(popup.popup);
        }else{
            logger.warn("Popup '"+name+"' not found.");
        }
    };

    if(window.closeWindow){
        logger.warn("window.closeWindow is already defined, open windows may not close properly.");
    }else{
        window.closeWindow = mod_ui.window.close;
    }

    mod_ui.iframe = function (url, options) {
        var default_options = {
            width: 300,
            height: 300,
            name: "Window" ,
            disablePopup:true,
            closeCallback: function(){
	            logger.log("Empty ui.iframe callback function.");
            }
        },

        opt = util.extend(true, {}, default_options, options),
        iframe = $("<iframe class='loginsignupframe'></iframe>"),//todo fix class dependency
		_iframe,popup;
		
        iframe.attr("src", url);
        iframe.css({
            width:"100%",
            height:"100%",
            border:"4px solid #cccccc",
            backgroundColor:"#FFF"
        });

        _iframe = iframe[0];

        popup = mod_ui.popup(iframe,opt);


        _iframe.close = function(){
             iframe.remove();
             popup.remove();
             mod_ui.unmask();
        };

        _iframe.resizeTo = function(width, height){
            if(width && height){
                popup.width(width);
                popup.height(height);
            }
        };

        _iframe.reposition = popup.reposition;

        return _iframe;
    };

    mod_ui.closeButton = function(clickCallback){
        var closeButton = $("<span></span>");
        closeButton.on("click",clickCallback);
        return closeButton;
    };

    mod_ui.popup = function (html, options) {
		var default_options,closeButton,mask,opt,popup;
        default_options = {
            width: 300,
            height: 300,
            name: "Window" ,
            closeCallback: function(){logger.log("Empty ui.popup callback function.");},
            disableScroll:false
        };
		
        opt = util.extend(true, {}, default_options, options);
        popup = $("<div></div>");
        popup.width(opt.width);
        popup.height(opt.height);
        popup.css({
//            margin:"0 auto",
//            marginTop:"5%",
            position:"absolute",
            zIndex:maskConfig.zIndex+1
        });
        popup.append(html);

        if(opt.disableScroll === true){
            mod_ui.disableScroll();
        }

        if(opt.closeCallback){
            if(window.closePopup){
                logger.warn("'window.closePopup' is already defined.");
            }
            if(window.disablePopup){
                logger.warn("'window.disablePopup' is already defined.");
            }

            window.closePopup = window.disablePopup = function(){
                popup.remove();
                mod_ui.unmask();

                if(opt.disableScroll === true){
                    mod_ui.enableScroll();
                }

                if(opt.closeCallback){
                    opt.closeCallback();
                }
                window.closePopup = null;
                window.disablePopup = null;
            };

            closeButton = mod_ui.closeButton(window.closePopup);

            closeButton.addClass("popup_close_button");
            //todo remove css from here
            closeButton.css({
                position: "absolute",
                top: "0px",
                right: "0px" ,
                cursor:"pointer",
                background:"url('/photo/25494620.cms') no-repeat scroll -160px -20px rgba(0, 0, 0, 0)",
                width:"18px",
                height:"18px"
            });

            popup.append(closeButton);
        }

        $("body").append(popup);

        mask = mod_ui.mask(popup,{},false);

        popup.reposition = function(){
            var $w = $(window),
            windowWidth = $w.width(),
            windowHeight = $w.height(),

            calcWidth = ((windowWidth/2)-(popup.width()/2)),
            calcHeight = ((windowHeight/2)-(popup.height()/2)) ;

            if(calcWidth < 0 ){calcWidth=0;}
            if(calcHeight < 0 ){calcHeight=0;}
            popup.css({
                left:calcWidth,
                top:calcHeight + $(window).scrollTop()
            });
        };

        popup.reposition();

        popup.close = function(){
            popup.remove();
            mod_ui.unmask();
        };

        event.subscribe("window.resize",function(){
            popup.reposition();
        });


        return popup;
    };



    mod_ui.img = function (url, attr) {
        var img = $("<img>");
        img.attr("src", url);
        util.each(attr, function (k, v) {
            img.attr(k, v);
        });
        return img;
    };


    mod_ui.anchor = function (url, text, attr) {
        var anchor = $("<a>");
        anchor.attr("href", url);
        anchor.text(text);
        util.each(attr, function (k, v) {
            anchor.attr(k, v);
        });
        return anchor;
    };

    /**
     * Checks if the provided element is in viewport.
     *
     * //todo create event 'inview' with callback which fires when element comes in view
     *
     * @param {selector/instance} elem element to be checked
     * @param {boolean} partial  when true,returns true when element is partially in view.
     * @param {int} skew Pixels to skew/move view from the top
     * @returns {boolean} true if element is in view
     */
    mod_ui.inView = function (elem, partial, skew) {
        skew = skew || 0;

        var $w = $(window), $e = $(elem),
		docViewTop,docViewBottom,elemTop,elemBottom,in_view;

        if($e.is(":hidden")){ // Element is hidden so its not in the view
            return false;
        }

        docViewTop = $w.scrollTop() - skew; //todo - skew
        docViewBottom = docViewTop + $w.height() + skew;//todo  + skew

        elemTop = $e.offset()?$e.offset().top:0;
        elemBottom = elemTop + $e.height();


        in_view = false;

        if (partial === true) {
            in_view = ((elemBottom > docViewTop ) && (elemTop <= docViewBottom ));
//            if(in_view){
//                logger.log("P:"+elemBottom +">"+docViewTop+","+elemTop+"<="+docViewBottom);
//            }
        } else {
            in_view = ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
//            if(in_view){
//                logger.log(elemBottom +"<="+docViewBottom+","+elemTop+">="+docViewTop);
//            }
        }

        return in_view;
    };


    mod_ui.init = function () {
        var th_scroll = util.throttle(500, function (e) {
            event.publish("window.scroll", e);
        }),

        th_load = function (e) {
            event.publish("window.load", e);
        },

        th_resize = util.throttle(500, function (e) {
            event.publish("window.resize", e);
        });

        if (window.addEventListener) {  // W3C DOM
            window.addEventListener("scroll", th_scroll, false);
            window.addEventListener("load", th_load, false);
            window.addEventListener("resize", th_resize, false);
        } else if (window.attachEvent) { // IE DOM
            window.attachEvent("onscroll", th_scroll);
            window.attachEvent("onload", th_load);
            window.attachEvent("onresize", th_resize);
        }
		
		$(function (e) {
			event.publish("document.ready", e);
		});
		
    };


    mod_ui.getGravatar = function (email, size) {
        if (!email || email.length === 0) {
            return null;
        }

        size = size || 80;

        return 'http://www.gravatar.com/avatar/' + util.md5(email.trim().toLowerCase()) + '.jpg?s=' + size;
    };

    mod_ui.maxlength = function (input, max, callback, parent, alertMsg) {

        $(parent || "body").off("keyup change", input);
        $(parent || "body").on("keyup change", input, function () {
            var val = $(this).val(),
            len = val.length;
            max = parseInt(max, 10);

            callback.call(this, max - len, len);
            if (len > max) {
                $(this).val(val.substr(0, max));
                if(alertMsg){
                    window.alert(alertMsg);
                }
                return false;
            }
        });
    };

    mod_ui.getActionReferences = function (currentEle, mainParent) {
        var obj = {current: $(currentEle)};
        obj.parent = mainParent ? obj.current.parents(mainParent) : obj.current;

        obj.parent.find("[data-plugin]").each(function (i, v) {
            obj[$(v).attr("data-plugin")] = $(this);
        });

        return obj;
    };

    mask = null;
    maskConfig = {
        position:"fixed",
        left:0,
        top:0,
        width: "100%",
        height: "100%",
        backgroundColor: "#000",
        opacity: 0.5 ,
        zIndex:99999
    };

    /**
     *
     * @param config to be provided once on a page.
     * @returns {null}
     * @param divEle Element to be shown above the mask
     * @param closeOnClick (default = false) If true, mask will be closed when clicked
     */
    mod_ui.mask = function (divEle,config,closeOnClick) {
        if (!mask) {
            mask = $("<div></div>");
            mask.popup = divEle;
            mask.css(util.extend(maskConfig,config));
//            mask.css("height", $(document).height());
            if(mask.popup){
                mask.popup.show();
            }
            if(typeof closeOnClick === 'undefined' || closeOnClick === true){
                mask.on("click",function(){
                    if(mask.popup){
                        mask.popup.hide();
                    }
                    mod_ui.unmask();
                });
            }
            $(document).one("keyup",function(e){     //todo move to common
                if(e.keyCode === 27){
                    if(mask){
                        if(mask.popup){
                            mask.popup.hide();
                        }
                        mod_ui.unmask();
                    }
                }
            });
            $("body").append(mask);
        } else {
            logger.warn("Already masked. Use ui.unmask() before masking again.");
        }
        return mask;
    };

    mod_ui.isMasked = function () {
        return mask ? true : false;
    };

    mod_ui.unmask = function () {
        if (mask) {
            if(mask.popup){
                mask.popup.hide();
            }
            mask.remove();
            mask = null;
        } else {
            logger.warn("No mask available. Use ui.mask() before unmasking.");
        }
    };

    mod_ui.tooltip = function(message, hideAfter){
        hideAfter = hideAfter || 5;

        var tooltip = $("<div></div>");
        if(hideAfter>0){
            timer.after(function(){
                tooltip.fadeOut(function(){

                });

            });
        }

        return tooltip;
    };

    mod_ui.dialog = function (divEle) {
        //var default_params = {},
        //prms = util.extend(true,{}, default_params, params),

        var dialog = $("<div></div>");
        dialog.css({
            backgroundColor: "#FFF"
        });

        dialog.html(divEle.html());

        return dialog;
    };

    mod_ui.customScrollbar = function (ele) {
        if(window.$ && window.$ && window.$.fn && window.$.fn.mCustomScrollbar){
            ele = window.$(ele);
            ele.mCustomScrollbar("destroy");
            ele.mCustomScrollbar({
                scrollButtons: {
                    enable: true
                },
                advanced: {
                    updateOnContentResize: true,
                    updateOnBrowserResize: true
                }
            });
        }else{
            logger.warn("mCustomScrollbar not found.");
        }
    };

    mod_ui.disableScroll = function(mainContainer){
        $("html,body").css({overflow:"hidden",height:"100%",padding:"0",margin:"0"});
        $(mainContainer || "#container").css({overflowY:"scroll",height:"100%"});
    };

    mod_ui.enableScroll = function(mainContainer){
        $("html,body").css({overflow:"",height:"",padding:"",margin:""});
        $(mainContainer || "#container").css({overflowY:"",height:""});
    };

    mod_ui.init();


    return  mod_ui;
});

/**
 * 'load' module.
 *
 * @module load
 * @requires event
 * @requires logger
 */
define( 'tiljs/load',[ "./event", "./logger", "./util", "./is" ], function ( event, logger, util, is ) {
	logger.log( "load loaded" );
	//    var LOADING = 0;
	//    var LOADED = 1;
	var mod_load = {};
	//    mod_load.js_queue = {};
	/**
	 * Load javascript file on a page
	 *
	 * @memberOf module:load#
	 * @function js
	 * @param {String} url URL of the javascript file
	 * @param {Function} [callback] function to be called when js is loaded
	 * @param {String} [id] id to be given to the javascript tag
	 * @param {Boolean} [async] true by default, set false to load synchronously
	 * @returns {HTMLElement} generated script tag element
	 * @example
	 *  require(['load'],function(load){
	 *     //Load javascript 'abc.js'
	 *     load.js('abc.js',function(){
	 *        console.log('abc.js loaded');
	 *     });
	 *
	 *     //Load javascript 'abc.js' , the script element is assigned provided id
	 *     load.js('abc.js',function(){
	 *        console.log('abc.js loaded');
	 *     },'element_id',true);
	 *  });
	 */
	mod_load.js = function ( url, callback, id, async ) {
		//TODO implement queue - prevent multiple js loading
		//        id = id || url.replace(/[^\w\s]/gi, ''); //todo BAD hack, implement queue
		//        var script =  document.getElementById(id);
		//        if(script && script.loaded){
		//            if(callback){
		//                callback();
		//            }
		//            return script;
		//        }
		var head = document.getElementsByTagName( "head" ) ? document.getElementsByTagName( "head" )[ 0 ] : null;
		if( head ) {
			var script = document.createElement( "script" );
			var done = false; // Handle Script loading
			if( id ) {
				script.id = id;
			}
			if( async ) {
				script.async = async;
			}
			if( !url ) {
				throw new Error( "Param 'url' not defined." );
			}
			script.src = url;
			script.onload = script.onreadystatechange = function () { // Attach handlers for all browsers
				if( !script.loaded && ( !this.readyState || this.readyState === "loaded" || this.readyState === "complete" ) ) {
					script.loaded = true;
					var endTime = new Date().getTime();
					var timeSpent = endTime - script.startTime;
					event.publish( "load.js", [ '_trackTiming', 'js', url, timeSpent, url ] );
					//                        track.ga(['_trackTiming', 'js', url , timeSpent, url ]);//todo identify label(last param)
					if( callback ) {
						try {
							callback();
						} catch( e ) { //to handle
							event.publish( "logger.error", e.stack );
						}
					}
					script.onload = script.onreadystatechange = null; // Handle memory leak in IE
				}
			};
			script.startTime = new Date().getTime();
			head.appendChild( script );
			return script;
		} else {
			logger.info( "Head Element not found. JS '" + url + "' not loaded. " );
			return null;
		}
	};
	/**
	 * Load css file on a page
	 *
	 * @memberOf module:load#
	 * @function css
	 *
	 * @param {String} href URL of the css file
	 * @param {Function} [callback] function to be called when
	 * @returns {HTMLElement} generated link tag element
	 * @example
	 *  require(['load'],function(load){
	 *     //Load stylesheet 'abc.css'
	 *     load.css('abc.css',function(){
	 *        console.log('abc.css loaded');
	 *     });
	 *  });
	 */
	mod_load.css = function ( href, callback ) {
		var headEle = document.getElementsByTagName( "head" ) ? document.getElementsByTagName( "head" )[ 0 ] : null;
		if( headEle ) {
			var cssLink = document.createElement( "link" );
			cssLink.setAttribute( "rel", "stylesheet" );
			cssLink.setAttribute( "type", "text/css" );
			cssLink.setAttribute( "href", href );
			headEle.appendChild( cssLink );
			if( callback ) {
				setTimeout( callback, 0 ); //TODO find better way
			}
		} else {
			logger.info( "Head Element not found. CSS '" + href + "' not loaded. " );
		}
		return cssLink;
	};
	/**
	 * Load iframe on a page
	 *
	 * @memberOf module:load#
	 * @function iframe
	 *
	 * @param {String} src URL of the iframe to be loaded
	 * @param {Number} width width of the iframe, defaults to 0
	 * @param {Number} height height of the iframe, defaults to 0
	 * @param {String} containerId id of the container in which the iframe is to be loaded
	 * @returns {HTMLElement} generated iframe element
	 * @example
	 *  require(['load'],function(load){
	 *     load.iframe('http://www.abc.com',100,100,'container_iframe');
	 *  });
	 */
	mod_load.iframe = function ( src, width, height, containerId ) {
		var container = document.getElementById( containerId ) || document.body;
		if( container ) {
			var i = document.createElement( "iframe" );
			i.src = src;
			i.scrolling = "no";
			i.frameBorder = "0";
			i.width = width || 0;
			i.height = height || 0;
			container.appendChild( i );
			return i;
		} else {
			logger.info( "Container with id '" + containerId + "' not found. iframe '" + src + "' not loaded. " );
		}
	};
	mod_load.image = function ( src ) {
		if( is.array( src ) ) {
			util.each( ajaxLogout, function ( i, v ) {
				mod_load.image( v );
			} );
		} else {
			var img = new Image();
			img.src = src;
		}
	};
	/**
	 * //TODO, remove private after implementing it.
	 *
	 * @private
	 */
	mod_load._content = function ( url, containerId ) {};
	/**
	 * Checks if the javascript or css is loaded
	 *
	 * @memberOf module:load#
	 * @function isLoaded
	 *
	 * @param path
	 * @returns {boolean}
	 * @example
	 *
	 *  require(['load'],function(load){
	 *     load.isLoaded('abc.js');   // returns true is abc.js is loaded
	 *     load.isLoaded('abc.css');  // returns true is abc.css is loaded
	 *  });
	 */
	mod_load.isLoaded = function ( path ) { //todo import jquery dependency
		return $( 'script[src="' + path + '"]' ).length > 0 || $( 'link[href="' + path + '"]' ).length > 0;
	};
	return mod_load;
} );

/**
 * 'localstorage' module.
 *
 * @module localstorage
 * @requires util
 * @requires json
 */
define( 'tiljs/localstorage',[ "module", "./util", "json", "./is", "./logger" ], function ( module, util, JSON, is, logger ) {
	var mod_localstorage = {};
	var default_config = {};
	var config = util.extend( true, {}, default_config, module.config() );
	var LS = window.localStorage;
	if( !LS ) {
		logger.warn( "'localStorage' is not available." );
		LS = {
			getItem: function () {},
			removeItem: function () {},
			setItem: function () {}
		};
	}
	/**
	 * Get value of a localstorage
	 *
	 * @param {String} [name] name of the localstorage for which value is required,
	 *                        if name is not provided an object with all localstorages is returned
	 * @returns {String | Array} value of the requested localstorage / Array of all localstorages
	 */
	mod_localstorage.get = function ( name ) {
		var val = LS.getItem( name );
		if( val != null ) {
			var record = JSON.parse( val );
			if( !record ) {
				return null;
			}
			return( new Date().getTime() < record.timestamp ) ? record.value : null;
		} else {
			return null;
		}
	};
	/**
	 * localStorage Set,Get,Delete
	 */
	mod_localstorage.getAll = function () {
		return mod_localstorage.get();
	};
	/**
	 * Remove a localstorage
	 *
	 * @param {String} name name of the localstorage to be removed
	 * @param {String} [path] path of the localstorage
	 * @param {String} [domain] domain of the localstorage
	 */
	mod_localstorage.remove = function ( name, path, domain ) {
		if( name ) {
			LS.removeItem( name );
		}
	};
	/**
	 * Set a localstorage
	 *
	 * @param {String} name name of the localstorage to be set
	 * @param {String} value value of the localstorage to be set
	 * @param {Number} [days] number of days for which the localstorage is to be set
	 * @param {String} [path] path of the localstorage to be set
	 * @param {String} [domain] domain of the localstorage to be set
	 * @param {Boolean} [secure] true if the localstorage is to be set on https only (to be implemented)
	 */
	mod_localstorage.set = function ( name, value, days, path, domain, secure ) {
		var date = new Date();
		var expirationMS = date.getTime() + ( ( days || 365 ) * 24 * 60 * 60 * 1000 );
		var record = {
			value: value,
			timestamp: new Date().getTime() + expirationMS,
			path: path,
			domain: domain
		};
		try {
			LS.setItem( name, JSON.stringify( record ) );
		} catch( e ) {
			//            if (e == 'QUOTA_EXCEEDED_ERR') {
			logger.error( 'Unable to save item in localStorage:' + name );
			//            }
		}
		//        return value;
	};
	return mod_localstorage;
} );

define('tiljs/apps/times/login',[
	"../../login",
	"../../util",
	"module",
	"../../page",
	"../../ajax",
	"../times/api",
	"../../is",
	"../../cookie",
	"../../ui",
	"../../logger",
	"../../event",
	"../../load",
	"../../localstorage"
],
	function ( login, util, module, page, ajax, api, is, cookie, ui, logger, event, load, localstorage ) {
		window.newLogin = true;// This enables twitter share dialog in /share.cms

		var default_config = {
			autoinit: false,
			multiuser: false, //allows multiple users to login at a time
			login: "",
			logout: "",
			check_user_status: "",
			mapping: null,
			renderer: false, //todo to be implemented
			login_return_url: "",
			logout_return_url: ""
		};
		var config = util.extend( true, {}, default_config, module.config() );
		try {
			document.domain = page.getDomain();
		} catch( e ) {
			logger.info( "Domain cannot be set:" + page.getDomain() );
		}
		//        window.__sso = function(params,callback){
		//            logger.info(params) ;
		//            //ui.unmask();
		//            mod_login.isLoggedIn(function(){
		//                if(callback){
		//                    callback(params);
		//                }
		//                if(loginCallback){
		//                    loginCallback(params);
		//                    loginCallback = null;
		//                }
		//            });
		//        };
		// to handle ios chrome issue - where parent is undefined
		var setCriOS;
		if(navigator.userAgent.match('CriOS')) {
			localStorage.removeItem('_ssodata');
			setCriOS = setCriOS || setInterval(function(){call_sso();}, 1000);
		}

		var call_sso = function(){
			if(localStorage.getItem('_ssodata') != null){
				__sso(JSON.parse(localStorage.getItem('_ssodata')));
				localStorage.removeItem('_ssodata');
			}
		}
		// to handle ios chrome issue - where parent is undefined
		var sso = function ( url, callback ) {
			loginWindow = ui.window( url, {
				width: 850,
				height: 780,
				scrollbars: 0
			}, function ( result ) {
				if( result && result.code === "200" ) {
					mod_login.isLoggedIn();
				} else {
					if( callback ) {
						callback( null, result );
					}
				}
			} );
			loginWindow.moveTo( 315, 250 );
		};
		var mod_login = login;
		var loginCallback = null;
		//        var loginErrorMsg = null;
		var loginData = null;
		var setLoginError = function ( loginErrorMsg ) {
			setLoginData( {
				error: {
					code: loginErrorMsg,
					message: config.messages[ loginErrorMsg ]
				}
			} );
		};
		var setLoginData = function ( data ) {
			loginData = data;
		};
		var reset = function () {
			return;
			if( loginWindow ) {
				loginWindow.close();
				loginWindow = null;
			}
			loginData = null;
		};
		window.getLoginCallback = function () {
			return loginCallback;
		};
		window.getLoginData = function () {
			return loginData;
		};
		window.getDomain = function () {
			return page.getDomain();
		};
		var setLocation = function ( href ) {
			if( is.iframe( loginWindow ) ) {
				loginWindow.src = href;
			} else {
				loginWindow.document.location.href = href;
			}
		};
		var loginResponse = function ( url, error, data ) {
			setLocation( url );
			if( error ) {
				setLoginError( error );
			} else if( data ) {
				setLoginData( data );
			}
		};
		var loginWindow = null;
		window.__sso = function ( data, url ) {
			logger.log( "__sso called" );
			logger.log( data );
			logger.log( loginWindow );
			if( data && loginWindow ) {
				logger.log( url );
				var currLoginData = loginData ? loginData.data : null;
				//|| data.ssoerror == "E119" || data.ssoerror == "E108" || data.ssoerror == "E104"*/
				//http://mytimes.indiatimes.com/stgredirectsocialtest.cms?status=signinsuccess&site=facebook&statustype=FBMappingLoginSuccess&channel=mytimes#_=_
				data.site = data.site || "email";
				if( data.closeWindow ) {
					logger.log( "Close pressed, closing window / popup" );
					loginWindow.close();
				} else if( data.status == "logout" ) {
					logger.log( "Closing window / popup" );
					loginWindow.close();
				} else if( data.status == "signinsuccess" || data.status == "ssosigninsuccess" ) {
					/*
                     When data.status == "signinsuccess"
                     data.statusType CAN BE "FBMappingLoginSuccess || TWMappingLoginSuccess" || "fb_mappingsaved_loginsuccess" || "TWRegisterLoginSuccess"
                     data.site CAN BE "twitter" || "facebook"
                     */
					logger.log( "Closing window / popup" );
					loginWindow.close();
					//                    setTimeout(function(){loginWindow.close();},500);
				} else if( ( data.site == "facebook" || data.site == "googleplus" ) && data.status == "MappingUpdated" ) {
					logger.log( "Closing window / popup" );
					loginWindow.close();
					//                    setTimeout(function(){loginWindow.close();},500);
				} else if( data.status == "ssosigninfailure" && data.ssoerror == "E119" ) { //E119-username with password incorrect
					loginResponse( config.base_url + "/loginview.cms?x=error&site=" + data.site, "email_failure" );
				} else if( data.status == "ssosigninfailure" && data.ssoerror == "E104" ) { //E104-email with password incorrect
					loginResponse( config.base_url + "/loginview.cms?x=error&site=" + data.site, "email_failure" );
				} else if( data.err == "E104" && data.facebooktoken ) { //E104-facebook not sending email
					loginResponse( config.base_url + "/loginview.cms?x=error&site=" + data.site, "facebook_failure_no_email" );
				} else if( data.status == "signinfailure" && data.error == "F101" ) {
					//                    {site: "facebook", status: "signinfailure", statustype: "fb_mappingfailure", sso: true}
					//                    {site: "facebook", status: "signinsuccess", statustype: "fb_mappingsaved_loginsuccess", sso: true}
					//                    {site: "facebook", status: "signinsuccess", statustype: "FBMappingLoginSuccess", sso: true}
					loginResponse( config.base_url + "/loginview.cms?x=error&site=" + data.site, data.site + "_failure" );
				} else if( data.status == "signinfailure" && data.error == "twitter" && data.site == "twitter" ) { // twitter user denied access to account
					loginResponse( config.base_url + "/loginview.cms?x=error&site=twitter", "twitter_failure_unknown" );
				} else if( data.status == "signinfailure" && data.error == "user_denied" && data.site == "facebook" ) { //facebook user denied access to the account
					loginResponse( config.base_url + "/loginview.cms?x=error&site=" + data.site, "facebook_failure_user_denied" );
				} else if( data.error == "E104" && data.site == "twitter" ) { //when log in with twitter and it is not linked
					loginResponse( config.base_url + "/socialconnect.cms?site=twitter", null, {
						data: data,
						twitter_connect: true
					} );
				} else if( data.error == "E119" && data.site == "twitter" ) { //when log in for linking twitter, username/email with wrong pasword
					loginResponse( config.base_url + "/socialconnect.cms?site=twitter", "twitter_link_username_failure" );
					loginData.data = currLoginData;
				} else if( data.status == "signinfailure" && data.error == "T102" && data.site == "twitter" ) { //when twitter server is not responding
					loginResponse( config.base_url + "/socialconnect.cms?site=" + data.site, "twitter_failure_server_error" );
				} else if( data.error == "E103" && data.site == "twitter" ) { //when trying to create a new account using already existing id after twitter connect
					loginResponse( config.base_url + "/socialconnect.cms?site=" + data.site, "twitter_failure_already_exist", loginData ? loginData.data : null );
					loginData.data = currLoginData;
				} else if( data.error == "SSO_INVALID_RES_CHK_MAIL_AV" && data.site == "twitter" ) { //when trying to create a new account a@b.com after twitter connect
					loginResponse( config.base_url + "/socialconnect.cms?site=" + data.site, "twitter_failure_invalid_email" );
					loginData.data = currLoginData;
				} else {
					logger.warn( "Login case not handled" );
					loginResponse( config.base_url + "/loginview.cms?x=error&site=" + data.site, "unknown_error" );
				}
			}
			logger.log( "Checking user status in __sso" );
			mod_login.isLoggedIn( loginCallback );
		};
		mod_login.closeWindow = window.closeLoginWindow = function () {
			if( loginWindow ) {
				loginWindow.close();
				//setTimeout(function(){loginWindow.close();},500);
			}
		};
		mod_login.setLoginWindowDimension = window.setLoginWindowDimension = function ( width, height ) {
			if( loginWindow ) {
				loginWindow.resizeTo( width, height );
				if( loginWindow.reposition ) { //TODO not working for window.open - popup
					loginWindow.reposition();
				}
				loginWindow.focus();
			}
		};
		mod_login.login = function ( callback, action ) {
			loginCallback = function ( user ) {
				event.publish( "user.login", user );
				if( callback ) {
					callback( user );
				}
			};
			reset();
			var login_url = "https://jsso.indiatimes.com/sso/identity/login?channel=" + page.getChannel().toLowerCase() + (action?"&action="+action:"") + (config.login_return_url?"&ru="+encodeURIComponent(config.login_return_url):"");
			sso( login_url, callback );
		};
		mod_login.loginWithTwitter = function ( callback ) {
			loginCallback = function ( user ) {
				event.publish( "user.login", user );
				if( callback ) {
					callback( user );
				}
			};
			reset();
			var login_url = "https://jsso.indiatimes.com/sso/identity/login/socialLogin?channel=" + page.getChannel().toLowerCase() + "&oauthsiteid=twitter" + (config.login_return_url?"&open=popup&ru="+encodeURIComponent(config.login_return_url):"");
			sso( login_url, callback );
		};
		mod_login.loginWithFacebook = function ( callback ) {
			loginCallback = function ( user ) {
				event.publish( "user.login", user );
				if( callback ) {
					callback( user );
				}
			};
			reset();
			var login_url = "https://jsso.indiatimes.com/sso/identity/login/socialLogin?channel=" + page.getChannel().toLowerCase() + "&oauthsiteid=facebook" + (config.login_return_url?"&open=popup&ru="+encodeURIComponent(config.login_return_url):"");
			sso( login_url, callback );
		};
		mod_login.loginWithGoogle = function ( callback ) {
			loginCallback = function ( user ) {
				event.publish( "user.login", user );
				if( callback ) {
					callback( user );
				}
			};
			reset();
			var login_url = "https://jsso.indiatimes.com/sso/identity/login/socialLogin?channel=" + page.getChannel().toLowerCase() + "&oauthsiteid=googleplus" + (config.login_return_url?"&open=popup&ru="+encodeURIComponent(config.login_return_url):"");
			sso( login_url, callback );
		};
		mod_login.logout = function ( callback ) {
			loginCallback = function () {
				event.publish( "user.logout" );
				if( callback ) {
					callback();
				}
			};
			reset();
			var logout_url = "https://jsso.indiatimes.com/sso/identity/profile/logout/external?channel=" + page.getChannel().toLowerCase() + (config.logout_return_url?"&ru="+encodeURIComponent(config.logout_return_url):"");
			//            sso(logout_url,callback);
			var ifr = load.iframe( logout_url );
			$( ifr ).load( function () {
				$( ifr ).remove();
				mod_login.removeUser();
				if( window.__sso ) {
					window.__sso();
				}
			} );
			localstorage.remove( "sso_user" );
			var domain = page.getDomain();
			cookie.remove( "ssoid", "/", domain );
			cookie.remove( "Fbsecuritykey", "/", domain );
			cookie.remove( "fbookname", "/", domain );
			cookie.remove( "CommLogP", "/", domain );
			cookie.remove( "CommLogU", "/", domain );
			cookie.remove( "FaceBookEmail", "/", domain );
			cookie.remove( "Fbimage", "/", domain );
			cookie.remove( "fbooklocation", "/", domain );
			cookie.remove( "Fboauthid", "/", domain );
			cookie.remove( "fbname", "/", domain );
			cookie.remove( "fbLocation", "/", domain );
			cookie.remove( "fbimage", "/", domain );
			cookie.remove( "fbOAuthId", "/", domain );
			cookie.remove( "MSCSAuth", "/", domain );
			cookie.remove( "MSCSAuthDetail", "/", domain );
			cookie.remove( "MSCSAuthDetails", "/", domain );
			cookie.remove( "Twimage", "/", domain );
			cookie.remove( "TwitterUserName", "/", domain );
			cookie.remove( "Fboauthid", "/", domain );
			cookie.remove( "Twoauthid", "/", domain );
			cookie.remove( "Twsecuritykey", "/", domain );
			cookie.remove( "ssosigninsuccess", "/", domain );
			cookie.remove( 'ssoid' );
			cookie.remove( 'MSCSAuthDetail' );
			cookie.remove( 'articleid' );
			cookie.remove( 'txtmsg' );
			cookie.remove( 'tflocation' );
			cookie.remove( 'tfemail' );
			cookie.remove( 'setfocus' );
			cookie.remove( 'fbookname' );
			cookie.remove( 'CommLogP' );
			cookie.remove( 'CommLogU' );
			cookie.remove( 'FaceBookEmail' );
			cookie.remove( 'Fbimage' );
			cookie.remove( 'fbooklocation' );
			cookie.remove( 'Fboauthid' );
			cookie.remove( 'Fbsecuritykey' );
			cookie.remove( "fbname" );
			cookie.remove( "fbLocation" );
			cookie.remove( "fbimage" );
			cookie.remove( "fbOAuthId" );
			cookie.remove( 'MSCSAuth' );
			cookie.remove( 'MSCSAuthDetail' );
			cookie.remove( 'MSCSAuthDetails' );
			cookie.remove( 'ssosigninsuccess' );
			cookie.remove( 'Twimage' );
			cookie.remove( 'TwitterUserName' );
			cookie.remove( 'Twoauthid' );
			cookie.remove( 'Twsecuritykey' );
			//            mod_login.removeUser();
			//            if(window.__sso){
			//                window.__sso();
			//            }
		};
		var mod_login_config = {
			check_user_status: function ( params, callback ) {
				var ssoid = cookie.get( "ssoid" ) || cookie.get( "ssoId" );
				var MSCSAuthDetails = cookie.get( "MSCSAuthDetails" ); //TODO remove when new login is implemented
				if( !ssoid && MSCSAuthDetails ) {
					ssoid = MSCSAuthDetails.split( "=" )[ 1 ];
				}
				if( ssoid && ssoid.length > 0 ) {
					var isLoggedInUser = login.getUser();
					if( isLoggedInUser ) {
						if( callback ) {
							callback( isLoggedInUser );
						}
					} else {
						if(MSCSAuthDetails){//old cookie is set
							api.getUsersInfo({ssoids: ssoid}, function (user) {
								if (callback) {
									callback(user[0] || user);
								}
							});
						}else {
							// set older cookie
							ajax.getJSONP('https://jsso.indiatimes.com/sso/crossdomain/getTicket?version=v1', function (data) {
								if (data.ticketId != null && data.ticketId != undefined && data.ticketId.length > 0) {
									var socialappurl = 'http://socialappsintegrator.indiatimes.com/socialsite/v1validateTicket?ticketId=' + data.ticketId + '&channel=' + page.getChannel().toLowerCase();
									// set old cookies
									ajax.getJSONP(socialappurl, function (data1) {
										if(location.hostname.indexOf(".indiatimes.com") > -1){
											api.getUsersInfo({ssoids: ssoid}, function (user) {
												if (callback) {
													callback(user[0] || user);
												}
											});
										}else {
											var socialappcdurl = 'http://' + location.hostname + '/v1validateTicket?ticketId=' + data.ticketId + '&channel=' + page.getChannel().toLowerCase();
											ajax.getJSONP(socialappcdurl, function (data) {
										// get user info from mytimes
										api.getUsersInfo({ssoids: ssoid}, function (user) {
											if (callback) {
												callback(user[0] || user);
											}
										});
									});
										}
									});
								} else {
									if (callback) {
										callback(null);
									}
								}
							});
						}
					}
				} else {
					if( callback ) {
						callback( null );
					}
				}
			},
			check_user_status1: function ( params, callback ) {
				var isLoggedInUser = login.getUser();
				if( isLoggedInUser ) {
					if( callback ) {
						callback( isLoggedInUser );
					}
				} else {
					ajax.getJSONP( "https://jsso.indiatimes.com/sso/crossdomain/getTicket", params, function ( result ) {
						var user = null;
						if( result && result.ticketId ) {
							ajax.getJSONP( "lib/validateTicket.php", {
								ticketId: result.ticketId,
								siteId: page.getSiteId(),
								type: "JSON"
							}, function ( _user ) {
								//                            ajax.getJSONP("https://jsso.indiatimes.com/sso/crossdomain/validateTicket", {ticketId: result.ticketId, siteId: page.getSiteId(), type: "JSON"}, function (_user) {
								var emailIds = _user.emailId;
								if( is.string( emailIds ) ) {
									emailIds = emailIds.split( "," );
								}
								api.getUsersInfo( {
									ssoids: emailIds.join( "," )
								}, function ( user ) {
									api.getBadges( {
										uid: emailIds.join( "," )
									}, function ( badges ) {
										api.getRewards( {
											uid: emailIds.join( "," )
										}, function ( rewards ) {
											//                                logger.log("Got User Info: " + user[0].uid);
											//                                mod_login.setUser(user[0]);
											user[ 0 ].badges = badges.output && badges.output.userbadgehistory ? badges.output.userbadgehistory[ 0 ].userbadges.activityBadge : [];
											user[ 0 ].rewards = rewards && rewards.output && rewards.output.user ? rewards.output.user : null;
											if( callback ) {
												callback( user[ 0 ] );
											}
											//                                event.publish("user.status", user[0]);
										} );
									} );
								} );
							} );
						} else {
							if( callback ) {
								callback( user );
							}
						}
					} );
				}
			},
			mapping: {
				//to : from
				"uid": "uid",
				"email": "EMAIL", // map email
				"id": "_id",
				"name": "FL_N",
				"username": "D_N_U",
				"fullName": "FL_N",
				"firstName": "F_N",
				"lastName": "L_N",
				"icon": "tiny",
				"link": "profile",
				"CITY": "CITY",
				"thumb": "thumb",
				"followersCount": "F_C",
				"FE_C": "FE_C",
				"I_U_A": "I_U_A",
				"I_I_L": "I_I_L",
				"badges": "badges",
				"rewards": "rewards",
				"whatsonid": "W_ID"
			}
		};
		mod_login.renderPlugins = function ( user ) {
			user = user || mod_login.getUser();
			$(function(){
				if( user ) {
					$( "[data-plugin='user-isloggedin']" ).show();
					$( "[data-plugin='user-notloggedin']" ).hide();
					$( "[data-plugin='user-name']" ).text( user.getFirstName() );
					//                $("[data-plugin='user-icon']").attr("data-default", $("[data-plugin='user-icon']").attr("src"));//todo debug data-src, was not working, fix in html also
					$( "[data-plugin='user-icon']" ).attr( "src", user.getIcon() ); //todo debug data-src, was not working, fix in html also
					$( "[data-plugin='user-thumb']" ).attr( "src", user.getThumb() );
					api.getRewards( {
						//uid: user.getEmail()
						uid: user.getUid()
					}, function ( rewards ) {
						if( rewards && rewards.output && rewards.output.user && rewards.output.user.levelName ) {
							$( "[data-plugin='user-points']" ).text( ( rewards.output.user.statusPoints ) );
							$( "[data-plugin='user-level']" ).text( ( rewards.output.user.levelName ) );
							$( "[data-plugin='user-points-wrapper']" )
								.show()
								.addClass( "points_" + rewards.output.user.levelName.toLowerCase() );
						} else {
							$( "[data-plugin='user-points-wrapper']" ).hide();
						}
					} );
				} else {
					$( "[data-plugin='user-icon']" ).attr( "src", config.default_user_icon ); //todo debug data-src, was not working, fix in html also
					$( "[data-plugin='user-thumb']" ).attr( "src", config.default_user_icon );
					$( "[data-plugin='user-isloggedin']" ).hide();
					$( "[data-plugin='user-notloggedin']" ).show();
				}

				$( "body" ).toggleClass( "loggedin", !!user );
				$( "body" ).toggleClass( "notloggedin", !user );
			});
		};
		mod_login.updateConfig( mod_login_config );
		return mod_login;
	} );

define('jsrender',["jquery"],function($){
    //if(!$.fn.render){
        //throw new Error("Missing js: http://timesofindia.indiatimes.com/jsrender.cms" );

    //}
    return $.fn.render;
});
define( 'tiljs/plugin/lazy',[ "../event", "../ui", "module", "../util" ], function ( event, ui, module, util ) {
	var mod_lazy = {};
	var default_config = {
		skew: 1000,
		error_image: null
	};
	var config = util.extend( true, {}, default_config, module.config() );
	mod_lazy.init = function () {
		var tthis = this;
		tthis.load();
		event.subscribe( "window.scroll", function () { //todo this wont work
			tthis.load();
		} )
	};
	mod_lazy.load = function () {
		var tthis = this;
		var imgEle = $( "[data-src]" );
		imgEle.each( function ( i, imgEleC ) {
			if( ui.inView( imgEleC, true, config.skew ) ) {
				tthis.loadEach( imgEleC );
			}
		} );
	};
	mod_lazy.loadEach = function ( imgEleC ) {
		var imgEle$ = $( imgEleC );
		if( config.error_image ) {
			imgEle$.error( function () {
				$( this ).attr( 'src', config.error_image );
			} );
		}
		imgEle$.attr( "src", imgEle$.attr( "data-src" ) );
		imgEle$.removeAttr( "data-src" );
	};
	mod_lazy.init();
	return mod_lazy;
} );

define( 'tiljs/comments',[ "./util", "./ui", "jquery", "module", "./is", "./plugin/lazy", "./event" ], function ( util, ui, $, module, is, lazy, event ) {
	var default_config = {
		gravatar: false,
		wrapper: "#comment-section", //data-plugin=['comments'] //All comments inside this
		main: "#comments", //data-plugin=['comments'] //Comment will be added here
		comment: "[data-plugin='comment']", //Each comment
		tmpl: "comment_tmpl",
		count: "[data-plugin='comments-count']",
		post_button: "[data-plugin='comment-post']",
		comment_input: "[data-plugin='comment-input']",
		form: "[data-plugin='comment-form']",
		loadonscroll: true
	};
	var mod_comments = function ( config ) {
		this.config = util.extend( true, {}, config );
	};
	mod_comments.config = util.extend( true, {}, default_config, module.config() );
	var sample_comment = [
		{
			text: "Hello World",
			name: "Del Sanic",
			image: "http://www.gravatar.com/avatar/?d=identicon",
			uid: "123456",
			badge: [
				{
					name: "Silver"
				},
				{
					name: "Gold"
				}
            ]
        },
		{
			text: "Test Comment",
			name: "John",
			image: "http://www.gravatar.com/avatar/?d=identicon",
			uid: "123457",
			badge: [
				{
					name: "Silver"
				},
				{
					name: "Gold"
				}
            ]
        }
    ];
	mod_comments.getConfig = function () {
		return this.config;
	};
	mod_comments.prototype.getConfig = function () {
		return this.config;
	};
	mod_comments.prototype.initialize = function () {
		var tthis = this;
		//        tthis.isLoading=0;
		this.comments = [];
		this.getData( function ( data, commentCount ) {
			tthis.renderCommentCount( commentCount || ( data ? data.length : 0 ) );
			if( data && data.length > 0 ) {
				tthis.renderNoComment( false );
				tthis.render( data );
			} else {
				tthis.renderNoComment( true );
			}
		} );
		if( tthis.config.loadonscroll === true ) {
			( function ( tthis ) {
				event.subscribe( "window.scroll", function () { //todo this wont work
					tthis.loadNextPg();
				} )
			} )( tthis );
		}
	};
	mod_comments.prototype.getData = function ( callback ) {
		//        var comment = util.extend(true,{},sample_comment);
		//        ui.getGravatar();
		callback( sample_comment );
	};
	mod_comments.prototype.renderCommentCount = function ( count, type ) {
		$( "[data-plugin='comment-count']" ).text( count );
	};
	mod_comments.prototype.renderNoComment = function ( showOrHide ) {
		if( showOrHide ) {
			$( "[data-plugin='comment-none']" ).show();
		} else {
			$( "[data-plugin='comment-none']" ).hide();
		}
	};
	mod_comments.prototype.render = function ( data, callback, append ) {
		var tthis = this;
		if( data ) {
			var root = $( tthis.config.wrapper + " " +tthis.config.main );
			if( append == false || typeof append === "undefined" ) {
				root.empty();
				////            root.append(this.renderInput()) ;
			}
			//            if (this.config.tmpl) {
			//                this.renderUsingTemplate(0,data,this.config.tmpl);
			//            } else {
			util.each( data, function ( i, v ) {
				root.append( tthis.renderEach( i, v ) );
				if( callback ) {
					callback( data );
				}
			} );
			lazy.load();
			//            }
		}
	};
	mod_comments.prototype.renderEach = function ( index, dataOne ) {
		if( this.config.tmpl && $.fn.render ) {
			return this.renderUsingTemplate( index, dataOne, this.config.tmpl );
		} else {
			event.publish("logger.error","$.fn.render not defined. Render using jquery. this.config.tmpl=" + this.config.tmpl  + "&$.fn.render="+ $.fn.render );
			return this.renderUsingjQuery( index, dataOne );
		}
	};
	mod_comments.prototype.renderUsingTemplate = function ( index, dataOne, tmpl, prepend ) {
		return $( "#" + tmpl ).render( dataOne );
	};
	mod_comments.prototype.renderUsingjQuery = function ( index, dataOne ) {
		var li = $( "<li></li>" );
		li.text( dataOne.name + ": " + dataOne.text );
		return li;
	};
	mod_comments.prototype.loadingDiv = function ( text ) {
		$( "[data-plugin='comment-loading']" ).text( text );
	};
	//    mod_comments.prototype.isLoading = function () {
	//        return this.isLoading === 1;
	//    };
	mod_comments.prototype.loadNextPg = function () {
		var tthis = this;
		//        var config = tthis.getConfig();
		if( tthis.isLoading === 0 && ui.inView( tthis.config.main + " .comment:last-child", true, 1000 ) ) { //todo remove li from here
			tthis.loadComment( tthis.config.commentType, function ( data ) {}, true, ++tthis.config.curpg );
		}
	};
	mod_comments.prototype.loading = function ( progress ) {
		this.loadingDiv( "Loading..." /*+ " " + (progress?progress + "%":"")*/ );
		this.isLoading = 1;
	};
	mod_comments.prototype.loaded = function () {
		this.loadingDiv( "" );
		this.isLoading = 0;
		event.publish( "comments.loaded", this );
		if( this.config.loadonscroll == false ) {
			$( this.config.wrapper ).append( "<div class='loadmore'>View more comments</div>" );
		}
	};
	mod_comments.prototype.loadedAll = function () {
		this.loadingDiv( "Loaded all comments" );
		$( this.config.wrapper + ' .loadmore' ).hide(); //todo change to data plugin
		if( this.comments.length == 0 ) {
			event.publish( "comments.loaded.none", this );
		} else if( this.comments.length >= 0 ) {
			event.publish( "comments.loaded.all", this );
		}
		this.isLoading = 2;
	};
	return mod_comments;
} );

define( 'tiljs/plugin/plugin',[ "../load", "../util", /* "jquery",*/ "../event", "../ui", "../logger", "../is" ], function ( load, util, /*$,*/ event, ui, logger, is ) {
	var default_config = {
		init: true,
		root: "body",
		dependency: function ( callback ) {
			//this is config here
			this.dep_data = {};
			if( callback ) {
				callback();
			}
		}
	};
	var mod_plugin = function ( id, config ) {
		if( !id ) {
			throw new Error( "'id' param is required." );
		}
		this.id = id;
		this.updateConfig( config );
		//        console.log("done");
	};
	mod_plugin.prototype.updateConfig = function ( config ) {
		this.config = util.extend( true, {}, default_config, config );
	};
	mod_plugin.prototype.find = function () {
		return $( this.config.root ).find( "[data-plugin='" + this.id + "']" );
	};
	mod_plugin.prototype.init = function () {
		event.publish( getEventName( "beforeinit" ) );
		//        var plugins = opt.find();
		//        logger.log(opt.id + " init:" + plugins.length);
		//        if (plugins && plugins.length > 0) {
		//            event.publish(getEventName("beforeloadjs"));
		//            if (opt.js && opt.isJSdependent(plugins)) {
		//                load.js(opt.js, function (options, plugins) {
		//                    return function () {
		//                        event.publish(getEventName("afterloadjs"));
		//                        opt.initPlugins(plugins);
		//                    };
		//                }(opt, plugins), opt.js_id);
		//            } else {
		//                opt.initPlugins(plugins);
		//            }
		//        }
		//        console.log("init" );
		if( is.funct( this.config.dependency ) ) {
			var tthis = this;
			this.config.dependency( function () {
				tthis.render();
			} );
		} else {
			this.render();
		}
	};
	mod_plugin.prototype.render = function () {
		event.publish( getEventName( "beforerender" ) );
		var plugin = this;
		this.find().each( function ( i, ele ) {
			plugin.renderEach( ele );
		} );
		event.publish( getEventName( "afterrender" ) );
	};
	mod_plugin.prototype.renderEach = function ( ele ) {
		//$(ele).html("yoo hoo");
		//Override this
	};
	var getEventName = function ( eventName ) {
		return "plugin." + this.id + eventName; //todo replace with ===> "plugin." + this.id + "." + eventName;
	};
	return mod_plugin;
} );

define( 'tiljs/plugin/dynamic',[ "../timer", "../util", "../plugin/plugin" ], function ( timer, util, plugin ) {
	var mod_uptime = new plugin( "dynamic-uptime" );
	mod_uptime.uptime = function () {
		var plugins = $( "[data-plugin='dynamic-uptime']" );
		plugins.each( function ( i, p ) {
			mod_uptime.renderEach( p );
		} );
	};
	mod_uptime.renderEach = function ( ele ) {
		//        var plugins = $("[data-plugin='dynamic-uptime']");
		//        plugins.each(function(i,p){
		var plugin = $( ele );
		var time = plugin.attr( "data-time" );
		var elapsedTime = timer.elapsedTime( time, {
			minute: "min",
			second: "sec"
		}, true );
		plugin.html( elapsedTime );
		//        });
	};
	timer.every( 60000 /*minute*/ , function () {
		mod_uptime.render();
	} );
	mod_uptime.init();
	return mod_uptime;
} );

define( 'tiljs/compatibility',[ "jquery" ], function ( $ ) {
	var mod_compatibility = {};
	//This function is not available in IE8 so implementing it.
	if( !Array.prototype.indexOf ) {
		Array.prototype.indexOf = mod_compatibility.Array_indexOf = function ( searchElement, fromIndex ) {
			if( this === undefined || this === null ) {
				throw new TypeError( '"this" is null or not defined' );
			}
			var length = this.length >>> 0; // Hack to convert object.length to a UInt32
			fromIndex = +fromIndex || 0;
			if( Math.abs( fromIndex ) === Infinity ) {
				fromIndex = 0;
			}
			if( fromIndex < 0 ) {
				fromIndex += length;
				if( fromIndex < 0 ) {
					fromIndex = 0;
				}
			}
			for( ; fromIndex < length; fromIndex++ ) {
				if( this[ fromIndex ] === searchElement ) {
					return fromIndex;
				}
			}
			return -1;
		};
	}
	//This function is not available in IE8 so implementing it.
	if( !String.prototype.trim ) {
		String.prototype.trim = function () {
			return this.replace( /^\s+|\s+$/gm, '' );
		};
	}
	//Utility function
	if( !String.prototype.splice ) {
		String.prototype.splice = function ( idx, rem, s ) {
			return( this.slice( 0, idx ) + s + this.slice( idx + Math.abs( rem ) ) );
		};
	}
	//Placeholders do not work in old browsers specially IE
	$( function () {
		//start added by Amit
		$( 'body' ).on( 'focus', '[placeholder]', function () {
			var input = $( this );
			if( input.val() === input.attr( 'placeholder' ) ) {
				input.val( '' );
				input.removeClass( 'placeholder' );
			}
		} );
		$( 'body' ).on( 'blur', '[placeholder]', function () {
			var input = $( this );
			if( input.val() === '' || input.val() === input.attr( 'placeholder' ) ) {
				input.addClass( 'placeholder' );
				input.val( input.attr( 'placeholder' ) );
			}
		} );
//		$( '[placeholder]' ).focus();
		$( '[placeholder]' ).blur();
		//        $('[placeholder]').parents('form').submit(function() {
		//            $(this).find('[placeholder]').each(function() {
		//                var input = $(this);
		//                if (input.val() == input.attr('placeholder')) {
		//                    input.val('');
		//                }
		//            })
		//        });
		//todo move to comments
		//        $('body').on('mouseenter','.comment-box', function() {
		//            var obj = $(this).find("[data-plugin='comment-user-follow_wrapper']");
		//            if(!obj.hasClass('dont_show')){
		//                obj.show();
		//            }
		//        });
		//        $('body').on('mouseleave','.comment-box', function() {
		//            var obj = $(this);
		//            obj.find("[data-plugin='comment-user-follow_wrapper']").hide();
		//        });
		//end added by Amit
	} );
	return mod_compatibility;
} );

define( 'tiljs/social/social',[ "../load", "../util", "jquery", "../event", "../ui", "../logger" ], function ( load, util, $, event, ui, logger ) {
	//    function SocialPlugin(id, social_parent) {
	//        var tthis = this;
	//
	//        this.id = id;
	//        this.social_id = social_parent.id;
	//        this.social_parent = social_parent;
	//
	//        var registerEvents = function () {
	//            var pluginName = tthis.getName();
	//            util.each(this.events, function (eventName, eventCallback) {
	//                $("body").on(eventName, "[data-plugin='" + pluginName + "']", function(e){
	//                    if(eventCallback){
	//                        eventCallback.call(this,e);
	//                    }
	//                    e.stopImmediatePropagation();
	//                    return false;
	//                });
	//            });
	//        };
	//
	//        this.readOptions = function (ele) {
	//            return util.data(ele);
	//        };
	//
	//        this.getName = function () {
	//            return this.social_id + "-" + this.id;
	//        };
	//
	//        this.init=function(){
	//
	//        };
	//
	//        this.preAttachPluginEvent = function (plugins) {
	//            var pluginInstance = plugins.filter("[data-plugin='" + this.getName() + "']");
	//            pluginInstance.each(function (kk, vv) {
	//                if (tthis.init) {
	//                    tthis.init(vv, tthis);
	//                }
	//            });
	//        };
	//    }
	var mod_social = function ( id ) {
		this.id = id;
		this.config = {
			js: null,
			js_id: null
		};
		this.config = function ( config ) {
			this.config = util.extend( true, {}, this.config, config );
			this.js = config.js;
			this.js_id = config.js_id;
		};
		/**
		 * @deprecated use this.config
		 * @param js
		 * @param js_id
		 */
		this.setJS = function ( js, js_id ) {
			this.js = js;
			this.js_id = js_id;
		};
		this.plugins = {};
		this._getPluginID = function ( id ) {
			return opt.id + "-" + id;
		};
		this.addPlugin = function ( plugin_options ) {
			opt.plugins[ this._getPluginID( plugin_options.id ) ] = plugin_options;
			if( plugin_options.alias ) {
				for( var i = 0; i < plugin_options.alias.length; i++ ) {
					var alias_options = util.extend( true, {}, plugin_options, {
						id: plugin_options.alias[ i ]
					} );
					delete alias_options.alias;
					this.addPlugin( alias_options );
				}
			}
		};
		this.getPlugin = function ( id ) {
			return opt.plugins[ this._getPluginID( id ) ];
		};
		var opt = this;
		this.beforeinit = function () {};
		this.afterloadjs = function () {};
		this.beforeloadjs = function () {};
		this.init = function ( config ) {
			if( opt.beforeinit ) {
				opt.beforeinit();
			}
			var plugins = opt.find();
			logger.log( opt.id + " init:" + plugins.length );
			if( plugins && plugins.length > 0 || config.parse === true ) {
				if( opt.beforeloadjs ) {
					opt.beforeloadjs();
				}
				//                console.log(opt.js_id + ":" + opt.js_id);
				if( opt.js && ( opt.isJSdependent( plugins ) || config.parse === true ) ) {
					load.js( opt.js, function ( options, plugins ) {
						return function () {
							if( opt.afterloadjs ) {
								opt.afterloadjs();
							}
							opt.initPlugins( plugins );
						};
					}( opt, plugins ), opt.js_id );
				} else {
					opt.initPlugins( plugins );
				}
			}
		};
		this.find = function () {
			return $( "[data-plugin^='" + this.id + "-']" );
		};
		this.isJSdependent = function ( plugins ) {
			for( var i = 0; i < plugins.length; i++ ) {
				var plugin = plugins[ i ];
				var pluginName = $( plugin ).attr( "data-plugin" );
				if( opt.plugins[ pluginName ] && opt.plugins[ pluginName ].js ) {
					return true;
				}
			}
			return false;
		};
		this.initPlugins = function ( plugins ) {
			this.registerPluginEvents();
			//            this.initPlugins(plugins);
		};
		this.registerPluginEvents = function () {
			util.each( opt.plugins, function ( pluginName, plugin ) {
				util.each( plugin.events, function ( eventName, eventCallback ) {
					//                        console.log("EVENT:"+pluginName+":"+eventName);
					$( "body" ).off( eventName, "[data-plugin='" + pluginName + "']" );
					$( "body" ).on( eventName, "[data-plugin='" + pluginName + "']", function ( e ) {
						if( eventCallback ) {
							try {
								event.publish( "social.action", {
									plugin: pluginName,
									event: eventName,
									element: this,
									dom_event: e
								} );
								eventCallback.call( this, e );
							} catch( e ) {
								event.publish( "logger.error", e.stack );
							}
						}
						e.stopImmediatePropagation();
						return false;
					} );
				} );
				var pluginInstance = $( "body" ).find( "[data-plugin='" + pluginName + "']" );
				pluginInstance.each( function ( kk, vv ) {
					if( plugin.init ) {
						try {
							plugin.init( vv, plugin );
						} catch( e ) {
							event.publish( "logger.error", e.stack );
						}
					}
				} );
			} );
		};
		this.preAttachPluginEvent = function ( plugins ) {
			//            console.log(opt.plugins);
			var plugincount = 0;
			util.each( opt.plugins, function ( k, v ) {
				var pluginInstance = plugins.filter( "[data-plugin='" + k + "']" );
				pluginInstance.each( function ( kk, vv ) {
					plugincount++;
					if( v.init ) {
						v.init( vv, v );
						$( vv ).css( "border", "2px solid red" );
					}
				} );
			} );
			logger.log( opt.id + " found plugin : " + plugincount + "/" + plugins.length );
		};
		this.renderPlugin = function ( ele, default_params, main_ele ) {
			var element = $( ele );
			var data = util.data( ele );
			var params = util.extend( true, {}, default_params, data );
			params.href = data.url || data.href || element.attr( "href" ) || params.href;
			var pluginEle = $( main_ele );
			//Using this because data method in jquery does not append dom element
			$.each( params, function ( name, value ) {
				pluginEle.attr( "data-" + name, value );
			} );
			element.empty();
			element.append( pluginEle );
			this.parse( ele );
		};
		this.getAbsoluteUrl = function ( url ) {
			var resolvedUrl = ( url && url.length > 0 && url !== "#" ) ? url : document.location.href;
			resolvedUrl = resolvedUrl.split( "#" )[ 0 ]; // remove #
			if( resolvedUrl && resolvedUrl.length > 2 && resolvedUrl[ 0 ] === "/" && resolvedUrl[ 1 ] !== "/" ) { //URL is relative, make it absolute
				resolvedUrl = location.protocol + "//" + location.host + resolvedUrl;
			}
			return resolvedUrl;
		};
		///Helper Methods
		this._share = function ( url, params, options, element ) {
			event.publish( "social.onBeforeShare", {
				params: params,
				element: element,
				network: this.id
			} );
			var _url = this.getAbsoluteUrl( url );
			var name = "social_share_" + new Date().getTime();
			var _options = $.extend( {
				name: name,
				width: 700,
				height: 400
			}, options );
			var popup = ui.window( _url + "?" + $.param( params ), _options );
			if( popup ) {
				popup.moveTo( 275, 275 );
			}
		};
	};
	mod_social.onBeforeShare = function ( callback ) {
		event.subscribe( "social.onBeforeShare", callback );
	};
	return mod_social;
} );

define( 'tiljs/social/facebook',[ "../social/social", "../ui", "../logger", "../event", "../util", "module", "jquery", "../ajax", "../is" ], function ( social, ui, logger, event, util, module, $, ajax, is ) {
	var default_config = {
		parse: false,
		init: true,
		appid: null,
		js: "//connect.facebook.net/en_US/sdk.js",
		//        js: "//connect.facebook.net/en_US/all.js",
		js_id: "facebook-jssdk",
		load_js: false,
		share_url: "http://www.facebook.com/sharer.php",
		oauth: false,
		status: false,
		xfbml: false,
		fb_version: 'v2.0',
		share_params: function ( url, title, summary, image ) {
			return {
				"url[url]": url
			}
		}
	};
	var config = util.extend( true, {}, default_config, module.config() );
	var mod_facebook = new social( "facebook", "facebook" );
	mod_facebook.setJS( config.js, config.js_id );
	var __isJSdependent = mod_facebook.isJSdependent;
	mod_facebook.isJSdependent = function ( plugins ) {
		return config.load_js || __isJSdependent( plugins );
	};
	mod_facebook.addPlugin( {
		id: "like",
		js: true,
		init: function ( ele, plugin ) {
			var element = $( ele );
			var data = util.data( ele );
			var default_params = {
				href: location.href,
				layout: "button_count", //standard , box_count, button_count, button
				"show-faces": "false", //false, true
				width: "100", //integer
				action: "like", //like , recommend,
				share: false //true, false
			};
			var params = util.extend( true, {}, default_params, data );
			params.href = data.url || data.href || element.attr( "href" ) || params.href;
			var fb_like = $( "<div class='fb-like'></div>" );
			$.each( params, function ( name, value ) {
				//Using this because data method in jquery does not append dom element
				fb_like.attr( "data-" + name, value );
			} );
			element.empty();
			element.append( fb_like );
			mod_facebook.parse( ele );
		}
	} );
	mod_facebook.addPlugin( {
		id: "comments",
		js: true,
		init: function ( ele, plugin ) {
			var element = $( ele );
			var data = util.data( ele );
			var default_params = {
				href: location.href,
				colorscheme: "light", //light, dark
				numposts: 10 //10
				//                width : "100"            //integer
			};
			var params = util.extend( true, {}, default_params, data );
			params.href = data.url || data.href || element.attr( "href" ) || params.href;
			var fb_comments = $( "<div class='fb-comments'></div>" );
			//Using this because data method in jquery does not append dom element
			$.each( params, function ( name, value ) {
				fb_comments.attr( "data-" + name, value );
			} );
			element.empty();
			element.append( fb_comments );
			mod_facebook.parse( element[ 0 ] );
			//            FB.xfbml.parse(ele);
		}
	} );
	mod_facebook.addPlugin( {
		id: "share",
		alias: [ "button" ],
		events: {
			click: function ( e ) {
				var data = util.data( this );
				mod_facebook.share( data.url || $( this ).attr( "href" ), data.title, data.summary, data.image, this );
			}
		}
	} );
	mod_facebook.share = function ( url, title, summary, image, element ) {
		if( false && window.FB && config.appid ) { //disabling this because proper app id not available
			FB.ui( {
				method: 'feed',
				display: 'popup',
				link: mod_facebook.getAbsoluteUrl( url ),
				picture: mod_facebook.getAbsoluteUrl( image ), //check picture should be correct
				name: title,
				//             caption: summary,
				description: summary
			}, function ( response ) {
				//                alert(response);
			} );
		} else {
			var shareParam = {
				u: mod_facebook.getAbsoluteUrl( url ),
				display: 'popup',
				sdk: 'joey'
				//                    'p[url]': mod_facebook.getAbsoluteUrl(url),
				//                    'p[title]': title,
				//                    'p[summary]': summary,
				//                    'p[image][0]': mod_facebook.getAbsoluteUrl(image)
			};
			if( !is.empty( config.appid ) ) {
				shareParam.app_id = config.appid;
			}
			mod_facebook._share(
				config.share_url, shareParam, {
					name: 'facebook_share_dialog',
					width: 626,
					height: 436
				}, element
			);
		}
	};
	mod_facebook.addPlugin( {
		id: "follow",
		events: {
			click: function ( e ) {
				mod_facebook.follow( $( this ).attr( "data-url" ) || $( this ).attr( "href" ) );
			}
		},
		init: function ( ele, plugin ) {}
	} );
	mod_facebook.follow = function ( url, options ) {
		var _url = mod_facebook.getAbsoluteUrl( url );
		var win = window.open( url, "_blank" );
		win.focus();
	};
	mod_facebook.addPlugin( {
		id: "login",
		js: true,
		events: {
			click: function ( e ) {
				mod_facebook.login();
			}
		}
	} );
	var __perms = null;
	mod_facebook.parse = function ( ele ) {
		if( typeof FB !== "undefined" ) {
			FB.XFBML.parse( ele );
		} else {
			logger.warn( "'FB' is required in parse" );
		}
	};
	mod_facebook.getGrantedPermissions = function ( callback ) {
		if( typeof FB !== "undefined" ) {
			mod_facebook.api( "/me/permissions", function ( resp ) {
				if( callback ) {
					callback( resp );
				}
			} );
		} else {
			logger.warn( "'FB' is required in login" );
		}
	};
	mod_facebook.hasPermissions = function ( permissions, callback ) {
		if( typeof FB !== "undefined" ) {
			mod_facebook.api( "/me/permissions", function ( resp ) {
				if( resp && resp.data && resp.data.length > 0 ) {
					var perms = typeof permissions === "string" ? permissions.split( "," ) : ( permissions instanceof Array ? permissions : [] );
					for( var i = 0; i < perms.length; i++ ) {
						var perm = resp.data[ 0 ];
						if( perm.hasOwnProperty( perms[ i ] ) ) {
							if( callback ) {
								callback( true );
								return null;
							}
						}
					}
				}
				if( callback ) {
					callback( false, resp && resp.error ? resp.error : null );
				}
			} );
		} else {
			logger.warn( "'FB' is required in login" );
		}
	};
	mod_facebook.getPermissions = function ( permissions, callback ) {
		if( typeof FB !== "undefined" ) {
			var perms = typeof permissions === "string" ? permissions : ( permissions instanceof Array ? permissions.join( "," ) : "" );
			mod_facebook.hasPermissions( permissions, function ( resp ) {
				if( resp === true ) {
					if( callback ) {
						callback( resp );
					}
				} else {
					mod_facebook.login( permissions, function () {
						mod_facebook.hasPermissions( permissions, function ( resp ) {
							if( callback ) {
								callback( resp );
							}
						} );
					} );
				}
			} );
		} else {
			logger.warn( "'FB' is required in login" );
		}
	};
	mod_facebook.login = function ( permissions, callback ) {
		if( typeof FB !== "undefined" ) {
			logger.log( permissions );
			var perms = typeof permissions === "string" ? permissions : ( permissions instanceof Array ? permissions.join( "," ) : "" );
			if( __perms != perms ) {
				//                __perms = perms;
				FB.login( callback, {
					scope: perms
				} ); //'email,user_likes'
			}
		} else {
			logger.warn( "'FB' is required in login" );
		}
	};
	mod_facebook.addPlugin( {
		id: "logout",
		events: {
			click: function ( e ) {
				mod_facebook.logout();
			}
		}
	} );
	mod_facebook.logout = function ( callback ) {
		if( typeof FB !== "undefined" ) {
			FB.logout( callback )
		} else {
			logger.warn( "'FB' is required in logout" );
		}
	};
	mod_facebook._createFBroot = function () {
		//Create fb-root div.
		var id = "fb-root";
		var div = $( "#" + id );
		if( !div || div.length == 0 ) {
			div = $( "<div></div>" );
			div.attr( "id", id );
			div.css( "display", "none" );
			$( "body" ).append( div );
		}
		return div;
	};
	mod_facebook.beforeinit = function () {
		if( !config.appid ) {
			logger.warn( "'config.appid' is Required" );
		}
	};
	mod_facebook.beforeloadjs = function () {
		mod_facebook._createFBroot();
	};
	mod_facebook.afterloadjs = function () {
		if( typeof FB !== "undefined" ) {
			logger.log( "FB js loaded" );
			if( config.init === true ) {
				FB.init( {
					appId: config.appid,
					oauth: config.oauth,
					status: config.status,
					cookie: true,
					xfbml: config.xfbml,
					version: config.fb_version
				} );
			}
			event.publish( "FB.onload", FB );

			FB.Event.subscribe( 'edge.remove', function ( response ) {
				event.publish( "FB.edge.remove", response );
			} );

			FB.Event.subscribe( 'edge.create', function ( response ) {
				event.publish( "FB.edge.create", response );
			} );

			FB.Event.subscribe( 'comment.create', function ( response ) {
				event.publish( "FB.comment.create", response );
			} );
			FB.Event.subscribe( 'auth.authResponseChange', function ( response ) {
				event.publish( "FB.auth.authResponseChange", response );
				if( response.status === 'connected' ) {
					// the user is logged in and has authenticated your
					// app, and response.authResponse supplies
					// the user's ID, a valid access token, a signed
					// request, and the time the access token
					// and signed request each expire
					var uid = response.authResponse.userID;
					var accessToken = response.authResponse.accessToken;
					event.publish( "FB.auth.authResponseChange.connected", response );
					event.publish( "FB.connected", FB );
				} else if( response.status === 'not_authorized' ) {
					// the user is logged in to Facebook,
					// but has not authenticated your app
					event.publish( "FB.auth.authResponseChange.not_authorised", response );
				} else {
					// the user isn't logged in to Facebook.
					event.publish( "FB.auth.authResponseChange.failed", response );
				}
			} );
		} else {
			logger.warn( "'FB' is required in afterloadjs" );
		}
	};
	var currentUser = null;
	mod_facebook.getUser = function ( callback ) {
		if( typeof FB !== "undefined" && !currentUser ) {
			FB.api( '/me', function ( response ) { //todo use mod_facebook.api
				if( callback ) {
					currentUser = response;
					callback( response ); //response is the basic user object
				}
			} );
		} else {
			if( callback ) {
				callback( currentUser );
			}
		}
	};
	mod_facebook.onlogin = function ( callback ) {
		event.subscribe( "FB.auth.authResponseChange.connected", function ( resp ) {
			mod_facebook.getUser( function ( user ) {
				if( callback ) {
					callback( user, resp );
				}
			} )
		} );
	};
	mod_facebook.oncomment = function ( callback ) {
		event.subscribe( "FB.comment.create", function ( resp ) {
			mod_facebook.getUser( function ( user ) {
				if( callback ) {
					callback( user, resp );
				}
			} )
		} );
	};
	mod_facebook.onlogout = function ( callback ) {
		event.subscribe( [ "FB.auth.authResponseChange.not_authorised", "FB.auth.authResponseChange.failed" ], function ( resp ) {
			if( callback ) {
				currentUser = null;
				callback( resp );
			}
		} );
	};
	mod_facebook.fql = function ( query, callback ) {
		if( typeof FB !== "undefined" ) {
			logger.log( query );
			FB.api( { //todo use mod_facebook.api
				method: 'fql.query',
				query: query
			}, callback );
		} else {
			logger.warn( "'FB' is required in fql" );
		}
	};
	//select name from page where page_id in (SELECT page_id FROM page WHERE page_id IN (SELECT uid, page_id, type FROM page_fan WHERE uid=me()) AND type='city')
	mod_facebook.getLikes = function ( callback ) {
		if( typeof FB !== "undefined" ) {
			FB.api( '/me/likes', /*{'limit': '5'},*/ function ( response ) { //todo use mod_facebook.api
				if( callback ) {
					currentUser = response;
					callback( response ); //response is the basic user object
				}
			} );
		}
	};
	mod_facebook.getLikesByCategory = function ( categories, callback ) {
		if( categories.length > 0 && is.string( categories ) ) {
			categories = categories.split( "," );
		}
		var catArr = [];
		if( categories.length > 0 ) {
			util.each( categories, function ( i, v ) {
				catArr.push( "type='" + v + "'" );
			} );
		}
		var catStr = catArr.length > 0 ? "AND (" + catArr.join( " OR " ) + ")" : "";
		mod_facebook.getUser( function ( user ) {
			if( user ) {
				mod_facebook.fql( "select name from page where page_id in (SELECT page_id FROM page WHERE page_id IN (SELECT uid, page_id, type FROM page_fan WHERE uid=me()) " + catStr + ")", function ( data ) {
					if( callback ) {
						callback( data );
					}
				} );
			} else {
				logger.warn( "User is not logged in. Cannot run facebook.getLikesByCategory." );
			}
		} );
	};
	/**
     * Paging Example :

     var facebook = require("social/facebook")
     facebook.getLikes(function(data){
            if(data){
                console.log(data);
                facebook.paging(data,arguments.callee); //This will recursively call facebook
            }
        });

     * @param data
     * @param callback
     * @returns {*}
     */
	mod_facebook.paging = mod_facebook.pageNext = function ( data, callback ) {
		//{"cursors":{"after":"MjY4ODE3MDI2NDgyMDU5","before":"MTYwMjI3OTE0MDM3NzU0"},
		// "next":"https://graph.facebook.com/100006612690893/likes?access_token=CAABrIHwVZA2GUh3ryguvIzDAHbM1SR0iHYnLHo2hZAp1gZDZD&limit=25&after=MjY4ODE3MDI2NDgyMDU5"}
		if( data && data.paging && data.paging.next ) {
			return ajax.getJSONP( data.paging.next, callback );
		} else {
			callback( null );
		}
	};
	mod_facebook.pagePrev = function ( data, callback ) {
		//{"cursors":{"after":"MjY4ODE3MDI2NDgyMDU5","before":"MTYwMjI3OTE0MDM3NzU0"},
		// "next":"https://graph.facebook.com/100006612690893/likes?access_token=CAABrIHwVZA2GUh3ryguvIzDAHbM1SR0iHYnLHo2hZAp1gZDZD&limit=25&after=MjY4ODE3MDI2NDgyMDU5"}
		if( data && data.paging && data.paging.previous ) {
			return ajax.getJSONP( data.paging.previous, callback );
		} else {
			callback( null );
		}
	};
	mod_facebook.getRegisteredUsersTxt = function ( callback ) {
		mod_facebook.getRegisteredUsers( function ( users ) {
			if( callback ) {
				if( users.length == 1 ) {
					callback( users[ 0 ].name + " has planned his/her trips on HappyTrips.com.", users );
				} else if( users.length > 1 ) {
					callback( users.length + " of your friends have planned their trips on HappyTrips.com", users );
				} else {
					callback( "None of your friends have planned their trips on HappyTrips.com.", users );
				}
			}
		} )
	};
	//    mod_facebook.getRegisteredUsersTxtImg = function (callback, count, link) {
	//        mod_facebook.getRegisteredUsersTxt(function (text, users) {
	//            if (users && users.length > 0) {
	//                var imgEle = $("<span></span>");
	//
	//                var maxImg = 10;
	//
	//                imgEle.empty();
	//                for (var i = 0; i < users.length && i < maxImg; i++) {
	//                    var user = users[i];
	//                    if (link === true) {
	//                        imgEle.append($('<a href="https://www.facebook.com/' + user.uid + '" target="_blank"><img src="' + user.pic_square_with_logo + '" alt="' + user.name + '" title="' + user.name + '" /></a>'));
	//                    } else {
	//                        imgEle.append($('<img src="' + user.pic_square_with_logo + '" alt="' + user.name + '" title="' + user.name + '" />'));
	//                    }
	//
	//                }
	//                if(!count || count === true){
	//                    imgEle.append($('<a href="https://www.facebook.com/' + user.uid + '" class="fbImgCount" target="_blank">+' + ((users.length) - i) + '</a>'));
	//                }
	//            }
	//        });
	//    };
	/**
	 * N users have registered on APP_ID
	 * @param callback
	 */
	mod_facebook.getRegisteredUsers = function ( callback ) {
		mod_facebook.fql( 'SELECT uid,name,pic_square_with_logo FROM user WHERE is_app_user AND uid IN (SELECT uid2 FROM friend WHERE uid1 = me())', callback );
	};
	mod_facebook.api = function () {
		var query = arguments[ 0 ];
		var method = typeof arguments[ 1 ] === "string" ? arguments[ 1 ] : "GET";
		var params = typeof arguments[ 2 ] === "object" ? arguments[ 2 ] : {};
		var callback = typeof arguments[ 1 ] === "function" ? arguments[ 1 ] : ( typeof arguments[ 2 ] === "function" ? arguments[ 2 ] : ( typeof arguments[ 3 ] === "function" ? arguments[ 3 ] : null ) );
		/*
         'since':'last week',
         'limit': '10',
         'offset': '20',
         'until': 'yesterday'
         */
		if( typeof FB !== "undefined" ) {
			logger.debug( query );
			FB.api( query, method, params, function ( response ) {
				if( callback ) {
					callback( response );
				}
			} );
		} else {
			logger.warn( "'FB' is required in api" );
		}
	};
	mod_facebook.getCheckins = function ( callback ) {
		mod_facebook.api( "/me/locations", callback );
	};
	mod_facebook.post = function ( message, callback ) {
		var _message = typeof arguments[ 0 ] === "string" ? {
			message: arguments[ 0 ]
		} : arguments[ 0 ];
		mod_facebook.login( "publish_stream", function () {
			mod_facebook.api( "/me/feed", "POST", _message, callback );
		} );
	};
	/**SELECT page_id,name
     FROM place
     WHERE distance(latitude, longitude, "28.6410126613", "77.2408139523") < 50000
     ORDER BY distance(latitude, longitude, "28.6410126613", "77.2408139523")
     LIMIT 10*/
	mod_facebook.checkin = function ( message, placeid, callback ) {
		mod_facebook.post( {
			message: message,
			place: placeid
		}, callback );
	};
	mod_facebook.init( config );
	return mod_facebook;
} );

define( 'tiljs/ext/date',[], function () {
	/*
	 * @version  0.5.0
	 * @author   Lauri Rooden - https://github.com/litejs/date-format-lite
	 * @license  MIT License  - http://lauri.rooden.ee/mit-license.txt
	 */
	! function ( Date, proto ) {
		var maskRe = /(["'])((?:[^\\]|\\.)*?)\1|YYYY|([MD])\3\3(\3?)|SS|([YMDHhmsW])(\5?)|[uUAZSwo]/g,
			yearFirstRe = /(\d{4})[-.\/](\d\d?)[-.\/](\d\d?)/,
			dateFirstRe = /(\d\d?)[-.\/](\d\d?)[-.\/](\d{4})/,
			timeRe = /(\d\d?):(\d\d):?(\d\d)?\.?(\d{3})?(?:\s*(?:(a)|(p))\.?m\.?)?(\s*(?:Z|GMT|UTC)?(?:([-+]\d\d):?(\d\d)?)?)?/i,
			wordRe = /.[a-z]+/g,
			unescapeRe = /\\(.)/g
			//, isoDateRe = /(\d{4})[-.\/]W(\d\d?)[-.\/](\d)/
			// ISO 8601 specifies numeric representations of date and time.
			//
			// The international standard date notation is
			// YYYY-MM-DD
			//
			// The international standard notation for the time of day is
			// hh:mm:ss
			//
			// Time zone
			//
			// The strings +hh:mm, +hhmm, or +hh (ahead of UTC)
			// -hh:mm, -hhmm, or -hh (time zones west of the zero meridian, which are behind UTC)
			//
			// 12:00Z = 13:00+01:00 = 0700-0500
		Date[ proto ].format = function ( mask ) {
			mask = Date.masks[ mask ] || mask || Date.masks[ "default" ]
			var self = this,
				get = "get" + ( mask.slice( 0, 4 ) == "UTC:" ? ( mask = mask.slice( 4 ), "UTC" ) : "" )
			return mask.replace( maskRe, function ( match, quote, text, MD, MD4, single, pad ) {
				text = single == "Y" ? self[ get + "FullYear" ]() % 100 : match == "YYYY" ? self[ get + "FullYear" ]() : single == "M" ? self[ get + "Month" ]() + 1 : MD == "M" ? Date.monthNames[ self[ get + "Month" ]() + ( MD4 ? 12 : 0 ) ] : single == "D" ? self[ get + "Date" ]() : MD == "D" ? Date.dayNames[ self[ get + "Day" ]() + ( MD4 ? 7 : 0 ) ] : single == "H" ? self[ get + "Hours" ]() % 12 || 12 : single == "h" ? self[ get + "Hours" ]() : single == "m" ? self[ get + "Minutes" ]() : single == "s" ? self[ get + "Seconds" ]() : match == "S" ? self[ get + "Milliseconds" ]() : match == "SS" ? ( quote = self[ get + "Milliseconds" ](), quote > 99 ? quote : ( quote > 9 ? "0" : "00" ) + quote ) : match == "u" ? ( self / 1000 ) >>> 0 : match == "U" ? +self : match == "A" ? Date[ self[ get + "Hours" ]() > 11 ? "pm" : "am" ] : match == "Z" ? "GMT " + ( -self.getTimezoneOffset() / 60 ) : match == "w" ? self[ get + "Day" ]() || 7 : single == "W" ? ( quote = new Date( +self + ( ( 4 - ( self[ get + "Day" ]() || 7 ) ) * 86400000 ) ), Math.ceil( ( ( quote.getTime() - quote[ "s" + get.slice( 1 ) + "Month" ]( 0, 1 ) ) / 86400000 + 1 ) / 7 ) ) : match == "o" ? new Date( +self + ( ( 4 - ( self[ get + "Day" ]() || 7 ) ) * 86400000 ) )[ get + "FullYear" ]() : quote ? text.replace( unescapeRe, "$1" ) : match
				return pad && text < 10 ? "0" + text : text
			} )
		}
		Date.am = "AM"
		Date.pm = "PM"
		Date.masks = {
			"default": "DDD MMM DD YYYY hh:mm:ss",
			"isoUtcDateTime": 'UTC:YYYY-MM-DD"T"hh:mm:ss"Z"'
		}
		Date.monthNames = "JanFebMarAprMayJunJulAugSepOctNovDecJanuaryFebruaryMarchAprilMayJuneJulyAugustSeptemberOctoberNovemberDecember".match( wordRe )
		Date.dayNames = "SunMonTueWedThuFriSatSundayMondayTuesdayWednesdayThursdayFridaySaturday".match( wordRe )
		//*/
		/*
		 * // In Chrome Date.parse("01.02.2001") is Jan
		 * n = +self || Date.parse(self) || ""+self;
		 */
		String[ proto ].date = Number[ proto ].date = function ( format ) {
			var m, temp, d = new Date,
				n = +this || "" + this
			if( isNaN( n ) ) {
				// Big endian date, starting with the year, eg. 2011-01-31
				if( m = n.match( yearFirstRe ) ) d.setFullYear( m[ 1 ], m[ 2 ] - 1, m[ 3 ] )
				else if( m = n.match( dateFirstRe ) ) {
					// Middle endian date, starting with the month, eg. 01/31/2011
					// Little endian date, starting with the day, eg. 31.01.2011
					temp = Date.middle_endian ? 1 : 2
					d.setFullYear( m[ 3 ], m[ temp ] - 1, m[ 3 - temp ] )
				}
				// Time
				m = n.match( timeRe ) || [ 0, 0, 0 ]
				d.setHours( m[ 6 ] && m[ 1 ] < 12 ? +m[ 1 ] + 12 : m[ 5 ] && m[ 1 ] == 12 ? 0 : m[ 1 ], m[ 2 ], m[ 3 ] | 0, m[ 4 ] | 0 )
				// Timezone
				if( m[ 7 ] ) {
					d.setTime( d - ( ( d.getTimezoneOffset() + ( m[ 8 ] | 0 ) * 60 + ( ( m[ 8 ] < 0 ? -1 : 1 ) * ( m[ 9 ] | 0 ) ) ) * 60000 ) )
				}
			} else d.setTime( n < 4294967296 ? n * 1000 : n )
			return format ? d.format( format ) : d
		}
	}( Date, "prototype" );
	return {};
} );

define('tiljs/apps/times/comments',[
	"module",
	"jquery",
	"json",
	"jsrender",
	"../times/api",
	"../../comments",
	"../../ajax",
	"../../plugin/dynamic",
	"../../util",
	"../../login",
	"../../event",
	"../../ui",
	"../../is",
	"../../string",
	"../../plugin/lazy",
	"../../cookie",
	"../../logger",
	"../../timer",
	"../../analytics/mytimes",
	"../../compatibility",
	"../../social/facebook",
	"../../page",
	"../../user",
	"../../ext/date"
],
    function (module, $, json, jsrender, api, comments, ajax, dynamic, util, login, event, ui, is, string, lazy,
              cookie, logger, timer, mytimes, compatibility, facebook, page, userClass, ext_date) {
        //todo use plugin module for data-plugin

        var CONSTANT = {
            RATE_TYPE: {
                AGREE: 100,
                DISAGREE: 101,
                RECOMMEND: 102,
                OFFENSIVE: 103
            }
        };
        
        var commentReplyFormHTML = '';

        var default_config = {
//            post_url :function(){
//                var url = "/postro.cms";
//
//                if (document.location.host == "test.indiatimes.com") {
//                    url = "lib/postComment.php";
//                }
//                return url;
//            }(),
//            validate_url : function(){
//                var validate_url = "/validatecomment.cms";
//                if (document.location.host == "test.indiatimes.com" || document.location.host == "test.happytrips.com") {
//                    validate_url = "lib/validateComment.php";
//                }
//                return validate_url;
//            }(),
//            rate_url : "/ratecomment_new.cms",
            validation: {
                minlength: 1
            },
            loadCommentFromMytimes:false,
            commentType:"comments",
            loadonscroll:true,
            sendCommentLiveEmail:true,
            loginRequiredForRating : true,
            nonloggedinComment : true,
            disabledirectcomment : false,
            maxchar:3000,
            maxCommentWrapLength:500,
            maxResponseCount:3,
            hideResponses:false,
            share_url:"/share.cms",
            verify_comment_url:"/json/cmtverified.cms",
			messages: {
                "name_required" :"Please enter your name.",
                "location_required" :"Please enter your location.",
                "captcha_required" :"Please enter captcha value.",
                "name_toolong" :"Name cannot be longer than 30 chars.",
                "name_not_string" :"Name can only contain alphabets.",
                "location_toolong" :"Location cannot be longer than 30 chars.",
                "location_not_string" :"Location can only contain alphabets.",
                "captcha_toolong" :"Captcha cannot be longer than 4 chars.",
                "captcha_number_only" :"Captcha value can only be a number.",
                "email_required" :"Please enter your email address.",
                "email_invalid" :"Please enter a valid email address.",
                "captcha_invalid" :"Please enter a valid captcha value.",
                "minlength": "You can't post this comment as the length it is too short. ",
                "blank": "You can't post this comment as it is blank.",
                "maxlength": "You have entered more than 3000 characters.",
                "popup_blocked": "Popup is blocked.",
                "has_url": "You can't post this comment as it contains URL.",
                "duplicate": "You can't post this comment as it is identical to the previous one.",
                "abusive": "You can't post this comment as it contains inappropriate content.",
                "self_agree": "You can't Agree with your own comment",
                "self_disagree": "You can't Disagree with your own comment",
                "self_recommend": "You can't Recommend your own comment",
                "self_offensive": "You can't mark your own comment as Offensive",
                "already_agree": "You have already Agreed with this comment",
                "already_disagree": "You have already Disagreed with this comment",
                "already_recommended": "You have already Recommended this comment",
                "already_offensive": "You have already marked this comment Offensive",
                "cant_agree_disagree": "You can't Agree and Disagree with the same comment",
                "cant_agree_offensive": "You can't Agree and mark the same comment Offensive",
                "cant_disagree_recommend": "You can't Disagree and Recommend the same comment",
                "cant_recommend_offensive": "You can't Recommend and mark the same comment Offensive",
                "permission_facebook": "You can't post to facebook. Post permission is required.",
                "offensive_reason": "Please select a reason.",
                "offensive_reason_text": "Please enter a reason." ,
                "offensive_reason_text_limit": "Please enter less than 200 chars.",
                "be_the_first_text": "Be the first one to review.",
				"no_comments_discussed_text": "None of the comments have been discussed.",
				"no_comments_up_voted_text": "None of the comments have been up voted.",
				"no_comments_down_voted_text": "None of the comments have been down voted."
            }
        };


        var times_comments = comments;


        comments.config = util.extend(true, {}, comments.config, default_config, module.config());

        times_comments.prototype.updateConfig = function (update_config) {
            //config = util.extend(true, config, update_config);
        };

//    login.init();

//        var isLoading = 0;
        var commentType = comments.config.commentType;
//        var curpg = 1;

        times_comments.prototype.initialize = function () {
            var tthis = this;
            this.config = util.extend(true, {}, comments.config, module.config(), this.config);
//            if (update_config) {
//                tthis.updateConfig(update_config);
//            }
			
			tthis.comments = [];
            tthis.commentCount = 0;
            tthis.countPresent = false;

            tthis.pageCount = 1;
//            tthis.commentsCacheDate = null;
//            tthis.commentsCacheUpdated = [];

            //todo move all this to config object
            tthis.config.comment_block_count = tthis.config.comment_block_count || 25;
            tthis.config.commentType = tthis.config.commentType || commentType;
//            tthis.config.attachInput = tthis.config.attachInput || true;
            tthis.config.attachReplyAction = tthis.config.attachReplyAction !== false;
            tthis.config.attachOpinionAction = tthis.config.attachOpinionAction !== false;
			
			if(tthis.config.disabledirectcomment){
				$(tthis.config.wrapper + ' [data-plugin="comment-form"]').hide();
			}
			if(login.getUser()){
                $( "[data-plugin='user-isloggedin']" ).show();
	            $( "[data-plugin='user-notloggedin']" ).hide();
            }
			else{
                $( "[data-plugin='user-isloggedin']" ).hide();
	            $( "[data-plugin='user-notloggedin']" ).show();
            }
			if(!tthis.config.nonloggedinComment && tthis.pageCount === 1){
                $(tthis.config.wrapper + ' [data-plugin="user-notloggedin"]').remove();
            }
			
            tthis.config.opinions = [
                {name: "Agree", id: "agree"},
                {name: "Disagree", id: "disagree"},
                {name: "Recommend", id: "recommended"},
                {name: "Offensive", id: "offensive"}
            ];

//            if (tthis.config.attachInput === true) {
//                tthis.attachInput();
//            }

            if (tthis.config.attachOpinionAction === true) {
                tthis.attachOpinionAction();
            }

            if (tthis.config.attachReplyAction === true) {
                tthis.attachReplyAction();
            }

            this.getData(function (data, commentCount) {


                tthis.renderCommentCount(commentCount || (data ? data.length : 0));
                if(data && data.length >0){
                    tthis.renderNoComment(false);
                    tthis.render(data);
                }else{
                    tthis.renderNoComment(true);
                }

                tthis.verifyEmailComment();

            });

            (function (tthis) {
                event.subscribe("window.scroll", function () {//todo this wont work
                    var config = tthis.getConfig();
                    if (tthis.config.loadonscroll === true && tthis.isLoading === 0 && ui.inView(tthis.config.wrapper + " #" + tthis.config.commentType + " [data-plugin='comment']:visible:last", true, 200)) {  //todo remove li from here
                        tthis.loadComment(tthis.config.commentType, function (data) {

                        }, true, ++tthis.pageCount);
                    }
                });
            })(tthis);


//            tthis.onLoaded(function (comment_ref) {
//                tthis.loadSavedComment();
//            });


//            login.onStatusChange(function(){
//                tthis.loadSavedComment();
//            });
        };

        times_comments.run = function (config) {
            //Comments
            this.config = util.extend(true, {}, comments.config, module.config());
            var msid = config && config.msid ? config.msid : window.msid;//22655412;//27810093/*,channel = "toi"*/;
            var main = config && config.main ? config.main : "#"+this.config.commentType;//22655412;//27810093/*,channel = "toi"*/;
            var wrapper = config && config.wrapper ? config.wrapper : this.config.wrapper;//22655412;//27810093/*,channel = "toi"*/;
            var toi_comment = {};
            toi_comment[this.config.commentType] = new comments({main: main, msid: msid, wrapper:wrapper});
            toi_comment[this.config.commentType].initialize();

            $(wrapper + ' #comment_sort').change(function () {
                var rootid = $(this).val();
                var id = "comments" + (rootid && rootid.length > 0 ? '_' + rootid : "");

                $(wrapper +' .comments-list').hide();
                $( wrapper + " #"+id).show();

                $(wrapper + ".comment-section .noComment").hide();

                if (toi_comment[id]) {
                    toi_comment[id].updateAfterCommentsLoaded();
                    toi_comment[id].attachReplyAction();
                    toi_comment[id].attachOpinionAction();
                    toi_comment[id].updateCachedRating();
                }else {
                    toi_comment[id] = new comments({main:"#" + id, msid: msid, commentType: id, wrapper:wrapper/*,attachReplyAction:false, attachOpinionAction:false*/});
                    toi_comment[id].initialize();
                }

                $(wrapper + " [data-plugin='comment-error-outer']").text("");
                $(wrapper + " [data-plugin='comment-error']").text("");
                event.publish("comment_error");
            });

            event.subscribe("user.status",function(user){
                if(!user){
                    $(wrapper + " [data-plugin='comment-error-outer']").text("");
                    $(wrapper + " [data-plugin='comment-error']").text("");
                    event.publish("comment_error");
                }
                var commenttype = "comments" + ($('#comment_sort').length && $('#comment_sort').val() !== '' ? '_' + $('#comment_sort').val(): "");
                // callback to get user rating from mytimes 
                if(user &&  toi_comment[commenttype]
                    && toi_comment[commenttype].config
                    && toi_comment[commenttype].config.withrating){
                    var tthis = toi_comment[commenttype];
                    var savedComment = tthis.getSavedComment();
                    var usr_data = {};
		            usr_data.userId = user.getId();
		            usr_data.baseEntityId = 0;
		            usr_data.uniqueAppKey = tthis.config.withrating;
		            usr_data.appKey = page.getChannel();
		            $.getJSON("http://myt.indiatimes.com/mytimes/alreadyRated?callback=?", usr_data,function(data){
		                event.publish("comments.user_has_rated",data);		                	                    
	                    if(window.msid == tthis.config.msid && tthis.getSavedComment()){
	                        data = (data && data !='') ? data : ' ';
			                savedComment.urs = data;
		                    tthis.saveComment(savedComment);
	                        tthis.loadSavedComment();
	                    }
		            });
                }
                
                toi_comment[commenttype].markFollowingAll(); //Added by Amit
            });

            event.subscribe("comments.loaded",function(commentObj){
                logger.log("comments.loaded:"+commentObj.config.commentType);
                commentObj.updateAfterCommentsLoaded();
                if(commentObj.config.hideResponses === true){
                    commentObj.hideExtraResponses();
                }
            });

            event.subscribe("comments.loaded.none",function(commentObj){
                logger.log("comments.loaded.none:"+commentObj.config.commentType);
                commentObj.updateAfterCommentsLoaded();
            });

            event.subscribe("login.error", function (err) {
                if (err && err.error) {
                    logger.error(err.error);
                } else {
                    logger.error(err);
                }
            });

            return toi_comment;
        };

        //Todo remove from prototype
        times_comments.prototype.attachUserToComment = function (commentObj, user) {
			logger.log("Attaching user to comment");
            commentObj.user = {
                "id": user.getId(),
                "uid": user.getId() ? user.getUid() : '',
                "name": user.getFullName(),
                "username": user.getUsername(),
                "location": user.getCITY(),
                "image": user.getThumb(),
                "email": user.getEmail()
//                    "followers": user.getFollowersCount(),
//                    ,"badge": [],  //todo
//                    "opinion": [],//todo
//                    "points": 1205,     //todo
//                    "pointslevel": "Silver"   //todo
            };
        };

        times_comments.prototype.hideExtraResponses = function(){
            var ttthis = this;
            $($(this.config.wrapper +" #"+this.config.commentType+" .comment-box.level1").get().reverse()).each(function(i,v){ 
           // $(".comment-box.level1").get().reverse().each(function(i,v){ 
                var tc = $(this);
                var tc_level=tc.data("level");
                var tc_next=tc.next();
                if(is.numberOnly(tc_next.data("level"))) {
                    var response_cnt=0;
                    while((tc_next.data("level")!=tc_level) && (tc_next.is(":hidden")) && (i < ttthis.config.comment_block_count))
                    {
                        if(response_cnt == ttthis.config.maxResponseCount){
                            tc_next.before('<div class="show_all_responses comment-box level'+(tc_level+1)+'" data-level="'+tc_level+'" data-action="all_responses"> Show all responses </div>');
                            break;
                        }else{
                            tc_next.slideDown();
                            tc_next=tc_next.next();
                            if(!is.numberOnly(tc_next.data("level"))) {
                                break;
                            }
                        }
                        response_cnt++;
                    }
                } 
            });
        };

        times_comments.prototype.updateAfterCommentsLoaded = function(){
            var commentObj = this;
            enablegdpr();
            if(this.pageCount === 1){
	            if(commentObj.config.commentType === this.config.commentType){
	                if(commentObj.comments.length > 0)   {
	                    logger.log("updateAfterCommentsLoaded : comments.loaded:"+commentObj.config.commentType);
	                    $(commentObj.config.wrapper + ".comment-section .sortby").show();
	                }else{
	                    logger.log("updateAfterCommentsLoaded : comments.loaded.none:"+commentObj.config.commentType);
	                    $(commentObj.config.wrapper + ".comment-section .sortby").hide();
	                    $(commentObj.config.wrapper + ".comment-section .noComment").text(commentObj.config.messages.be_the_first_text).show();
	                }
	            }
	
	           if(commentObj.countPresent !== true){
	               logger.log("updateAfterCommentsLoaded : countPresent:false:" +commentObj.config.commentType);
	                if(commentObj.comments.length === 0)   {
                	    $(commentObj.config.wrapper + ".comment-section .noComment").text(commentObj.config.messages.be_the_first_text).show();
                	} else if(commentObj.config.commentType === "comments_discussed"){
	                    $(commentObj.config.wrapper + ".comment-section .noComment").text(commentObj.config.messages.no_comments_discussed_text).show();
	                } else if(commentObj.config.commentType === "comments_agree"){
	                    //$(commentObj.config.wrapper + ".comment-section .noComment").text(commentObj.config.messages.no_comments_up_voted_text).show();
	                } else if(commentObj.config.commentType === "comments_disagree"){
	                    $(commentObj.config.wrapper + ".comment-section .noComment").text(commentObj.config.messages.no_comments_down_voted_text).show();
	                }
	           }         
            }
        };

        times_comments.prototype.renderCommentCount = function (count) {
            var tthis = this;
            function displayCount(type, cnt) {
                $("[data-plugin='comment-count" + (type ? "-" + type : "") + "']").text(cnt ? cnt : "0");
                $(tthis.config.wrapper + " [data-plugin='comment-count" + (type ? "-" + type : "") + "']").text(cnt ? cnt : "0");
                event.publish("comment.count",(cnt ? cnt : "0"));
            }

            if (is.object(count)) {
                displayCount("", count.newest);
                displayCount("oldest", count.oldest);
                displayCount("recommended", count.recommended);
                displayCount("discussed", count.discussed);
                displayCount("agree", count.agree);
                displayCount("disagree", count.disagree);
            } else {
                displayCount("", count);
            }
        };


        /**
         *
         * @param commentType newest(default),oldest,recommended,discussed,agree,disagree
         * @param callback
         * @param append true if comments visible are not to be cleaned
         * @param curpg
         */
        times_comments.prototype.loadComment = function (commentType, callback, append, curpg) {
            var tthis = this;
            if(tthis.config.loadCommentFromMytimes == true){
                tthis._loadCommentMytimes(commentType, function (data) {
                    tthis.render(data, callback, append);
                }, curpg);
            }else{
                tthis._loadComment(commentType, function (data) {
                    tthis.render(data, callback, append);
                }, curpg);
            }
        };

        times_comments.prototype.getData = function (callback) {
            var tthis = this;
            if(tthis.config.loadCommentFromMytimes == true){
                this._loadCommentMytimes(this.config.commentType, callback);
            }else{
                this._loadComment(this.config.commentType, callback);
            }
        };

        times_comments.prototype._loadComment = function (commentType, callback, curpg) {
            var tthis = this;
            var config = tthis.config;//getConfig();
            tthis.loading();

//        var _comments_url = comments_url(config.msid);
            logger.log("loading comments: " + config.msid);
            api.api(commentType, {msid: config.msid, curpg: curpg || 1/*,channel:config.channel*/}, function (comments_data) {
//                tthis.loading(50);

                logger.log("loaded comments: " + config.msid);
                var commentsD = comments_data.articlecomment
                    || comments_data.new_cmtofart2_nit
                    || comments_data.new_cmtofart2_nit_v1
                    || comments_data.articleshow_othcmtofart
                    || comments_data.new_cmtofart2_nit_sub_dev || (comments_data.mytuserdata?comments_data:null);
//                tthis.commentsCacheDate = tthis.commentsCacheDate || (commentsD.tdate&&commentsD.tdate.date?commentsD.tdate.date:logger.warn("Comments do not have tdate."));

                var userData = commentsD.mytuserdata && commentsD.mytuserdata['array'] ? (is.array(commentsD.mytuserdata['array']) ? commentsD.mytuserdata['array'] : [commentsD.mytuserdata['array']]) : null;
//                var commentCount = commentsD.rothrd ? commentsD.rothrd.opctr : null;
                var commentsData = commentsD.rothrd && commentsD.rothrd.op ? (is.array(commentsD.rothrd.op) ? commentsD.rothrd.op : [commentsD.rothrd.op]) : null;
                if (!commentsData || commentsData.length == 0) {
                    tthis.loadedAll();
                    callback([]);
                    return null;
                }

                tthis.flagData = commentsData[0];
                var commentCount = {
                    total: commentsD.rothrd.opctr,
                    newest: commentsD.rothrd.opctr,
                    oldest: commentsD.rothrd.opctr,
                    recommended: commentsD.rothrd.recommendcount,
                    discussed: commentsD.rothrd.opctrtopcnt, // total top level comments
                    disagree: commentsD.rothrd.disagreecount,
                    agree: commentsD.rothrd.agreecount
                };
                var commentPageCount = {
                    comments: Math.ceil(commentsD.rothrd.opctrtopcnt/config.comment_block_count),
                    comments_oldest: Math.ceil(commentsD.rothrd.opctrtopcnt/config.comment_block_count),
                    comments_recommended: Math.ceil(commentsD.rothrd.recommendcount/config.comment_block_count),
                    comments_discussed: Math.ceil(commentsD.rothrd.opctrtopcnt/config.comment_block_count),
                    comments_disagree: Math.ceil(commentsD.rothrd.disagreecount/config.comment_block_count),
                    comments_agree: Math.ceil(commentsD.rothrd.agreecount/config.comment_block_count)
                };

                var users = [];//getUsers(commentsData);
//                console.log("---------------------");
                var i = 0;
                if (userData) {
                    for (i = 0; i < userData.length; i++) {
                        users.push(userData[i].sso);
                    }
                }

                var commentsResult = [];

                for (var c = 0; c < commentsData.length; c++) {
                    var cmt = commentsData[c];
                    var user = userData ? userData[users.indexOf(cmt.roaltdetails.fromaddress)] : null;

                    var comment = {
                        index: ++tthis.commentCount,
                        id: cmt.messageid,
                        comment: is.string(cmt.optext) ? $("<div/>").text(cmt.optext).html() : "",
                        level: cmt.level,
                        childcount: cmt.CHILD ? cmt.CHILD.length : 0,
                        parentuid: cmt.parentuid,
                        parentusername: cmt.parentusername,
                        time: cmt.rodate ? util.getDate(cmt.rodate).getTime():"",//1384934349412,    //cmt.rodate //19 Nov, 2013 01:04 PM
                        abstime:cmt.rodate,
                        opinion: (function (cmt) {
                            var opinions = [];
                            if (tthis.config.opinions) {
                                for (var o = 0; o < tthis.config.opinions.length; o++) {
                                    opinions.push({name: tthis.config.opinions[o].name, id: tthis.config.opinions[o].id, count: cmt[tthis.config.opinions[o].id]});
                                }
                            }
                            return opinions.length > 0 ? opinions : null;
                        }(cmt)),

                        user: {
                            //TODO fix multiple condition check ,user tthis.attachUserToComment(comment,user);
                            id: user && user._id ? user._id : cmt.roaltdetails.fromname,
                            username: user ? user.D_N_U : null,
                            name: cmt.roaltdetails.fromname || (user && user.FL_N ? user.FL_N : ""),    //name used from comment because name in replies to does not match user name in mytimes
                            location: user && user.CITY && !is.object(user.CITY) && !is.empty(user.CITY) ? user.CITY : (!is.empty(cmt.roaltdetails.location) && !is.object(cmt.roaltdetails.location) ? cmt.roaltdetails.location : null),
                            image: user && user.thumb ? user.thumb : cmt.roaltdetails.imageurl,
                            email: user ? user.sso : "",
                            rate: cmt.roaltdetails.urs, 
							followers: user ? user.F_C : 0,
                            follower_text: (user ? user.F_C : 0) > 1 ? ('('+user.F_C+' followers)') : ((user ? user.F_C : '') > 0 ? '('+user.F_C+' follower)' : ''),
                            points: user && user.reward && user.reward.user && user.reward.user.statusPoints ? user.reward.user.statusPoints : null,
                            pointslevel: user && user.reward && user.reward.user && user.reward.user.levelName ? user.reward.user.levelName : null,
                            pointsNeeded: (function(user){
                                var points_needed = '';
                                var user_points = user && user.reward && user.reward.user && user.reward.user.statusPoints ? user.reward.user.statusPoints : null;
                                if(user_points == null || user_points < 250){
                                    points_needed = 250 - user_points;
                                } else if(user_points > 249 && user_points < 5000){
                                    points_needed = 5000 - user_points;
                                } else if(user_points > 4999 && user_points < 25000){
                                    points_needed = 25000 - user_points;
                                } else if(user_points > 24999 && user_points < 250000){
                                    points_needed = 250000 - user_points;
                                }
                                return points_needed;
                            }(user)),

                            badge: (function (user) {
                                var badges = [];
                                if (user && user.rewardpoint && user.rewardpoint.userbadges && user.rewardpoint.userbadges.activityBadge) {
                                    var activityBadge = user.rewardpoint.userbadges.activityBadge;
                                    if(!is.array(activityBadge)){ // Backend gives object when length = 1, converting ot array
                                        activityBadge = [activityBadge];
                                    }
                                    for (var d = 0; d < activityBadge.length; d++) {  //todo remove loop
                                        var badge = activityBadge[d].currentBadge;
                                        badges.push({name: badge.bname, count: badge.level, image: badge.bimg, desc: badge.desc, levelDesc: util.decodeHTML(badge.levelDesc)});
                                    }
                                }
                                return badges.length > 0 ? badges : null;
                            }(user))

                        }
                    };

                    commentsResult.push(comment);
//                        if( c > 10){
//                            break;
//                        }
                    logger.log("Parsed comment " + c);
                }

                tthis.comments = tthis.comments.concat(commentsResult);

                logger.log("rendering  " + commentsResult.length + " comments");
                callback(commentsResult, commentCount);
                logger.log("rendered " + commentsResult.length + " comments");

                tthis.loaded();
                var curpgn = curpg || 1;
                if (tthis.config.loadonscroll == false && !commentsResult || !(curpgn < commentPageCount[tthis.config.commentType])) {
                    tthis.loadedAll();
                }else if (tthis.config.loadonscroll == true && !commentsResult || commentsResult.length == 0 ) {
                    tthis.loadedAll();
                }

            });

            return null;
        };


        times_comments.prototype.parseComment = function (cmt, commentsResult, level) {
            var ttthis = this;
            var user = cmt.user_detail || {
                FL_N:cmt.A_D_N
            };
            user.reward = {user:cmt.user_reward};
            user.rewardpoint={userbadges : cmt.user_reward_point_info};
            user.rate = cmt && cmt.U_R ? cmt.U_R : '';
            var tthis = this;

            level = level || 1;

            var comment = {
                index: ++tthis.commentCount,
                id: cmt.A_U_I,
//                comment: cmt.C_T , //is.string(cmt.C_T) ? $("<div/>").text(cmt.C_T).html() : "",
                comment: is.string(cmt.C_T) ? $("<div/>").html(cmt.C_T).text() : "",
                trimcom: (function(cmt){
                    var t_c = is.string(cmt.C_T) ? $("<div/>").html(cmt.C_T).text() : "";
                    return util.trimText(t_c,ttthis.config.maxCommentWrapLength);
                }(cmt)),
                authrid:(cmt.C_A_ID ?cmt.C_A_ID:""),
                level: level,
                parentuid: cmt.O_ID,
                parentusername: cmt.O_D_N,
                time: (cmt.A_DT ? util.getDate(parseInt(cmt.A_DT,10)).getTime():""),//1384934349412,    //cmt.rodate //19 Nov, 2013 01:04 PM
                abstime:cmt.A_DT? util.getDate(parseInt(cmt.A_DT,10)).format("DD MMM, YYYY HH:mm A"):"",
                opinion: (function (cmt) {
                    var opinions = [];

                    opinions.push({name: "Agree",       id: "AC_A_C", count: (cmt.AC_A_C) ? cmt.AC_A_C : 0});
                    opinions.push({name: "Disagree",    id: "AC_D_C", count: (cmt.AC_D_C) ? cmt.AC_D_C : 0});
                    opinions.push({name: "Recommend",   id: "AC_R_C", count: (cmt.AC_R_C) ? cmt.AC_R_C : 0});
                    opinions.push({name: "Offensive",   id: "AC_O_C", count: (cmt.AC_O_C) ? cmt.AC_O_C : 0});

                    return opinions.length > 0 ? opinions : null;
                }(cmt)),

                user: { 
                    //TODO fix multiple condition check ,user tthis.attachUserToComment(comment,user);
                    id: user && user._id ? user._id : null,
                    username: user && user.D_N_U ? user.D_N_U : null,
                    name: user && user.FL_N ? user.FL_N : "",    //name used from comment because name in replies to does not match user name in mytimes
                    location: user && user.CITY && !is.object(user.CITY) && !is.empty(user.CITY) ? user.CITY : null,
                    image: user && user.thumb ? user.thumb : "",
                    email: user && user.sso ? user.sso : "",
                    rate:user && user.rate ? user.rate : "",
                    followers: user && user.F_C ? user.F_C : 0,
                    follower_text: (user ? user.F_C : 0) > 1 ? ('('+user.F_C+' followers)') : ((user ? user.F_C : '') > 0 ? '('+user.F_C+' follower)' : ''),
                    points: user && user.reward && user.reward.user && user.reward.user.statusPoints ? user.reward.user.statusPoints : null,
                    pointslevel: user && user.reward && user.reward.user && user.reward.user.levelName ? user.reward.user.levelName : null,
                    pointsNeeded: (function(user){
                        var points_needed = '';
                        var user_points = user && user.reward && user.reward.user && user.reward.user.statusPoints ? user.reward.user.statusPoints : null;
                        if(user_points == null || user_points < 250){
                            points_needed = 250 - user_points;
                        } else if(user_points > 249 && user_points < 5000){
                            points_needed = 5000 - user_points;
                        } else if(user_points > 4999 && user_points < 25000){
                            points_needed = 25000 - user_points;
                        } else if(user_points > 24999 && user_points < 250000){
                            points_needed = 250000 - user_points;
                        }
                        return points_needed;
                    }(user)),

                    badge: (function (user) {
                        var badges = [];
                        if (user && user.rewardpoint && user.rewardpoint.userbadges && user.rewardpoint.userbadges.activityBadge) {
                            var activityBadge = user.rewardpoint.userbadges.activityBadge;
                            if(!is.array(activityBadge)){ // Backend gives object when length = 1, converting ot array
                                activityBadge = [activityBadge];
                            }
                            for (var d = 0; d < activityBadge.length; d++) {  //todo remove loop
                                var badge = activityBadge[d].currentBadge;
                                badges.push({name: badge.bname, count: badge.level, image: badge.bimg, desc: badge.desc, levelDesc: util.decodeHTML(badge.levelDesc)});
                            }
                        }
                        return badges.length > 0 ? badges : null;
                    }(user))

                }
            };
            commentsResult.push(comment);

            if(cmt.CHILD){
                for(var cmtChild in cmt.CHILD){
                    tthis.parseComment(cmt.CHILD[cmtChild], commentsResult, level + 1);
                }
            }

        };

        times_comments.prototype._loadCommentMytimes = function (commentType, callback, curpg) {
            var tthis = this;
            var config = tthis.config;//getConfig();
            tthis.loading();

//        var _comments_url = comments_url(config.msid);
            logger.log("loading comments: " + config.msid);
            api.api(commentType, {msid: config.msid, pagenum: curpg || 1/*,channel:config.channel*/}, function (comments_data) {
//                tthis.loading(50);

                logger.log("loaded comments: " + config.msid);
                var commentsData = comments_data;
//                tthis.commentsCacheDate = tthis.commentsCacheDate || (commentsData.tdate&&commentsData.tdate.date?commentsD.tdate.date:logger.warn("Comments do not have tdate."));

                if (!commentsData || commentsData.length == 0) {
                    tthis.loadedAll();
                    callback([]);
                    return null;
                }

                tthis.flagData = commentsData[0];
                var commentCount = {
                       total : commentsData[0].totalcount,
                       newest : commentsData[0].totalcount,
                       oldest : commentsData[0].totalcount,
                       recommended : commentsData[0].totalcount,
                       discussed : commentsData[0].totalcount,
                       disagree : commentsData[0].totalcount,
                       agree : commentsData[0].totalcount
                   };
//                var commentCount = {
//                    total: commentsD.rothrd.opctr,
//                    newest: commentsD.rothrd.opctr,
//                    oldest: commentsD.rothrd.opctr,
//                    recommended: commentsD.rothrd.recommendcount,
//                    discussed: commentsD.rothrd.opctrtopcnt, // total top level comments
//                    disagree: commentsD.rothrd.disagreecount,
//                    agree: commentsD.rothrd.agreecount
//                };
                var commentPageCount = {
                    comments: Math.ceil(commentCount.newest/config.comment_block_count),
                    comments_oldest: Math.ceil(commentCount.oldest/config.comment_block_count),
                    comments_recommended: Math.ceil(commentCount.recommended/config.comment_block_count),
                    comments_discussed: Math.ceil(commentCount.discussed/config.comment_block_count),
                    comments_disagree: Math.ceil(commentCount.disagree/config.comment_block_count),
                    comments_agree: Math.ceil(commentCount.agree/config.comment_block_count)
                };

//                var users = [];//getUsers(commentsData);
////                console.log("---------------------");
//                var i = 0;
//                if (userData) {
//                    for (i = 0; i < userData.length; i++) {
//                        users.push(userData[i].sso);
//                    }
//                }

                var commentsResult = [];

                for (var c = 1; c < commentsData.length; c++) {
                    var cmt = commentsData[c];
                    var reason = null;

                    if(cmt.DLT == 1){
                    	reason = "DLT fail";
                    }else if(is.empty(cmt.C_T)){
                    	reason = "Blank fail";
                    }
                    if(reason){
                    	logger.log("Not Parsed " + reason + " comment " + c);
                    }else{
                    	tthis.parseComment(cmt,commentsResult);
                    	logger.log("Parsed comment " + c)
                    }
                }

                tthis.countPresent = commentsData[0].countPresent || false;
                tthis.comments = tthis.comments.concat(commentsResult);

                logger.log("rendering  " + commentsResult.length + " comments");
                callback(commentsResult, commentCount);
                logger.log("rendered " + commentsResult.length + " comments");

                tthis.loaded();
                var curpgn = curpg || 1;
                if (tthis.config.loadonscroll == false && !commentsResult || !(curpgn < commentPageCount[tthis.config.commentType])) {
                    tthis.loadedAll();
                }else if (tthis.config.loadonscroll == true && !commentsResult || commentsResult.length == 0 ) {
                    tthis.loadedAll();
                }

            });

            return null;
        };
        function replaceAll(find, replace, str) {
            return str.replace(new RegExp(find, 'g'), replace);
        }


        var domain = "http://jcmsdev.indiatimes.com";

        var getUsers = function (data) {
            var ssoids = {};
            for (var i = 0; i < data.length; i++) {
                ssoids[(data[i].roaltdetails.fromaddress)] = {};
            }
            return ssoids;
        };


        times_comments.prototype.postToSocial = function (message, sites, closeCallback) {
            var tthis = this;
            var domainOnly = util.getDomainOnly();

            var user = login.getUser();

            window.newShare = true;
            window.newShareData = {
//                site:"facebook",
                sites:sites,
                message:message,
                link:window.facebooklink || page.getMeta('og:url'),
                title:window.facebookktitle || page.getMeta('og:title'),
                domain:window.location.host,
                picture:window.fb_Img || page.getMeta('og:image'),
                desc:window.facebooksyn || page.getMeta('og:description')  ,
                facebook : user?user.facebook:null,
                twitter : user?user.twitter:null
            };

            var url = tthis.config.share_url + "?sites=" + sites;
            var socialPost = ui.window(url, {width: 600, height: 230, name: "socialPost", closeCallback: closeCallback});
            if(socialPost){
	            try{
                    socialPost.moveTo(250, 240);
	            }catch(e){
		            logger.log("Handled IE Exception.");
		            logger.error(e);
	            }
            } else{
//                window.popupblocked = 1;
//                if(is.desktop()){
//                    twitterPost = ui.iframe(url, {width: 575, height: 314, name: "twitterPost", closeCallback: closeCallback});
//                }else{
                logger.error("Popups are blocked. Please enable them.");
//                }
            }
        };

        times_comments.prototype.postToTwitter = function (message, closeCallback) {
            var domainOnly = util.getDomainOnly();

            if (message.length > 100) {
                message = message.substring(0, 99) + "...";
            }

            window.log = window.log || function(){};
            window.popupblocked = 0;
            cookie.remove("fbcheck", "/", domainOnly);
            cookie.remove("twtcheck", "/", domainOnly);

            cookie.set("twtcheck", "1", 1, "/", domainOnly);
            cookie.set("usercomt", message, 1, "/", domainOnly);

            var url = "/stgredirectpagetest.cms";
            var twitterPost = ui.window(url, {width: 600, height: 230, name: "twitterPost", closeCallback: closeCallback});
            if(twitterPost){
                twitterPost.moveTo(250, 240);
            } else{
                window.popupblocked = 1;
                if(is.desktop()){
                    twitterPost = ui.iframe(url, {width: 575, height: 314, name: "twitterPost", closeCallback: closeCallback});
                }else{
                    alert("Popups are blocked. Please enable them.");
                }
            }



//            var url = "https://twitter.com/share?text=" + message + "&url=" + document.location.href;
//            var twitterPost = ui.window(url, {width: 750, height: 500, name: "twitterPost", closeCallback: closeCallback});
//            if(twitterPost){
//                try{//Not working in IE10 - TOIPR-5367
//                    twitterPost.moveTo(275, 275);
//                }catch(e){
//                    logger.warn("Exception handled for IE10");
//                    logger.error(e);
//                }
//            }else{
//                if(is.desktop()){
//                    ui.iframe(url, {width: 750, height: 500, name: "twitterPost", closeCallback: closeCallback});
//                }else{
//                    alert("Popups are blocked. Please enable them.");
//                }
//            }
        };

        times_comments.prototype.postToFacebook = function (message, closeCallback) {
            var domainOnly = util.getDomainOnly();
            var tthis = this;

            //todo remove cookie use
            if (window.localStorage) {
                localStorage.setItem("usercomt", message);
            }

            window.log = window.log || function(){};
            window.popupblocked = 0;
            cookie.remove("fbcheck", "/", domainOnly);
            cookie.remove("twtcheck", "/", domainOnly);

            cookie.set("fbcheck", "1", 1, "/", domainOnly);
            cookie.set("usercomt", message, 1, "/", domainOnly);

            var url = "/stgredirectpagetest.cms";
//            facebook.getPermissions("publish_stream",function(got_permission){
//                if(got_permission === true){
                    var facebookPost = ui.window(url, {width: 600, height: 230, name: "facebookPost", closeCallback: closeCallback});
                    if(facebookPost){
                        facebookPost.moveTo(250, 240);
                    } else{
                        window.popupblocked = 1;
                        if(is.desktop()){
                            facebookPost = ui.iframe(url, {width: 575, height: 314, name: "facebookPost", closeCallback: closeCallback});
                        }else{
                            alert("Popups are blocked. Please enable them.");
                        }
                    }
//                }   else{
//                    tthis.error(config.messages.permission_facebook);
//                }
//            });


            /*
             if (window.FB) {  //todo use facebook plugin
             FB.ui({
             method: "stream.publish",
             message: message,
             attachment: {media: [
             {type: "image", href: window.facebooklink, src: window.fb_Img}
             ], name: window.facebookktitle,
             caption: "timesofindia.indiatimes.com",
             description: window.facebooksyn,
             href: window.facebooklink},
             user_message_prompt: "What's on your mind?"}, function (d) {
             //                    if (d && d.post_id) {
             //                    } else {
             //                    }
             });
             }else{
             logger.warn("'window.FB' is not defined. Will try again after 2secs.");
             timer.available("FB",function(FB){
             if(FB){
             toi_comments.postToFacebook(message);
             }else{
             logger.error("'window.FB' is not defined. Giving up.");
             }
             },3000, 10);
             }     */
        };

        times_comments.prototype.attachOpinionAction = function () {
            logger.log("Attaching Opinion Action");
            var tthis = this;
            var opinions = this.config.opinions;

            for (var o = 0; o < opinions.length; o++) {
                var opinion = opinions[o];
                (function (opinion) {
                    $(tthis.config.wrapper).off("click", "[data-action='comment-" + opinion.id + "']");
                    $(tthis.config.wrapper).on("click", "[data-action='comment-" + opinion.id + "']", function () {
                        var ref = ui.getActionReferences(this, tthis.config.comment);
                        var parent = ref.parent;

                        var opinionid = parent.attr("data-id");
                        //rateAgree, rateDisagree, rateRecommend, rateOffensive
                        (function (tthis, parent, opinion, ref) {
                            tthis["rate" + opinion.name](opinionid, function (errMsg) {
                                if (errMsg) {
                                    tthis.error(errMsg, ref["comment-error-outer"]);
                                } else {
                                    tthis.error("", ref["comment-error-outer"]);
                                    var count_ele = parent.find("[data-plugin='comment-" + opinion.id + "-count']");
                                    var currentCount = parseInt(count_ele.text(), 10);
                                    $(count_ele).text(++currentCount);
                                }
                            }, ref);
                        }(tthis, parent, opinion, ref));
                        return false;
                    });
                }(opinion))
            }

            $(tthis.config.wrapper).off('focus','[data-plugin="comment-input"]');
            $(tthis.config.wrapper).on('focus','[data-plugin="comment-input"]', function() {
                var ref = ui.getActionReferences(this, tthis.config.comment);
                if(!commentReplyFormHTML){
					commentReplyFormHTML = $(tthis.config.wrapper + " [data-plugin='comment-form']")[0].outerHTML;
				}
                if(ref["comment-form"] && !ref["comment-form"].hasClass('full')){
                    ref["comment-form"].addClass("full");
                    event.publish("comment.form.addclass.full", this);
                }else if(ref["comment-reply"] && !ref["comment-reply"].hasClass('full')){
                    ref["comment-reply"].addClass("full");
                    event.publish("comment.reply.form.addclass.full", this);
                }
            });


            $(tthis.config.wrapper).off("focus", "[data-plugin='get-user-name']");
            $(tthis.config.wrapper).on("focus", "[data-plugin='get-user-name']",function(){
                var ref = ui.getActionReferences(this, tthis.config.comment);
                if(ref["comment-form-login"]){
                    ref["comment-form-login"].removeClass("noreg");
                    var $set_user_captcha = ref['set-user-captcha'];
                    if($set_user_captcha.text().length == 0){
                        $set_user_captcha.text(Math.floor((Math.random()*10)) +"+" +Math.floor((Math.random()*10)) +"=");
                    }
                }
            });

            $(tthis.config.wrapper).off("click", "[data-action='offensive_popup_submit']");
            $(tthis.config.wrapper).on("click", "[data-action='offensive_popup_submit']", function () {
                var ref = ui.getActionReferences(this, tthis.config.comment);
                var opinionid = ref.parent.attr("data-id");
                var reason = null;
                reason = ref.offensive_popup.find("input:checked").val();

                if(!reason){
                    tthis.error( tthis.config.messages.offensive_reason, ref["comment-offensive-error"]);
                }else if(reason == "Others"){
                    var reasonVal = ref.offensive_popup_reason.val().trim();
                    if(!reasonVal || reasonVal == ""){
                        tthis.error( tthis.config.messages.offensive_reason_text, ref["comment-offensive-error"]);
                        reason = null;
                    } else{
                        if(reasonVal.length > 200){
                            tthis.error( tthis.config.messages.offensive_reason_text_limit, ref["comment-offensive-error"]);
                            reason = null;
                        }else{
                            reason = "Others" + ": " + reasonVal;
                        }
//                        tthis.rateOffensive(opinionid,function(errMsg){
//                            if (errMsg) {
//                                tthis.error(errMsg, ref["comment-offensive-error"]);
//                            } else {
//                                tthis.error("", ref["comment-offensive-error"]);
//
//                                if(ref.parent){
//                                    ref.parent.find("[data-action='comment-offensive-already']").show();
//                                    ref.parent.find("[data-action='comment-offensive']").hide();
//                                }
//
//                                ref.offensive_popup.hide();
//                            }
//                        },ref, reason);
                    }
                }

                if(reason){
                    tthis.rate(opinionid, CONSTANT.RATE_TYPE.OFFENSIVE, 0, function(error,resp){
                        if(error){
                            tthis.error( error, ref["comment-offensive-error"]);
                        }else if(!error && ref.parent){
//                            ref.parent.find("[data-action='comment-offensive-already']").show();
//                            ref.parent.find("[data-action='comment-offensive']").hide();
//                            ref.offensive_popup.hide();
                            ref.offensive_popup.addClass("submitted");
                        }


                    }, ref, reason);
                }


//                ref.offensive_popup.find();
            });


            $(tthis.config.wrapper).off("click", "[data-action='offensive_popup_close']");
            $(tthis.config.wrapper).on("click", "[data-action='offensive_popup_close']", function () {
                var ref = ui.getActionReferences(this, tthis.config.comment);

                if(ref.offensive_popup && ref.offensive_popup.hasClass("submitted")){
                    ref.parent.find("[data-action='comment-offensive-already']").show();
                    ref.parent.find("[data-action='comment-offensive']").hide();
                }
                ref.offensive_popup.hide();

                return false;
            });

        };

        times_comments.prototype.attachReplyAction = function () {
            logger.log("Attaching Reply Action");
            var tthis = this;

            $(tthis.config.wrapper).off("click", "[data-action='all_responses']");
            $(tthis.config.wrapper).on("click", "[data-action='all_responses']", function(){
                var tc_level=$(this).data("level"),
                    tc_next=$(this).next();
                while(tc_next.data("level")!=tc_level)
                {   
                    tc_next.slideDown();
                    tc_next=tc_next.next();
                    if(!is.numberOnly(tc_next.data("level"))) {
                        break;
                    }
                }
                $(this).remove();
            });
            
            $(tthis.config.wrapper).off('click',"[data-action='more-response-length']");
            $(tthis.config.wrapper).on("click","[data-action='more-response-length']",function(){
                $(this).parent().hide();
                $(this).parent().next().show();
            });
            

            $(tthis.config.wrapper).off("click", "[data-action='comment-reply']");
            $(tthis.config.wrapper).on("click", "[data-action='comment-reply']", function () {
                var ref = ui.getActionReferences(this, tthis.config.comment);
				
                var parent = ref["parent"];
                if (ref["comment-reply"]) {
                    ref["comment-reply"].show();
					if(is.defined(tthis.config.rating) && tthis.config.rating){
						if(userInfo.userRating != null && userInfo.userRating != undefined && userInfo.userRating != "") {
							$(tthis.config.wrapper + " [data-plugin='comment-rating']").val($(tthis.config.wrapper + " [data-plugin='rating-list'] [data-plugin='rating-vaue'][val="+userInfo.userRating+"]").html());
						}
					}
                } else {
                    if(commentReplyFormHTML){
						var commentForm = $(commentReplyFormHTML);
					} else {
						var commentForm = $(tthis.config.wrapper + " [data-plugin='comment-form']");
					}
                    var commentFormHTML = $("<div>" + commentForm.html()+ "</div>");
                    var user = login.getUser();
                    if (user) {
						commentFormHTML.find( "[data-plugin='user-thumb']" ).attr( "src", user.getThumb() );
						commentFormHTML.find( "[data-plugin='user-notloggedin']" ).hide();
						commentFormHTML.find( "[data-plugin='user-isloggedin']" ).show();
					} else {
						commentFormHTML.find( "[data-plugin='user-thumb']" ).attr( "src", tthis.config.default_user_icon );
						commentFormHTML.find( "[data-plugin='user-isloggedin']" ).hide();
						commentFormHTML.find( "[data-plugin='user-notloggedin']" ).show();
					}
					commentFormHTML.find("[data-clear='true']").remove();
                    var tmpl = $.templates( "<div class='" + commentForm.attr("class") + " reply' data-plugin='comment-reply'>" +commentFormHTML.html() + "</div>");
                    parent.append(tmpl.render([
                        {}
                    ]));
					if(is.defined(tthis.config.rating) && tthis.config.rating){
						//$('.readrate').remove();
						if(userInfo.userRating != null && userInfo.userRating != undefined && userInfo.userRating != "") {
							$(tthis.config.wrapper + " [data-plugin='comment-rating']").val($(tthis.config.wrapper + " [data-plugin='rating-list'] [data-plugin='rating-vaue'][val="+userInfo.userRating+"]").html());
						}
					}
                }
                //updated references
                ref = ui.getActionReferences(this, tthis.config.comment);
//                ref["comment-input"].keyup();
//                ref["comment-input"].focus();
                tthis.error("", ref["comment-error"]);
	            //Trigger placeholders
	            $( '[placeholder]' ).blur();
                return false;

            });

            $(tthis.config.wrapper).off("click", "[data-action='toggle_replies']");
            $(tthis.config.wrapper).on("click", "[data-action='toggle_replies']", function () {
                var t=$(this);
                var tc=t.parent();
                var tc_level=tc.data("level");
                var tc_next=tc.next();
                var tstatus=t.attr("data-togglereplies");
                if(is.numberOnly(tc_next.data("level")) && tstatus=="show") {
                    t.attr("data-togglereplies","hide");
                    while(tc_next.data("level")!=tc_level)
                    {
                        tc_next.slideDown();
                        tc_next=tc_next.next();
                        if(!is.numberOnly(tc_next.data("level"))) {
                            break;
                        }
                    }
                } else {
                    t.attr("data-togglereplies","show");
                    while(tc_next.data("level")!=tc_level)
                    {
                        tc_next.slideUp();
                        tc_next=tc_next.next();
                        if(!is.numberOnly(tc_next.data("level"))) {
                            break;
                        }
                    }
                }
            });

            $(tthis.config.wrapper).off('click','.loadmore');
            $(tthis.config.wrapper).on('click','.loadmore',function(){
                $(this).remove();
                if (tthis.config.loadonscroll == false){
                    tthis.loadComment(tthis.config.commentType, function (data) {
                    }, true, ++tthis.pageCount);
                }
            });

            $(tthis.config.wrapper).off("keyup", "[data-plugin='comment-input']");
            $(tthis.config.wrapper).on("keyup", "[data-plugin='comment-input']", function (e) {
                if (e.keyCode === 27) {//Esc Key
                    var ref = ui.getActionReferences(this, tthis.config.comment);
                    ref["comment-input"].val("");
                    ref["comment-reply"].hide();
                }
            });

            ui.maxlength("[data-plugin='comment-input']", tthis.config.maxchar, function (remainingChar, messageLength) {
                var ref = ui.getActionReferences(this, tthis.config.comment);
                var parent = ref["parent"];
                var charsrem = ref["comment-input-remaining"];
                charsrem.text(remainingChar>=0?remainingChar:0);

                if(tthis.config.messages.maxlength && messageLength > tthis.config.maxchar){
                    tthis.error(tthis.config.messages.maxlength, ref["comment-error"]);
                }  else{
                    tthis.error("", ref["comment-error"]);
                }

            }, tthis.config.wrapper);


            $(tthis.config.wrapper).off("click", "[data-action='comment-close']");
            $(tthis.config.wrapper).on("click", "[data-action='comment-close']", function () {
                var ref = ui.getActionReferences(this, tthis.config.comment);

                var reply = ref["comment-reply"];
                var input = ref["comment-input"];

                input.val("");
                reply.hide();
                tthis.error("", ref["comment-error"]);

                return false;//todo remove this
            });

//            var bbb = $("#badge_tmpl").render([{}]);
//            $(tthis.config.wrapper).on("mouseenter mouseleave","[data-plugin='comment-user-badge']",function(evt){
//                var badge = $(this);
//                var name = badge.attr("data-name");
//                 if(evt.type == "mouseenter"){
//                     logger.log("enter:"+name);
//                     badge.append(bbb);
//                 }else if(evt.type == "mouseleave"){
//                     logger.log("leave:"+name);
//                     $(badge).find(".badge_new").remove();
//                 }
//            });

            tthis.onLoaded(function (tthis) {
//                tthis =  _tthis;   //changing reference of comments input when another set of comments is loaded
//                if(tthis.commentsCacheDate){
                    tthis.updateCachedRating();
//                }

                if (tthis.followee) {
                    tthis.markFollowingAll();
                } else {
                    mytimes.getFollowee(function (data) {
                        tthis.followee = data;
                        tthis.markFollowingAll();
                    });
                }

                lazy.load();
                dynamic.uptime();
            });

            $(tthis.config.wrapper).off("click", "[data-plugin='comment-user-follow']");
            $(tthis.config.wrapper).on("click", "[data-plugin='comment-user-follow']", function () {
                var currentEle = $(this);
                var currentUser = login.getUser();
                var ref = ui.getActionReferences(this, tthis.config.comment);
                var userId = ref.parent.attr("data-userid");
                var userName = ref.parent.attr("data-user");
                if (currentUser && currentUser.getId() != userId) {
                    (function(userId,userName){
                        mytimes.followUser(userId, function (dataa) {
                            tthis.followee = tthis.followee || [];
                            var currFollowee = {_id:userId,F_N:userName} ;
                            tthis.followee.push(currFollowee);
                            tthis.markFollowingId(currFollowee);
                        });
                    }(userId,userName))
                } else {
                    logger.warn("Cannot follow self.");
                }
            });


            $(tthis.config.wrapper).off("click",
                "[data-plugin='comment-post'],[data-plugin='comment-facebook-post'],[data-plugin='comment-twitter-post'],[data-plugin='comment-google-post'],[data-plugin='comment-email-post']");
            $(tthis.config.wrapper).on("click",
                "[data-plugin='comment-post'],[data-plugin='comment-facebook-post'],[data-plugin='comment-twitter-post'],[data-plugin='comment-google-post'],[data-plugin='comment-email-post']", function () {
                    event.publish("comment.post.start");
                    var ref = ui.getActionReferences(this, tthis.config.comment);

                    var parent = ref["parent"];
                    var reply = ref["comment-reply"];
                    var errorElement = ref["comment-error"];
                    var reply_input = ref["comment-input"];


                    var parentid = parent.attr("data-id");
                    var parentlevel = parent.attr("data-level");

                    var rootid = parent.attr("data-id");
                    var rootlevel = parent.attr("data-level");

                    var preventInfinite = 10;
                    var prevParent = parent;
                    while (rootlevel > 1 && --preventInfinite > 0) {
                        prevParent = prevParent.prev();
                        if (prevParent.attr("data-plugin") === "comment") {
                            rootid = prevParent.attr("data-id");
                            rootlevel = prevParent.attr("data-level");
                        } else {
                            break;
                        }
                    }

                    tthis.error("", errorElement);

                    var val = (reply_input.val() === reply_input.attr("placeholder"))?"":reply_input.val().trim();

//                    val = $("<div/>").text(val).html(); // encoded html content

                    tthis.add(val, parentid, rootid, rootlevel, function (commentObj, errMsg) {
                        tthis.renderComment(commentObj, errMsg);
                        event.publish("comment.post.end");
                    }, false, this);
//                reply_input.val("");
                    return false; //todo remove
                });

        };

        times_comments.prototype.markFollowingAll = function () {
            var tthis = this;
            var followees = tthis.followee;
            var comments = $(tthis.config.wrapper);
            var user = login.getUser();
            if (user) {
                logger.info("Showing follow links");
                comments.find("[data-plugin='comment-user-follow_wrapper']").show();

                //Hide current user follow
                //Start Added by Amit
                //comments.find("[data-plugin='comment'][data-userid!='" + user.getId() + "'] [data-plugin='comment-user-follow_wrapper']").show();
                comments.find("[data-plugin='comment'][data-userid ='" + user.getId() + "'] [data-plugin='comment-user-follow_wrapper']").hide();
//                comments.find("[data-plugin='comment'][data-userid !='" + user.getId() + "'] [data-plugin='comment-user-follow_wrapper']").removeClass('dont_show');
                // End Added by Amit
                //Mark following
                util.each(followees, function (i, followee) {
                    tthis.markFollowingId(followee);
                });
            } else {
                //Added by Amit
                comments.find("[data-plugin='comment-user-follow_wrapper']").hide();
                logger.info("Follow links : Not logged in.");
            }
        };

        times_comments.prototype.markFollowingId = function (followee) {
            var tthis = this;
            var user = login.getUser();
            var comments = $(tthis.config.wrapper);
            if (user) {
                var foloweeEle = comments.find("[data-userid='" + followee._id + "'] .follow");

                if (foloweeEle && foloweeEle.length > 0) {
                    logger.info("Mark Following : " + user.getFullName() + "(" + user.getId() + ") >> " + followee.F_N + "(" + followee._id + ")");
                    tthis.markFollowing(foloweeEle);
                } else {
                    logger.info("Mark Following : " + user.getFullName() + "(" + user.getId() + ") >> " + followee.F_N + "(" + followee._id + ") " + " - NA");
                }
            } else {
                logger.info("Follow link : Not logged in.");

            }
        };

        times_comments.prototype.markFollowing = function (currentEle) {
            if (currentEle && currentEle.length > 0) {
                currentEle.html("<span class='divider'></span><i class='icon-follow'></i>Following");
                currentEle.removeClass("follow");
                currentEle.addClass("following");
                currentEle.removeAttr("data-plugin");
                currentEle.attr("title", currentEle.attr("title").replace("Follow", "Following"));
            }
        };

        times_comments.prototype.onLoaded = function (callback) {
            event.subscribe("comments.loaded", callback);
        };

        var savedComment = null;
        times_comments.prototype.saveComment = function (comment) {
            savedComment = comment;
            cookie.set("comment" + window.msid, json.stringify(comment), 1, document.location.pathname);
        };

        times_comments.prototype.getSavedComment = function () {
            var commentCookie = cookie.get("comment" + window.msid);
            if (is.defined(commentCookie)) {
                return json.parse(commentCookie);
            }else if(savedComment){
                return savedComment;
            } else {
                return null;
            }
        };

        times_comments.prototype.removeSavedComment = function () {
            savedComment = null;
            return cookie.remove("comment" + window.msid, document.location.pathname);
        };

        times_comments.prototype.loadSavedComment = function () {
            var tthis = this;
            var user = login.getUser();
            var savedComment = tthis.getSavedComment();
            if (savedComment) {
                if(user){
                    if(!tthis.config.rating || savedComment.urs != ''){
                        savedComment.urs = tthis.config.rating ? $.trim(savedComment.urs) : '';
	                    logger.log("Has saved comment and user is logged in.");
	                    logger.log(savedComment);
	                    tthis.attachUserToComment(savedComment, user);
	//                    if(savedComment.type != "comment-email-post") {
	                        tthis.removeSavedComment();
	                        tthis.post(savedComment, function (savedComment, errMsg) {
	                            tthis.renderComment(savedComment, errMsg);	
	                        });
	//                    }
					}	
                } else{
                    logger.log("Saved comment but user not logged in");
                }
            } else{
                logger.log("No Saved comment.");
            }
        };

        times_comments.prototype.verifyEmailComment = function () {
            var url_params = util.getParam();

//            if(!is.empty(url_params.messageid) && !is.empty(url_params.r)){

                var tthis = this;
                tthis.verify( function (commentObj, errMsg) {
                    if(is.object(commentObj)){
                        tthis.renderComment(commentObj, errMsg, true);
                        $(tthis.config.wrapper + " [data-plugin='comment-verified-msg']").show();
                        tthis.removeSavedComment();
                    }
                });
//            }

        };

        times_comments.prototype.saveRatingValidation = function (opinionid, typeid, rateid, val, ref) {
            logger.log("Save Rating: " +this.config.commentType+":" + opinionid);

            var user = login.getUser();
            var userId = user?user.getId():0;

            var rating = ["rateV", opinionid, typeid, rateid];
//            var ratingC = ["rateC", opinionid, typeid, rateid];
            cookie.set(rating.join(":"), val || "1", 1, document.location.pathname);
//            cookie.set(ratingC.join(":"), val || "1", (1/24/60)*7, document.location.pathname);//7 minutes
//            this.commentsCacheUpdated.push(ratingC.join(":"));
        };

        times_comments.prototype.getSavedRatingValidation = function (opinionid, typeid, rateid, ref) {
            var user = login.getUser();
            var userId = user?user.getId():0;

            var rating = ["rateV", opinionid, typeid, rateid];
            var commentCookie = cookie.get(rating.join(":"));
            if (is.defined(commentCookie)) {
                return commentCookie;
            } else {
                return null;
            }
        };

        times_comments.prototype.updateCachedRating = function () {
            var tthis = this;
            var cookies = cookie.getAll();
            var comments = $(tthis.config.wrapper + " #" +this.config.commentType);
            util.each(cookies,function(key,value){
                if(key.indexOf("rateV") === 0/* && tthis.commentsCacheUpdated.indexOf(key) < 0 */) {
//                    if(value == tthis.commentsCacheDate){
                        logger.log("Updating Rating from cookie: " + key);
//                        tthis.commentsCacheUpdated.push(key);
                        var keyArr = key.split(":");
                        var rating = {
                            opinionid:parseInt(keyArr[1],10),
                            typeid:parseInt(keyArr[2],10),
                            rateid:parseInt(keyArr[3],10),
                            commentType:tthis.config.commentType
                        };
                        //todo merge two if cases
                        var eleType = "";
                        switch(rating.typeid){
                            case CONSTANT.RATE_TYPE.AGREE:
                                eleType = "comment-agree-count";
                                break;
                            case CONSTANT.RATE_TYPE.DISAGREE:
                                eleType = "comment-disagree-count";
                                break;
                        }
                        if(eleType != ""){
                            var commentRateCount = comments.find("[data-id='" + rating.opinionid + "']  [data-plugin='" + eleType + "']");
                            var val = parseInt(commentRateCount.text(),10);
                            var cookieVal = parseInt(cookie.get(key),10);
                            if(cookieVal > val){
                                commentRateCount.text(cookieVal);
                                logger.log("Updated Rating from cookie: "+key+" : " + val + " updated to " +cookieVal);
                            }else{
                                //It is already updated
                                logger.log(key+" Already Updated");
                            }
                        }
//                    }else{//Remove cookie because comments have refreshed
                        //cookie.remove(key);
//                    }
                }
            });
        };


//        toi_comments.prototype.saveRating = function (opinionid, typeid, rateid, val, ref) {
//            var rating = ["rate", opinionid, typeid, rateid];
//            cookie.set(rating.join(":"), val || "1", 1, document.location.pathname);
//        };
//
//        toi_comments.prototype.getSavedRating = function (opinionid, typeid, rateid, ref) {
//            var rating = ["rate", opinionid, typeid, rateid];
//            var commentCookie = cookie.get(rating.join(":"));
//            if (is.defined(commentCookie)) {
//                return commentCookie;
//            } else {
//                return null;
//            }
//        };

//        toi_comments.prototype.removeSavedRating = function (opinionid, typeid, rateid) {
//            var rating = ["rate", opinionid, typeid, rateid];
//            return cookie.remove(rating.join(":"), document.location.pathname);
//        };
//
//        toi_comments.prototype.loadSavedRating = function (opinionid, typeid, rateid) {
//            var tthis = this;
//            var user = login.getUser();
//            var savedRating = tthis.getSavedRating(opinionid, typeid, rateid);
//            if (savedRating && user) {
//                logger.log("Has saved rating and user is logged in.");
//                logger.log(savedRating);
//
//                tthis.rate(savedRating.opinionid, savedRating.typeid, savedRating.rateid, function (savedComment) {
//                });
//            }
//        };

        times_comments.prototype.validateRating = function (opinionid, typeid, rateid, ref, callback) {
            logger.log("Validate Rating: " + opinionid);
            var config = this.config;
            var user = login.getUser();
            var savedRating = this.getSavedRatingValidation(opinionid, typeid, rateid, ref);
            if (ref) {
                if (user && ref.parent && ref.parent.attr("data-userid") == user.getId()) { //Own comment
                    switch (typeid) {
                        case CONSTANT.RATE_TYPE.AGREE:
                            callback(config.messages.self_agree);
                            break;
                        case CONSTANT.RATE_TYPE.DISAGREE:
                            callback(config.messages.self_disagree);
                            break;
                        case CONSTANT.RATE_TYPE.RECOMMEND:
                            callback(config.messages.self_recommend);
                            break;
                        case CONSTANT.RATE_TYPE.OFFENSIVE:
                            callback(config.messages.self_offensive);
                            break;
                    }
                } else if (savedRating && savedRating.length > 0) {
                    switch (typeid) {
                        case CONSTANT.RATE_TYPE.AGREE:
                            callback(config.messages.already_agree);
                            break;
                        case CONSTANT.RATE_TYPE.DISAGREE:
                            callback(config.messages.already_disagree);
                            break;
                        case CONSTANT.RATE_TYPE.RECOMMEND:
                            callback(config.messages.already_recommended);
                            break;
                        case CONSTANT.RATE_TYPE.OFFENSIVE:
                            callback(config.messages.already_offensive);
                            break;
                    }
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.AGREE, rateid, ref) && typeid == CONSTANT.RATE_TYPE.DISAGREE) {
                    callback(config.messages.cant_agree_disagree);
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.DISAGREE, rateid, ref) && typeid == CONSTANT.RATE_TYPE.AGREE) {
                    callback(config.messages.cant_agree_disagree);
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.AGREE, rateid, ref) && typeid == CONSTANT.RATE_TYPE.OFFENSIVE) {
                    callback(config.messages.cant_agree_offensive);
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.OFFENSIVE, rateid, ref) && typeid == CONSTANT.RATE_TYPE.AGREE) {
                    callback(config.messages.cant_agree_offensive);
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.DISAGREE, rateid, ref) && typeid == CONSTANT.RATE_TYPE.RECOMMEND) {
                    callback(config.messages.cant_disagree_recommend);
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.RECOMMEND, rateid, ref) && typeid == CONSTANT.RATE_TYPE.DISAGREE) {
                    callback(config.messages.cant_disagree_recommend);
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.OFFENSIVE, rateid, ref) && typeid == CONSTANT.RATE_TYPE.RECOMMEND) {
                    callback(config.messages.cant_recommend_offensive);
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.RECOMMEND, rateid, ref) && typeid == CONSTANT.RATE_TYPE.OFFENSIVE) {
                    callback(config.messages.cant_recommend_offensive);
                } else {
                    callback();
                }
            } else {
                logger.error("Cannot validate rating. 'ref' is undefined.");
            }
        };

        times_comments.prototype.validateComment = function (commentObj, callback, ref) {
            event.publish("comment.post.validating");
            var comment = commentObj.comment;
            var config = this.config;

            logger.info("Validating Comment: " + comment);
			commentObj.urs = (commentObj.urs === '') ? undefined : commentObj.urs ; 
            if (is.empty(comment)) {
                callback(config.messages.blank);
            } else if (is.url(comment)) {
                callback(config.messages.has_url);
            } else if (comment.length < config.validation.minlength) {
                callback(config.messages.minlength);
            } else if(is.defined(this.config.rating) && this.config.rating && (!is.defined(commentObj.urs))){
                callback(config.messages.ratemendate);
            } else{
                var msgObj = this.commentObjToMsgObj(commentObj);
//                var validate_url = config.validate_url;
                msgObj.message = msgObj.message.toLowerCase();
                api.validateComment(msgObj,function (response) {
                    if (response === "duplicatecontent") {
                        event.publish("comment.post.validation.failed",response);
                        callback(config.messages.duplicate);
                    } else if (response === "abusivecontent") {
                        event.publish("comment.post.validation.failed",response);
                        callback(config.messages.abusive);
                    } else {
                        event.publish("comment.post.validated");
                        callback();
                    }
                });

//                ajax.post(validate_url, msgObj, function (response) {
//                    if (response === "duplicatecontent") {
//                        event.publish("comment.post.validation.failed",response);
//                        callback(config.messages.duplicate);
//                    } else if (response === "abusivecontent") {
//                        event.publish("comment.post.validation.failed",response);
//                        callback(config.messages.abusive);
//                    } else {
//                        event.publish("comment.post.validated");
//                        callback();
//                    }
//                }).error(function(event,xhr, e){
//                    event.publish("comment.post.error",e);
//                });
            }
        };

        times_comments.prototype.add = function (msg, parentId, rootId, level, callback, cnsl, currentEle) {
            logger.info("Adding Comment: " + msg);
			var tthis = this;
	        $(tthis.config.wrapper + " [data-plugin='comment-verified-msg']").hide(); //todo move to reset function           

            var config = this.getConfig();
            var user = login.getUser();

            var ref = ui.getActionReferences(currentEle, tthis.config.comment);//todo optimize
			
			/*if($("[data-plugin='comment-rating']").length > 0){
				var rating_input = $("input[data-plugin='comment-rating']").val().trim();
				 if ($("input[data-plugin='comment-rating']:first").val().trim() == "Rate this movie") {
					this.error("Please rate before review", ref["comment-error"]);
					return;
				}
			}*/

            var commentObj = {
                index: -1,
                id: -1,
                comment: msg.trim(),
                level: level || 1,
                parentId: parentId || 0,
                rootId: rootId || 0,
                parentuid: ref.parent ? ref.parent.attr("data-id") : null,
                parentusername: ref.parent ? ref.parent.attr("data-user") : null,
                type: currentEle ? $(currentEle).attr("data-plugin") : "comment-post",
                time: new Date().getTime(), //todo user server time,
                configid:"41083278",
                url:window.location.href.split("?")[0].split("#")[0],
                rotype: typeof tthis.config.rotype == 'undefined' ? 0 : tthis.config.rotype, // for article by default
				urs: typeof userInfo === 'object' ?  userInfo.userRating : ''
				
            };
			/*if(typeof userInfo === 'object'){
				commentObj.urs = userInfo.userRating;
			}*/
            var facebookCheckbox = ref["comment-facebook"];
            var twitterCheckbox = ref["comment-twitter"];

            commentObj.social = [];
            if (facebookCheckbox && facebookCheckbox.is(':checked')) {
                commentObj.social.push("facebook");
            }

            if (twitterCheckbox && twitterCheckbox.is(':checked')) {
                commentObj.social.push("twitter");
            }



            if(!user){
                var cmtUser = (commentObj.type == "comment-post"?this.getCommentUser(ref):null);
                if(is.object(cmtUser))  {
                    user = cmtUser;
                }else if(cmtUser){
                    return;
                }
            }


            if (user) {
                
                
                tthis.attachUserToComment(commentObj, user);

                if (cnsl === true) {
//                    console.log(msgObj);
                    if (callback) {
                        callback(commentObj);
                    }
                } else {
                    tthis.post(commentObj, callback);
                }
                //todo add class .highlight
            } else {
//                this.validateComment(commentObj, function (error) {
//                    if (!is.empty(error)) {
//                        if (callback) {
//                            callback(commentObj, error);
//                        }
//                        return;
//                    }

                    switch (commentObj.type){
                        case "comment-facebook-post":
                            tthis.commentOnFacebook(commentObj,ref);
                            break;
                        case "comment-google-post":
                            tthis.commentOnGoogle(commentObj,ref);
                            break;
                        case "comment-twitter-post":
                            tthis.commentOnTwitter(commentObj,ref);
                            break;
                        case "comment-email-post":
                            tthis.commentUsingEmail(commentObj,ref);
                            break;
                        default:
                            tthis.commentWithoutLogin(commentObj,ref);
                            break;
                    }

//                },ref);
            }


            /*
             //http://timesofindia.indiatimes.com/fbpostcomment.cms?article=http://timesofindia.indiatimes.com/city/delhi/Thieves-who-stole-judges-car-held/articleshow/26115345.cms&access_token=CAABrIHwVZA2UBACX0gAvHb8VBrOCmK6F3fmNZCSY8UL86A9wjUBwZAZChjmV3Np4VaBV41DYJNhagZA83pipQaacl66yYxNSzVsoZCi2CqWZCgfT22WfYHf9f3JzgYD11LxptjZCxZCjDdxdZA6K5BcLijMNDCv8JScHqmoMrOq9F6l5C2G5VDP3v2q699gbfwPtYo3YSGx8CLrwZDZ
             */
        };

        times_comments.prototype.getCommentUser = function(ref) {
            var config = this.config;

            var $get_user_name = ref['get-user-name'];
            var $get_user_email = ref['get-user-email'];
            var $get_user_location = ref['get-user-location'];
            var $set_user_captcha =ref['set-user-captcha'];
            var $get_user_captcha = ref['get-user-captcha'];

            if($get_user_name && $get_user_email && $get_user_name.length > 0 && $get_user_email.length > 0){
                var user = {};

                user.name = $get_user_name.val().trim();
                user.email = $get_user_email.val().trim();
                if($get_user_location && $get_user_location.length > 0){
                    user.location = $get_user_location.val().trim();
                }

                if(user.name.length == 0 || user.name == $get_user_name.attr("placeholder")){
                    this.error(config.messages.name_required, ref["comment-error"]);
                    return true;
                }

                if(user.name.length > 30){
                    this.error(config.messages.name_toolong, ref["comment-error"]);
                    return true;
                }

                if(!is.alphaOnly(user.name)){
                    this.error(config.messages.name_not_string, ref["comment-error"]);
                    return true;
                }

                if($get_user_location && $get_user_location.length > 0){
                    if(user.location.length == 0 || user.location == $get_user_location.attr("placeholder")){
                        this.error(config.messages.location_required, ref["comment-error"]);
                        return true;
                    }

                    if(user.location.length > 30){
                        this.error(config.messages.location_toolong, ref["comment-error"]);
                        return true;
                    }

                    if(!is.alphaOnly(user.location)){
                        this.error(config.messages.location_not_string, ref["comment-error"]);
                        return true;
                    }
                }

                if(user.email.trim().length == 0 || user.email == $get_user_email.attr("placeholder")){
                    this.error(config.messages.email_required, ref["comment-error"]);
                    return true;
                }

                if(!is.email(user.email.trim())){
                    this.error(config.messages.email_invalid, ref["comment-error"]);
                    return true;
                }

                if($get_user_captcha && $get_user_captcha.val().trim().length == 0 ){
                    this.error(config.messages.captcha_required, ref["comment-error"]);
                    return true;
                }

                if($get_user_captcha && $get_user_captcha.val().trim().length > 4){
                    this.error(config.messages.captcha_toolong, ref["comment-error"]);
                    return true;
                }

                if($get_user_captcha && !is.numberOnly($get_user_captcha.val().trim())){
                    this.error(config.messages.captcha_number_only, ref["comment-error"]);
                    return true;
                }

                if($set_user_captcha && $get_user_captcha &&!eval($set_user_captcha.text() + "=" + $get_user_captcha.val())){
                    this.error(config.messages.captcha_invalid, ref["comment-error"]);
                    return true;
                }



                return userClass.getNewUser(user,{
                        "id": "id",
                        "username": "username",
                        "thumb": "thumb",
                        "email": "email",
                        "name": "name",
                        "fullName": "name",
                        "CITY": "location"
                });

//                app: "toiipad",
//                    useripaddress: "202.134.162.148",
//                    location: "India",
//                    fromname: "Lawrance",
//                    fromaddress: "darrylthebest@gmail.com",
//                    rotype: 0

            }
        };
        times_comments.prototype.commentOnFacebook = function (commentObj, ref) {
            var tthis  = this;
            var domainOnly = util.getDomainOnly();
//            commentObj.social.push("facebook");
            tthis.saveComment(commentObj);
            cookie.set("clickkepfbtart", "1", 1, "/", domainOnly);
            cookie.set("clickkepfbtart" + window.msid, window.msid, 1, "/", domainOnly);
            login.loginWithFacebook(function(){
                tthis.loadSavedComment();
            });
        };

        times_comments.prototype.commentOnGoogle = function (commentObj, ref) {
            var tthis  = this;
//            commentObj.social.push("google");
            tthis.saveComment(commentObj);
            login.loginWithGoogle(function(){
                tthis.loadSavedComment();
            });
        };

        times_comments.prototype.commentOnTwitter = function (commentObj, ref) {
            var tthis  = this;
            var domainOnly = util.getDomainOnly();
//            commentObj.social.push("twitter");
            tthis.saveComment(commentObj);
            cookie.set("clickkeptwtart", "1", 1, "/", domainOnly);
            cookie.set("clickkeptwtart" + window.msid, window.msid, 1, "/", domainOnly);
            login.loginWithTwitter(function(){
                tthis.loadSavedComment();
            });
        };

        times_comments.prototype.commentUsingEmail = function (commentObj, ref) {
            var tthis  = this;
            var domainOnly = util.getDomainOnly();
            tthis.saveComment(commentObj);
            cookie.set("clickkepssoart", "1", 1, "/", domainOnly);
            cookie.set("clickkepssoart" + window.msid, window.msid, 1, "/", domainOnly);
            login.login(function(){
                tthis.loadSavedComment();
            });
        };

//        toi_comments.prototype.commentWithoutLogin = function (commentObj, ref) {
//            if (window.resetRegisterForm1 && window.putMathQ && window.lightbox2n) {    //todo, this is toi specific and is bad code, couldn't help it
//                window.resetRegisterForm1();
//                $("#registerForm1 #comments").val(commentObj.comment);
//                $("#registerForm1 #parentid").val(commentObj.parentId);
//                $("#registerForm1 #rootid").val(commentObj.rootId);
//
//                window.putMathQ(4);
//                window.lightbox2n();
//                window.scrollTo(0, 0);
//
//                var input = ref["comment-input"];
//                if (input) {
//                    input.val("");
//                    input.keyup();
////                    input.focus();
//                }
//            } else {
//                logger.error("'window.resetRegisterForm1 or window.putMathQ or window.lightbox2n' is not defined.");
//            }
//            this.loadSavedComment();
//        };


        times_comments.prototype.commentWithoutLogin = function (commentObj, ref) {
            var tthis = this;
            var domainOnly = util.getDomainOnly();

            tthis.saveComment(commentObj);
            cookie.set("clickkepssoart","1",1,"/",domainOnly);
            cookie.set("clickkepssoart"+window.msid,window.msid,1,"/",domainOnly);
            login.login(function(){
                if(!(tthis.config.rating)){
                    tthis.loadSavedComment();
                }
            });
        };
        /**
         * Verify comment posted using email
         *
         * @param commentObj
         * @param callback
         */
        times_comments.prototype.verify = function (callback, ref) {
            //http://timesofindia.indiatimes.com/cmtverified/29255491.cms?cmtid=23834874&r=1393492740078
            var tthis = this;
            var url = tthis.config.verify_comment_url;

            var msgObj = {
                msid:window.msid,
                cmtid:null,//ActionParams > messageid
                r:null//ActionParams > r
            };

            var url_params = util.getParam();

            msgObj.cmtid = url_params.messageid;
            msgObj.r = url_params.r;

            if(msgObj.msid && msgObj.cmtid && msgObj.r){
	            logger.info("Verifying Comment.");

	            ajax.get(url, msgObj, function (response) {
                   if( response.cmtverified.commentbyid &&  response.cmtverified.commentbyid.roaltdetails){
                       var cmt = response.cmtverified.commentbyid;
                       cmt.roaltdetails =  cmt.roaltdetails.roaltdetails;

                       var commentObj = {
                           index: -1,
                           id: -1,
                           comment: is.string(cmt.message) ? $("<div/>").text(cmt.message).html() : "",
                           level: 1,
                           parentuid: cmt.parentuid,
                           parentusername: cmt.parentusername,
                           abstime: msgObj.r ? util.getDate(msgObj.r).getTime():"",//1384934349412,    //cmt.rodate //19 Nov, 2013 01:04 PM
                           time: msgObj.r,
                           type: "comment-post",
                           user: {
                               name: cmt.roaltdetails.fromname ,
                               location: cmt.roaltdetails.location
                           }
                       };


                        if (callback) {
                            callback(commentObj);
                        }
                   }else{
                       if (callback) {
                           callback(null,{});
                       }
                   }
                });
            }

	        if(url_params.register === "1"){
		        logger.info("Display register window.");

		        if(!login.getUser()){
		            login.login();
		        }
	        }
        };
        times_comments.prototype.post = function (commentObj, callback, ref) {
            var tthis = this;
//            var url = this.config.post_url;


            

//            if (!is.empty(commentObj.social)) {
//                if (commentObj.social.indexOf("facebook") > -1) {//todo use common function
//                    tthis.postToFacebook(commentObj.comment, function () {
//                        if (commentObj.social.indexOf("twitter") > -1) {//open twitter dialog after fb dialog closes
//                            tthis.postToTwitter(commentObj.comment);
//                        }
//                    });
//                } else if (commentObj.social.indexOf("twitter") > -1) {
//                    tthis.postToTwitter(commentObj.comment);
//                } else if (commentObj.social.indexOf("google") > -1) {
//                    tthis.postToGoogle(commentObj.comment);
//                }
//            }


            /*if (!is.empty(commentObj.social)) {
                if(is.defined(tthis.config.rating) && tthis.config.rating){
                    if( is.defined(tthis.config.moviename) && tthis.config.moviename){
                        // for movie review
                        var socialtext = "Movie: "+tthis.config.moviename+", My Rating "+(commentObj.urs/2)+"/5. "+commentObj.comment;
                        tthis.postToSocial(socialtext,commentObj.social.join(","));
                    }
                }
                else{
                    tthis.postToSocial(commentObj.comment,commentObj.social.join(","));
                }
            }*/

            if (!is.empty(commentObj.social)) {
                if(is.defined(tthis.config.rating) && tthis.config.rating ){
                    if(is.defined(tthis.config.moviename) && tthis.config.moviename){
                        // for movie review
                        var socialtext = "Movie: "+tthis.config.moviename+", My Rating "+(commentObj.urs/2)+"/5. "+commentObj.comment;
                        tthis.postToSocial(socialtext,commentObj.social.join(","));
                    }
                    else if(is.defined(tthis.config.techprdtname) && tthis.config.techprdtname){
                        // for tech prdt review
                        var socialtext = tthis.config.techprdtname+" : "+"My Rating "+(commentObj.urs/2)+"/5, "+commentObj.comment;
                        tthis.postToSocial(socialtext,commentObj.social.join(","));
                    }
                }
                else{
                    tthis.postToSocial(commentObj.comment,commentObj.social.join(","));
                }
            }


            //todo move to api
            tthis.validateComment(commentObj, function (error) {
                if (!is.empty(error)) {
                    if (callback) {
                        callback(commentObj, error);
                    }
                    return;
                }
                event.publish("comment.post.posting");
                logger.info("Posting Comment:" + commentObj.comment);


                var msgObj = tthis.commentObjToMsgObj(commentObj);
                msgObj.pcode = page.getChannel();

                //todo implemente and check commented code.
                if(tthis.config.sendCommentLiveEmail === false){
                    msgObj.verifyuser = 1; //comment is posted, email is not sent
	                api.postCommentWithoutVerification(msgObj,function (response) {
		                event.publish("comment.post.posted");
		                if (callback) {
			                callback(commentObj);
		                }
	                });
                }else{
                    if(tthis.config.enableparam  && msgObj.rotype != 0){
                        msgObj.url = msgObj.url + '?' + window.location.search.replace("?", "").split("&")[0] + '&';
                        msgObj.configid = '46516605'; // to handle param include
                    }
                    else if(msgObj.rotype != 0){
                        msgObj.url = msgObj.url + '?';
                    }
                    api.postComment(msgObj,function (response) {
                        event.publish("comment.post.posted");
                        if (callback) {
                            callback(commentObj);
                        }
                    });
                }

//                ajax.post(url, msgObj, function (response) {
//                        event.publish("comment.post.posted");
//                        if (callback) {
//                            callback(commentObj);
//                        }
//                    })
//                    .error(function(event,xhr,e){
//                        event.publish("comment.post.error",e);
//                    });

            },ref);
        };

        times_comments.prototype.commentObjToMsgObj = function (commentObj) {
            var config = this.config;//getConfig();

            var msgObj = {
                //                    hostid:83,//259:travel
                //                    rchid:-2128958273,//2147477992:travel
                fromname: commentObj.user ? commentObj.user.name : null,
                fromaddress: commentObj.user ? commentObj.user.email : null,
                userid: commentObj.user ? (commentObj.user.id?commentObj.user.uid:"qrst") : "qrst", // todo use SSO
//                fbemailid: commentObj.user ? commentObj.user.email : null,//todo use fb email
                location: commentObj.user ? commentObj.user.location : null,
                imageurl: commentObj.user ? commentObj.user.image : null,
                loggedstatus: commentObj.user && commentObj.user.id ? 1 : 0,

                message: commentObj.comment,
                roaltdetails: 1,
                ArticleID: config.msid,
                msid: config.msid,
                parentid: commentObj.parentId,
                rootid: commentObj.rootId,
                url:commentObj.url,
                configid:commentObj.configid,
				urs:commentObj.urs ? commentObj.urs : null,
				rotype:commentObj.rotype
            };

            if(!commentObj.user.id){
                msgObj.verifyuser = 1; //To prevent sending "You comment is live mail" in case of non-logged in user.
            }
            return msgObj;
        };

        times_comments.prototype.error = function (errMsg, wrapper) {
            if (is.visible(wrapper)) {
                wrapper.text(errMsg);
            } else if (!is.empty(errMsg)) {
                alert(errMsg);
            }
            event.publish("comment_error");
        };

        times_comments.prototype.rateAgree = times_comments.prototype.rateUpVote = function (opinionid, callback, ref) {
//            login.loginType(15);
            logger.log("Agree Comment: " + opinionid);
            this.rate(opinionid, CONSTANT.RATE_TYPE.AGREE, 0, callback, ref);
//            login.loginType();
            //http://timesofindia.indiatimes.com/ratecomment_new.cms?opinionid=22554141&typeid=100&rateid=1
            //http://myt.indiatimes.com/mytimes/addActivity?activityType=Agreed&appKey=TOI&parentCommentId=22554141&baseEntityType=ARTICLE&objectType=A&url=
        };

        times_comments.prototype.rateDisagree = times_comments.prototype.rateDownVote = function (opinionid, callback, ref) {
//            login.loginType(16);
            logger.log("Disagree Comment: " + opinionid);
            this.rate(opinionid, CONSTANT.RATE_TYPE.DISAGREE, 0, callback, ref);
//            login.loginType();
            //http://timesofindia.indiatimes.com/ratecomment_new.cms?opinionid=22587427&typeid=101&rateid=1
            //http://myt.indiatimes.com/mytimes/addActivity?activityType=Disagreed&appKey=TOI&parentCommentId=22587427&baseEntityType=ARTICLE&objectType=A&url=
        };

        times_comments.prototype.rateRecommend = function (opinionid, callback, ref) {
//            login.loginType(17);
            logger.log("Recommend Comment: " + opinionid);
            this.rate(opinionid, CONSTANT.RATE_TYPE.RECOMMEND, 0, callback, ref);
//            login.loginType();
            //http://timesofindia.indiatimes.com/ratecomment_new.cms?opinionid=22587134&typeid=102&rateid=1
            //http://myt.indiatimes.com/mytimes/addActivity?activityType=Reccomended&appKey=TOI&parentCommentId=22587134&baseEntityType=ARTICLE&objectType=A&url=
        };

        times_comments.prototype.flag = times_comments.prototype.rateOffensive = function (opinionid, callback, ref, reason) {
            var tthis = this;
            logger.log("Flag/Offensive Comment: " + opinionid);

            $(ref.offensive_popup).off("click");
            $(ref.offensive_popup).on("click", function () {
                var ref = ui.getActionReferences(this, tthis.config.comment);
                var valReason = ref.offensive_popup.find("input:checked").val();
                if(valReason && valReason.length > 0 && valReason == "Others"){
                    ref.offensive_popup_reason.show();
                }else{
                    ref.offensive_popup_reason.hide();
                }

                tthis.error("",  ref["comment-offensive-error"]);

//                return false;
            });

            ref.offensive_popup.show();
            return;
            /*
             Find this comment offensive?
             Choose your reason below and click on the Submit button. This will alert our moderators to take action
             Reason for reporting:
             Foul language
             Defamatory
             Inciting hatred against a certain community
             Out of context/Spam
             Others

             */

//            //todo remove dependency on articleshow, use common rating.
//            var tthis = this;
//            //todo code repeated in rating also
//            var rating = {opinionid: opinionid, typeid: CONSTANT.RATE_TYPE.OFFENSIVE, rateid: 0};
//
//            var user = login.getUser();
//
//            if (user) {
//                this.validateRating(opinionid, rating.typeid, rating.rateid, ref, function (error) {
//                    if (!is.empty(error)) {
//                        if (callback) {
//                            callback(error);
//                        }
//                        return;
//                    }
//                    tthis.rate(opinionid, CONSTANT.RATE_TYPE.OFFENSIVE, rating.rateid, callback, ref);
//                });
//            } else {
//                (function (rating, callback, ref) {
//                    login.login(function () {
//                        tthis.rateOffensive(rating.opinionid, callback, ref);
//                    }, true);
//                }(rating, callback, ref));
//            }

            //http://timesofindia.indiatimes.com/offensiveService/offence.asmx/getOffencivecomment?ofusername=delsanic&ofreason=Out%20of%20context/Spam&ofcommenteroid=22586038&ofcommenthostid=83&ofcommentchannelid=-2128958273&ofcommentid=29127171&ofuserisloggedin=1&ofuserssoid=delsanic@gmail.com&ofuseremail=delsanic@gmail.com
            //http://timesofindia.indiatimes.com/ratecomment_new.cms?opinionid=22586038&typeid=103&rateid=1
        };

        times_comments.prototype.showRateOffensiveBox = function (opinionid, callback, ref) {
//            ref.current.find('[data-plugin="offensive_popup"]').show();
//            ref.current.find('[data-plugin="offensive_popup_submit"]').show();
//            ref.current.find('[data-plugin="offensive_popup_close"]').show();

        };

        times_comments.prototype.rate = function (opinionid, typeid, rateid, callback, ref, reason) {
            logger.log("Rate Comment: " + opinionid + ":" + typeid);
            var tthis = this;

            var rating = {opinionid: opinionid, typeid: typeid, rateid: rateid || 0};

            if(reason){
                rating.ofreason = reason;
            }

            var user = login.getUser();

            if (user) {
                logger.log("Rate Comment user is logged in");
                this.validateRating(opinionid, typeid, rateid, ref, function (error) {
                    if (!is.empty(error)) {
                        if (callback) {
                            callback(error);
                        }
                        return;
                    }
                    var val = parseInt(ref["comment-agree-count"].text(),10);
                    tthis.saveRatingValidation(opinionid, typeid, rateid, val + 1, ref);
                    api.rateComment(rating, function (resp) {
                        if (callback) {
                            callback(null, resp);
                        }
                    },user);
//                    ajax.get(tthis.config.rate_url, rating, function (resp) {
//                        if (callback) {
//                            callback(null, resp);
//                        }
//                    });
                });
            } else {
                logger.log("Rate Comment user is NOT logged in, login prompted.");
                (function (rating, callback, ref) {
                    if(tthis.config.loginRequiredForRating === true){
                        login.login(function () {
                            logger.log("Rate Comment user logged in, continue rating.");
                            tthis.rate(rating.opinionid, rating.typeid, rating.rateid, callback, ref, rating.reason);
                        }, true);
                    }else{
                        logger.log("Rate Comment user NOT logged in, continue rating.");
                        tthis.validateRating(rating.opinionid, rating.typeid, rating.rateid, ref, function (error) {
                            if (!is.empty(error)) {
                                if (callback) {
                                    callback(error);
                                }
                                return;
                            }
                            var val = parseInt(ref["comment-agree-count"].text(),10);
                            tthis.saveRatingValidation(rating.opinionid, rating.typeid, rating.rateid, val + 1, ref);
                            api.rateComment(rating, function (resp) {
                                if (callback) {
                                    callback(null, resp);
                                }
                            });

//                            ajax.get(tthis.config.rate_url, rating, function (resp) {
//                                if (callback) {
//                                    callback(null, resp);
//                                }
//                            });
                        });
                    }
                }(rating, callback, ref));
            }

        };

        times_comments.prototype.renderComment = function (commentObj, errMsg, verified) {
            var tthis = this;
            if (commentObj) {
                var ref = ui.getActionReferences(tthis.config.wrapper +" [data-id='" + commentObj.parentId + "']");

                var parent = ref["parent"];
                var input = ref["comment-input"];
                var facebookCheckbox = ref["comment-facebook"];
                var twitterCheckbox = ref["comment-twitter"];

                if (errMsg) {
                    event.publish("comment.post.error",errMsg);
                    this.error(errMsg, ref["comment-error"]);
                } else {
                    event.publish("comment.post.rendering", input);
                    commentObj.comment =  $("<div/>").text(commentObj.comment).html();
                    commentObj.user.rate = commentObj.urs;
                    if (this.isReply(commentObj)) {
                        var reply = ref["comment-reply"];						
                        //Convert to number if level is not a number
                        if(!is.number(commentObj.level)){
                            commentObj.level = parseInt(commentObj.level,10);
                        }
                        ++commentObj.level; //TODO check if level needs to be increased.

                        if((commentObj && commentObj.user && commentObj.user.id) || !ref['set-user-captcha']){
                            parent.after(this.renderEach(-1, commentObj, true));
                            reply.hide();
                        } else{
                            parent.after("<div class='comment-box highlight'>"+$(tthis.config.wrapper + " [data-plugin='comment-verification-msg']").html()+"</div>");
                            reply.hide();
                        }
                    } else {
                        if((commentObj && commentObj.user && commentObj.user.id) || verified == true || !ref['set-user-captcha']){
                            $(tthis.config.wrapper+' '+this.config.main).prepend(this.renderEach(-1, commentObj, true));
                        }else{
                            $(tthis.config.wrapper+' '+this.config.main).prepend("<div class='comment-box highlight'>"+$(tthis.config.wrapper + " [data-plugin='comment-verification-msg']").html()+"</div>");
                        }
                    }

                    if (input) {
                        input.val("");
                        input.keyup();
                        input.focus();
                        if(facebookCheckbox){
                            facebookCheckbox.prop('checked', false);
                        }
                        if(twitterCheckbox){
                            twitterCheckbox.prop('checked', false);
                        }

                        var $get_user_captcha = ref['get-user-captcha'];
                        var $set_user_captcha = ref['set-user-captcha'];
                        if($get_user_captcha){
                            $get_user_captcha.val("");
                        }
                        if($set_user_captcha){
                            $set_user_captcha.text(Math.floor((Math.random()*10)) +"+" +Math.floor((Math.random()*10)) +"=");
                        }
                    }
                    this.error("", ref["comment-error"]);
                }
				//
                lazy.load();
            } else {
                this.error(errMsg);
            }

        };

        times_comments.prototype.renderUsingTemplate = function (index, dataOne, tmpl, prepend) {
            return $("#" + tmpl).render(dataOne, {
                formatNumber: function (val) {
                    return util.formatNumber(val);
                },
                timeToDate:function(time){
                    return new Date(time);
                }, parseDate: function (time) {
                    if (jsonDate != null) {
                        var date = new Date(time);
                        var newDate = $.fullCalendar.formatDate(date, "MMM dd, yyyy");
                        return newDate;
                    }
                }
            });
        };

        times_comments.prototype.isReply = function (cmt) {
            return cmt.parentuid && cmt.parentusername;
        };

//        toi_comments.prototype.parseTmpl = function (data) {
//            return $("#tmpl1").render(data, { formatNumber: function (val) {
//                return util.formatNumber(val);
//            }});
//        };

        times_comments.prototype.loadingDiv = function (text) {
            var tthis = this;
            if (text === "Loading...") {
                $(tthis.config.wrapper + " [data-plugin='comment-loading']").show();
            } else {
                $(tthis.config.wrapper + " [data-plugin='comment-loading']").hide();
            }
        };

        return times_comments;
    });


/*
   Comment Logic
	  Non Logged In with email verification
	     URL: /postro.cms
		 fromname	Del Sanic
		 location	India
		 fromaddress	delsanic@gmail.com
		 message	old ui
		 userid	qrst
		 msid	30937855
		 ArticleID	30937855
		 roaltdetails	1
		 parentid	0
		 rootid	0
		 loggedstatus	0
		 verifyuser	1


	  Non logged in without email verification
	     URL:/postro_nover.cms
		 fromname	Del
		 fromaddress	delsanic@gmail.com
		 userid	qrst
		 location
		 imageurl
		 loggedstatus	0
		 message	wish tmpl
		 roaltdetails	1
		 ArticleID	30937855
		 msid	30937855
		 parentid	0
		 rootid	0
		 url	http://toidev.indiatimes.com/wish/30937855.cms
		 configid	41083278
		 verifyuser	1
		 pcode	TOI


 */
;
require({

shim: {
	// "jquery": {"exports":"jQuery"},
	// "json": {"exports":"JSON"} ,
	"jsrender": {
	"exports": "jQuery.fn.render",
	deps: [ 'jquery' ]
	}
},   
paths: {
        jsrender: "http://timesofindia.indiatimes.com/jsrender.cms?"
    } ,
  config:{
	 "tiljs/apps/times/login":{
         login_return_url:"http://"+location.hostname+"/phpsso",
         logout_return_url:"http://"+location.hostname+"/phpsso/?channel=ml&status=logout"
     },
     "tiljs/apps/times/comments":{
         share_url:"http://"+location.hostname+"/phpshare",
         comment_block_count:15,
         commentType:"comments",
         default_user_icon:"http://timesofindia.indiatimes.com/photo/29251859.cms",
         messages: {
			"name_required" :"പേര് രേഖപ്പെടുത്തുക",
			"location_required" :"സ്ഥലം രേഖപ്പെടുത്തുക",
			"captcha_required" :"ക്യാപ്ച്ച രേഖപ്പെടുത്തുക",
			"name_toolong" :"പേരിന്‍റെ ദൈർഘ്യം കൂടുതലാണ്",
			"name_not_string" :"പേരിൽ അക്ഷരങ്ങൾ മാത്രമേ ഉൾപ്പെടുത്താനാവൂ",
			"location_toolong" :"സ്ഥലപ്പേരിന്‍റെ ദൈർഘ്യം കൂടുതലാണ്",
			"location_not_string" :"സ്ഥലപ്പേരിൽ അക്ഷരങ്ങൾ മാത്രമേ ഉൾപ്പെടുത്താനാവൂ",
			"captcha_toolong" :"ക്യാപ്ച്ച 4 ക്യാരക്ടർ മാത്രമേ ഉള്ളൂ",
			"captcha_number_only" :"ക്യാപ്ച്ച അക്കങ്ങൾ മാത്രമാണ്",
			"email_required" :"ഇ മെയിൽ അഡ്രസ് രേഖപ്പെടുത്തുക",
			"email_invalid" :"ദയവായി വാലിഡ് ആയ ഒരു ഇ മെയിൽ അഡ്രസ് രേഖപ്പെടുത്തുക",
			"captcha_invalid" :"ദയവായി വാലിഡ് ആയ ഒരു ക്യാപ്ച്ച രേഖപ്പെടുത്തുക",
			"minlength": "കമന്‍റിന് ദൈർഘ്യം വളരെ കുറവായതിനാൽ പോസ്റ്റ് ചെയ്യാനാവില്ല",
			"blank": "കമന്‍റ് ബ്ലാങ്ക് ആയതിനാൽ പോസ്റ്റ് ചെയ്യാനാവില്ല",
			"maxlength": "മൂവായിരത്തിലധികം ക്യാരക്ടേഴ്സ് രേഖപ്പെടുത്തിയിരിക്കുന്നു",
			"popup_blocked": "പോപ്പ് അപ്പ് ബ്ലോക്ക് ചെയ്തിരിക്കുകയാണ്",
			"has_url": "കമന്‍റിൽ യുആർഎൽ ഉള്ളതിനാൽ പോസ്റ്റ് ചെയ്യാൻ സാധിക്കില്ല",
			"duplicate": "ഇതേ കമന്‍റ് മുമ്പ് ചെയ്തതിനാൽ പോസ്റ്റ് ചെയ്യാൻ സാധിക്കില്ല",
			"abusive": "യോജിക്കാത്ത ഉള്ളടക്കമായതിനാൽ പോസ്റ്റ് ചെയ്യാൻ കഴിയില്ല",
			"self_agree": "നിങ്ങളുടെ കമന്‍റ് നിങ്ങൾക്കു തന്നെ യോജിക്കാൻ ചെയ്യാൻ കഴിയില്ല",
			"self_disagree": "നിങ്ങളുടെ കമന്‍റ് നിങ്ങൾക്കു തന്നെ വിയോജിക്കാൻ ചെയ്യാൻ കഴിയില്ല",
			"self_recommend": "നിങ്ങളുടെ കമന്‍റ് നിങ്ങൾക്കു തന്നെ റെക്കമെന്‍റ് ചെയ്യാൻ കഴിയില്ല",
			"self_offensive": "നിങ്ങളുടെ കമന്‍റ് നിങ്ങൾക്കു തന്നെ നിരോധിക്കേണ്ടതെന്ന് മാർക്ക് ചെയ്യാൻ കഴിയില്ല",
			"already_agree": "ഈ കമന്‍റിനോട് യോജിപ്പ് രേഖപ്പെടുത്തിക്കഴിഞ്ഞു",
			"already_disagree": "ഈ കമന്‍റിനോട് വിയോജിപ്പ് രേഖപ്പെടുത്തിക്കഴിഞ്ഞു",
			"already_recommended": "ഈ കമന്‍റ് റെക്കമെന്‍റ് ചെയ്തു കഴിഞ്ഞു",
			"already_offensive": "ഈ കമന്‍റ് നിരോധിക്കണമെന്ന് മാർക്ക് ചെയ്തു കഴിഞ്ഞു",
			"cant_agree_disagree": "ഒരേ കമന്‍റിൽ യോജിപ്പും വിയോജിപ്പും രേഖപ്പെടുത്താനാവില്ല",
			"cant_agree_offensive": "ഒരേ കമന്‍റിൽ യോജിപ്പും നിരോധനവും രേഖപ്പെടുത്താനാവില്ല",
			"cant_disagree_recommend": "ഒരേ കമന്‍റിൽ വിയോജിപ്പും റെക്കമെന്‍റും ചെയ്യാനാവില്ല",
			"cant_recommend_offensive": "ഒരേ കമന്‍റ് റെക്കമെന്‍റും നിരോധനവും ചെയ്യാനാവില്ല",
			"permission_facebook": "പോസ്റ്റ് പെർമിഷൻ ഇല്ലാതെ ഫെയ്സ്ബുക്കിൽ പോസ്റ്റ് ചെയ്യാനാവില്ല",
			"offensive_reason": "ഒരു കാരണം തെരഞ്ഞെടുക്കുക",
			"offensive_reason_text": "ഒരു കാരണം രേഖപ്പെടുത്തുക" ,
			"offensive_reason_text_limit": "അക്ഷരങ്ങൾ 200 കവിയരുത്",
			"be_the_first_text": "ആദ്യത്തെ റിവ്യൂ രേഖപ്പെടുത്തുക",
			"no_comments_discussed_text": "കമന്‍റുകളൊന്നും ചർച്ച ചെയ്യപ്പെട്ടിട്ടില്ല",
			"no_comments_up_voted_text": "കമന്‍റുകളൊന്നും വോട്ട് അപ്പ് ചെയ്യപ്പെട്ടിട്ടില്ല",
			"no_comments_down_voted_text": "കമന്‍റുകളൊന്നും വോട്ട് ഡൗൺ ചെയ്യപ്പെട്ടിട്ടില്ല"
		 }
     },
     "tiljs/apps/times/api":{
comments : {url:site_url+"/wp-admin/admin-ajax.php?action=commentsdata"},
comments_newest : {url:site_url+"/wp-admin/admin-ajax.php?action=commentsdata"},
comments_oldest : {url:site_url+"/wp-admin/admin-ajax.php?action=commentsdata"},
comments_recommended : {url:site_url+"/wp-admin/admin-ajax.php?action=commentsdata"},
comments_discussed :{url:site_url+"/wp-admin/admin-ajax.php?action=commentsdata"},
comments_agree:{url:site_url+"/wp-admin/admin-ajax.php?action=commentsdata"},
comments_disagree:{url:site_url+"/wp-admin/admin-ajax.php?action=commentsdata"},
post_comment: {url:site_url+"/wp-admin/admin-ajax.php?action=postComment"},
validate_comment : {url:site_url+"/wp-admin/admin-ajax.php?action=validatecomment"},
rate_comment : {url:site_url+"/wp-admin/admin-ajax.php?action=ratecomment"},
 rate_comment_offensive : {url:site_url+"/wp-admin/admin-ajax.php?action=offensive",params: {ofcommenthostid: 295,ofcommentchannelid: 2147478027}}
 },
"tiljs/page":{
            channel:"ml",
            siteId:"9830e1f81f623b33106acc186b93374e",
            domain:"samayam.com"
        },
         "tiljs/social/facebook":{
            appid:1452558595050529,
            load_js: true
        } 
 

  }
})


define('config',[],function(){return {};});


define("login", ["tiljs/apps/times/login"], function (m) {
	return m;
});
define("comments", ["tiljs/apps/times/comments"], function (m) {
	return m;
});
define('index',[
		"login",
		"comments" ,
		"./config"
],
	function (login, comments, config) {
login.init();
	} );



require(["index"]);
