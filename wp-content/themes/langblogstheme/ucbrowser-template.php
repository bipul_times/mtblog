<?php 
	$title = get_the_title($postID);
	$subtitle = get_post_meta($postID, 'wps_subtitle', true );
?>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
      <meta content="no-cache" http-equiv="Cache-Control">
      <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
      <title><?php echo ($subtitle ? $subtitle : $title); ?></title>
      <link href="//img.ucweb.com/s/uae/g/1s/flow/flow-thirdpart.css" rel="stylesheet" type="text/css">     
      <script src="//img.ucweb.com/s/uae/g/1s/flow/flow-thirdpart-preload.js"></script> 
   </head>
   <body cz-shortcut-listen="true">
      <div id="w-banner"></div>
      <div class="w-article" id="w-article">
         <header class="article-header">
            <h1 class="w-article-title"><?php echo $title; ?></h1>
            <div class="w-article-meta"><span class="w-article-source"><?php echo get_bloginfo('name'); ?></span><time class="w-article-time"><?php echo date('M d, Y, H:i',strtotime($post->post_date)); ?></time></div>
         </header>
         <article>
            <div class="w-article-text">
			   <?php 
					$img = get_the_post_thumbnail_url( $postID, 'post-thumbnail');
					if(!$img){
						$img = get_user_avatar(get_the_author_meta('ID'));
					}
					if($img){
			   ?>
			   <res-image data-src="<?php echo $img; ?>" data-width="" data-height="" data-title="<?php echo ($subtitle ? $subtitle : $title); ?>"></res-image>
			   <?php } ?>
			   <br>
               <arttextxml>
                  <div class="section1">
                     <?php $theContent = str_replace('http://author.blogs.navbharattimes.indiatimes.com','https://blogs.navbharattimes.indiatimes.com',get_the_content());
                     $theContent = str_replace('http://author.blogs.navbharattimes.indiatimes.com','https://blogs.navbharattimes.indiatimes.com',$theContent); 
                     
                     $splitPattern = '/(<img[^>]*>)/U';

					$theContent = preg_replace_callback(
						$splitPattern,
						function ($matches) {
							$array = array();
							$array1 = array();
							$array2 = array();
							$array3 = array();
							preg_match( '/src="([^"]*)"/i', $matches[0], $array ) ;
							preg_match( '/width="([^"]*)"/i', $matches[0], $array1 ) ;
							preg_match( '/height="([^"]*)"/i', $matches[0], $array2 ) ;
							preg_match( '/alt="([^"]*)"/i', $matches[0], $array3 ) ;
							$matches[0] = '<res-image data-src="'.$array[1].'" data-width="'.$array1[1].'" data-height="'.$array2[1].'" data-title="'.$array3[1].'"></res-image>';
							return $matches[0];
						},
						$theContent
					);
					echo $theContent;
                     ?>
                  </div>
               </arttextxml>
            </div>
         </article>
         <div class="w-article-original"><a href="<?php echo get_the_permalink($postID); ?>">
            READ SOURCE
            </a>
         </div>
         <script src="//img.ucweb.com/s/uae/g/1s/flow/zepto.js" type="text/javascript"></script>
         <!--<script src="//img.ucweb.com/s/uae/g/1s/flow/flow-thirdpart.js" type="text/javascript"></script>-->
         <script src="//ucnews.ucweb.com/cp-lib/js/sdk.js" type="text/javascript"></script>
         <div id="w-related"></div>
      </div>
      <div id="w-footer">
         <div class="cp-story-frame">
            <div class="p-story" data-region="story" data-deep="">
            </div>
            <div id="info" class="info" style="display: none">
               <div class="info_inner ">
                  <div style="display: none" class="loading com">
                     <span class="icon-loading"></span> loading ···
                  </div>
                  <div style="display: none" class="toast com">
                     <div class="content"></div>
                  </div>
                  <div style="display: none" class="error com">
                     <p>'Network error!' <a class="retry" href="javascript:window.location.reload()">
                        &nbsp; Refresh</a>
                     </p>
                  </div>
               </div>
               <div style="display: none;" class="head_info">
                  <div style="display: none" class="message com">
                     <div class="title"></div>
                     <div class="content"></div>
                  </div>
               </div>
            </div>
            <div id="toast" style="display: none">
               <div class="toast_custom">
                  <div class="content"></div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>
