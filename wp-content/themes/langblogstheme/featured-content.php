<?php
/**
 * The template for displaying featured content
 *
 * @package WordPress
 * @subpackage BlogsTheme
 * @since Blogs Theme 1.0
 *
 */
?>
<?php $featured_posts = twentyfourteen_get_featured_posts(); ?>
<?php if(is_array($featured_posts) && count($featured_posts) > 0) { ?>
<div class="container featured">
    <div class="row" data-vr-zone="featured">
	<?php
		/**
		 * Fires before the Twenty Fourteen featured content.
		 *
		 * @since Twenty Fourteen 1.0
		 */
		//do_action( 'twentyfourteen_featured_posts_before' );

		$i = 0;
		foreach ( (array) $featured_posts as $order => $post ) :
			if($i==3){
				break;
			}
			setup_postdata( $post );
			
			// Include the featured content template.
			get_template_part( 'content', 'featured-post' );
			$i++;
		endforeach;

		/**
		 * Fires after the Twenty Fourteen featured content.
		 *
		 * @since Twenty Fourteen 1.0
		 */
		//do_action( 'twentyfourteen_featured_posts_after' );

		//wp_reset_postdata();
	?>
	</div><!-- .featured-content-inner -->
</div><!-- #featured-content .featured-content -->
<?php } ?>
