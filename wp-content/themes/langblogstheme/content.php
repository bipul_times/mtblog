<?php
/**
 * The default template for displaying content
 *
 * Used for single.
 *
 * @package WordPress
 * @subpackage BlogsTheme
 * @since BlogsTheme 1.0
 */
global $my_settings;
$id = get_the_ID();
$blogTerms = get_the_terms($id, 'blog');
if(is_array($blogTerms)){
    $blog  = array_values($blogTerms)[0]; //[0];					
    if(!empty($blog)){
        $authorName = get_the_author_meta('display_name');
        $pic = get_user_avatar(get_the_author_meta('ID'));
        if ( has_post_thumbnail($id)) { 
            $pic = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'single-post-thumbnail' )[0];
        }
?>
<div class="media article" data-vr-contentbox="">
	<a class="pull-left" pg="Author_Pos" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?>" title="<?php echo $my_settings['go_to_the_profile_of_txt']; ?> <?php echo $authorName ?>">
    	<img class="media-object" src="<?php  echo $pic ?>" alt="<?php echo $my_settings['go_to_the_profile_of_txt']; ?> <?php echo $authorName ?>" >
	</a>
    <div class="media-body">
    	<?php the_title( '<h2 class="media-heading"><a pg="Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h2>' );?>
        <div class="media-meta">
            <span class="date"><?php echo esc_attr( get_the_date() ).', <span class="time">'.esc_attr( get_the_time() ).'</span>  IST' ?></span> 
            <a class="author" pg="Author_Pos" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' )) ?>" rel="author" title="<?php echo $my_settings['go_to_the_profile_of_txt']; ?><?php echo $authorName ?>"><?php echo $authorName ?></a> in 
            <a class="blog" pg="Blog_Pos" href="<?php  echo get_term_link($blog->term_id,'blog') ?>" sl-processed="1" title="<?php echo $my_settings['go_to_txt']; ?> <?php echo $blog->name ?>"><?php echo $blog->name ?></a> <b>|</b>  
            <span class="cat"><?php the_category(', ')?></span> 
        </div>
        <div class="content"><a pg="Blog_Pos" href="<?php echo esc_url( get_permalink() ) ?>" style="color:#665 !important"  title="<?php echo $my_settings['go_to_txt']; ?> <?php echo get_the_title()?>"><?php the_excerpt(); ?></a></div>
        <div class="commentwrapper cmts">
            <div id="sharebar"  class="social-likes social-likes_notext" data-zeroes="yes" data-url="<?php the_permalink(); ?>" >
                <div class="facebook" title="<?php echo $my_settings['share_link_fb_txt']; ?>"></div>
                <div class="twitter" title="<?php echo $my_settings['share_link_twitter_txt']; ?>" data-title="<?php echo $blog->name.' : '.get_the_title() ?>" data-via="<?php echo $my_settings['twitter_handle']; ?>"></div>
                <div class="linkedin" title="<?php echo $my_settings['share_link_linkedin_txt']; ?>" data-title="<?php echo $blog->name.' : '.get_the_title() ?>"></div>
                <div class="plusone" title="Share link on Google+" data-title="<?php echo $blog->name.' : '.get_the_title() ?>"></div>
            </div>
            <div class="smailto"><a title="<?php echo $my_settings['mail_link_txt']; ?>" href="mailto:%20?subject=<?php echo rawurlencode ($blog->name.' : '.get_the_title()) ?>&body=<?php echo rawurlencode($my_settings['read_complete_article_here_txt'].' - ');the_permalink(); echo '%0D%0AExcerpt : '.rawurlencode (html_entity_decode(get_the_excerpt()));   ?>" target="_top"></a></div>
            <!--<div class="g-plusone" data-href="<?php //the_permalink(); ?>"></div>-->
        </div>
    </div>
</div>
<?php
    }
}
?>
