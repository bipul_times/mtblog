<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define('DB_NAME', 'mtblogs');

/** MySQL database username */
//define('DB_USER', 'ProdBlgs');

/** MySQL database password */
//define('DB_PASSWORD', 'Pr0dB1gs@102');

/** MySQL hostname */
//define('DB_HOST', '192.168.37.100');

/** The name of the database for WordPress */
define( 'DB_NAME', getenv('dbName') );

/** MySQL database username */
define( 'DB_USER', getenv('dbUser') );

/** MySQL database password */
define( 'DB_PASSWORD', getenv('dbPassword') );

/** MySQL hostname */
define( 'DB_HOST', getenv('dbHost') );

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'rDpV11YnTzblITPh0Wc,bL.+/IoTQ6>{yJ*gQIZW>Sc|t<Y@c8`41P|=|;aJ=(km');
define('SECURE_AUTH_KEY',  '|$ ^%9k..YrvSGo#B,;.qiW/8z8oYLI>[H6lF9 ;d,,$h}mD;?.Gh ;S*q?aXS2v');
define('LOGGED_IN_KEY',    'r/}/DM9IO>N%WI9_4X8}6cp<-4p170?Ng_)EZ>/lf2]wCV^8PZnuN4bq^#^t%no{');
define('NONCE_KEY',        '*HK=o/ z&7S:DdB7s6^s-TOe},5N[#]CXrx>WO$w_={H:KXv0lpa@EC/zW[~w$|}');
define('AUTH_SALT',        '+4H)-$Utj.t$1iA*unuCre[q%_F!G#t2;Eq}3C<y-i[wXlvN*~+7mMyV54hGx$1#');
define('SECURE_AUTH_SALT', '.W~M&k3XF~x$^[4:{~Hp-]v-~)du}&FuR^V]p.zZOxs(Ke05,1^},pCzUr|(bVeY');
define('LOGGED_IN_SALT',   'yaC``&uw{7A)|/rh[N+V&#>Ta=~?+^RW-9hawWWHe $d(pHeYzO|p[%?=z@ZI`FL');
define('NONCE_SALT',       'J)6J1@|^;vg<~Jp3H>o3YlTfz`[(aoV/t> JvFj6Qme,R]RSb+11B$qo5UzOQ2^i');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('AUTOMATIC_UPDATER_DISABLED', true);

// $scheme = 'http';
// if(!isset($_SERVER['SCRIPT_URI'])){
//         $scheme = "https";
// }else{
//         $urlArr = parse_url($_SERVER['SCRIPT_URI']);
//         $scheme = $urlArr['scheme'];
// }

$localDomains = array();
$localDomains[0] = "local.blogs.navbharattimes.indiatimes.com";
$localDomains[1] = "local.blogs.maharashtratimes.indiatimes.com"; 
$localDomains[2] = "local.blogs.eisamay.indiatimes.com"; 
$localDomains[3] = "local.readerblogs.navbharattimes.indiatimes.com";
$localDomains[4] = "local.blogs.tamil.samayam.com"; 
$localDomains[5] = "local.blogs.vijaykarnataka.indiatimes.com"; 
$localDomains[6] = "local.blogs.malayalam.samayam.com";
$localDomains[7] = "local.blogs.telugu.samayam.com";
if((isset($_SERVER['HTTP_FRONT_END_HTTPS']) && $_SERVER['HTTP_FRONT_END_HTTPS'] == "on") || in_array($_SERVER['HTTP_HOST'], $localDomains)) {
    $scheme = 'https://';
    $_SERVER['HTTPS'] = "on";
} else if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
    $scheme = 'https://';
} else {
    $scheme = 'http://';
} 


define('WP_HOME',$scheme.$_SERVER['HTTP_HOST']);
define('WP_SITEURL',$scheme.$_SERVER['HTTP_HOST']);


//define('WP_HOME','http://blogs.maharashtratimes.indiatimes.com');
//define('WP_SITEURL','http://blogs.maharashtratimes.indiatimes.com');
define('AUTOSAVE_INTERVAL', 120 );  // seconds (default is 60)
define('WP_POST_REVISIONS', false ); // disable post revisions

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
